# syntax=docker/dockerfile:1.2

FROM node:15-slim as base

# Install python on slim
RUN apt-get update || : \
  && apt-get install --no-install-recommends -y \
  python \
  build-essential \
  git \
  && apt-get clean autoclean \
  && apt-get autoremove --yes \
  && rm -rf /var/lib/apt/lists/*

FROM base as deps

# Add the project files into the app directory and set as working directory
RUN mkdir /app
WORKDIR /app

ADD package.json .
ADD package-lock.json .

RUN npm install

FROM deps as builder

ADD src ./src
ADD images ./images
ADD typings ./typings
ADD test/ ./test
ADD .eslintignore .
ADD .eslintrc* .
ADD tsconfig.json .
ADD webpack.config.js .
ADD webpack.prod.config.js .
ADD _redirects .
ADD .env.example .

ARG CI_COMMIT_SHA
ARG CI_COMMIT_TIMESTAMP

RUN --mount=type=secret,id=env,dst=/app/.env grep MEMBERSHIP_API_URL /app/.env
RUN --mount=type=secret,id=env,dst=/app/.env npm run deploy

FROM caddy:2.4.0 as server

COPY --from=builder /app/dist/ /srv/ui
ADD server/Caddyfile /etc/caddy/Caddyfile
EXPOSE 80 443
