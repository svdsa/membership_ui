const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const child_process = require('child_process')
const hasbin = require('hasbin')
const dotenv = require('dotenv-safe')
dotenv.config()

function git(command) {
  if (hasbin.sync('git')) {
    try {
      return child_process
        .execSync(`git ${command}`, { encoding: 'utf8' })
        .trim()
    } catch (e) {
      return null
    }
  } else {
    console.warn('No git process found')
  }
}

function stringifyValues(object) {
  return Object.entries(object).reduce(
    (acc, curr) => ({
      ...acc,
      [`process.env.${curr[0]}`]: JSON.stringify(curr[1]),
    }),
    {}
  )
}

const CI_COMMIT_SHA =
  JSON.stringify(process.env.CI_COMMIT_SHA) ||
  JSON.stringify(git('rev-parse --short HEAD')) ||
  'unknown'

const CI_COMMIT_TIMESTAMP =
  JSON.stringify(process.env.CI_COMMIT_TIMESTAMP) ||
  JSON.stringify(git('log -1 --format=%aI')) ||
  'n/a'

module.exports = {
  entry: ['./src/js/index.tsx'],
  mode: 'production',
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
    filename: './bundle.js',
    chunkFilename: './[name].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(j|t)s[x]?$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
          },
        },
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
        },
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/font-woff',
        },
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/octet-stream',
        },
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader',
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'image/svg+xml',
        },
      },
    ],
  },
  optimization: {
    moduleIds: 'named',
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    new webpack.DefinePlugin({
      process: {},
      'process.env': {},
      ...stringifyValues(dotenv.config().parsed),
      'process.env.NODE_ENV': '"production"',
      'process.env.CI_COMMIT_SHA': CI_COMMIT_SHA,
      'process.env.CI_COMMIT_TIMESTAMP': CI_COMMIT_TIMESTAMP,
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: './src/index.html',
          to: 'index.html',
        },
        {
          from: '_redirects',
        },
        {
          from: 'images',
          to: 'images/',
        },
      ],
    }),
  ],
  resolve: {
    modules: ['node_modules'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
      'react/jsx-dev-runtime': 'react/jsx-dev-runtime.js',
      'react/jsx-runtime': 'react/jsx-runtime.js',
    },
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  },
}
