import { ZodError } from 'zod'

export type ZodSafeParseResult<T> = ZodSafeParseSuccess<T> | ZodSafeParseError

export type ZodSafeParseSuccess<T> = { success: true; data: T }

export type ZodSafeParseError = { success: false; error: ZodError }
