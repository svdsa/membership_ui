// see https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/react-table/Readme.md
// uncomment types for plugins in use

import {
  UseExpandedHooks,
  UseExpandedInstanceProps,
  UseExpandedOptions,
  UseExpandedRowProps,
  UseExpandedState,
} from 'react-table'

declare module 'react-table' {
  // take this file as-is, or comment out the sections that don't apply to your plugin configuration

  export interface TableOptions<D extends object>
    extends UseExpandedOptions<D>,
      // UseFiltersOptions<D>,
      // UseGlobalFiltersOptions<D>,
      // UseGroupByOptions<D>,
      // UsePaginationOptions<D>,
      // UseResizeColumnsOptions<D>,
      // UseRowSelectOptions<D>,
      // UseRowStateOptions<D>,
      // UseSortByOptions<D>,
      // note that having Record here allows you to add anything to the options, this matches the spirit of the
      // underlying js library, but might be cleaner if it's replaced by a more specific type that matches your
      // feature set, this is a safe default.
      Record<string, any> {}

  export interface Hooks<D extends object = {}> extends UseExpandedHooks<D> {} // UseSortByHooks<D> // UseRowSelectHooks<D>, // UseGroupByHooks<D>,

  export interface TableInstance<D extends object = {}>
    extends UseExpandedInstanceProps<D> {} // UseSortByInstanceProps<D> // UseRowStateInstanceProps<D>, // UseRowSelectInstanceProps<D>, // UsePaginationInstanceProps<D>, // UseGroupByInstanceProps<D>, // UseGlobalFiltersInstanceProps<D>, // UseFiltersInstanceProps<D>, // UseColumnOrderInstanceProps<D>,

  export interface TableState<D extends object = {}>
    extends UseExpandedState<D> {} // UseSortByState<D> // UseColumnOrderState<D>, // UseRowStateState<D>, // UseRowSelectState<D>, // UseResizeColumnsState<D>, // UsePaginationState<D>, // UseGroupByState<D>, // UseGlobalFiltersState<D>, // UseFiltersState<D>,

  // export interface ColumnInterface<D extends Record<string, unknown> = Record<string, unknown>>
  //   extends UseFiltersColumnOptions<D>,
  //     UseGlobalFiltersColumnOptions<D>,
  //     UseGroupByColumnOptions<D>,
  //     UseResizeColumnsColumnOptions<D>,
  //     UseSortByColumnOptions<D> {}

  // export interface ColumnInstance<D extends Record<string, unknown> = Record<string, unknown>>
  //   extends UseFiltersColumnProps<D>,
  //     UseGroupByColumnProps<D>,
  //     UseResizeColumnsColumnProps<D>,
  //     UseSortByColumnProps<D> {}

  // export interface Cell<D extends Record<string, unknown> = Record<string, unknown>, V = any>
  //   extends UseGroupByCellProps<D>,
  //     UseRowStateCellProps<D> {}

  export interface Row<D extends object = {}> extends UseExpandedRowProps<D> {} // UseRowStateRowProps<D> // UseRowSelectRowProps<D>, // UseGroupByRowProps<D>,
}
