const webpack = require('webpack')
const DashboardPlugin = require('webpack-dashboard/plugin')
const path = require('path')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const packageJson = require('./package.json')
const dotenv = require('dotenv-safe')
dotenv.config()

const child_process = require('child_process')
const hasbin = require('hasbin')
const { DefinePlugin } = require('webpack')

function git(command) {
  if (hasbin.sync('git')) {
    return child_process.execSync(`git ${command}`, { encoding: 'utf8' }).trim()
  } else {
    console.warn('No git process found')
  }
}

const DEV_SERVER_PORT = process.env.DEV_SERVER_PORT || 3000
const DEV_SERVER_HOST = process.env.DEV_SERVER_HOST || 'localhost'

const DEFAULT_BABEL_CONFIG = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          browsers: 'last 2 versions',
        },
      },
    ],
    '@babel/preset-typescript',
    '@babel/preset-react',
  ],
  plugins: [
    [
      '@babel/plugin-proposal-decorators',
      {
        legacy: true,
      },
    ],
    [
      '@babel/plugin-proposal-class-properties',
      {
        loose: true,
      },
    ],
    'react-hot-loader/babel',
  ],
}

function renderBabelConfig() {
  const packageBabelConfig = packageJson.babel
  const options = {
    cacheDirectory: true,
    babelrc: false,
  }

  if (packageBabelConfig != null) {
    const plugins = packageBabelConfig.plugins || options.plugins || []

    return {
      ...options,
      ...packageBabelConfig,
      plugins: [...plugins, 'react-hot-loader/babel'],
    }
  } else {
    return {
      ...options,
      ...DEFAULT_BABEL_CONFIG,
    }
  }
}

function stringifyValues(object) {
  return Object.entries(object).reduce(
    (acc, curr) => ({
      ...acc,
      [`process.env.${curr[0]}`]: JSON.stringify(curr[1]),
    }),
    {}
  )
}

module.exports = {
  mode: 'development',
  devtool: 'cheap-module-source-map',
  entry: [
    `webpack-dev-server/client?http://localhost:${DEV_SERVER_PORT}`,
    './src/js/index.tsx',
  ],
  devServer: {
    host: DEV_SERVER_HOST,
    port: DEV_SERVER_PORT,
    hot: true,
    historyApiFallback: true,
    compress: true,
    static: {
      directory: path.join(__dirname, './dist'),
    },
  },
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
    filename: './bundle.js',
    chunkFilename: './[name].bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.(scss|css)$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(j|t)s[x]?$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: renderBabelConfig(),
        },
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
        },
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/font-woff',
        },
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/octet-stream',
        },
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader',
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'image/svg+xml',
        },
      },
    ],
  },
  optimization: {
    moduleIds: 'named',
  },
  plugins: [
    new DashboardPlugin(),
    new ForkTsCheckerWebpackPlugin(),
    new DefinePlugin({
      process: {},
      'process.env': {},
      ...stringifyValues(dotenv.config().parsed),
      'process.env.NODE_ENV': '"development"',
      'process.env.CI_COMMIT_SHA': JSON.stringify(
        git('rev-parse --short HEAD')
      ),
      'process.env.CI_COMMIT_TIMESTAMP': JSON.stringify(
        git('log -1 --format=%aI')
      ),
    }),
  ],
  resolve: {
    modules: ['node_modules'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
      // HACK https://github.com/facebook/react/issues/20235#issuecomment-750911623
      'react/jsx-dev-runtime': 'react/jsx-dev-runtime.js',
      'react/jsx-runtime': 'react/jsx-runtime.js',
    },
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  },
}
