import { FETCH_DATA_SUCCESS } from '../../../src/js/redux/actions/fetchDataActions'
import member, {
  INITIAL_STATE,
} from '../../../src/js/redux/reducers/memberReducers'

describe('memberReducers', () => {
  function generateMemberData(memberId) {
    return {
      id: memberId,
      do_not_call: false,
      do_not_email: false,
      is_eligible: false,
      info: {
        email_address: `testmember${memberId}@dsausa.org`,
        first_name: `Test Member #${memberId}`,
        last_name: 'Last Name',
      },
      meetings: [
        'January 2000 General Meeting',
        'January 2000 Special Meeting',
      ],
      roles: [],
      votes: [],
    }
  }

  describe('FETCH_DATA_SUCCESS', () => {
    it('updates the member', () => {
      const action = {
        type: FETCH_DATA_SUCCESS,
        keyPath: ['user'],
        store: 'member',
        data: generateMemberData(1),
      }

      const result = member(INITIAL_STATE, action)
      expect(result.user.data).toStrictEqual(generateMemberData(1))
    })
  })
})
