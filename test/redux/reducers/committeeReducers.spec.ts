import { COMMITTEES } from '../../../src/js/redux/constants/actionTypes'
import committees from '../../../src/js/redux/reducers/committeeReducers'

describe('committeeReducers', () => {
  describe('COMMITTEES.UPDATE_LIST', () => {
    it('adds a new committee to the committee list', () => {
      const state = {
        byId: {
          1: {
            id: 1,
            name: 'test-1',
          },
        },
        other: 'value',
        form: {
          create: {
            name: '',
            inSubmission: false,
          },
        },
      }
      const action = {
        type: COMMITTEES.UPDATE_LIST,
        payload: [
          {
            id: 2,
            name: 'test-2',
          },
        ],
      }
      const result = committees(state, action)

      expect(result).toStrictEqual({
        byId: {
          1: {
            id: 1,
            name: 'test-1',
          },
          2: {
            id: 2,
            name: 'test-2',
          },
        },
        other: 'value',
        form: {
          create: {
            name: '',
            inSubmission: false,
          },
        },
      })
    })

    it('updates an existing committee in the committee list', () => {
      const state = {
        byId: {
          1: {
            id: 1,
            name: 'test-1',
          },
          2: {
            id: 2,
            name: 'test-2',
          },
        },
        other: 'value',
        form: {
          create: {
            name: '',
            inSubmission: false,
          },
        },
      }
      const action = {
        type: COMMITTEES.UPDATE_LIST,
        payload: [
          {
            id: 2,
            name: 'updated-2',
          },
        ],
      }
      const result = committees(state, action)

      expect(result).toStrictEqual({
        byId: {
          1: {
            id: 1,
            name: 'test-1',
          },
          2: {
            id: 2,
            name: 'updated-2',
          },
        },
        other: 'value',
        form: {
          create: {
            name: '',
            inSubmission: false,
          },
        },
      })
    })
  })
})
