import { ELECTIONS } from '../../../src/js/redux/constants/actionTypes'
import elections from '../../../src/js/redux/reducers/electionReducers'

describe('electionReducers', () => {
  function generateBasicElectionData(electionId) {
    return {
      id: electionId,
      name: `Election ${electionId}`,
      status: 'draft',
      voting_begins_epoch_millis: 1577865600000,
      voting_ends_epoch_millis: 1577952000000,
    }
  }

  function generateElectionData(electionId) {
    return {
      id: electionId,
      name: `Election ${electionId}`,
      status: 'draft',
      number_winners: 1,
      voting_begins_epoch_millis: 1577865600000,
      voting_ends_epoch_millis: 1577952000000,
      author: null,
      description: null,
      description_img: null,
      candidates: [],
      sponsors: [],
      transitions: ['publish', 'cancel'],
    }
  }

  describe('ELECTIONS.FETCH_ELECTIONS', () => {
    it('adds elections', () => {
      const state = { byId: { [1]: generateElectionData(1) } }

      const action = {
        type: ELECTIONS.FETCH_ELECTIONS,
        payload: [generateBasicElectionData(1), generateBasicElectionData(2)],
      }
      //console.log(action.payload.toString())

      const result = elections(state, action)
      expect(result).toStrictEqual({
        byId: {
          1: generateBasicElectionData(1),
          2: generateBasicElectionData(2),
        },
      })
    })
  })

  describe('ELECTIONS.FETCH_ELECTION', () => {
    it('adds an election', () => {
      const state = { byId: { [1]: generateElectionData(1) } }

      const action = {
        type: ELECTIONS.FETCH_ELECTION,
        payload: generateElectionData(2),
      }

      const result = elections(state, action)
      expect(result).toStrictEqual({
        byId: {
          1: generateElectionData(1),
          2: generateElectionData(2),
        },
      })
    })
  })
})
