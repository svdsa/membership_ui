// test-utils.jsx
import { configureStore, Store } from '@reduxjs/toolkit'
import { render as rtlRender } from '@testing-library/react'
import React from 'react'
import { Provider } from 'react-redux'
// Import your own reducer
import rootReducer, { RootReducer } from '../src/js/redux/reducers/rootReducer'

interface RenderWithStoreProps {
  preloadedState?: Partial<RootReducer>
  store?: Store<RootReducer>
}

export function renderWithStore<T>(
  ui: React.ReactElement<T>,
  {
    preloadedState = undefined,
    store,
    ...renderOptions
  }: RenderWithStoreProps = {}
): ReturnType<typeof rtlRender> {
  const workingStore =
    store ?? configureStore({ reducer: rootReducer, preloadedState })
  const Wrapper: React.FC<any> = ({ children }) => {
    return <Provider store={workingStore}>{children}</Provider>
  }

  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}
