export const sc2017 = {
  ballot_count: 83,
  candidates: {
    '1': {
      image_url: null,
      name: 'Charles Davis',
    },
    '2': {
      image_url: null,
      name: 'Darby Thomas',
    },
    '3': {
      image_url: null,
      name: 'David Carlos Salaverry',
    },
    '4': {
      image_url: null,
      name: 'Jennifer Snyder',
    },
    '5': {
      image_url: null,
      name: 'Preston Rhea',
    },
    '6': {
      image_url: null,
      name: 'Shannon Malloy',
    },
    '7': {
      image_url: null,
      name: 'Dale Smith',
    },
    '8': {
      image_url: null,
      name: 'Steven Monacelli',
    },
    '9': {
      image_url: null,
      name: 'Teresa Pratt',
    },
    '10': {
      image_url: null,
      name: 'Xavier Aubuchon-Mendoza',
    },
    '32': {
      image_url: null,
      name: 'Alisha Foster',
    },
    '33': {
      image_url: null,
      name: 'Ryan Moore',
    },
  },
  quota: 14,
  round_information: [
    {
      transfer_source: 2,
      transfer_type: 'excess',
      votes: {
        '1': {
          starting_votes: 1.0,
          votes_received: 0.0,
        },
        '2': {
          starting_votes: 26.0,
          votes_received: 0.0,
        },
        '3': {
          starting_votes: 3.0,
          votes_received: 1.3846,
        },
        '4': {
          starting_votes: 15.0,
          votes_received: 4.1536,
        },
        '5': {
          starting_votes: 4.0,
          votes_received: 0.46154,
        },
        '6': {
          starting_votes: 5.0,
          votes_received: 0.46154,
        },
        '7': {
          starting_votes: 1.0,
          votes_received: 0.92308,
        },
        '8': {
          starting_votes: 3.0,
          votes_received: 0.0,
        },
        '9': {
          starting_votes: 5.0,
          votes_received: 0.92308,
        },
        '10': {
          starting_votes: 2.0,
          votes_received: 0.0,
        },
        '32': {
          starting_votes: 16.0,
          votes_received: 3.6921,
        },
        '33': {
          starting_votes: 2.0,
          votes_received: 0.0,
        },
      },
    },
    {
      transfer_source: 32,
      transfer_type: 'excess',
      votes: {
        '1': {
          starting_votes: 1.0,
          votes_received: 0.42256,
        },
        '3': {
          starting_votes: 4.3845,
          votes_received: 0.0,
        },
        '4': {
          starting_votes: 19.156,
          votes_received: 1.9792,
        },
        '5': {
          starting_votes: 4.4615,
          votes_received: 0.0,
        },
        '6': {
          starting_votes: 5.4615,
          votes_received: 1.5567,
        },
        '7': {
          starting_votes: 1.923,
          votes_received: 0.28912,
        },
        '8': {
          starting_votes: 3.0,
          votes_received: 0.28912,
        },
        '9': {
          starting_votes: 5.923,
          votes_received: 1.1565,
        },
        '10': {
          starting_votes: 2.0,
          votes_received: 0.0,
        },
        '32': {
          starting_votes: 19.694,
          votes_received: 0.0,
        },
        '33': {
          starting_votes: 2.0,
          votes_received: 0.0,
        },
      },
    },
    {
      transfer_source: 4,
      transfer_type: 'excess',
      votes: {
        '1': {
          starting_votes: 1.4225,
          votes_received: 0.3826,
        },
        '3': {
          starting_votes: 4.3845,
          votes_received: 0.0,
        },
        '4': {
          starting_votes: 21.134,
          votes_received: 0.0,
        },
        '5': {
          starting_votes: 4.4615,
          votes_received: 0.43516,
        },
        '6': {
          starting_votes: 7.0181,
          votes_received: 4.0633,
        },
        '7': {
          starting_votes: 2.2122,
          votes_received: 0.98672,
        },
        '8': {
          starting_votes: 3.2891,
          votes_received: 0.0,
        },
        '9': {
          starting_votes: 7.0794,
          votes_received: 1.2661,
        },
        '10': {
          starting_votes: 2.0,
          votes_received: 0.0,
        },
        '33': {
          starting_votes: 2.0,
          votes_received: 0.0,
        },
      },
    },
    {
      transfer_source: 1,
      transfer_type: 'elimination',
      votes: {
        '1': {
          starting_votes: 1.8051,
          votes_received: 0.0,
        },
        '3': {
          starting_votes: 4.3845,
          votes_received: 0.0,
        },
        '5': {
          starting_votes: 4.8967,
          votes_received: 0.33756,
        },
        '6': {
          starting_votes: 11.081,
          votes_received: 0.13344,
        },
        '7': {
          starting_votes: 3.1989,
          votes_received: 0.045044,
        },
        '8': {
          starting_votes: 3.2891,
          votes_received: 0.0,
        },
        '9': {
          starting_votes: 8.3456,
          votes_received: 0.28912,
        },
        '10': {
          starting_votes: 2.0,
          votes_received: 0.0,
        },
        '33': {
          starting_votes: 2.0,
          votes_received: 1.0,
        },
      },
    },
    {
      transfer_source: 10,
      transfer_type: 'elimination',
      votes: {
        '3': {
          starting_votes: 4.3845,
          votes_received: 2.0,
        },
        '5': {
          starting_votes: 5.2343,
          votes_received: 0.0,
        },
        '6': {
          starting_votes: 11.215,
          votes_received: 0.0,
        },
        '7': {
          starting_votes: 3.2439,
          votes_received: 0.0,
        },
        '8': {
          starting_votes: 3.2891,
          votes_received: 0.0,
        },
        '9': {
          starting_votes: 8.6347,
          votes_received: 0.0,
        },
        '10': {
          starting_votes: 2.0,
          votes_received: 0.0,
        },
        '33': {
          starting_votes: 3.0,
          votes_received: 0.0,
        },
      },
    },
    {
      transfer_source: 33,
      transfer_type: 'elimination',
      votes: {
        '3': {
          starting_votes: 6.3845,
          votes_received: 0.0,
        },
        '5': {
          starting_votes: 5.2343,
          votes_received: 1.0,
        },
        '6': {
          starting_votes: 11.215,
          votes_received: 0.0,
        },
        '7': {
          starting_votes: 3.2439,
          votes_received: 2.0,
        },
        '8': {
          starting_votes: 3.2891,
          votes_received: 0.0,
        },
        '9': {
          starting_votes: 8.6347,
          votes_received: 0.0,
        },
        '33': {
          starting_votes: 3.0,
          votes_received: 0.0,
        },
      },
    },
    {
      transfer_source: 8,
      transfer_type: 'elimination',
      votes: {
        '3': {
          starting_votes: 6.3845,
          votes_received: 0.28912,
        },
        '5': {
          starting_votes: 6.2343,
          votes_received: 1.0,
        },
        '6': {
          starting_votes: 11.215,
          votes_received: 2.0,
        },
        '7': {
          starting_votes: 5.2439,
          votes_received: 0.0,
        },
        '8': {
          starting_votes: 3.2891,
          votes_received: 0.0,
        },
        '9': {
          starting_votes: 8.6347,
          votes_received: 0.0,
        },
      },
    },
    {
      transfer_source: 7,
      transfer_type: 'elimination',
      votes: {
        '3': {
          starting_votes: 6.6736,
          votes_received: 0.0,
        },
        '5': {
          starting_votes: 7.2343,
          votes_received: 1.0,
        },
        '6': {
          starting_votes: 13.216,
          votes_received: 2.3998,
        },
        '7': {
          starting_votes: 5.2439,
          votes_received: 0.0,
        },
        '9': {
          starting_votes: 8.6347,
          votes_received: 1.8441,
        },
      },
    },
    {
      transfer_source: 6,
      transfer_type: 'excess',
      votes: {
        '3': {
          starting_votes: 6.6736,
          votes_received: 0.32504,
        },
        '5': {
          starting_votes: 8.2343,
          votes_received: 0.32323,
        },
        '6': {
          starting_votes: 15.618,
          votes_received: 0.0,
        },
        '9': {
          starting_votes: 10.479,
          votes_received: 0.80088,
        },
      },
    },
    {
      transfer_source: 3,
      transfer_type: 'elimination',
      votes: {
        '3': {
          starting_votes: 6.9987,
          votes_received: 0.0,
        },
        '5': {
          starting_votes: 8.5576,
          votes_received: 3.4753,
        },
        '9': {
          starting_votes: 11.281,
          votes_received: 2.7427,
        },
      },
    },
    {
      transfer_source: 9,
      transfer_type: 'excess',
      votes: {
        '5': {
          starting_votes: 12.034,
          votes_received: 0.021012,
        },
        '9': {
          starting_votes: 14.025,
          votes_received: 0.0,
        },
      },
    },
  ],
  winners: [
    {
      candidate: 2,
      round: 1,
    },
    {
      candidate: 32,
      round: 2,
    },
    {
      candidate: 4,
      round: 3,
    },
    {
      candidate: 6,
      round: 9,
    },
    {
      candidate: 9,
      round: 11,
    },
  ],
}

export default sc2017
