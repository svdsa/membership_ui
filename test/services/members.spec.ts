import { MemberRole } from '../../src/js/client/MemberClient'
import { MemberState } from '../../src/js/redux/reducers/memberReducers'
import {
  getRelevantRoles,
  isCommitteeAdmin,
} from '../../src/js/services/members'

function createMember(roles = []): MemberState {
  return {
    isAdmin: false,
    user: {
      loading: false,
      err: null,
      data: {
        roles,
      },
    },
  }
}

function createRole(name: string, committee?): Partial<MemberRole> {
  const committee_name = committee == null ? 'general' : committee
  return {
    role: name,
    committee: committee_name,
    committee_name,
  }
}

describe('members.isCommitteeAdmin', () => {
  it('returns true when given a general admin and no committee provided', () => {
    const member = createMember([createRole('admin')])
    const result = isCommitteeAdmin(member)
    expect(result).toBe(true)
  })

  it('returns true when given an admin of any committee and no committee provided', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member)
    expect(result).toBe(true)
  })

  it('returns true when given a general admin and committee is "any"', () => {
    const member = createMember([createRole('admin')])
    const result = isCommitteeAdmin(member, 'any')
    expect(result).toBe(true)
  })

  it('returns true when given an admin of any committee and committee is "any"', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, 'any')
    expect(result).toBe(true)
  })

  it('returns false when given an admin of any committee and committee is null', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, null)
    expect(result).toBe(false)
  })

  it('returns false when given an admin of any committee and committee is an empty array', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, [])
    expect(result).toBe(false)
  })

  it('returns false when given an admin of any committee and committee is an empty string', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, '')
    expect(result).toBe(false)
  })

  it('returns true when given the committee for which they are an admin', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, 'tech')
    expect(result).toBe(true)
  })

  it('returns false when given a committee for which they are NOT an admin', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, 'healthcare')
    expect(result).toBe(false)
  })

  it('returns true when given one of the committees for which they are an admin', () => {
    const member = createMember([
      createRole('admin', 'tech'),
      createRole('admin', 'healthcare'),
    ])
    const result = isCommitteeAdmin(member, 'healthcare')
    expect(result).toBe(true)
  })

  it('returns true when given a list of committees that contains one for which they are an admin', () => {
    const member = createMember([createRole('admin', 'tech')])
    const result = isCommitteeAdmin(member, ['healthcare', 'tech'])
    expect(result).toBe(true)
  })

  it('returns false when given a list of committees and a general admin who is not an admin of any of the given committees', () => {
    const member = createMember([createRole('admin')])
    const result = isCommitteeAdmin(member, ['healthcare', 'tech'])
    expect(result).toBe(false)
  })
})

describe('members.getRelevantCommittees', () => {
  it('returns the list of committees that the user admins or is active in', () => {
    const member = createMember([
      createRole('admin', 'committee 1'),
      createRole('active', 'committee 2'),
      createRole('who cares', 'committee 3'),
    ])
    const result = getRelevantRoles(member)
    expect(result).toStrictEqual([
      {
        role: 'admin',
        committee: 'committee 1',
        committee_name: 'committee 1',
      },
      {
        role: 'active',
        committee: 'committee 2',
        committee_name: 'committee 2',
      },
    ])
  })
})
