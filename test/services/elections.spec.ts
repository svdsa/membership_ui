import { ElectionStatus } from '../../src/js/client/ElectionClient'
import {
  electionStatus,
  getVoteStatusForElection,
} from '../../src/js/services/elections'

function fixtureEligiblePollsOpenAndPublished({
  id = new Date().getTime(),
  when = new Date(),
} = {}) {
  const anHourAgo = new Date(when.setHours(when.getHours() - 1))
  const nextHour = new Date(when.setHours(when.getHours() + 1))
  return {
    election: {
      id,
      name: 'placeholder name',
      start_time: new Date(when.setHours(when.getHours() - 1)).toJSON(),
      end_time: new Date(when.setHours(when.getHours() + 1)).toJSON(),
      status: 'published' as ElectionStatus,
      voting_begins_epoch_millis: anHourAgo.getTime(),
      voting_ends_epoch_millis: nextHour.getTime(),
    },
    vote: {
      election_id: id,
      voted: false,
      election_name: 'placeholder name',
      election_status: 'published' as ElectionStatus,
    },
    when,
  }
}

const ONE_DAY = 24 * 60 * 60 * 1000
const now = new Date()
const yesterday = new Date(now.getTime() - ONE_DAY)
const tomorrow = new Date(now.getTime() + ONE_DAY)

describe('elections.electionStatus', () => {
  it('returns "polls open" when the election is published and starts before now and ends after now', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished()
    const result = electionStatus(election, now)
    expect(result).toBe('polls open')
  })

  it('returns "polls open" when the election is published and has no start date and ends in the future', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished({ when: now })
    const result = electionStatus(
      { ...election, voting_ends_epoch_millis: undefined },
      now
    )
    expect(result).toBe('polls open')
  })

  it('returns "not started" when the election is published and ends in the past', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished({
      when: yesterday,
    })
    const result = electionStatus(election, now)
    expect(result).toBe('polls closed')
  })

  it('returns "polls closed" when the election is published and ends in the past', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished({
      when: tomorrow,
    })
    const result = electionStatus(election, now)
    expect(result).toBe('polls not open')
  })
})

describe('elections.getVoteStatusForElection', () => {
  it('returns "eligible" when the election status is "polls open" and the eligible voter has NOT voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = getVoteStatusForElection(election, vote)
    expect(result).toBe('eligible')
  })

  it('returns "eligible" when the election status is "draft" and the eligible voter has NOT voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = getVoteStatusForElection(
      {
        ...election,
        status: 'draft',
      },
      vote
    )
    expect(result).toBe('eligible')
  })

  it('returns "eligible" when the election status is "final" and the eligible voter has NOT voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = getVoteStatusForElection(
      {
        ...election,
        status: 'draft',
      },
      vote
    )
    expect(result).toBe('eligible')
  })

  it('returns "already voted" when the election status is "polls open" and the eligible voter has voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = getVoteStatusForElection(election, {
      ...vote,
      voted: true,
    })
    expect(result).toBe('already voted')
  })

  it('returns "already voted" when the election status is "draft" and the eligible voter has voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = getVoteStatusForElection(
      { ...election, status: 'draft' },
      { ...vote, voted: true }
    )
    expect(result).toBe('already voted')
  })

  it('returns "already voted" when the election status is "final" and the eligible voter has voted', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = getVoteStatusForElection(
      { ...election, status: 'final' },
      { ...vote, voted: true }
    )
    expect(result).toBe('already voted')
  })

  it('returns "ineligible" when the vote has a different election.id', () => {
    const { election, vote } = fixtureEligiblePollsOpenAndPublished()
    const result = getVoteStatusForElection(election, {
      ...vote,
      election_id: election.id - 1,
    })
    expect(result).toBe('ineligible')
  })

  it('returns "ineligible" when the vote is null', () => {
    const { election } = fixtureEligiblePollsOpenAndPublished()
    const result = getVoteStatusForElection(election, null)
    expect(result).toBe('ineligible')
  })
})
