import React from 'react'
import renderer from 'react-test-renderer'
import FieldGroup from '../../../src/js/components/common/FieldGroup'

describe('<FieldGroup />', () => {
  let testContext: any

  beforeEach(() => {
    testContext = {}
  })

  it('renders the AddEligibleVoter option map use case', (done) => {
    const tree = renderer.create(
      <FieldGroup
        formKey="election_id"
        componentClass="select"
        label="Election"
        options={{ hello: 'hello', world: 'world' }}
        placeholder="Select an election"
        value={'12345'}
        onFormValueChange={(formKey, value) => null}
        required
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders the AddMeeting use case', (done) => {
    const tree = renderer.create(
      <FieldGroup
        formKey="meeting_id"
        componentClass="select"
        label="Meeting"
        options={{ meeting_id: 'Meeting', meeting_2: 'Meeting 2' }}
        placeholder="Select a meeting"
        value={'54321'}
        onFormValueChange={(formKey, value) => null}
        required
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders the AddRole use case', (done) => {
    const tree = renderer.create(
      <FieldGroup
        formKey="role"
        componentClass="select"
        options={['admin', 'member']}
        label="Role"
        value="member"
        onFormValueChange={(formKey, value) => null}
        required
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders the Committee use case', (done) => {
    const tree = renderer.create(
      <FieldGroup
        formKey="name"
        componentClass="input"
        type="text"
        label="Committee Name"
        value="Mocha Testing Committee"
        onFormValueChange={(formKey, value) => null}
        required
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders the CreateElection datetime use case', (done) => {
    const tree = renderer.create(
      <FieldGroup
        formKey="start_time"
        componentClass="datetime"
        label="Start Time"
        value={new Date(420420420)}
        onFormValueChange={(formKey, value) => null}
        timezone="America/Los_Angeles"
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders the CreateElection number use case', (done) => {
    const tree = renderer.create(
      <FieldGroup
        formKey="number_winners"
        componentClass="input"
        type="number"
        step={1}
        min={1}
        label="Number of winners"
        value={6}
        onFormValueChange={(formKey, value) => null}
        required
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })

  // TODO: This doesn't seem to render a textarea. Should we fix this?
  it('renders the CreateElection textarea use case', (done) => {
    const tree = renderer.create(
      <FieldGroup
        formKey="description"
        componentClass="input"
        type="text"
        rows={5}
        label="Description (truncated to 2048 characters)"
        onFormValueChange={(formKey, value) => null}
        value=""
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders the CreateElection text use case', (done) => {
    const tree = renderer.create(
      <FieldGroup
        formKey="description_img"
        componentClass="input"
        type="text"
        label="Link to a description image"
        onFormValueChange={(formKey, value) => null}
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })
})
