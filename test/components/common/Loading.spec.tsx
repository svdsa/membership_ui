import React from 'react'
import renderer from 'react-test-renderer'
import Loading from '../../../src/js/components/common/Loading'

describe('<Loading />', () => {
  it('renders', (done) => {
    const tree = renderer.create(<Loading />)
    expect(tree).toMatchSnapshot()
    done()
  })
})
