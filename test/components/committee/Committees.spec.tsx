import '@testing-library/jest-dom'
import { screen } from '@testing-library/react'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import React from 'react'
import Committees from '../../../src/js/components/committee/Committees'
import { c } from '../../../src/js/config'
import { MemberState } from '../../../src/js/redux/reducers/memberReducers'
import { renderWithStore } from '../../testUtils'

const server = setupServer(
  rest.get(
    `${process.env.MEMBERSHIP_API_URL}/committee/list`,
    (req, res, ctx) => {
      const committees = [
        {
          id: 1,
          name: 'Unit Testing Committee',
          provisional: false,
          viewable: true,
          email: 'unittesting@example.com',
          admins: [],
          members: [],
          active_members: [],
          inactive: false,
        },
        {
          id: 2,
          name: 'Squeamish Ossifrage',
          provisional: false,
          viewable: true,
          email: 'unittesting@example.com',
          admins: [],
          members: [],
          active_members: [],
          inactive: false,
        },
      ]
      return res(ctx.json(committees))
    }
  ),
  rest.post(`${process.env.MEMBERSHIP_API_URL}/committee`, (req, res, ctx) => {
    return res(ctx.status(400))
  }),
  rest.post(`${process.env.MEMBERSHIP_API_URL}/*`, (req, res, ctx) => {
    return res(ctx.status(500))
  })
)

const adminUserState: MemberState = {
  isAdmin: true,
  user: {
    data: {
      roles: [
        {
          role: 'admin',
          committee: 'general',
        },
      ],
    },
    loading: false,
    err: null,
  },
}

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

describe('<Committees />', () => {
  it(', without a logged in user, renders nothing', (done) => {
    const { container } = renderWithStore(<Committees />)
    expect(container).toMatchSnapshot()
    done()
  })

  it('renders a blank committee screen and form when no committees are provided', async () => {
    server.use(
      rest.get(
        `${process.env.MEMBERSHIP_API_URL}/committee/list`,
        (req, res, ctx) => {
          return res(ctx.json([]))
        }
      )
    )
    const { container } = renderWithStore(<Committees />, {
      preloadedState: {
        member: adminUserState,
      },
    })
    expect(container).toMatchSnapshot()
    const button = screen.getByText(`Add ${c('GROUP_NAME_SINGULAR')}`, {
      selector: 'button',
    })
    expect(button).toBeVisible()

    await expect(
      screen.findByText('Unit Testing', { exact: false }, { timeout: 100 })
    ).rejects.not.toBeNull()
  })

  it('renders committees and the form', async () => {
    const { container } = renderWithStore(<Committees />, {
      preloadedState: {
        member: adminUserState,
      },
    })

    const button = screen.getByText(`Add ${c('GROUP_NAME_SINGULAR')}`, {
      selector: 'button',
    })
    expect(button).toBeVisible()

    expect(
      await screen.findByText('Unit Test', { exact: false })
    ).toBeInTheDocument()
    expect(
      await screen.findByText('Squeamish Ossifrage', { exact: false })
    ).toBeInTheDocument()

    expect(container).toMatchSnapshot()
  })

  xit('fails to create a committee with no info', async () => {
    const { container } = renderWithStore(<Committees />, {
      preloadedState: {
        member: adminUserState,
      },
    })

    const button = screen.getByText(`Add ${c('GROUP_NAME_SINGULAR')}`, {
      selector: 'button',
    })
    expect(button).toBeVisible()

    button.click()
  })
})
