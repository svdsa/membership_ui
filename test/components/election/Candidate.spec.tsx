import React from 'react'
import renderer from 'react-test-renderer'
import Candidate from '../../../src/js/components/election/Candidate'

describe('<Candidate />', () => {
  it('renders a name', (done) => {
    const tree = renderer.create(<Candidate name="a spooky spectre" />)
    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders a name and image', (done) => {
    const tree = renderer.create(
      <Candidate
        name="Gritty"
        imageUrl="https://en.wikipedia.org/wiki/Gritty_(mascot)"
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders a name, image, and height', (done) => {
    const tree = renderer.create(
      <Candidate
        name="eight"
        imageUrl="https://en.wikipedia.org/wiki/8"
        height={8}
      />
    )

    expect(tree).toMatchSnapshot()
    done()
  })
})
