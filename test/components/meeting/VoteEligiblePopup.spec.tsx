import React from 'react'
import renderer from 'react-test-renderer'
import VoteEligiblePopup from '../../../src/js/components/meeting/VoteEligiblePopup'

describe('<VoteEligiblePopup />', () => {
  it('renders an eligible (numVotes > 0) invisible popup', (done) => {
    const tree = renderer.create(
      <VoteEligiblePopup numVotes={1} visible={false} />
    )
    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders an eligible (numVotes > 0) visible popup', (done) => {
    const tree = renderer.create(
      <VoteEligiblePopup numVotes={1} visible={true} />
    )
    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders an ineligible (numVotes == 0) invisible popup', (done) => {
    const tree = renderer.create(
      <VoteEligiblePopup numVotes={0} visible={false} />
    )
    expect(tree).toMatchSnapshot()
    done()
  })

  it('renders an ineligible (numVotes == 0) visible popup', (done) => {
    const tree = renderer.create(
      <VoteEligiblePopup numVotes={0} visible={true} />
    )
    expect(tree).toMatchSnapshot()
    done()
  })
})
