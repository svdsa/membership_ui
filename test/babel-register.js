const register = require('@babel/register').default
require('core-js/stable')
require('regenerator-runtime/runtime')

register({ extensions: ['.ts', '.tsx', '.js', '.jsx'] })
