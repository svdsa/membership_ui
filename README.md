# DSA SF Membership Portal UI

A graphical user interface for the [DSA SF Membership API](https://gitlab.com/DSASanFrancisco/membership_api)

# Installation

- Install HomeBrew from https://brew.sh
- Download and install Node / NPM

Our recommendation is to install node through [nvm](https://github.com/creationix/nvm). The instructions are:

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install v10.6.0
nvm use
```

You can also use homebrew:

```
brew install node
```

- Create your `.env` config file

```
cp .env.example .env
# edit .env to your liking
```

- Install dependencies

```
npm install
```

- (optional) Consider configuring Git to use the blame-ignore-revs file:

```
git config blame.ignoreRevsFile .git-blame-ignore-revs
```

- Run the server

```
npm run dev
```
