import { History } from 'history'
import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { IndexRedirect, IndexRoute, Route, Router } from 'react-router'
import { HistoryUnsubscribe } from 'react-router-redux'
import { bindActionCreators, Dispatch } from 'redux'
import AddMember from 'src/js/components/admin/AddMember'
import Admin from 'src/js/components/admin/Admin'
import AdminNavShell from 'src/js/components/admin/AdminNavShell'
import MemberList from 'src/js/components/admin/MemberList'
import App from 'src/js/components/App'
import RequireAuth from 'src/js/components/auth/requireAuth'
import SigningOut from 'src/js/components/auth/SigningOut'
import Callback from 'src/js/components/callback/Callback'
import Committee from 'src/js/components/committee/Committee'
import Committees from 'src/js/components/committee/Committees'
import MyCommittees from 'src/js/components/committee/MyCommittees'
import NotFound from 'src/js/components/common/NotFound'
import CreateElection from 'src/js/components/election/CreateElection'
import ElectionDetail from 'src/js/components/election/ElectionDetail'
import ElectionEdit from 'src/js/components/election/ElectionEdit'
import Elections from 'src/js/components/election/Elections'
import EnterVote from 'src/js/components/election/EnterVote'
import MyElections from 'src/js/components/election/MyElections'
import PrintBallots from 'src/js/components/election/PrintBallots'
import { RankedChoiceVisualization } from 'src/js/components/election/RankedChoiceVisualization'
import SignInKiosk from 'src/js/components/election/SignInKiosk'
import ViewVote from 'src/js/components/election/ViewVote'
import Vote from 'src/js/components/election/Vote'
import CollectiveDuesScreen from 'src/js/components/external/collective-dues'
import NewsletterSignupScreen from 'src/js/components/external/newsletter-signup'
import CreateMeeting from 'src/js/components/meeting/CreateMeeting'
import MeetingAdmin from 'src/js/components/meeting/MeetingAdmin'
import MeetingDetailPage from 'src/js/components/meeting/MeetingDetailPage'
import MeetingKioskPage from 'src/js/components/meeting/MeetingKioskPage'
import MeetingRsvp from 'src/js/components/meeting/MeetingRsvp'
import Meetings from 'src/js/components/meeting/Meetings'
import MyMeetings from 'src/js/components/meeting/MyMeetings'
import ProxyTokenConsume from 'src/js/components/meeting/ProxyTokenConsume'
import ProxyTokenCreate from 'src/js/components/meeting/ProxyTokenCreate'
import MemberPage from 'src/js/components/membership/MemberPage'
import MyContactInfo from 'src/js/components/membership/MyContactInfo'
import MyMembership from 'src/js/components/membership/MyMembership'
import {
  c,
  PAGE_HEADING_HTML_ID,
  PAGE_TITLE_TEMPLATE,
  USE_ANONYMOUS_SIGNUPS,
  USE_COLLECTIVE_DUES,
  USE_ELECTIONS,
} from 'src/js/config'
import ContactsList from 'src/js/pages/admin/contacts/index'
import CreateContact from 'src/js/pages/admin/contacts/new'
import ContactFromId from 'src/js/pages/admin/contacts/[id]'
import CustomFieldsList from 'src/js/pages/admin/custom_fields/index'
import CreateCustomField from 'src/js/pages/admin/custom_fields/new'
import CustomFieldFromId from 'src/js/pages/admin/custom_fields/[id]'
import ImportContactsCsv from 'src/js/pages/admin/imports/csv/contacts'
import ImportEvents from 'src/js/pages/admin/imports/index'
import ImportEventContacts from 'src/js/pages/admin/imports/[id]/contacts'
import ImportEventPage from 'src/js/pages/admin/imports/[id]/index'
import ImportEventOverview from 'src/js/pages/admin/imports/[id]/_overview'
import TagsList from 'src/js/pages/admin/tags/index'
import MainPage from 'src/js/pages/index'
import LogIn from 'src/js/pages/login'
import Profile from 'src/js/pages/profile'
import { fullySignOut } from 'src/js/redux/actions/auth/index'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import focusOnElement from 'src/js/util/focusOnElement'

interface RoutesOwnProps {
  history: History<any> & HistoryUnsubscribe
}

type RoutesProps = RoutesStateProps & RoutesDispatchProps & RoutesOwnProps

class Routes extends Component<RoutesProps> {
  constructor(props) {
    super(props)

    this.props.history.listen(({ action }) => {
      // Only change focus when moving to a new URL (not on redirects).
      if (action === 'PUSH') {
        focusOnElement({
          id: PAGE_HEADING_HTML_ID,
        })
      }
    })
  }

  shouldComponentUpdate() {
    return false
  }

  render() {
    const electionRoutes = (
      <>
        <Route path="my-elections" component={RequireAuth(MyElections)} />
        <Route
          path="elections/:electionId"
          component={RequireAuth(ElectionDetail)}
        />
        <Route
          path="my-elections/view-vote"
          component={RequireAuth(ViewVote)}
        />
        <Route
          path="elections/:electionId/results"
          component={RequireAuth(RankedChoiceVisualization)}
        />
        <Route
          path="elections/:electionId/results/fullscreen"
          component={RequireAuth(RankedChoiceVisualization)}
        />
        <Route
          path="admin/elections/:electionId/vote"
          component={RequireAuth(EnterVote)}
        />
        <Route
          path="elections/:electionId/print"
          component={RequireAuth(PrintBallots)}
        />
        <Route path="vote/:electionId" component={RequireAuth(Vote)} />
        <Route path="elections" component={RequireAuth(Elections)} />
      </>
    )

    const electionAdminRoutes = (
      <>
        <Route path="elections" component={RequireAuth(Elections)} />
        <Route
          path="elections/create"
          component={RequireAuth(CreateElection)}
        />
        <Route
          path="elections/:electionId/edit"
          component={RequireAuth(ElectionEdit)}
        />
        <Route
          path="elections/:electionId/signin"
          component={RequireAuth(SignInKiosk)}
        />
      </>
    )

    return (
      <>
        <Helmet
          titleTemplate={PAGE_TITLE_TEMPLATE}
          defaultTitle="DSA Member Portal"
        >
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href={c('URL_CHAPTER_FAVICON_32')}
          />
          <link
            rel="icon"
            type="image/png"
            sizes="192x192"
            href={c('URL_CHAPTER_FAVICON_192')}
          />
        </Helmet>
        <Router history={this.props.history}>
          {/* Unauthenticated routes */}
          <Route path="login" component={LogIn} />
          {USE_ANONYMOUS_SIGNUPS ? (
            <Route path="/newbies">
              <Route path="signup" component={NewsletterSignupScreen} />
              {USE_COLLECTIVE_DUES ? (
                <Route
                  path="collective-dues"
                  component={CollectiveDuesScreen}
                />
              ) : null}
            </Route>
          ) : null}
          {/* Authenticated routes */}
          <Route path="/" component={App}>
            <IndexRedirect to="home" />
            <Route path="process" component={Callback} />
            <Route
              path="logout"
              onEnter={() => {
                this.props.fullySignOut()
              }}
              component={SigningOut}
            />
            <Route path="meetings" component={RequireAuth(Meetings)} />
            <Route
              path="meetings/create"
              component={RequireAuth(CreateMeeting)}
            />

            {/* Admin and admin-bar routes */}
            <Route path="admin" component={RequireAuth(Admin)} />
            <Route path="admin" component={AdminNavShell}>
              <Route path="members" component={RequireAuth(MemberList)} />
              <Route path="contacts" component={RequireAuth(ContactsList)} />
              <Route path="members/create" component={RequireAuth(AddMember)} />
              <Route
                path="contacts/new"
                component={RequireAuth(CreateContact)}
              />
              <Route
                path="contacts/:contactId"
                component={RequireAuth(ContactFromId)}
              />
              <Route path="imports" component={RequireAuth(ImportEvents)} />
              <Route
                path="import/csv/contacts"
                component={RequireAuth(ImportContactsCsv)}
              />
              <Route
                path="members/:memberId"
                component={RequireAuth(MemberPage)}
              />
              <Route path="committees" component={RequireAuth(Committees)} />
              <Route
                path="imports/:reviewSessionId"
                component={RequireAuth(ImportEventPage)}
              >
                <IndexRoute component={RequireAuth(ImportEventOverview)} />
                <Route
                  path="contacts"
                  component={RequireAuth(ImportEventContacts)}
                />
              </Route>
              <Route path="meetings" component={RequireAuth(Meetings)} />
              <Route
                path="meetings/create"
                component={RequireAuth(CreateMeeting)}
              />
              <Route
                path="custom-fields"
                component={RequireAuth(CustomFieldsList)}
              />
              <Route
                path="custom-fields/new"
                component={RequireAuth(CreateCustomField)}
              />
              <Route
                path="custom-fields/:customFieldId"
                component={RequireAuth(CustomFieldFromId)}
              />
              <Route path="tags" component={RequireAuth(TagsList)} />
              {USE_ELECTIONS ? electionAdminRoutes : null}
            </Route>

            {/* Authenticated routes */}
            <Route>
              <Route path="home" component={RequireAuth(MainPage)} />
              <Route path="member" component={RequireAuth(MemberPage)} />
              <Route
                path="meetings/:meetingId/kiosk"
                component={RequireAuth(MeetingKioskPage)}
              />
              <Route
                path="meetings/:meetingId/proxy-token"
                component={RequireAuth(ProxyTokenCreate)}
              />
              <Route
                path="meetings/:meetingId/proxy-token/:proxyTokenId"
                component={RequireAuth(ProxyTokenConsume)}
              />
              <Route
                path="meetings/:meetingId/details"
                component={RequireAuth(MeetingDetailPage)}
              />
              <Route
                path="meetings/:meetingId/rsvp/:token"
                component={RequireAuth(MeetingRsvp)}
              />
              <Route
                path="meetings/:meetingId/admin"
                component={RequireAuth(MeetingAdmin)}
              />
              <Route
                path="committees/:committeeId"
                component={RequireAuth(Committee)}
              />
              <Route
                path="my-membership"
                component={RequireAuth(MyMembership)}
              />
              <Route
                path="my-committees"
                component={RequireAuth(MyCommittees)}
              />
              <Route path="my-meetings" component={RequireAuth(MyMeetings)} />
              <Route
                path="my-contact-info"
                component={RequireAuth(MyContactInfo)}
              />
              <Route path="profile" component={RequireAuth(Profile)} />
              {USE_ELECTIONS ? electionRoutes : null}
            </Route>
            <Route path="*" component={NotFound} />
          </Route>
        </Router>
      </>
    )
  }
}

const mapStateToProps = (state: RootReducer) => state
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fullySignOut }, dispatch)

type RoutesStateProps = RootReducer
type RoutesDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  RoutesStateProps,
  RoutesDispatchProps,
  RoutesOwnProps,
  RootReducer
>(
  mapStateToProps,
  mapDispatchToProps
)(Routes)
