import cx from 'classnames'
import React from 'react'
import {
  Button,
  ListGroup,
  OverlayTrigger,
  Popover,
  PopoverProps,
} from 'react-bootstrap'
import { MdOutlineLocationOff, MdOutlineLocationOn } from 'react-icons/md'
import { LaxPostalAddressResponse } from 'src/js/api/schemas'
import { TimeAgoSpan } from 'src/js/components/common/TimeAgoSpan'

const getFullAddressString = (response: LaxPostalAddressResponse): string => {
  return `${response.line1} ${response.line2}, ${response.city} ${response.state} ${response.zipcode}`
}

interface PostalAddressDetailsPopoverProps extends Partial<PopoverProps> {
  address: LaxPostalAddressResponse
}

const PostalAddressDetailsPopover = React.forwardRef<
  HTMLDivElement,
  PostalAddressDetailsPopoverProps
>(({ address, ...props }, ref) => (
  <Popover ref={ref} {...props} id={getFullAddressString(address)}>
    {address.date_archived != null && (
      <div className="bg-warning px-3 p-2">
        <div className="font-weight-bold">
          Address archived <TimeAgoSpan date={address.date_archived} />
        </div>
        {address.reason_archived != null && (
          <span>Reason: {address.reason_archived}</span>
        )}
      </div>
    )}
    <Popover.Content className="p-0">
      <ListGroup variant="flush" as="dl">
        {address.consent_to_mail != null && (
          <ListGroup.Item className="p-2 px-3 display-flex flex-row justify-content-between">
            <dt>Consent to send mail?</dt>
            <dd>{address.consent_to_mail ? 'Yes' : 'No'}</dd>
          </ListGroup.Item>
        )}
        {address.contact != null && (
          <ListGroup.Item className="p-2 px-3 display-flex flex-row justify-content-between">
            <dt>Belongs to</dt>
            <dd>
              <code>{address.contact}</code>
            </dd>
          </ListGroup.Item>
        )}
      </ListGroup>
    </Popover.Content>
  </Popover>
))

export interface PostalAddressCardProps {
  address: LaxPostalAddressResponse
}

export const PostalAddressCard: React.FC<PostalAddressCardProps> = ({
  address,
}) => (
  <OverlayTrigger
    trigger={['click']}
    placement="bottom"
    rootClose={true}
    overlay={<PostalAddressDetailsPopover address={address} />}
  >
    <Button
      variant="light"
      className="p-1 pt-0 text-left d-flex flex-row align-items-center"
      onClick={(e) => {
        e.preventDefault()
        e.stopPropagation()
      }}
    >
      <div className="mr-2">
        {address.consent_to_mail === false ? (
          <MdOutlineLocationOff />
        ) : (
          <MdOutlineLocationOn />
        )}
      </div>
      <div
        className={cx('text-underline-dotted', 'text-primary', {
          'text-danger': address.consent_to_mail === false,
          'text-strikethrough': address.date_archived != null,
        })}
      >
        {address.line1 && (
          <>
            <span className="address-line1">{address.line1}</span>
            <br />
          </>
        )}
        {address.line2 && (
          <>
            <span className="address-line2">{address.line2}</span>
            <br />
          </>
        )}
        {address.city && (
          <>
            <span className="address-city">{address.city}</span>{' '}
          </>
        )}
        {address.state && (
          <>
            <span className="address-city">{address.state}</span>,{' '}
          </>
        )}
        {address.zipcode}
      </div>
    </Button>
  </OverlayTrigger>
)
