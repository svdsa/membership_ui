import { FormikProps } from 'formik'
import React from 'react'
import { Alert, Col, Form, Row } from 'react-bootstrap'
import { ContactCreateRequest } from 'src/js/api/schemas'
import { MemberPronounBundle } from 'src/js/client/MemberClient'
import { DeveloperInfo } from 'src/js/components/common/DeveloperInfo'
import { LoadingButton } from 'src/js/components/common/LoadingButton'
import PageHeading from 'src/js/components/common/PageHeading'
import { PronounSelect } from 'src/js/components/membership/EditPronouns'
import {
  bundlePronouns,
  parseBundlesToDeclensions,
  renderDeclensions,
} from 'src/js/components/membership/pronounUtils'
import { FormikStatus } from 'src/js/util/formik'

export interface ContactFormProps extends FormikProps<ContactCreateRequest> {
  status?: FormikStatus
}

export const ContactForm: React.FC<ContactFormProps> = ({
  values,
  errors,
  status,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  setFieldValue,
  isSubmitting,
  isValid,
  dirty,
}) => {
  return (
    <Form onSubmit={handleSubmit}>
      <section className="pb-2 mb-2 border-bottom">
        <PageHeading level={3} className="h4 pb-2">
          Name and pronouns
        </PageHeading>
        <Form.Group as={Row} controlId="full_name">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">
              Full name (required)
            </Form.Label>
            <Form.Text className="text-muted pb-2">
              The person's full name. For official business (elections,
              administration) only.
            </Form.Text>
          </Col>
          <Col sm={9}>
            <Form.Control
              type="text"
              placeholder="W. E. B. Du Bois"
              name="full_name"
              value={values.full_name}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.full_name && !errors.full_name}
              isInvalid={touched.full_name && errors.full_name != null}
            />
            {touched.full_name && errors.full_name && (
              <Form.Text className="text-danger pb-2">
                {errors.full_name}
              </Form.Text>
            )}
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="display_name">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">Display name</Form.Label>
            <Form.Text className="text-muted pb-2">
              An informal name used day-to-day, like a first name or nickname.
              For use in informal chapter spaces (for example, in the portal, or
              with other members).
            </Form.Text>
          </Col>
          <Col sm={9}>
            <Form.Control
              type="text"
              placeholder="Will"
              name="display_name"
              value={values.display_name || ''}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.display_name && !errors.display_name}
              isInvalid={touched.display_name && errors.display_name != null}
            />
            {touched.display_name && errors.display_name && (
              <Form.Text className="text-danger pb-2">
                {errors.display_name}
              </Form.Text>
            )}
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="pronouns">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">Pronouns</Form.Label>
            <Form.Text className="text-muted pb-2">
              The person's personal pronouns. For use in chapter spaces.
            </Form.Text>
          </Col>
          <Col sm={9}>
            <PronounSelect
              pronouns={parseBundlesToDeclensions(
                values.pronouns as MemberPronounBundle
              )}
              onChange={(pronouns) =>
                setFieldValue(
                  'pronouns',
                  bundlePronouns(pronouns.map(renderDeclensions))
                )
              }
            />
          </Col>
        </Form.Group>
      </section>
      <section className="pb-2 mb-2 border-bottom">
        <PageHeading level={3} className="h4 pb-2">
          Contact methods
        </PageHeading>

        <Form.Group as={Row} controlId="email_addresses">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">
              Email addresses
            </Form.Label>
            <Form.Text className="text-muted pb-2">
              The person's email addresses and consent information.
            </Form.Text>
          </Col>
          <Col sm={9}>
            <Form.Control
              plaintext
              readOnly
              placeholder="adding email addresses via this form is not yet implemented - use the CSV import flow or edit the contact after creating to add email addresses to a contact"
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="phone_numbers">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">
              Phone numbers
            </Form.Label>
            <Form.Text className="text-muted pb-2">
              The person's phone numbers and consent information.
            </Form.Text>
          </Col>
          <Col sm={9}>
            <Form.Control
              plaintext
              readOnly
              placeholder="adding phone numbers via this form is not yet implemented - use the CSV import flow or edit the contact after creating to add phone numbers to a contact"
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="mailing_addresses">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">
              Mailing addresses
            </Form.Label>
            <Form.Text className="text-muted pb-2">
              The person's location, from coarse (postal code) to specific (a
              mailing address). Precise addresses are only used for official
              business and with consent (for example, for collective dues
              billing or physical mail).
            </Form.Text>
          </Col>
          <Col sm={9}>
            <Form.Control
              plaintext
              readOnly
              placeholder="adding mailing addresses via this form is not yet implemented - use the CSV import flow to add mailing addresses to a contact"
            />
          </Col>
        </Form.Group>
      </section>
      <section className="pb-2 mb-2 border-bottom">
        <PageHeading level={3} className="h4 pb-2">
          Additional info
        </PageHeading>

        <Form.Group as={Row} controlId="tags">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">Tags</Form.Label>
          </Col>
          <Col sm={9}>
            <Form.Control
              plaintext
              readOnly
              placeholder="adding tags via this form is not yet implemented - use the CSV import flow to add tags to a contact"
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="admin_notes">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">Admin notes</Form.Label>
          </Col>
          <Col sm={9}>
            <Form.Control
              as="textarea"
              readOnly
              placeholder="adding admin notes via this form not yet implemented"
            />
          </Col>
        </Form.Group>
      </section>
      <section className="pb-2 mb-2 border-bottom">
        <PageHeading level={3} className="h4 pb-2">
          Custom fields
        </PageHeading>

        <Form.Group as={Row} controlId="custom_fields">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">
              Custom fields
            </Form.Label>
          </Col>
          <Col sm={9}>
            <Form.Control
              plaintext
              readOnly
              placeholder="adding custom fields via this form is not yet implemented - use the CSV import flow to add custom fields to a contact"
            />
          </Col>
        </Form.Group>
      </section>
      <section className="py-2 mb-2">
        <Row>
          <Col sm={3}></Col>
          <Col sm={9}>
            <LoadingButton
              isLoading={isSubmitting}
              disabled={!dirty || !isValid}
              type="submit"
              variant="success"
              disabledVariant="outline-success"
            >
              Create new contact
            </LoadingButton>

            {status?.formError != null && (
              <Alert className="mt-2" variant="danger">
                {status.formError.message}
                <DeveloperInfo data={status.formError.innerError} />
              </Alert>
            )}

            <hr />

            <DeveloperInfo data={values} />
          </Col>
        </Row>
      </section>
    </Form>
  )
}
