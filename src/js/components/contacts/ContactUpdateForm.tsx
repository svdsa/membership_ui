import { ErrorMessage, FormikProps } from 'formik'
import React from 'react'
import { Alert, Col, Form, Row } from 'react-bootstrap'
import { ContactRequestNullish } from 'src/js/api/schemas'
import { MemberPronounBundle } from 'src/js/client/MemberClient'
import { DeveloperInfo } from 'src/js/components/common/DeveloperInfo'
import { LoadingButton } from 'src/js/components/common/LoadingButton'
import PageHeading from 'src/js/components/common/PageHeading'
import { PronounSelect } from 'src/js/components/membership/EditPronouns'
import {
  bundlePronouns,
  parseBundlesToDeclensions,
  renderDeclensions,
} from 'src/js/components/membership/pronounUtils'
import { FormikStatus } from 'src/js/util/formik'

export interface ContactFormProps extends FormikProps<ContactRequestNullish> {
  status?: FormikStatus
}

export const ContactUpdateForm: React.FC<ContactFormProps> = ({
  values,
  errors,
  status,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  setFieldValue,
  isSubmitting,
  isValid,
  dirty,
}) => {
  return (
    <Form onSubmit={handleSubmit}>
      <section className="pb-2 mb-2 border-bottom">
        <PageHeading level={3} className="h4 pb-2">
          Profile
        </PageHeading>
        <Form.Group as={Row} controlId="full_name">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">
              Full name (required)
            </Form.Label>
            <Form.Text className="text-muted pb-2">
              For official business (elections, administration).
            </Form.Text>
          </Col>
          <Col sm={9}>
            <Form.Control
              type="text"
              placeholder="W. E. B. Du Bois"
              name="full_name"
              value={values.full_name ?? undefined}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.full_name && !errors.full_name}
              isInvalid={touched.full_name && errors.full_name != null}
            />
            {touched.full_name && errors.full_name && (
              <Form.Text className="text-danger pb-2">
                {errors.full_name}
              </Form.Text>
            )}
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="display_name">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">Display name</Form.Label>
            <Form.Text className="text-muted pb-2">
              A colloquial, informal name for use in chapter spaces.
            </Form.Text>
          </Col>
          <Col sm={9}>
            <Form.Control
              type="text"
              placeholder="Will"
              name="display_name"
              value={values.display_name || ''}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.display_name && !errors.display_name}
              isInvalid={touched.display_name && errors.display_name != null}
            />
            {touched.display_name && errors.display_name && (
              <Form.Text className="text-danger pb-2">
                {errors.display_name}
              </Form.Text>
            )}
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="pronouns">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">Pronouns</Form.Label>
            <Form.Text className="text-muted pb-2">
              For others to refer to.
            </Form.Text>
          </Col>
          <Col sm={9}>
            <PronounSelect
              pronouns={parseBundlesToDeclensions(
                values.pronouns as MemberPronounBundle
              )}
              onChange={(pronouns) =>
                setFieldValue(
                  'pronouns',
                  bundlePronouns(pronouns.map(renderDeclensions))
                )
              }
            />
            <ErrorMessage
              name="pronouns"
              component={Form.Text}
              className="text-danger pb-2"
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="biography">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">Biography</Form.Label>
            <Form.Text className="text-muted pb-2">
              Talk about yourself.
            </Form.Text>
          </Col>
          <Col sm={9}>
            <Form.Control
              as="textarea"
              name="biography"
              value={values.biography || ''}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.biography && !errors.biography}
              isInvalid={touched.biography && errors.biography != null}
            />
            <ErrorMessage
              name="biography"
              component={Form.Text}
              className="text-danger pb-2"
            />
          </Col>
        </Form.Group>
      </section>
      <section className="pb-2 mb-2 border-bottom">
        <PageHeading level={3} className="h4 pb-2">
          Admin info
        </PageHeading>
        <Form.Group as={Row} controlId="admin_notes">
          <Col sm={3} className="pt-2">
            <Form.Label className="font-weight-medium">Notes</Form.Label>
            <Form.Text className="text-muted pb-2">
              Only visible to chapter leadership.
            </Form.Text>
          </Col>
          <Col sm={9}>
            <Form.Control
              as="textarea"
              name="admin_notes"
              value={values.admin_notes ?? undefined}
              onChange={handleChange}
              onBlur={handleBlur}
              isValid={touched.admin_notes && !errors.admin_notes}
              isInvalid={touched.admin_notes && errors.admin_notes != null}
            />
            <ErrorMessage
              name="admin_notes"
              component={Form.Text}
              className="text-danger pb-2"
            />
          </Col>
        </Form.Group>
      </section>
      <section className="py-2 mb-2">
        <Row>
          <Col sm={3}></Col>
          <Col sm={9}>
            <LoadingButton
              isLoading={isSubmitting}
              disabled={!dirty || !isValid}
              type="submit"
              variant="success"
              disabledVariant="outline-success"
            >
              Save changes
            </LoadingButton>

            {status?.formError != null && (
              <Alert className="mt-2" variant="danger">
                {status.formError.message}
                <DeveloperInfo data={status.formError.innerError} />
              </Alert>
            )}

            <hr />

            <DeveloperInfo data={values} />
          </Col>
        </Row>
      </section>
    </Form>
  )
}
