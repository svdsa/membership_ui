import type { Meta, Story } from '@storybook/react'
import React from 'react'
import { IdContact, IdPostalAddress } from 'src/js/api/schemas'
import {
  PostalAddressCard,
  PostalAddressCardProps,
} from 'src/js/components/contacts/PostalAddressCard'

export default {
  title: 'Contacts/PostalAddressCard',
  component: PostalAddressCard,
} as Meta

const Template: Story<PostalAddressCardProps> = (args) => (
  <PostalAddressCard {...args} />
)

export const Simple: Story<PostalAddressCardProps> = Template.bind({})
Simple.args = {
  address: {
    object: 'postal_address',
    city: 'San Jose',
    consent_to_mail: true,
    date_archived: null,
    date_created: new Date('2021-07-01T00:00:00+00:00'),
    id: 'pad_KXnOjYlamlxW413z' as IdPostalAddress,
    line1: '123 Fake St',
    line2: 'Apt 4',
    reason_archived: null,
    source_created: null,
    state: 'CA',
    zipcode: '95123',
    contact: 'ct_2eOl9vxOmwMEAk4j' as IdContact,
  },
}
