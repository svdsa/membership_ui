import React from 'react'
import { Badge } from 'react-bootstrap'
import { MemberPronounBundle } from 'src/js/client/MemberClient'
import {
  parseBundlesToDeclensions,
  PronounDeclensions,
  renderDeclensions,
} from 'src/js/components/membership/pronounUtils'

interface PronounsSpanProps {
  pronouns: PronounDeclensions
}

const PronounsSpan: React.FC<PronounsSpanProps> = ({ pronouns }) => (
  <span>{renderDeclensions(pronouns)}</span>
)

interface PronounBadgeProps {
  pronouns: PronounDeclensions
}

export const PronounBadge: React.FC<PronounBadgeProps> = ({ pronouns }) => (
  <Badge pill variant="secondary">
    <PronounsSpan pronouns={pronouns} />
  </Badge>
)

export interface PronounBadgeListProps {
  pronouns: PronounDeclensions[]
}

export const PronounBadgeList: React.FC<PronounBadgeListProps> = ({
  pronouns,
}) => (
  <>
    {pronouns.map((ps, index) => (
      <span key={index} className="mr-2">
        <PronounBadge pronouns={ps} />
      </span>
    ))}
  </>
)

export interface PronounBadgeListFromBundleProps {
  bundle: MemberPronounBundle
}

export const PronounBadgeListFromBundle: React.FC<PronounBadgeListFromBundleProps> =
  ({ bundle }) => (
    <PronounBadgeList pronouns={parseBundlesToDeclensions(bundle)} />
  )
