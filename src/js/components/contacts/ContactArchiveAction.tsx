import React, { useEffect, useState } from 'react'
import { Alert, Button, Card, Form, Spinner } from 'react-bootstrap'
import {
  useArchiveContactMutation,
  useUnarchiveContactMutation,
} from 'src/js/api'
import { ContactResponse, IdContact } from 'src/js/api/schemas'
import TypeToConfirmDialog from 'src/js/components/common/TypeToConfirmDialog'
import { showNotification } from 'src/js/util/util'

export interface ContactArchiveActionProps {
  contact: ContactResponse
  onArchive?(): void
  onUnarchive?(): void
  archiveImmediately?: boolean
}

export const ContactArchiveAction: React.FC<ContactArchiveActionProps> = ({
  contact,
  archiveImmediately,
  onArchive,
  onUnarchive,
}) => {
  const [
    archiveContact,
    {
      isLoading: isArchiving,
      isError: archiveIsError,
      error: archiveError,
      isSuccess: archiveIsSuccess,
    },
  ] = useArchiveContactMutation()
  const [
    unarchiveContact,
    {
      isLoading: isUnarchiving,
      isError: unarchiveIsError,
      error: unarchiveError,
      isSuccess: unarchiveIsSuccess,
    },
  ] = useUnarchiveContactMutation()
  const [showConfirmArchiveModal, setShowConfirmArchiveModal] = useState(false)
  const [archiveReason, setArchiveReason] = useState('')

  useEffect(() => {
    if (archiveIsError || unarchiveIsError || archiveImmediately) {
      setShowConfirmArchiveModal(false)
    }

    if (archiveIsSuccess) {
      showNotification(
        'Archival successful',
        contact != null
          ? `The contact "${contact.full_name}" was archived successfully. 🥳`
          : 'The contact was archived successfully.'
      )

      if (onArchive != null) {
        onArchive()
      }
    }

    if (unarchiveIsSuccess) {
      showNotification(
        'Restoration successful',
        contact != null
          ? `The contact "${contact.full_name}" was restored successfully. 🥳`
          : 'The contact was restored successfully.'
      )

      if (onUnarchive != null) {
        onUnarchive()
      }
    }
  }, [
    archiveIsError,
    archiveIsSuccess,
    unarchiveIsSuccess,
    unarchiveIsError,
    setShowConfirmArchiveModal,
    contact,
    onArchive,
    archiveImmediately,
    onUnarchive,
  ])

  const handleConfirmArchive: React.MouseEventHandler<HTMLButtonElement> = (
    e
  ) => {
    if (archiveImmediately) {
      archiveContact({
        id: contact.id,
        body: { reason_archived: undefined },
      })
    } else {
      setShowConfirmArchiveModal(true)
    }
  }

  return (
    <Card border="danger">
      <Card.Body>
        {contact.date_archived != null ? (
          <>
            <Card.Title>Restore {contact.full_name} from archive</Card.Title>
            <p>
              Restoring a contact will make them show up again in lists,
              searches, and other places in the portal.
            </p>
            <p>
              The person will be able to sign in again and participate in
              chapter activities, and will start to receive communications again
              if they were opted in.
            </p>
            <Button
              variant="warning"
              onClick={() => unarchiveContact(contact.id)}
            >
              {isUnarchiving ? (
                <Spinner animation="border" size="sm" />
              ) : (
                'Restore from archive'
              )}
            </Button>
          </>
        ) : (
          <>
            <Card.Title>Archive {contact.full_name}</Card.Title>
            <p>
              Archiving a contact <em>deactivates</em> the person's
              participation in the chapter.
            </p>
            <p>This will:</p>
            <ul>
              <li>
                hide them from lists, searches, and other places in the portal
              </li>
              <li>
                disable their portal account, so they will no longer be able to
                sign in
              </li>
              <li>
                exclude them from chapter elections and activities that require
                an active contact
              </li>
              {/* TODO only show if chapter uses future communications integrations */}
              <li>
                pause any newsletters or text subscriptions they may have from
                the chapter (if the chapter uses the portal for communications)
              </li>
            </ul>
            <p>An admin can restore the contact at any time.</p>
            <Button variant="danger" onClick={handleConfirmArchive}>
              Archive
            </Button>
          </>
        )}
        {archiveError && (
          <Alert variant="danger">
            <Alert.Heading>Error when archiving custom field</Alert.Heading>
            <pre>{JSON.stringify(archiveError, null, 2)}</pre>
          </Alert>
        )}
        {unarchiveError && (
          <Alert variant="danger">
            <Alert.Heading>Error when restoring custom field</Alert.Heading>
            <pre>{JSON.stringify(unarchiveError, null, 2)}</pre>
          </Alert>
        )}
      </Card.Body>
      <TypeToConfirmDialog
        onHide={() => setShowConfirmArchiveModal(false)}
        show={showConfirmArchiveModal}
        title={`Really archive "${contact.full_name}"?`}
        confirmHintText={
          <span>
            To confirm archival, please type{' '}
            <strong>{contact.full_name}</strong> into the box below.
          </span>
        }
        isLoading={isArchiving}
        typeToConfirmText={contact.full_name}
        actionText={`Yes, I want to archive "${contact.full_name}"`}
        onConfirm={() => {
          archiveContact({
            id: contact.id as IdContact,
            body: {
              reason_archived:
                archiveReason.length > 0 ? archiveReason : undefined,
            },
          })
        }}
      >
        <p>You are about to archive the contact for {contact.full_name}.</p>
        <Form.Control
          value={archiveReason}
          onChange={(e) => setArchiveReason(e.currentTarget.value)}
          placeholder="Reason for archive (optional)"
        />
      </TypeToConfirmDialog>
    </Card>
  )
}
