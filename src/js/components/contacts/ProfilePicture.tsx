import cx from 'classnames'
import { head, last } from 'lodash'
import React from 'react'
import { ContactResponse } from 'src/js/api/schemas'
import { Discs as Identicon } from 'src/js/vendor/identicon'

const validSizes = ['xs', 'sm', 'md', 'lg', 'xl'] as const
const validSizesSet = new Set(validSizes)
type ValidSizes = typeof validSizes[number]

export interface ProfilePictureProps {
  contact: ContactResponse
  size?: 'xs' | 'sm' | 'md' | 'lg' | 'xl'
  showPlaceholderText?: boolean
  placeholderText?: string
}

const generatePlaceholderText = (contact: ContactResponse): string => {
  const nameInitials = contact.full_name.split(/\s+/).map((token) => token[0])
  if (nameInitials.length < 2) {
    return contact.full_name.slice(0, 2)
  }

  return [head(nameInitials), last(nameInitials)].join('')
}

const sizeMapping: { [k in ValidSizes]: string } = {
  xs: '24px',
  sm: '32px',
  md: '40px',
  lg: '64px',
  xl: '256px',
}

const textSizeMapping: { [k in ValidSizes]: string } = {
  xs: 'h6',
  sm: 'h5',
  md: 'h4',
  lg: 'h3',
  xl: 'display-3',
}

export const ProfilePicture: React.FC<ProfilePictureProps> = ({
  contact,
  showPlaceholderText = true,
  placeholderText,
  size = 'md',
}) => {
  const picSize = validSizesSet.has(size) ? sizeMapping[size] : size
  const textSize = validSizesSet.has(size) ? textSizeMapping[size] : 'h3'

  if (contact.profile_pic != null) {
    return (
      <img
        src={contact.profile_pic}
        alt={`Profile picture for ${contact.display_name}`}
        className="rounded-circle"
        style={{
          width: picSize,
          height: picSize,
          objectFit: 'cover',
        }}
      />
    )
  } else {
    return (
      <div
        style={{
          position: 'relative',
          width: picSize,
          height: picSize,
        }}
        className="rounded-circle"
      >
        <Identicon
          seed={contact.email_addresses.data?.[0]?.value ?? contact.full_name}
          className="rounded-circle"
        />
        {showPlaceholderText && (
          <div
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '100%',
            }}
            className="d-flex justify-content-center align-items-center"
          >
            <span
              className={cx(
                'd-block text-light m-0 p-0 text-unselectable',
                textSize
              )}
            >
              {placeholderText?.slice(0, 2) ?? generatePlaceholderText(contact)}
            </span>
          </div>
        )}
      </div>
    )
  }
}
