import type { Meta, Story } from '@storybook/react'
import React from 'react'
import {
  ProfilePicture,
  ProfilePictureProps,
} from 'src/js/components/contacts/ProfilePicture'
import { FAKE_contactResponse } from 'src/js/stories/data'

export default {
  title: 'Contacts/ProfilePicture',
  component: ProfilePicture,
} as Meta

const ProfilePictureTemplate: Story<ProfilePictureProps> = (args) => (
  <ProfilePicture {...args} />
)

export const WithPicture: Story<ProfilePictureProps> =
  ProfilePictureTemplate.bind({})
WithPicture.args = {
  contact: FAKE_contactResponse,
}

export const Generated: Story<ProfilePictureProps> =
  ProfilePictureTemplate.bind({})
Generated.args = {
  contact: { ...FAKE_contactResponse, profile_pic: null },
}
