import type { Meta, Story } from '@storybook/react'
import React from 'react'
import { MemberPronounBundle } from 'src/js/client/MemberClient'
import {
  PronounBadgeList,
  PronounBadgeListFromBundle,
  PronounBadgeListFromBundleProps,
  PronounBadgeListProps,
} from 'src/js/components/contacts/PronounBadgeList'

export default {
  title: 'Contacts/PronounBadgeList',
  component: PronounBadgeList,
} as Meta

const PronounBadgeListTemplate: Story<PronounBadgeListProps> = (args) => (
  <PronounBadgeList {...args} />
)

export const Multiple: Story<PronounBadgeListProps> =
  PronounBadgeListTemplate.bind({})
Multiple.args = {
  pronouns: [
    {
      subject: 'she',
      object: 'her',
      possessiveDeterminer: 'her',
      possessive: 'hers',
      reflexive: 'herself',
    },
    {
      subject: 'they',
      object: 'them',
      possessiveDeterminer: 'their',
      possessive: 'theirs',
      reflexive: 'themselves',
    },
  ],
}

const PronounBadgeListFromBundleTemplate: Story<PronounBadgeListFromBundleProps> =
  (args) => <PronounBadgeListFromBundle {...args} />

export const FromBundle: Story<PronounBadgeListFromBundleProps> =
  PronounBadgeListFromBundleTemplate.bind({})
FromBundle.args = {
  bundle: 'she/her;they/them' as MemberPronounBundle,
}
