import { skipToken } from '@reduxjs/toolkit/dist/query'
import { compact, head, stubTrue } from 'lodash'
import React, { useCallback, useMemo, useState } from 'react'
import {
  AsyncTypeahead,
  TypeaheadMenuProps,
  TypeaheadResult,
} from 'react-bootstrap-typeahead'
import Highlighter from 'react-highlight-words'
import { useSearchContactsQuery } from 'src/js/api'
import { ContactResponse } from 'src/js/api/schemas'

const renderSuggestion = (
  contact: TypeaheadResult<ContactResponse>,
  props: TypeaheadMenuProps<ContactResponse>
) => (
  <>
    <Highlighter
      searchWords={[props.text ?? '']}
      activeClassName="bg-warning"
      textToHighlight={contact.full_name}
    />
  </>
)

export interface ContactSearchAutosuggestProps {
  onContactSelected(contact: ContactResponse | null): void
  value?: ContactResponse
}

export const ContactSearchAutosuggest: React.FC<ContactSearchAutosuggestProps> =
  ({ onContactSelected, value }) => {
    const [query, setQuery] = useState('')
    const {
      data: contactsList,
      isLoading: contactsLoading,
      error: contactsError,
    } = useSearchContactsQuery(query.length > 0 ? { q: query } : skipToken)

    const handleSearch = useCallback(
      (value: string) => {
        setQuery(value.trim().toLocaleLowerCase())
      },
      [setQuery]
    )

    const handleSelectionChange = useCallback(
      (contacts: ContactResponse[]) => {
        if (contacts.length === 0) {
          onContactSelected(null)
          return
        }

        onContactSelected(head(contacts) ?? null)
      },
      [onContactSelected]
    )

    const defaultSelection = useMemo(() => compact([value]), [value])

    return (
      <AsyncTypeahead
        filterBy={stubTrue} // bypass filtering since the backend will do that
        clearButton
        id="contact-search"
        labelKey="full_name"
        isLoading={contactsLoading}
        minLength={2}
        multiple={false}
        onSearch={handleSearch}
        options={contactsList?.data ?? []}
        placeholder="Search by name, email, phone..."
        renderMenuItemChildren={renderSuggestion}
        onChange={handleSelectionChange}
        defaultSelected={defaultSelection}
        useCache={false}
      />
    )
  }
