import { compact } from 'lodash/fp'
import React, { useMemo } from 'react'
import { Nav, Navbar, NavbarBrand, NavDropdown } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { useGetContactMeQuery, useGetUserMeQuery } from 'src/js/api'
import { NavLinkEntry } from 'src/js/components/admin/Admin'
import { ContactName } from 'src/js/components/common/ContactName'
import { hasAdminPermission } from 'src/js/util/permissions'
import logo from '../../../../images/dsa-logo.png'

const navMap: NavLinkEntry[] = compact([
  {
    link: '/home',
    label: 'Home',
  },
  // {
  //   link: '/profile',
  //   label: 'Profile'
  // },
  // {
  //   link: '/my-membership',
  //   label: 'Membership'
  // },
  // USE_ELECTIONS && {
  //   link: '/my-elections',
  //   label: 'Elections'
  // },
  // {
  //   link: '/my-committees',
  //   label: capitalize(c('GROUP_NAME_PLURAL'))
  // },
  // {
  //   link: '/my-meetings',
  //   label: 'Meetings'
  // }
])

const Navigation: React.FC = () => {
  const { data: user } = useGetUserMeQuery()
  const { data: contact, isLoading: contactLoading } = useGetContactMeQuery()

  const links = useMemo(
    () =>
      navMap.map(({ link, label }) => (
        <Nav.Item key={link} className="dsaNavItem">
          {link != null ? (
            <LinkContainer to={link}>
              <Nav.Link>{label}</Nav.Link>
            </LinkContainer>
          ) : (
            <Nav.Link>{label}</Nav.Link>
          )}
        </Nav.Item>
      )),
    [navMap]
  )

  return (
    <Navbar collapseOnSelect tabIndex={-1} expand="md" className="mainNav">
      <NavbarBrand href="/home">
        <img
          src={logo}
          alt=""
          className="d-inline-block align-top"
          style={{ maxHeight: '55px' }}
        />
      </NavbarBrand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse>
        <Nav className="mr-auto">{links}</Nav>
        <Nav>
          {user != null && hasAdminPermission(user.permissions) && (
            <Nav.Item key="admin-link" className="dsaNavItem">
              <LinkContainer to="/admin">
                <Nav.Link>Admin</Nav.Link>
              </LinkContainer>
            </Nav.Item>
          )}
          {contact != null ? (
            <NavDropdown
              role="menu"
              alignRight
              title={<ContactName contact={contact} />}
              id="portal-navbar-profile-menu"
            >
              <LinkContainer role="menuitem" to="/profile">
                <NavDropdown.Item>
                  Signed in as <strong>{contact.full_name}</strong>
                </NavDropdown.Item>
              </LinkContainer>
              <NavDropdown.Divider />
              <LinkContainer role="menuitem" to="/profile">
                <NavDropdown.Item>Your profile</NavDropdown.Item>
              </LinkContainer>
              <NavDropdown.Divider />
              <LinkContainer role="menuitem" to="/logout">
                <NavDropdown.Item>Log out</NavDropdown.Item>
              </LinkContainer>
            </NavDropdown>
          ) : (
            <Nav.Item className="dsaNavItem">
              <LinkContainer to="/logout">
                <Nav.Link>Logout</Nav.Link>
              </LinkContainer>
            </Nav.Item>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

export default Navigation
