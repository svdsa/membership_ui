import { fromPairs } from 'lodash/fp'
import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { useMutation } from 'react-query'
import { CommitteeMember } from 'src/js/client/CommitteeClient'
import CommitteeLeadership, {
  CommitteeLeader,
  ReplaceLeaderArgs,
} from 'src/js/client/CommitteeLeadersClient'
import FieldGroup from '../common/FieldGroup'

interface ReplaceLeaderModalProps {
  committeeId: number
  roles: CommitteeLeader[]
  members: CommitteeMember[]
  onComplete: (CommitteeLeader) => void
  onCancel: () => void
}

const ReplaceLeaderModal: React.FC<ReplaceLeaderModalProps> = ({
  committeeId,
  roles,
  members,
  onComplete,
  onCancel,
}) => {
  const { mutate: submitInner } = useMutation<
    CommitteeLeader,
    Error | null,
    ReplaceLeaderArgs
  >((args) => CommitteeLeadership.replaceLeader(args))
  const roleOptions = fromPairs(
    roles.map((role) => [
      role.id,
      `${role.role_title} (${role.member_name ?? 'Unfilled'})`,
    ])
  )
  const [selectedRole, setSelectedRole] = useState('')
  const selectedRoleMemberId = selectedRole
    ? roles.find((role) => `${role.id}` === selectedRole)?.member_id
    : null
  const memberOptions = {
    ...fromPairs<string>(
      members
        .filter((member) => member.id !== selectedRoleMemberId)
        .map((member) => [`${member.id}`, member.name])
    ),
    null: '(Remove without replacement)',
  }
  const [selectedMember, setSelectedMember] = useState('')
  const submitEnabled = selectedRole && selectedMember
  const submit = async () => {
    const result = await submitInner({
      committeeId,
      leaderId: parseInt(selectedRole, 10),
      memberId: selectedMember === 'null' ? null : parseInt(selectedMember, 10),
    })
    onComplete(result)
  }

  return (
    <Modal.Body>
      <p>Replace which member holds a leadership&nbsp;role</p>
      <Form onSubmit={(e) => e.preventDefault()}>
        <FieldGroup
          formKey="leadership_role_id"
          label="Role"
          componentClass="select"
          options={roleOptions}
          placeholder="Select a role"
          value={selectedRole}
          onFormValueChange={(k, v) => setSelectedRole(v)}
          required
        />
        <FieldGroup
          formKey="member_id"
          label="Member"
          componentClass="select"
          options={memberOptions}
          placeholder="Select a member"
          value={selectedMember}
          onFormValueChange={(k, v) => setSelectedMember(v)}
          required
        />
        <div>
          <Button type="button" onClick={submit} disabled={!submitEnabled}>
            Submit
          </Button>{' '}
          <Button type="button" onClick={onCancel}>
            Cancel
          </Button>
        </div>
      </Form>
    </Modal.Body>
  )
}

export default ReplaceLeaderModal
