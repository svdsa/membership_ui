import { sortBy } from 'lodash'
import { capitalize } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Link, RouteComponentProps } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import { c } from 'src/js/config'
import { Committee } from 'src/js/redux/reducers/committeeReducers'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import * as actions from '../../redux/actions/committeeActions'
import { isAdmin } from '../../services/members'
import FieldGroup from '../common/FieldGroup'
import PageHeading from '../common/PageHeading'

interface CreateCommitteeForm {
  name: string

  /**
   * Admin emails (comma-separated)
   */
  admin_list: string
  provisional: boolean
}

interface CommitteesState {
  committee: CreateCommitteeForm
}

interface CommitteesParamProps {}
interface CommitteesRouteParamProps {}

type CommitteesOtherProps = RouteComponentProps<
  CommitteesParamProps,
  CommitteesRouteParamProps
>
type CommitteesProps = CommitteesStateProps &
  CommitteesDispatchProps &
  CommitteesOtherProps

class Committees extends Component<CommitteesProps, CommitteesState> {
  constructor(props) {
    super(props)
    this.state = {
      committee: { name: '', admin_list: '', provisional: false },
    }
  }

  componentDidMount() {
    this.props.fetchCommittees()
  }

  updateCreateCommitteeForm = <K extends keyof CreateCommitteeForm>(
    formKey: K,
    value: CreateCommitteeForm[K]
  ) => {
    if (this.props.committees.form.create.inSubmission) {
      return
    }
    this.setState({ committee: { ...this.state.committee, [formKey]: value } })
  }

  renderCommittee(committee: Committee) {
    const id = committee.id
    const name = committee.name
    const inactive = committee.inactive

    return (
      <div key={`committee-${id}`}>
        <Link to={`/committees/${id}`}>{name}</Link>
        {inactive ? ' (Inactive)' : null}
      </div>
    )
  }

  createCommitteeFromState(committee: CreateCommitteeForm) {
    const admin_list = (committee.admin_list || '').split(',').filter((v) => v)
    this.props.createCommittee({
      name: committee.name,
      admin_list,
      provisional: committee.provisional,
    })
  }

  render() {
    if (!isAdmin(this.props.member)) {
      return null
    }

    const committees = sortBy(
      Object.values(this.props.committees.byId),
      (committee: Committee) => committee.name
    ).map((committee: Committee) => this.renderCommittee(committee))
    return (
      <Container>
        <Helmet>
          <title>{c('GROUP_NAME_PLURAL')}</title>
        </Helmet>
        <Row>
          <Col sm={4}>
            <PageHeading level={1}>
              {capitalize(c('GROUP_NAME_PLURAL'))}
            </PageHeading>
            <h2>Add {c('GROUP_NAME_SINGULAR')}</h2>
            <Form onSubmit={(e) => e.preventDefault()}>
              <FieldGroup
                formKey="name"
                componentClass="input"
                type="text"
                label={`${capitalize(c('GROUP_NAME_SINGULAR'))} Name`}
                value={this.state.committee.name}
                onFormValueChange={this.updateCreateCommitteeForm}
                required
              />
              <FieldGroup
                formKey="admin_list"
                componentClass="input"
                type="text"
                label="Admin Emails (comma-separated)"
                value={this.state.committee.admin_list}
                onFormValueChange={this.updateCreateCommitteeForm}
                required
              />
              <FieldGroup
                formKey="provisional"
                componentClass="checkbox"
                label="Provisional?"
                value={this.state.committee.provisional}
                onFormValueChange={this.updateCreateCommitteeForm}
              />
              <Button
                type="submit"
                onClick={(e) => {
                  e.preventDefault()
                  this.createCommitteeFromState(this.state.committee)
                }}
              >
                Add {c('GROUP_NAME_SINGULAR')}
              </Button>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col>
            <h2>{capitalize(c('GROUP_NAME_PLURAL'))}</h2>
            {committees}
          </Col>
        </Row>
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(actions, dispatch)

type CommitteesStateProps = RootReducer
type CommitteesDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CommitteesStateProps,
  CommitteesDispatchProps,
  {},
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(Committees)
