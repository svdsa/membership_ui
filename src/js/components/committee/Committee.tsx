import produce from 'immer'
import { sortBy } from 'lodash'
import React, { Component } from 'react'
import { Button, Container, Modal, Table } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { FiMinusCircle } from 'react-icons/fi'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import { CommitteeDetailed, Committees } from 'src/js/client/CommitteeClient'
import CommitteeLeadership, {
  CommitteeLeader,
} from 'src/js/client/CommitteeLeadersClient'
import Loading from 'src/js/components/common/Loading'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { EligibleMemberListEntry, Members } from '../../client/MemberClient'
import {
  addAdmin,
  fetchCommittee,
  markMemberInactive,
  removeAdmin,
  requestCommitteeMembership,
} from '../../redux/actions/committeeActions'
import { isAdmin, isCommitteeAdmin } from '../../services/members'
import FieldGroup from '../common/FieldGroup'
import MemberSearchField from '../common/MemberSearchField'
import PageHeading from '../common/PageHeading'
import AddLeadershipRoleModal from './AddLeadershipRoleModal'
import ReplaceLeaderModal from './ReplaceLeaderModal'

interface CommitteeParamProps {
  committeeId: string
}
interface CommitteeState {
  inactive: boolean | null
  leaders: CommitteeLeader[] | null
  showAddRoleModal: boolean
  showReplaceLeaderModal: boolean
}
interface CommitteeRouteParamProps {}
type CommitteeOtherProps = RouteComponentProps<
  CommitteeParamProps,
  CommitteeRouteParamProps
>
type CommitteeProps = CommitteeStateProps &
  CommitteeDispatchProps &
  CommitteeOtherProps

class Committee extends Component<CommitteeProps, CommitteeState> {
  constructor(props) {
    super(props)
    this.state = {
      inactive: null,
      leaders: null,
      showAddRoleModal: false,
      showReplaceLeaderModal: false,
    }
  }

  static getDerivedStateFromProps(
    nextProps: CommitteeProps,
    nextState: CommitteeState
  ) {
    if (nextState.inactive === null) {
      const committeeId = parseInt(nextProps.params.committeeId, 10)
      const committee: CommitteeDetailed | undefined =
        nextProps.committees.byId[committeeId]
      if (committee) {
        return {
          inactive: committee.inactive,
        }
      }
    }
    return nextState
  }

  componentDidMount() {
    this.props.fetchCommittee(this.getCommitteeId())
    this.fetchCommitteeLeaders(this.getCommitteeId())
  }

  async fetchCommitteeLeaders(committeeId: number) {
    const leaders = await CommitteeLeadership.list(committeeId)
    this.setState({ leaders })
  }

  getCommitteeId(): number {
    return parseInt(this.props.params.committeeId, 10)
  }

  addRoleComplete = (newRole: CommitteeLeader) => {
    this.setState(
      (state, props) => {
        if (!state?.leaders) {
          return null
        }
        return { leaders: [...state.leaders, newRole] }
      },
      () => this.setState({ showAddRoleModal: false })
    )
  }

  replaceLeaderComplete = (modifiedRole: CommitteeLeader) => {
    this.setState(
      (state, props) => {
        if (!state?.leaders) {
          return null
        }
        const replacedIndex = state.leaders.findIndex(
          (leader) => leader.id === modifiedRole.id
        )
        if (replacedIndex === -1) {
          return { leaders: state.leaders.push(modifiedRole) }
        }
        return {
          leaders: produce((draft) => {
            draft[replacedIndex] = modifiedRole
          })(state.leaders),
        }
      },
      () => this.setState({ showReplaceLeaderModal: false })
    )
  }

  render() {
    const committee: CommitteeDetailed | undefined =
      this.props.committees.byId[this.getCommitteeId()]

    if (committee == null) {
      return <Loading />
    }

    const canEdit =
      isAdmin(this.props.member) ||
      isCommitteeAdmin(this.props.member, committee.name)

    const leaders = this.state.leaders || []

    const adminMembers = sortBy(committee.admins, (member) => member.name)
    const admins = adminMembers.map((member) => (
      <div key={`committee-admin-${member.id}`} className="committee-member">
        {canEdit && (
          <>
            <Button
              onClick={(e) => this.props.removeAdmin(member.id, committee.id)}
              aria-label={`Remove admin role for ${c('GROUP_NAME_SINGULAR')} ${
                committee.name
              } from member ${member.name}`}
            >
              <FiMinusCircle />
            </Button>{' '}
          </>
        )}
        {member.name}
      </div>
    ))

    const viewerId = this.props.member.user.data?.id
    const activeMembers = committee.active_members
    const viewerIsMember = activeMembers.find(
      (member) => member.id === viewerId
    )
    const activeMemberList = sortBy(activeMembers, (member) => member.name).map(
      (member) => (
        <div key={`committee-active-${member.id}`} className="committee-member">
          {canEdit ? (
            <>
              <Button
                onClick={(e) =>
                  this.props.markMemberInactive(member.id, committee.id)
                }
                aria-label={`Mark member ${member.name} as in active in ${c(
                  'GROUP_NAME_SINGULAR'
                )} ${committee.name} from member `}
              >
                <FiMinusCircle />
              </Button>{' '}
            </>
          ) : null}
          {member.name}
        </div>
      )
    )

    return (
      <Container>
        <Helmet>
          <title>{committee.name}</title>
        </Helmet>
        <PageHeading level={1}>{committee.name}</PageHeading>
        {committee.email ? (
          <div style={{ marginBottom: '2rem' }}>
            Contact Leadership:{' '}
            <a href={`mailto:${committee.email}`}>{committee.email}</a>
          </div>
        ) : null}
        {/* TODO: "connect on Slack"; needs slack channel in db & api */}
        <div style={{ marginBottom: '2rem' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <h3>Elected Leadership</h3>
            {canEdit ? (
              <div>
                <Button
                  type="button"
                  onClick={() => this.setState({ showAddRoleModal: true })}
                >
                  Add Role
                </Button>{' '}
                <Button
                  type="button"
                  onClick={() =>
                    this.setState({ showReplaceLeaderModal: true })
                  }
                >
                  {/* TODO: allow nulling out */}
                  Replace Leader
                </Button>
              </div>
            ) : null}
          </div>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Name</th>
                <th>Role</th>
                <th>Term Start</th>
                <th>Term End</th>
              </tr>
            </thead>
            <tbody>
              {leaders.map((leader) => (
                <tr key={leader.id}>
                  <td
                    style={{
                      color: leader.member_name ? undefined : 'red',
                    }}
                  >
                    {leader.member_name ? leader.member_name : 'Unfilled'}
                  </td>
                  <td>{leader.role_title}</td>
                  <td>
                    {leader.term_start_date
                      ? new Date(leader.term_start_date).toLocaleDateString()
                      : 'N/A'}
                  </td>
                  <td>
                    {leader.term_end_date
                      ? new Date(leader.term_end_date).toLocaleDateString()
                      : 'N/A'}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Modal
            show={this.state.showAddRoleModal}
            centered
            size="lg"
            backdrop="static"
          >
            <AddLeadershipRoleModal
              committeeId={committee.id}
              onComplete={this.addRoleComplete}
              onCancel={() => this.setState({ showAddRoleModal: false })}
            />
          </Modal>
          <Modal
            show={this.state.showReplaceLeaderModal}
            centered
            size="lg"
            backdrop="static"
          >
            <ReplaceLeaderModal
              committeeId={committee.id}
              roles={leaders}
              members={committee.active_members}
              onComplete={this.replaceLeaderComplete}
              onCancel={() => this.setState({ showReplaceLeaderModal: false })}
            />
          </Modal>
        </div>
        <div style={{ marginBottom: '2rem' }}>
          <h3>Portal Admins</h3>
          <p>
            These individuals were appointed by {c('GROUP_NAME_SINGULAR')}{' '}
            leaders to help manage the active member list, send{' '}
            {c('GROUP_NAME_SINGULAR')} emails, create meetings, and run
            elections.
          </p>
          {admins}
          {canEdit ? (
            <div style={{ marginTop: '1rem' }}>
              <MemberSearchField
                prompt="Add member:"
                onMemberSelected={(member: EligibleMemberListEntry) => {
                  this.props.addAdmin(member.id, committee.id)
                }}
                id="member-search-add-admin"
              />
            </div>
          ) : null}
        </div>
        {/*
          TODO: email rules;
          reuse most of EmailRules page,
          update GET API to take committee_id filter
        */}
        <div style={{ marginBottom: '2rem' }}>
          <h3>Active Members</h3>
          <p>
            All dues-paying active members are eligible to vote in chapter-wide
            elections, regardless of their general meeting attendance record.
            Being active may also impact a member's ability to vote in{' '}
            {c('GROUP_NAME_SINGULAR')} elections. Each{' '}
            {c('GROUP_NAME_SINGULAR')} defines their own criteria for being
            "active", so be sure to reach out to your committee about the rules.
          </p>
          {canEdit ? null : (
            <p>
              If you've been active in this {c('GROUP_NAME_SINGULAR')} and
              aren't listed, send a request to {c('GROUP_NAME_SINGULAR')}{' '}
              leadership to have them confirm{' '}
              <Button
                type="button"
                onClick={() =>
                  this.props.requestCommitteeMembership(committee.id)
                }
                disabled={!committee?.id}
              >
                Request confirmation
              </Button>
            </p>
          )}
          {viewerIsMember || canEdit ? activeMemberList : null}
          {canEdit ? (
            <div style={{ marginTop: '2rem' }}>
              <MemberSearchField
                prompt="Add member:"
                onMemberSelected={this.onMemberSelected}
                id="member-search-add-member"
              />
            </div>
          ) : null}
        </div>
        {canEdit ? (
          <div style={{ marginBottom: '2rem' }}>
            <h3>Edit</h3>
            <FieldGroup
              componentClass="checkbox"
              formKey="inactive"
              label="Mark inactive?"
              value={this.state.inactive || false}
              onFormValueChange={(_, inactive) => {
                if (
                  inactive &&
                  !confirm(
                    'Are you sure you want to mark the committee as inactive?'
                  )
                ) {
                  return
                }
                this.editInactiveStatus(inactive)
              }}
            />
          </div>
        ) : null}
      </Container>
    )
  }

  onMemberSelected = async (member: EligibleMemberListEntry) => {
    const memberId = member.id
    const committeeId = this.getCommitteeId()

    const committee: CommitteeDetailed | undefined =
      this.props.committees.byId[committeeId]

    if (committee != null) {
      const existingMember = committee.members.find(
        (member) => member.id === memberId
      )
      if (existingMember === null) {
        await Members.addRole(memberId, 'member', committeeId)
      }
    }
    await Members.addRole(memberId, 'active', committeeId)

    this.props.fetchCommittee(committeeId)
  }

  async editInactiveStatus(inactive: boolean) {
    try {
      const committeeId = this.getCommitteeId()
      await Committees.editInactiveStatus(committeeId, inactive)
      this.setState({ inactive })
    } catch (err) {
      console.error('Failed to edit committee inactive status')
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      fetchCommittee,
      markMemberInactive,
      addAdmin,
      removeAdmin,
      requestCommitteeMembership,
    },
    dispatch
  )

type CommitteeStateProps = RootReducer
type CommitteeDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CommitteeStateProps,
  CommitteeDispatchProps,
  {},
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(Committee)
