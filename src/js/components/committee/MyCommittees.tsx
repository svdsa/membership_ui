import { capitalize, sortBy } from 'lodash/fp'
import React, { Component } from 'react'
import { Col, Container, Table } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Link, RouteComponentProps } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import { CommitteeDetailed } from 'src/js/client/CommitteeClient'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import * as actions from '../../redux/actions/committeeActions'
import {
  isAdmin,
  isCommitteeAdmin,
  isMemberLoaded,
} from '../../services/members'
import ConfirmCommittee from '../committee/ConfirmCommittee'
import Loading from '../common/Loading'
import PageHeading from '../common/PageHeading'

interface MyCommitteesParamProps {}
interface MyCommitteesRouteParamProps {}

type MyCommitteesOtherProps = RouteComponentProps<
  MyCommitteesParamProps,
  MyCommitteesRouteParamProps
>
type MyCommitteesProps = MyCommitteesStateProps &
  MyCommitteesDispatchProps &
  MyCommitteesOtherProps

class MyCommittees extends Component<MyCommitteesProps> {
  componentDidMount() {
    this.props.fetchCommittees()
  }

  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    const adminView =
      isAdmin(this.props.member) || isCommitteeAdmin(this.props.member)

    const pageTitle = `My ${capitalize(c('GROUP_NAME_PLURAL'))}`

    return (
      <Container>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <PageHeading level={1}>{pageTitle}</PageHeading>
        {this.renderActiveCommittees()}
        <h3>Other committees</h3>
        {this.renderAllCommittees()}
      </Container>
    )
  }

  getCommittees(isActive: boolean): CommitteeDetailed[] {
    const activeCommitteeIds = new Set(
      this.props.member.user.data?.roles
        .filter(
          (role) => role.role === 'active' && role.committee_name !== 'general'
        )
        .map((role) => role.committee_id) || []
    )

    const filteredCommittees: CommitteeDetailed[] = Object.values(
      this.props.committees.byId
    ).filter(
      (committee) =>
        (isActive && activeCommitteeIds.has(committee.id)) ||
        (!isActive && !activeCommitteeIds.has(committee.id))
    )
    const sortByName = sortBy<CommitteeDetailed>((committee) => committee.name)

    return sortByName(filteredCommittees)
  }

  renderActiveCommittees(): JSX.Element {
    const committees = this.getCommittees(true)
    const currentlyActive =
      committees.length === 0 ? (
        <p>You're currently not active in any {c('GROUP_NAME_PLURAL')}.</p>
      ) : (
        this.renderCommittees(committees)
      )

    return (
      <div style={{ marginBottom: '3rem' }}>
        {currentlyActive}
        <p>
          If you've been active in a {c('GROUP_NAME_SINGULAR')} but don't see it
          listed, send a request to the co-chairs to have them confirm:
        </p>
        <Container>
          <Col xs={12} sm={8} md={6} lg={4}>
            <ConfirmCommittee />
          </Col>
        </Container>
      </div>
    )
  }

  renderAllCommittees(): JSX.Element {
    const committees = this.getCommittees(false)
    return (
      <div>
        <p>
          Our chapter has numerous {c('GROUP_NAME_PLURAL')} focused on various
          types of work, both internal and external. To find out more about what
          they do,{' '}
          <a href={c('URL_CHAPTER_GROUP_INFO')}>click here for more info</a>.
        </p>
        <p>
          If you'd like to get in touch with the leadership for a{' '}
          {c('GROUP_NAME_SINGULAR')}, use the list below:
        </p>
        {this.renderCommittees(committees)}
      </div>
    )
  }

  renderCommittees(committees: CommitteeDetailed[]) {
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>{capitalize(c('GROUP_NAME_SINGULAR'))}</th>
            <th>Leadership Email</th>
          </tr>
        </thead>
        <tbody>
          {committees.map((committee) => (
            <tr key={committee.id}>
              <td>
                <Link to={`/committees/${committee.id}`}>{committee.name}</Link>
              </td>
              <td>
                <a href={`mailto:${committee.email}`}>{committee.email}</a>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(actions, dispatch)

type MyCommitteesStateProps = RootReducer
type MyCommitteesDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MyCommitteesStateProps,
  MyCommitteesDispatchProps,
  {},
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(MyCommittees)
