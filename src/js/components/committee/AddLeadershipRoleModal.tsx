import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { useMutation, useQuery } from 'react-query'
import CommitteeLeadership, {
  AddLeadershipRoleArgs,
  CommitteeLeader,
} from 'src/js/client/CommitteeLeadersClient'
import FieldGroup from '../common/FieldGroup'

interface AddLeadershipRoleProps {
  committeeId: number
  onComplete: (CommitteeLeader) => void
  onCancel: () => void
}

const AddLeadershipRole: React.FC<AddLeadershipRoleProps> = ({
  committeeId,
  onComplete,
  onCancel,
}) => {
  const { mutate: submitInner } = useMutation<
    CommitteeLeader,
    Error | null,
    AddLeadershipRoleArgs
  >((args) => CommitteeLeadership.addLeadershipRole(args))
  const roleTitlesQuery = useQuery('roleTitles', () =>
    CommitteeLeadership.listRoleTitles()
  )
  const roleTitleOptions = [...(roleTitlesQuery.data || []), 'Add Custom Role']
  const [selectedRoleTitle, setSelectedRoleTitleInner] = useState('')
  const [showCustomRole, setShowCustomRole] = useState(false)
  const setSelectedRoleTitle = (roleTitle: string) => {
    setShowCustomRole(roleTitle === 'Add Custom Role')
    setSelectedRoleTitleInner(roleTitle)
  }
  const [customRoleTitle, setCustomRoleTitle] = useState('')
  const [termLimitMonths, setTermLimitMonths] = useState(0)
  const submitEnabled =
    selectedRoleTitle && (!showCustomRole || customRoleTitle)
  const submit = async () => {
    const result = submitInner({
      committeeId,
      roleTitle: showCustomRole ? customRoleTitle : selectedRoleTitle,
      termLimitMonths,
    })
    onComplete(result)
  }

  return (
    <Modal.Body>
      <p>Add a new leadership&nbsp;role</p>
      <Form onSubmit={(e) => e.preventDefault()}>
        <FieldGroup
          formKey="role_title"
          label="Role Title"
          componentClass="select"
          options={roleTitleOptions}
          placeholder="Select a role title"
          value={selectedRoleTitle}
          onFormValueChange={(k, v) => setSelectedRoleTitle(v)}
          required
        />
        {showCustomRole ? (
          <FieldGroup
            formKey="custom_role_title"
            componentClass="input"
            type="text"
            label="Custom Role Title"
            value={customRoleTitle}
            onFormValueChange={(k, v) => setCustomRoleTitle(v)}
            required
          />
        ) : null}
        <FieldGroup
          formKey="term_limit_months"
          componentClass="input"
          type="number"
          step={1}
          min={0}
          label="Term Limit in Months (optional)"
          value={termLimitMonths}
          onFormValueChange={(k, v) => setTermLimitMonths(v)}
          required
        />
        <div>
          <Button type="button" onClick={submit} disabled={!submitEnabled}>
            Submit
          </Button>{' '}
          <Button type="button" onClick={onCancel}>
            Cancel
          </Button>
        </div>
      </Form>
    </Modal.Body>
  )
}

export default AddLeadershipRole
