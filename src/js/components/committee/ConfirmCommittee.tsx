import { capitalize, filter, flow, sortBy } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, Form } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { mapCommitteesToName } from 'src/js/components/committee/utils'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import * as actions from '../../redux/actions/committeeActions'
import FieldGroup from '../common/FieldGroup'

type ConfirmCommitteeProps = ConfirmCommitteeStateProps &
  ConfirmCommitteeDispatchProps

interface ConfirmCommitteeState {
  committeeId: number | null
}

class ConfirmCommittee extends Component<
  ConfirmCommitteeProps,
  ConfirmCommitteeState
> {
  constructor(props) {
    super(props)
    this.state = { committeeId: null }
  }

  componentDidMount() {
    this.props.fetchCommittees()
  }

  updateCommitteeId = (formKey: 'committeeId', value: string) => {
    const parsed = parseInt(value)
    if (parsed < 0) {
      this.setState({ committeeId: null })
    } else {
      this.setState({ committeeId: parsed })
    }
  }

  render() {
    const memberCommittees = this.props.member.user.data?.roles
      .filter((role) => role.role === 'active')
      .map((role) => role.committee_name)
      .filter((committee) => committee !== 'general')

    const filterToUnjoinedCommitteeNames = filter<string>(
      (c) => !memberCommittees?.includes(c)
    )

    // TODO better types (check if lodash types have improved since TypeScript 4)
    const committeeIdsByName: string[] = flow(
      mapCommitteesToName,
      filterToUnjoinedCommitteeNames,
      sortBy
    )(this.props.committees.byId)

    return (
      <Form>
        <FieldGroup
          formKey="committeeId"
          componentClass="select"
          options={committeeIdsByName}
          label={capitalize(c('GROUP_NAME_SINGULAR'))}
          onFormValueChange={this.updateCommitteeId}
          value={this.state.committeeId}
          placeholder={`Select a ${c('GROUP_NAME_SINGULAR')}`}
        />
        <Form.Row>
          <Button
            type="button"
            onClick={() =>
              this.state.committeeId != null
                ? this.props.requestCommitteeMembership(this.state.committeeId)
                : null
            }
            disabled={this.state.committeeId == null}
          >
            Request confirmation
          </Button>
        </Form.Row>
      </Form>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(actions, dispatch)

type ConfirmCommitteeStateProps = RootReducer
type ConfirmCommitteeDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  ConfirmCommitteeStateProps,
  ConfirmCommitteeDispatchProps,
  {},
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(ConfirmCommittee)
