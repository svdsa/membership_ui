import { mapValues } from 'lodash/fp'
import { Committee } from 'src/js/client/CommitteeClient'

export const mapCommitteesToName = mapValues<Committee, string>((c) => c.name)
