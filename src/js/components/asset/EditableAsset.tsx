import { find } from 'lodash'
import { isFunction } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, ToggleButton, ToggleButtonGroup } from 'react-bootstrap'
import { connect } from 'react-redux'
import Assets, {
  RawAsset,
  ResolvedAsset,
  ResolvedAssetContentType,
} from '../../client/AssetClient'
import { EXTERNAL_URL, PRESIGNED_URL } from '../../models/assets'
import { deleteAsset } from '../../redux/actions/assetActions'
import { RootReducer } from '../../redux/reducers/rootReducer'
import {
  contentTypeForFile,
  contentTypeForUrl,
  sanitizeFileName,
} from '../../util/assetUtils'
import { Modify } from '../../util/typeUtils'

type EmptyResolvedAsset = Modify<ResolvedAsset, { id: number | null }>
const EMPTY_ASSET: EmptyResolvedAsset = {
  id: null,
  title: '',
  asset_type: null,
  content_type: '',
  labels: [],
  relative_path: '',
  last_updated: '',
  view_url: '',
}

export type AssetForm = Modify<
  RawAsset,
  {
    id: number | null
    content_type: ResolvedAssetContentType | null
    last_updated: string | null
  }
>

const EMPTY_FORM: AssetForm = {
  id: null,
  title: '',
  last_updated: null,
  asset_type: null,
  content_type: null,
  labels: [],
  relative_path: '',
}

const RESET_STATE: Partial<EditableAssetState> = {
  asset: EMPTY_ASSET,
  form: EMPTY_FORM,
  existingAssetUrl: '',
  newAssetPath: '',
  newAssetFile: null,
  newTitle: '',
  publicAssetUrl: '',
}

type ModeType =
  | 'attach_public_asset'
  | 'attach_protected_asset'
  | 'upload_protected_asset'

const Mode = Object.freeze<{ [key: string]: ModeType }>({
  ATTACH_PUBLIC_ASSET: 'attach_public_asset',
  ATTACH_PROTECTED_ASSET: 'attach_protected_asset',
  UPLOAD_PROTECTED_ASSET: 'upload_protected_asset',
})

interface EditableAssetOwnProps {
  asset?: ResolvedAsset
  prefix: string
  labels: string[]
  onSave?: OnSaveHandler
  onDelete?: OnDeleteHandler
  onSaveExistingProtectedAsset?: OnSaveHandler
  onSavePublicAsset?: OnSaveHandler
  onUploadAndSaveProtectedAsset?: OnSaveHandler
  defaultMode?: ModeType
  alt?: string
  calcDefaultPath?(f: File): string
  afterSave?: (form: AssetForm, file?: File) => void
  afterDelete?: () => void
  src?: string
  editable?: boolean
  form?: AssetForm
}

type EditableAssetStateProps = RootReducer

type EditableAssetProps = EditableAssetOwnProps & EditableAssetStateProps

export type OnSaveHandler = (
  form: AssetForm,
  file?: File
) => Promise<ResolvedAsset>
export type OnDeleteHandler = (asset: ResolvedAsset) => void
interface EditableAssetState {
  asset: EmptyResolvedAsset | ResolvedAsset
  editing: boolean
  editable: boolean
  emptyForm: AssetForm
  emptyState: Partial<EditableAssetState>
  form: AssetForm
  labels: string[]
  mode: ModeType | null
  prefix: string
  afterSave?: (form: AssetForm, file?: File) => void
  afterDelete?: () => void
  onDelete?: OnDeleteHandler
  onSave?: OnSaveHandler
  onSaveExistingProtectedAsset?: OnSaveHandler
  onSavePublicAsset?: OnSaveHandler
  onUploadAndSaveProtectedAsset?: OnSaveHandler
  hasEditFunction?: boolean
  hasSaveFunction?: boolean
  existingAssetPath: string
  existingAssetUrl: string
  publicAssetUrl: string
  newAssetPath: string
  newAssetFile: File | null
  newTitle: string
}

class EditableAsset extends Component<EditableAssetProps, EditableAssetState> {
  constructor(props) {
    super(props)

    const labels = props.labels || []
    const emptyForm = { ...EMPTY_FORM, labels: [] }
    this.state = {
      asset: EMPTY_ASSET,
      form: EMPTY_FORM,
      editable: false,
      editing: false,
      mode: null,
      emptyForm,
      emptyState: {},
      labels,
      prefix: props.prefix || '',

      // TODO: Move these to props?
      existingAssetPath: '',
      existingAssetUrl: '',
      publicAssetUrl: '',
      newAssetPath: '',
      newAssetFile: null,
      newTitle: '',
    }
  }

  static defaultCalcDefaultPath = (f: File) => sanitizeFileName(f.name)

  calcDefaultPath =
    this.props.calcDefaultPath ?? EditableAsset.defaultCalcDefaultPath

  static getDerivedStateFromProps(
    nextProps: EditableAssetProps,
    nextState: EditableAssetState
  ) {
    // TODO: Add default save method for updating assets?
    const onSave = isFunction(nextProps.onSave) ? nextProps.onSave : undefined
    const onDelete = isFunction(nextProps.onDelete)
      ? nextProps.onDelete
      : (asset: ResolvedAsset) => deleteAsset({ id: asset.id })

    const onSavePublicAsset = isFunction(nextProps.onSavePublicAsset)
      ? nextProps.onSavePublicAsset
      : onSave
    const onSaveExistingProtectedAsset = isFunction(
      nextProps.onSaveExistingProtectedAsset
    )
      ? nextProps.onSaveExistingProtectedAsset
      : onSave
    const onUploadAndSaveProtectedAsset = isFunction(
      nextProps.onUploadAndSaveProtectedAsset
    )
      ? nextProps.onUploadAndSaveProtectedAsset
      : onSave

    const afterSave = nextProps.afterSave ?? undefined
    const afterDelete = nextProps.afterDelete ?? undefined

    let asset =
      (nextProps.asset as EmptyResolvedAsset) || nextState.asset || EMPTY_ASSET
    if (nextProps.src) {
      asset = {
        ...asset,
        view_url: nextProps.src,
      }
    }
    const assetId = asset.id
    const hasSaveFunction = [
      onSavePublicAsset,
      onSaveExistingProtectedAsset,
      onUploadAndSaveProtectedAsset,
      onSave,
    ].some(isFunction)

    const hasEditFunction = hasSaveFunction || !!onDelete

    const editingDisabled = nextProps.editable === false
    if (editingDisabled && !assetId) {
      throw new Error(
        'Cannot create EditableAsset with editable=false unless an existing asset is given'
      )
    }

    // Set next state
    const editable = !editingDisabled && hasEditFunction
    const editing = nextState.editing || (editable && !asset.id)
    const mode = nextState.mode || EditableAsset.defaultMode(nextProps)

    // Set defaults
    const labels = nextProps.labels || nextState.labels || []
    const emptyForm: AssetForm = {
      ...EMPTY_FORM,
      labels,
    }
    const form = nextProps.form || nextState.form || emptyForm
    const prefix = nextProps.prefix || nextState.prefix || ''

    const finalState: EditableAssetState = {
      ...nextState,
      asset,
      editing,
      editable,
      emptyForm,
      emptyState: EditableAsset.getEmptyState(nextProps, nextState),
      form,
      labels,
      mode,
      prefix,
      afterSave,
      afterDelete,
      onDelete,
      onSave,
      onSaveExistingProtectedAsset,
      onSavePublicAsset,
      onUploadAndSaveProtectedAsset,
      hasEditFunction,
      hasSaveFunction,
    }

    return finalState
  }

  static getEmptyState = (
    props: EditableAssetProps,
    state: EditableAssetState
  ): Partial<EditableAssetState> => {
    return {
      ...RESET_STATE,
      form: EditableAsset.emptyForm(state),
      mode: EditableAsset.defaultMode(props),
    }
  }

  static emptyForm = (state: EditableAssetState): AssetForm => {
    const labels = state != null ? state.labels : []
    return {
      ...EMPTY_FORM,
      labels,
    }
  }

  static defaultMode = (props: EditableAssetProps) => {
    return props.defaultMode || Mode.ATTACH_PUBLIC_ASSET
  }

  resetState = (
    merge: Partial<EditableAssetState> = { asset: EMPTY_ASSET }
  ) => {
    const { editable, emptyState } = this.state
    const finalState: Partial<EditableAssetState> = {
      editing: editable && !merge.asset!.id,
      ...emptyState,
      ...merge,
    }
    this.setState({ ...this.state, ...finalState })
  }

  defaultAssetPathForFile = (file) => {
    const defaultPath = file && this.calcDefaultPath(file)
    return defaultPath || ''
  }

  newAssetPathOrDefault = () => {
    return (
      this.state.newAssetPath ||
      this.defaultAssetPathForFile(this.state.newAssetFile)
    )
  }

  /*
   * START: Common Asset Fields
   */

  updateAssetTitle: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    const newTitle = e.target.value
    this.setState({ newTitle })
  }

  /*
   * START: New Protected Assets
   */

  updateNewProtectedAssetPath: React.ChangeEventHandler<HTMLInputElement> = (
    e
  ) => {
    const newAssetPath = e.target.value
    this.setState({ newAssetPath })
  }

  updateNewProtectedAssetFile: React.ChangeEventHandler<HTMLInputElement> = (
    e
  ) => {
    const newAssetFile = e.target.files && e.target.files[0]
    if (!newAssetFile) {
      return
    }
    const contentType = contentTypeForFile(newAssetFile)
    this.setState((state) => ({
      newAssetFile,
      form: {
        ...state.form,
        content_type: contentType,
      },
    }))
  }

  validateNewProtectedAssetPath = (path: string) => {
    const relativePath = this.expandProtectedAssetPath(path)
    const matchingAsset = find(
      this.props.assets.byId,
      (a) => a.relative_path === relativePath
    )
    return !matchingAsset
  }

  createNewProtectedAsset = async () => {
    const { form, newAssetFile = null, newTitle } = this.state
    const contentType =
      newAssetFile != null ? contentTypeForFile(newAssetFile) : null
    const relativePath = this.expandProtectedAssetPath(
      this.newAssetPathOrDefault()
    )
    const body: AssetForm = {
      ...form,
      asset_type: PRESIGNED_URL,
      relative_path: relativePath,
      content_type: contentType,
      title: newTitle,
    }

    if (
      this.state.onUploadAndSaveProtectedAsset != null &&
      newAssetFile != null
    ) {
      await this.state.onUploadAndSaveProtectedAsset(body, newAssetFile)
      this.resetState({
        asset: EMPTY_ASSET,
      })

      if (this.state.afterSave != null) {
        this.state.afterSave(body)
      }
    }
  }

  renderNewProtectedAssetUploadBox = () => {
    return (
      this.state.onUploadAndSaveProtectedAsset && (
        <div className="asset-editor asset-new-protected-upload-box">
          <label>
            Upload Content:{' '}
            <input type="file" onChange={this.updateNewProtectedAssetFile} />
          </label>
        </div>
      )
    )
  }

  renderNewProtectedAssetPathEditor = () => {
    const { newAssetPath, newAssetFile, newTitle } = this.state
    const pathOrDefault = this.newAssetPathOrDefault()
    const isDuplicate = !this.validateNewProtectedAssetPath(pathOrDefault)
    return (
      <div className="asset-editor asset-new-protected-path-editor">
        <p>
          <label>
            Path:{' '}
            <input
              type="text"
              placeholder={this.defaultAssetPathForFile(newAssetFile)}
              value={newAssetPath}
              onChange={this.updateNewProtectedAssetPath}
            />
          </label>
        </p>
        <p>{this.expandProtectedAssetPath(pathOrDefault)}</p>
        <p>
          <label>
            Title:{' '}
            <input
              type="text"
              value={newTitle}
              onChange={this.updateNewProtectedAssetPath}
            />
          </label>
        </p>
        <p>
          <Button disabled={isDuplicate} onClick={this.createNewProtectedAsset}>
            Attach
          </Button>
          <Button onClick={this.cancelEdit}>Cancel</Button>
        </p>
        {isDuplicate && <div>(path already taken)</div>}
      </div>
    )
  }

  /*
   * Start: Existing Protected Assets
   */

  expandProtectedAssetPath = (path) => {
    return `${this.state.prefix}${path}`
  }

  updateExistingProtectedAssetPath = (e) => {
    const existingAssetPath = e.target.value
    this.setState({
      existingAssetPath,
    })
  }

  previewExistingProtectedAsset = async () => {
    const { existingAssetPath, form } = this.state
    const fullExistingAssetPath =
      this.expandProtectedAssetPath(existingAssetPath)

    const url = await Assets.getViewUrl(fullExistingAssetPath)
    if (url) {
      const contentType = contentTypeForUrl(url)
      const updatedForm: AssetForm = {
        ...form,
        content_type: contentType,
        asset_type: PRESIGNED_URL,
      }
      this.setState({
        form: updatedForm,
        existingAssetUrl: url,
      })
    }
  }

  attachExistingProtectedAsset = async () => {
    const { existingAssetPath, existingAssetUrl, form, newTitle } = this.state
    const relativePath = this.expandProtectedAssetPath(existingAssetPath)
    const contentType = contentTypeForUrl(existingAssetUrl)
    const body: AssetForm = {
      ...form,
      asset_type: PRESIGNED_URL,
      content_type: contentType,
      relative_path: relativePath,
      title: newTitle,
    }

    if (this.state.onSaveExistingProtectedAsset != null) {
      await this.state.onSaveExistingProtectedAsset(body)
      this.resetState({
        asset: EMPTY_ASSET,
      })

      if (this.state.afterSave != null) {
        this.state.afterSave(body)
      }
    } else {
      throw new Error('No existing protected asset save handler defined')
    }
  }

  renderExistingProtectedAssetPathEditor = () => {
    const { existingAssetPath, existingAssetUrl, newTitle } = this.state
    const fullExistingAssetPath = this.expandProtectedAssetPath(
      existingAssetPath || ''
    )
    const actionButton = existingAssetUrl ? (
      <Button onClick={this.attachExistingProtectedAsset}>Attach</Button>
    ) : (
      <Button onClick={this.previewExistingProtectedAsset}>Preview</Button>
    )
    return (
      <div className="asset-editor asset-existing-path-editor">
        <p>
          <label>
            Path: {`/${this.state.prefix}`}
            <input
              type="text"
              placeholder={'Relative path in storage'}
              value={existingAssetPath}
              onChange={this.updateExistingProtectedAssetPath}
            />
          </label>
        </p>
        <p>
          <label>
            Title:{' '}
            <input
              type="text"
              placeholder={'Title of content'}
              value={newTitle}
              onChange={this.updateAssetTitle}
            />
          </label>
        </p>
        <p>
          {actionButton}
          {this.showCancelButton() && (
            <Button onClick={this.cancelEdit}>Cancel</Button>
          )}
        </p>
      </div>
    )
  }

  /*
   * START: Public Assets
   */

  updatePublicAssetUrl = (e) => {
    const publicAssetUrl = e.target.value
    this.setState({ publicAssetUrl })
  }

  attachPublicAsset = async () => {
    const { publicAssetUrl, form, newTitle } = this.state
    const contentType = contentTypeForUrl(publicAssetUrl)
    const body: AssetForm = {
      ...form,
      asset_type: EXTERNAL_URL,
      external_url: publicAssetUrl,
      content_type: contentType,
      title: newTitle,
    }

    if (this.state.onSavePublicAsset != null) {
      await this.state.onSavePublicAsset(body)
      this.resetState({
        asset: EMPTY_ASSET,
      })

      if (this.state.afterSave != null) {
        this.state.afterSave(body)
      }
    } else {
      throw new Error('No public asset save handler defined')
    }
  }

  renderPublicAssetUrlEditor = () => {
    const { publicAssetUrl, newTitle } = this.state
    return (
      <div className="asset-editor asset-public-url-editor">
        <p>
          <label>
            URL:{' '}
            <input
              type="text"
              placeholder="Full url to asset"
              value={publicAssetUrl}
              onChange={this.updatePublicAssetUrl}
            />
          </label>
        </p>
        <p>
          <label>
            Title:{' '}
            <input
              type="text"
              placeholder={'Title of content'}
              value={newTitle}
              onChange={this.updateAssetTitle}
            />
          </label>
        </p>
        <p>
          <Button onClick={this.attachPublicAsset}>Attach</Button>
          {this.showCancelButton() && (
            <Button onClick={this.cancelEdit}>Cancel</Button>
          )}
        </p>
      </div>
    )
  }

  /*
   * END
   */

  cancelEdit = () => {
    this.resetState({ asset: this.state.asset, editing: false })
  }

  delete = () => {
    if (this.showDeleteButton()) {
      const { asset } = this.state
      if (
        asset.id != null && // should always be null, but make sure
        confirm(`Are you sure you would like to delete asset #${asset.id}`) &&
        this.state.onDelete != null
      ) {
        this.state.onDelete(asset as ResolvedAsset)

        if (this.state.afterDelete != null) {
          this.state.afterDelete()
        }
      }
    }
  }

  openEditForm = () => {
    this.setState({ editing: true })
  }

  showUploadSourcePicker = () => {
    const { editable, editing, hasSaveFunction } = this.state
    return editable && editing && hasSaveFunction
  }

  showEditButton = () => {
    const { editable, editing } = this.state
    return editable && !editing
  }

  showCancelButton = () => {
    const { asset, editable, editing } = this.state
    return editable && editing && !!asset.id
  }

  showDeleteButton = () => {
    const { asset, editable, editing } = this.state
    return editable && editing && this.state.onDelete && asset.id
  }

  renderTitle() {
    const { asset, newTitle } = this.state
    const currentTitle = asset.title
    const title = currentTitle || newTitle
    return <h4>{title || '(Untitled)'}</h4>
  }

  renderPreviewBar = () => {
    return (
      <div className="asset-preview-bar">
        {this.showEditButton() && (
          <Button onClick={this.openEditForm}>Edit</Button>
        )}
        {this.showCancelButton() && (
          <Button onClick={this.cancelEdit}>Cancel</Button>
        )}
        {this.showDeleteButton() && (
          <Button onClick={this.delete}>Delete</Button>
        )}
      </div>
    )
  }

  renderPreview = () => {
    const {
      asset,
      editing,
      form,
      publicAssetUrl,
      existingAssetUrl,
      mode,
      newAssetFile,
    } = this.state
    const currentAssetUrl = asset.view_url
    let previewUrl: string | null = null
    if (editing) {
      switch (mode) {
        case Mode.UPLOAD_PROTECTED_ASSET:
          previewUrl =
            newAssetFile != null ? URL.createObjectURL(newAssetFile) : null
          break
        case Mode.ATTACH_PUBLIC_ASSET:
          previewUrl = publicAssetUrl
          break
        case Mode.ATTACH_PROTECTED_ASSET:
          previewUrl = existingAssetUrl
          break
      }
    }
    const displayUrl = previewUrl || currentAssetUrl
    if (displayUrl) {
      const contentType =
        asset.content_type || form.content_type || contentTypeForUrl(displayUrl)
      const relativePath = asset.relative_path || ''
      let view
      switch (contentType) {
        case 'audio':
          view = <audio src={displayUrl} controls />
          break
        case 'image':
          const altTextParts = relativePath.split('/')
          const contentPath = altTextParts.slice(0, 2)
          const fileNameParts = altTextParts.slice(2)
          const altText =
            this.props.alt ||
            [...contentPath, contentType, ...fileNameParts].join(' ')
          view = (
            <img src={displayUrl} alt={altText} style={{ maxWidth: '400px' }} />
          )
          break
        case 'video':
          view = <video controls src={displayUrl} />
          break
        default:
          view = <p>Unrecognized Content</p>
      }
      return <div className="asset-preview-content">{view}</div>
    }
  }

  renderUploadSourcePicker = () => {
    if (!this.showUploadSourcePicker()) {
      return null
    }
    const { mode } = this.state
    return (
      <div style={{ marginBottom: '1.5rem' }}>
        <ToggleButtonGroup
          name="upload-source"
          onChange={(value: ModeType) => {
            this.setState({ editing: true, mode: value })
          }}
          type="radio"
          className="upload-source-picker"
          value={this.state.mode ?? undefined}
        >
          {this.state.onUploadAndSaveProtectedAsset && (
            <ToggleButton value={Mode.UPLOAD_PROTECTED_ASSET}>
              Upload New
            </ToggleButton>
          )}
          {this.state.onSaveExistingProtectedAsset && (
            <ToggleButton value={Mode.ATTACH_PROTECTED_ASSET}>
              Attach Existing
            </ToggleButton>
          )}
          {this.state.onSavePublicAsset && (
            <ToggleButton value={Mode.ATTACH_PUBLIC_ASSET}>
              Attach Public URL
            </ToggleButton>
          )}
        </ToggleButtonGroup>
      </div>
    )
  }

  renderEditForm = () => {
    const { newAssetFile, mode } = this.state
    if (this.showUploadSourcePicker()) {
      switch (mode) {
        case Mode.ATTACH_PUBLIC_ASSET:
          return this.renderPublicAssetUrlEditor()
        case Mode.ATTACH_PROTECTED_ASSET:
          return this.renderExistingProtectedAssetPathEditor()
        case Mode.UPLOAD_PROTECTED_ASSET:
          return newAssetFile
            ? this.renderNewProtectedAssetPathEditor()
            : this.renderNewProtectedAssetUploadBox()
      }
    }
  }

  render() {
    return (
      <div>
        {this.renderUploadSourcePicker()}
        {this.renderEditForm()}
        {this.showUploadSourcePicker() ? <div>Preview:</div> : null}
        {this.renderTitle()}
        {this.renderPreviewBar()}
        {this.renderPreview()}
      </div>
    )
  }
}

export default connect<
  EditableAssetStateProps,
  null,
  EditableAssetOwnProps,
  RootReducer
>((state) => state)(EditableAsset)
