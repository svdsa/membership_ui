import classNames from 'classnames'
import { get as _get } from 'lodash'
import React, { Component } from 'react'
import NotificationSystem, { System } from 'react-notification-system'
import { AppFooter } from 'src/js/components/common/AppFooter'
import { ErrorBoundary } from 'src/js/components/common/ErrorBoundary'
import { MAIN_HTML_ID } from '../config'
import { isFullscreen } from '../util/isFullscreen'
import { serviceLocator } from '../util/serviceLocator'
import SkipLink from './common/SkipLink'
import Navigation from './navigation/Navigation'

export default class App extends Component {
  notificationSystemRef = React.createRef<System>()

  componentDidMount() {
    serviceLocator.notificationSystem = this.notificationSystemRef
  }

  render() {
    let navContainer: JSX.Element | null = null
    const pathname = _get(this.props, ['location', 'pathname'], '')

    if (!pathname || !isFullscreen(pathname)) {
      navContainer = (
        <div className="nav-container">
          <Navigation />
        </div>
      )
    }
    return (
      <div
        className={classNames('app-container', {
          fullscreen: isFullscreen(pathname),
        })}
      >
        <SkipLink targetId={MAIN_HTML_ID}>Skip to main content</SkipLink>
        {navContainer}
        <div id={MAIN_HTML_ID} className="content-container" tabIndex={-1}>
          <ErrorBoundary>{this.props.children}</ErrorBoundary>
        </div>

        <NotificationSystem ref={this.notificationSystemRef} />
        <footer className="px-3 pt-3 bg-light">
          <AppFooter />
        </footer>
      </div>
    )
  }
}
