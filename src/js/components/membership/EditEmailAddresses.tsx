import cx from 'classnames'
import React, { useMemo, useState } from 'react'
import {
  Badge,
  Button,
  Col,
  ListGroup,
  ListGroupItem,
  OverlayTrigger,
  Row,
  Tooltip,
} from 'react-bootstrap'
import { FiPlus, FiTrash } from 'react-icons/fi'
import { MemberEmailAddress } from 'src/js/client/MemberClient'
import FieldGroup, { VALID_EMAIL } from 'src/js/components/common/FieldGroup'
import { ProvisionalMemberEmailAddress } from 'src/js/components/membership/EditMemberModal'

const BlankEmailAddress: Partial<MemberEmailAddress> = {
  email_address: '',
  name: '',
  preferred: false,
}

interface AddlEmailAddressFormProps {
  existingAddresses: MemberEmailAddress[] | undefined
  changes: ProvisionalMemberEmailAddress[]
  onAddEmail(newAddress: Partial<MemberEmailAddress>): void
  onDeleteEmail(email: string): void
  onUndoChange(email: string): void
}

export const AdditionalEmailAddressForm: React.FC<AddlEmailAddressFormProps> =
  ({ existingAddresses, changes, onAddEmail, onDeleteEmail, onUndoChange }) => {
    const [newEmail, setNewEmail] =
      useState<Partial<MemberEmailAddress>>(BlankEmailAddress)

    const handleChangeEmail = <T extends keyof MemberEmailAddress>(
      formKey: T,
      value: MemberEmailAddress[T]
    ) => {
      setNewEmail({ ...newEmail, [formKey]: value })
    }

    const handleAddEmail: React.MouseEventHandler<HTMLButtonElement> = (e) => {
      onAddEmail(newEmail)
      setNewEmail(BlankEmailAddress)
    }

    const adds: ProvisionalMemberEmailAddress[] = useMemo(
      () => changes.filter((change) => change.action === 'add'),
      [changes]
    )

    const deletes: ProvisionalMemberEmailAddress[] = useMemo(
      () => changes.filter((change) => change.action === 'delete'),
      [changes]
    )

    const existing: MemberEmailAddress[] = useMemo(
      () => existingAddresses ?? [],
      [existingAddresses]
    )

    const newEmailValidationError: string | null = useMemo(() => {
      const email = newEmail.email_address
      const addSet = new Set(
        adds.map((change) => change.emailAddress.email_address)
      )
      const deleteSet = new Set(
        deletes.map((change) => change.emailAddress.email_address)
      )
      const existingSet = new Set(existing.map((addr) => addr.email_address))

      if (email == null || email.length === 0) {
        return "Email can't be blank"
      }

      if (!VALID_EMAIL.test(email)) {
        return "Email doesn't look like a valid address"
      }

      if (deleteSet.has(email)) {
        return "Can't add an email marked for deletion. Undo the deletion instead"
      }

      if (addSet.has(email) || existingSet.has(email)) {
        return "Can't add the same email more than once"
      }

      return null
    }, [newEmail, adds])

    const combinedAddresses: ProvisionalMemberEmailAddress[] = useMemo(() => {
      const deleteSet = new Set(
        deletes.map((change) => change.emailAddress.email_address)
      )
      const existingWithDeletes: ProvisionalMemberEmailAddress[] = existing.map(
        (address) => ({
          emailAddress: address,
          action: deleteSet.has(address.email_address)
            ? ('delete' as const)
            : undefined,
        })
      )
      return [...existingWithDeletes, ...adds]
    }, [existingAddresses, changes])

    return (
      <section className="emailaddr-container">
        {combinedAddresses.length === 0 && (
          <div className="emailaddr-empty">
            <p>No additional email addresses</p>
          </div>
        )}
        {combinedAddresses.length > 0 && (
          <header>Additional email addresses</header>
        )}
        <ListGroup className="emailaddr-list">
          {combinedAddresses.map(({ emailAddress: address, action }) => (
            <ListGroupItem
              key={address.email_address}
              className={cx(
                'emailaddr-entry',
                'modifylist-entry',
                action != null
                  ? `emailaddr-action-${action} modifylist-action-${action}`
                  : null
              )}
            >
              <Row>
                <Col md={5}>
                  <span className="emailaddr-address">
                    {address.email_address}
                  </span>
                </Col>
                <Col md={5}>
                  <span className="emailaddr-name">{address.name}</span>
                  <span className="emailaddr-badges">
                    {address.preferred ? (
                      <Badge variant="primary">Preferred</Badge>
                    ) : null}
                    {address.verified ? (
                      <Badge variant="success">Verified</Badge>
                    ) : null}
                  </span>
                </Col>
                <Col md={2}>
                  {action == null && (
                    <Button
                      onClick={() => onDeleteEmail(address.email_address!)}
                      disabled={!address.email_address}
                      className="delete-email-button"
                      block
                    >
                      <FiTrash />
                    </Button>
                  )}
                  {action != null && (
                    <Button
                      onClick={() => onUndoChange(address.email_address!)}
                      block
                    >
                      Undo
                    </Button>
                  )}
                </Col>
              </Row>
            </ListGroupItem>
          ))}
          <AdditionalEmailAddressComposer
            value={newEmail}
            disableSave={newEmailValidationError != null}
            validationError={newEmailValidationError}
            onChange={handleChangeEmail}
            onSave={handleAddEmail}
          />
        </ListGroup>
      </section>
    )
  }

interface AddlEmailAddressComposerProps {
  value: Partial<MemberEmailAddress>
  disableSave: boolean
  validationError: string | null
  onChange<T extends keyof MemberEmailAddress>(
    formKey: T,
    value: MemberEmailAddress[T]
  ): void
  onSave: React.MouseEventHandler<HTMLButtonElement>
}

const AdditionalEmailAddressComposer: React.FC<AddlEmailAddressComposerProps> =
  ({ value, disableSave, validationError, onChange, onSave }) => {
    return (
      <ListGroupItem>
        <Row>
          <Col md={5}>
            <FieldGroup
              layout="row"
              formKey="email_address"
              componentClass="input"
              label="Email address"
              onFormValueChange={onChange}
              type="email"
              placeholder="yourname@protonmail.com"
              value={value.email_address}
            />
          </Col>
          <Col md={5}>
            <FieldGroup
              layout="row"
              formKey="name"
              componentClass="input"
              label="Name (optional)"
              onFormValueChange={onChange}
              type="text"
              placeholder="my super secure email"
              value={value.name}
            />
          </Col>
          <OverlayTrigger
            overlay={
              value.email_address != null &&
              value.email_address.length > 0 &&
              validationError != null ? (
                <Tooltip id="new-email-validation-error">
                  {validationError}
                </Tooltip>
              ) : (
                <span />
              )
            }
            placement="bottom"
          >
            <Col
              md={2}
              className="add-email-button-container add-entry-button-container"
            >
              <Button
                onClick={onSave}
                disabled={disableSave}
                className="add-email-button"
                block
              >
                <FiPlus />
              </Button>
            </Col>
          </OverlayTrigger>
        </Row>
      </ListGroupItem>
    )
  }
