import { capitalize, values } from 'lodash/fp'
import React, { useState } from 'react'
import { Button, Col, Form, InputGroup, Modal, Row } from 'react-bootstrap'
import { Typeahead } from 'react-bootstrap-typeahead'
import { MemberPronouns } from 'src/js/client/MemberClient'
import PageHeading from 'src/js/components/common/PageHeading'
import {
  KnownPronouns,
  parsePronounsAsDeclensions,
  PronounDeclensions,
  renderDeclensions,
  uniqByPronouns,
} from 'src/js/components/membership/pronounUtils'

interface PronounSelectProps {
  pronouns: PronounDeclensions[]
  placeholder?: string
  onChange(pronouns: PronounDeclensions[]): void
}

export const PronounSelect: React.FC<PronounSelectProps> = ({
  pronouns,
  placeholder = 'Select pronouns',
  onChange,
}) => {
  const [input, setInput] = useState<PronounDeclensions[]>(pronouns)
  const [showCustomModal, setShowCustomModal] = useState(false)

  const handleClickAddCustomPronoun = () => setShowCustomModal(true)
  const handleHideCustomModal = () => setShowCustomModal(false)
  const handleCreateCustomPronouns = (declensions: PronounDeclensions) => {
    handleHideCustomModal()
    setInput(uniqByPronouns([...input, declensions]))
  }
  const handleChangePronouns = (pronouns: MemberPronouns[]) => {
    const parsedDeclensions = parsePronounsAsDeclensions(pronouns)
    setInput(parsedDeclensions)
    onChange(parsedDeclensions)
  }

  return (
    <>
      <Form.Group>
        <Typeahead
          id="pronouns-select"
          multiple={true}
          placeholder={placeholder}
          options={KnownPronouns}
          selected={input.map(renderDeclensions)}
          onChange={handleChangePronouns}
        />
      </Form.Group>
      <section className="add-pronouns">
        Don't see the correct pronouns listed?{' '}
        <a onClick={handleClickAddCustomPronoun} href="#">
          Add pronouns...
        </a>
      </section>
      <AddCustomPronounsModal
        show={showCustomModal}
        onHide={handleHideCustomModal}
        onCreatePronouns={handleCreateCustomPronouns}
      />
    </>
  )
}

interface AddCustomPronounsModalProps {
  show: boolean
  declensions?: PronounDeclensions
  onHide(): void
  onCreatePronouns(declensions: PronounDeclensions): void
}

export const AddCustomPronounsModal: React.FC<AddCustomPronounsModalProps> = ({
  show,
  onHide,
  declensions,
  onCreatePronouns,
}) => {
  const [input, setInput] = useState<PronounDeclensions>(
    declensions ?? {
      subject: '',
      object: '',
      possessiveDeterminer: '',
      possessive: '',
      reflexive: '',
    }
  )

  const declensionsDefined = values(input)
  const isValid =
    declensionsDefined.length > 0 && declensionsDefined.every((d) => d != '')

  const handleCreatePronouns: React.MouseEventHandler<HTMLButtonElement> =
    () => {
      if (isValid) {
        onCreatePronouns(input)
      }
    }

  const handleChange =
    (
      key: keyof PronounDeclensions
    ): React.ChangeEventHandler<HTMLInputElement> =>
    (e) =>
      setInput({ ...input, [key]: e.target.value })

  return (
    <Modal show={show} onHide={onHide} size="lg">
      <Modal.Header>
        <Modal.Title>Add custom pronouns</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Row>
            <Col md={12}>
              <p>
                Enter your pronouns below. You can use the examples to see how
                these pronouns might be used in conversation.
              </p>
              <p>
                If you use multiple pronouns, please enter them one at a time as
                separate pronouns. Enter the first set of pronouns, hit "Save
                pronouns", then press "Add pronouns..." as many times as you
                need.
              </p>
              <InputGroup>
                <Form.Control
                  type="text"
                  placeholder="they"
                  required
                  value={input.subject}
                  onChange={handleChange('subject')}
                />
                <Form.Control
                  type="text"
                  placeholder="them"
                  required
                  value={input.object}
                  onChange={handleChange('object')}
                />
                <Form.Control
                  type="text"
                  placeholder="their"
                  required
                  value={input.possessiveDeterminer}
                  onChange={handleChange('possessiveDeterminer')}
                />
                <Form.Control
                  type="text"
                  placeholder="theirs"
                  required
                  value={input.possessive}
                  onChange={handleChange('possessive')}
                />
                <Form.Control
                  type="text"
                  placeholder="themselves"
                  required
                  value={input.reflexive}
                  onChange={handleChange('reflexive')}
                />
              </InputGroup>
            </Col>
          </Row>
        </Form>
        <PronounExamples declensions={input} />
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onHide}>
          Discard
        </Button>
        <Button
          variant="primary"
          onClick={handleCreatePronouns}
          disabled={!isValid}
        >
          Save pronouns
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

interface PronounExamplesProps {
  declensions: PronounDeclensions
}

const PronounExamples: React.FC<PronounExamplesProps> = ({ declensions }) => (
  <section className="pronoun-examples">
    <header>
      <PageHeading level={3}>Some examples</PageHeading>
    </header>
    <p className="pronoun-example">
      <strong>{capitalize(declensions.subject || 'they')}</strong> believed in
      the power of unions.
    </p>
    <p className="pronoun-example">
      I went with <strong>{declensions.object || 'them'}</strong> to canvass for
      a candidate.
    </p>
    <p className="pronoun-example">
      <strong>{capitalize(declensions.subject || 'they')}</strong> will always
      be proud of <strong>{declensions.possessiveDeterminer || 'their'}</strong>{' '}
      community.
    </p>
    <p className="pronoun-example">
      The action was <strong>{declensions.possessive || 'theirs'}</strong> to
      coordinate.
    </p>
    <p className="pronoun-example">
      <strong>{capitalize(declensions.subject || 'they')}</strong> joined DSA to
      help everyone around them, not just{' '}
      <strong>{declensions.reflexive || 'themselves'}</strong>.
    </p>
  </section>
)
