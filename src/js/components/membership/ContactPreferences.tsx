import React, { Component } from 'react'
import { Form } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import {
  UpdateMemberAttributeKeys,
  UpdateMemberAttributes,
} from 'src/js/client/MemberClient'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { updateMember } from '../../redux/actions/memberActions'

type ContactPreferencesProps = ContactPreferencesStateProps &
  ContactPreferencesDispatchProps

class ContactPreferences extends Component<ContactPreferencesProps> {
  render() {
    const memberData = this.props.member.user.data

    return (
      <div>
        <Form.Check
          checked={!memberData?.do_not_email}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            this.updateMember('do_not_email', !event.target.checked)
          }
          type="checkbox"
          label={`Receive emails from ${c('CHAPTER_NAME')}`}
        />
        <Form.Check
          checked={!memberData?.do_not_call}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            this.updateMember('do_not_call', !event.target.checked)
          }
          type="checkbox"
          label={`Receive calls or texts from ${c('CHAPTER_NAME')}`}
        />
      </div>
    )
  }

  updateMember(attribute: UpdateMemberAttributeKeys, newValue: boolean) {
    const update: Partial<UpdateMemberAttributes> = { [attribute]: newValue }
    this.props.updateMember(update)
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ updateMember }, dispatch)

type ContactPreferencesStateProps = RootReducer
type ContactPreferencesDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  ContactPreferencesStateProps,
  ContactPreferencesDispatchProps,
  {},
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(ContactPreferences)
