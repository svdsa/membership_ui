import React, { useState } from 'react'
import { Button, Modal } from 'react-bootstrap'
import {
  EligibleMemberListEntry,
  MemberDetailed,
  Members,
} from 'src/js/client/MemberClient'
import MemberSearchField from '../common/MemberSearchField'

interface MergeMemberModalProps {
  show: boolean
  member: MemberDetailed
  onHide(): void
  onSaveComplete(member: MemberDetailed): void
}

const MergeMemberModal: React.FC<MergeMemberModalProps> = ({
  show,
  member,
  onHide,
  onSaveComplete,
}) => {
  const [memberToRemove, setMemberToRemove] =
    useState<EligibleMemberListEntry | null>(null)

  const onMemberSelected = (member) => {
    setMemberToRemove(member)
  }

  const onMerge = async () => {
    if (memberToRemove !== null) {
      if (member.id == memberToRemove.id) {
        return
      }
      const result = await Members.merge(member.id, memberToRemove.id)
      if (result) {
        onSaveComplete(member)
        onHide()
      }
    }
  }

  return (
    <>
      <Modal show={show} onHide={onHide} className="info-modal" size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Merge with another member record</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Search for another member record to merge into this one. Data such
            as meeting attendance, votes, etc will be combined, and the searched
            for record will be deleted.
          </p>
          {memberToRemove !== null ? (
            <>
              {memberToRemove.id == member.id ? (
                <p>
                  This is the same member record as the one you wish to merge
                  into. Please search again.
                </p>
              ) : (
                <p>
                  Please confirm the member record you wish to merge and delete:
                </p>
              )}
              <p className="pl-5">
                <b>{memberToRemove.name}</b>
                <br />
                {memberToRemove.email}
              </p>
              <p className="pl-5">
                <Button
                  onClick={() => setMemberToRemove(null)}
                  size="sm"
                  variant="outline-secondary"
                >
                  Search again?
                </Button>
              </p>
            </>
          ) : (
            <MemberSearchField onMemberSelected={onMemberSelected} />
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={onMerge}
            disabled={
              memberToRemove === null ||
              (memberToRemove !== null && memberToRemove.id === member.id)
            }
          >
            Merge
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default MergeMemberModal
