import { capitalize } from 'lodash/fp'
import pluralize from 'pluralize'
import React, { useState } from 'react'
import {
  Button,
  Col,
  Form,
  ListGroup,
  ListGroupItem,
  Modal,
  Nav,
  Row,
  Tab,
  ToggleButton,
  ToggleButtonGroup,
} from 'react-bootstrap'
import { FiChevronDown, FiChevronUp } from 'react-icons/fi'
import { useMutation } from 'react-query'
import AutoTextarea from 'react-textarea-autosize'
import {
  AdminUpdateMemberAttributes,
  MemberDetailed,
  MemberEmailAddress,
  MemberPhoneNumber,
  Members,
} from 'src/js/client/MemberClient'
import FieldGroup from 'src/js/components/common/FieldGroup'
import PageHeading from 'src/js/components/common/PageHeading'
import { AdditionalEmailAddressForm } from 'src/js/components/membership/EditEmailAddresses'
import { AdditionalPhoneNumberForm } from 'src/js/components/membership/EditPhoneNumbers'
import { PronounSelect } from 'src/js/components/membership/EditPronouns'
import {
  bundlePronouns,
  parseBundlesToDeclensions,
  PronounDeclensions,
  renderDeclensions,
} from 'src/js/components/membership/pronounUtils'
import { c } from 'src/js/config'
import { showNotification } from 'src/js/util/util'

export interface ProvisionalMemberEmailAddress {
  emailAddress: Partial<MemberEmailAddress>
  action?: 'add' | 'delete'
}

export interface ProvisionalMemberPhoneNumber {
  phoneNumber: Partial<MemberPhoneNumber>
  action?: 'add' | 'delete'
}

interface UpdateMemberMutationProps {
  memberId: number
  attributes: Partial<AdminUpdateMemberAttributes>
}

const updateMember = async ({
  memberId,
  attributes,
}: UpdateMemberMutationProps) => {
  return Members.updateAdmin(memberId, attributes)
}

interface UpdateEmailMutationProps {
  memberId: number
  emailAddress: ProvisionalMemberEmailAddress
}

const updateEmail = async ({
  memberId,
  emailAddress: { action, emailAddress },
}: UpdateEmailMutationProps) => {
  switch (action) {
    case 'add': {
      return Members.addEmailAddress(memberId, emailAddress)
    }
    case 'delete': {
      return Members.deleteEmailAddress(memberId, emailAddress.email_address!)
    }
  }
}

interface UpdatePhoneMutationProps {
  memberId: number
  phoneNumber: ProvisionalMemberPhoneNumber
}

const updatePhone = async ({
  memberId,
  phoneNumber: { action, phoneNumber },
}: UpdatePhoneMutationProps) => {
  switch (action) {
    case 'add': {
      return Members.addPhoneNumber(memberId, phoneNumber)
    }
    case 'delete': {
      return Members.deletePhoneNumber(memberId, phoneNumber.phone_number!)
    }
  }
}

interface EditMemberModalProps {
  show: boolean
  member: MemberDetailed
  onHide(): void
  onSaveComplete(member: MemberDetailed): void
}

const EditMemberModal: React.FC<EditMemberModalProps> = ({
  show,
  member: originalMember,
  onHide,
}) => {
  const [member, setMember] = useState<MemberDetailed>(originalMember)
  const info = member.info
  const [update, setUpdate] = useState<Partial<AdminUpdateMemberAttributes>>({})

  const memberId = member.id
  const [existingEmails, setExistingEmails] = useState<
    MemberEmailAddress[] | undefined
  >(info.additional_email_addresses)
  const [existingNumbers, setExistingNumbers] = useState<
    MemberPhoneNumber[] | undefined
  >(info.phone_numbers)

  const [pronouns, setPronouns] = useState<PronounDeclensions[]>(
    parseBundlesToDeclensions(info.pronouns)
  )
  const [emailChanges, setEmailChanges] = useState<
    ProvisionalMemberEmailAddress[]
  >([])
  const [phoneChanges, setPhoneChanges] = useState<
    ProvisionalMemberPhoneNumber[]
  >([])
  const { mutate: mutateMember } = useMutation(updateMember, {
    onSuccess: (updateResponse) => {
      if (updateResponse != null) {
        setMember(updateResponse.data)
      }
    },
  })
  const { mutate: mutateEmail } = useMutation(updateEmail, {
    onSuccess: (response) => {
      if (response != null) {
        setExistingEmails(response)
      }
    },
  })
  const { mutate: mutatePhone } = useMutation(updatePhone, {
    onSuccess: (response) => {
      if (response != null) {
        setExistingNumbers(response)
      }
    },
  })

  const resetToDefaults = () => {
    setUpdate({})
    setPronouns(parseBundlesToDeclensions(info.pronouns))
    setEmailChanges([])
    setPhoneChanges([])
  }

  const handleClose = () => {
    onHide()
  }

  const handleCleanupAndClose = () => {
    resetToDefaults()
    handleClose()
  }

  const handleSaveMember = async () => {
    if (Object.keys(update).length === 0) {
      return
    }

    mutateMember({ memberId, attributes: update })
  }

  const handleSaveEmail = async () => {
    for (const change of emailChanges) {
      mutateEmail({ memberId, emailAddress: change })
    }
  }

  const handleSavePhone = async () => {
    for (const change of phoneChanges) {
      mutatePhone({ memberId, phoneNumber: change })
    }
  }

  const handleSave = async () => {
    handleSaveEmail()
    handleSavePhone()
    handleSaveMember()
    resetToDefaults()
  }

  const handleSaveAndExit = async () => {
    await handleSave()
    handleCleanupAndClose()
  }

  const handlePronounsChange = (pronouns: PronounDeclensions[]) => {
    setPronouns(pronouns)
    setUpdate({
      ...update,
      pronouns: bundlePronouns(pronouns.map(renderDeclensions)),
    })
  }

  const handleFormUpdate = <K extends keyof AdminUpdateMemberAttributes>(
    formKey: K,
    value: AdminUpdateMemberAttributes[K]
  ) => {
    setUpdate({ ...update, [formKey]: value })
  }

  const handleBiographyChange: React.ChangeEventHandler<HTMLTextAreaElement> = (
    e
  ) => {
    setUpdate({ ...update, biography: e.target.value })
  }

  const handleAddEmail = (newEmail: Partial<MemberEmailAddress>) => {
    if (
      existingEmails?.some(
        (item) => item.email_address === newEmail.email_address
      )
    ) {
      showNotification(
        'Email address already exists',
        'This member already has this address'
      )
      return
    }

    const filteredChanges = emailChanges.filter(
      (change) => change.emailAddress.email_address !== newEmail.email_address
    )

    setEmailChanges([
      ...filteredChanges,
      { emailAddress: newEmail, action: 'add' },
    ])
  }

  const handleDeleteEmail = (email: string) => {
    if (existingEmails?.every((item) => item.email_address !== email)) {
      showNotification(
        "Email address doesn't exist",
        "Can't delete non-existent address from member"
      )
      return
    }

    const filteredChanges = emailChanges.filter(
      (change) => change.emailAddress.email_address !== email
    )

    setEmailChanges([
      ...filteredChanges,
      { emailAddress: { email_address: email }, action: 'delete' },
    ])
  }

  const handleUndoEmail = (email: string) => {
    setEmailChanges([
      ...emailChanges.filter(
        (change) => change.emailAddress.email_address !== email
      ),
    ])
  }

  const handleAddPhone = (newPhone: Partial<MemberPhoneNumber>) => {
    if (
      existingNumbers?.some(
        (item) => item.phone_number === newPhone.phone_number
      )
    ) {
      showNotification(
        'Phone address already exists',
        'This member already has this address'
      )
      return
    }

    const filteredChanges = phoneChanges.filter(
      (change) => change.phoneNumber.phone_number !== newPhone.phone_number
    )

    setPhoneChanges([
      ...filteredChanges,
      { phoneNumber: newPhone, action: 'add' },
    ])
  }

  const handleDeletePhone = (number: string) => {
    if (existingNumbers?.every((item) => item.phone_number !== number)) {
      showNotification(
        "Phone number doesn't exist",
        "Can't delete non-existent number from member"
      )
      return
    }

    const filteredChanges = phoneChanges.filter(
      (change) => change.phoneNumber.phone_number !== number
    )

    setPhoneChanges([
      ...filteredChanges,
      { phoneNumber: { phone_number: number }, action: 'delete' },
    ])
  }

  const handleUndoPhone = (number: string) => {
    setPhoneChanges([
      ...phoneChanges.filter(
        (change) => change.phoneNumber.phone_number !== number
      ),
    ])
  }

  type OptInOut = 'opt-in' | 'opt-out'

  const hasOptedOut = (optInOut: OptInOut): boolean => optInOut == 'opt-out'

  const handleToggleDoNotEmail = (optOutStatus: OptInOut) => {
    setUpdate({ ...update, do_not_email: hasOptedOut(optOutStatus) })
  }

  const handleToggleDoNotCall = (optOutStatus: OptInOut) => {
    setUpdate({ ...update, do_not_call: hasOptedOut(optOutStatus) })
  }

  return (
    <>
      <Modal show={show} onHide={handleClose} className="info-modal" size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Edit info</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Tab.Container id="member-info-tab-container" defaultActiveKey="info">
            <Row>
              <Col sm={3}>
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="info">Member info</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="contact">Contact preferences</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="email">Email addresses</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="phone">Phone numbers</Nav.Link>
                  </Nav.Item>
                </Nav>
              </Col>
              <Col sm={9}>
                <Tab.Content>
                  <Tab.Pane eventKey="info">
                    <Form>
                      <Row>
                        <Col md="6">
                          <FieldGroup
                            layout="row"
                            formKey="first_name"
                            componentClass="input"
                            label="First name"
                            onFormValueChange={handleFormUpdate}
                            type="text"
                            placeholder="First name"
                            value={update.first_name || info.first_name}
                          />
                          <FieldGroup
                            layout="row"
                            formKey="last_name"
                            componentClass="input"
                            label="Last name"
                            onFormValueChange={handleFormUpdate}
                            type="text"
                            placeholder="Last name"
                            value={update.last_name || info.last_name}
                          />
                        </Col>
                        <Form.Group as={Col} md="6" controlId="Pronouns">
                          <Form.Label>Pronouns</Form.Label>
                          <PronounSelect
                            pronouns={pronouns}
                            onChange={handlePronounsChange}
                          />
                        </Form.Group>
                      </Row>
                      <hr />
                      <Row>
                        <Form.Group as={Col} md="12" controlId="biography">
                          <Form.Label>Biography</Form.Label>
                          <AutoTextarea
                            className="bio-textarea"
                            minRows={5}
                            maxRows={8}
                            value={update.biography}
                            onChange={handleBiographyChange}
                            placeholder="Add your bio"
                          />
                        </Form.Group>
                      </Row>
                    </Form>
                  </Tab.Pane>
                  <Tab.Pane eventKey="contact">
                    <Row>
                      <Col md={8}>
                        <Form.Label>
                          Receive emails from {c('CHAPTER_NAME')}
                        </Form.Label>
                      </Col>
                      <Col md={4}>
                        <ToggleButtonGroup
                          type="radio"
                          name="do-not-email"
                          value={update.do_not_email || member.do_not_email}
                          onChange={handleToggleDoNotEmail}
                        >
                          <ToggleButton
                            value="opt-in"
                            variant={
                              update.do_not_email || member.do_not_email
                                ? 'outline-success'
                                : 'success'
                            }
                          >
                            Opt in
                          </ToggleButton>
                          <ToggleButton
                            value="opt-out"
                            variant={
                              update.do_not_email || member.do_not_email
                                ? 'danger'
                                : 'outline-danger'
                            }
                          >
                            Opt out
                          </ToggleButton>
                        </ToggleButtonGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={8}>
                        <Form.Label>
                          Receive calls or texts from {c('CHAPTER_NAME')}
                        </Form.Label>
                      </Col>
                      <Col md={4}>
                        <ToggleButtonGroup
                          type="radio"
                          name="do-not-email"
                          value={update.do_not_call || member.do_not_call}
                          onChange={handleToggleDoNotCall}
                        >
                          <ToggleButton
                            value="opt-in"
                            variant={
                              update.do_not_call || member.do_not_call
                                ? 'outline-success'
                                : 'success'
                            }
                          >
                            Opt in
                          </ToggleButton>
                          <ToggleButton
                            value="opt-out"
                            variant={
                              update.do_not_call || member.do_not_call
                                ? 'danger'
                                : 'outline-danger'
                            }
                          >
                            Opt out
                          </ToggleButton>
                        </ToggleButtonGroup>
                      </Col>
                    </Row>
                  </Tab.Pane>
                  <Tab.Pane eventKey="email">
                    <Row>
                      <Form.Group as={Col} md="12" controlId="primary-email">
                        <Form.Label>Primary email address</Form.Label>
                        <p>
                          This is the email we use for your login and other DSA
                          features. You currently can't change this.
                        </p>
                        <strong>{info.email_address}</strong>
                      </Form.Group>
                    </Row>
                    <hr />
                    <Row>
                      <Col md={12}>
                        <AdditionalEmailAddressForm
                          existingAddresses={existingEmails}
                          changes={emailChanges}
                          onAddEmail={handleAddEmail}
                          onDeleteEmail={handleDeleteEmail}
                          onUndoChange={handleUndoEmail}
                        />
                      </Col>
                    </Row>
                  </Tab.Pane>
                  <Tab.Pane eventKey="phone">
                    <Row>
                      <Col md={12}>
                        <AdditionalPhoneNumberForm
                          existingNumbers={existingNumbers}
                          changes={phoneChanges}
                          onAddPhone={handleAddPhone}
                          onDeletePhone={handleDeletePhone}
                          onUndoChange={handleUndoPhone}
                        />
                      </Col>
                    </Row>
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </Modal.Body>
        <Modal.Footer>
          <EditFooter
            update={update}
            emailChanges={emailChanges}
            phoneChanges={phoneChanges}
            onDiscard={handleCleanupAndClose}
            onSaveChanges={handleSave}
            onSaveAndExit={handleSaveAndExit}
          />
        </Modal.Footer>
      </Modal>
    </>
  )
}

interface ActionButtonHandlers {
  onDiscard: React.MouseEventHandler<HTMLButtonElement>
  onSaveChanges: React.MouseEventHandler<HTMLButtonElement>
  onSaveAndExit: React.MouseEventHandler<HTMLButtonElement>
}

interface EditChangeData {
  update: Partial<AdminUpdateMemberAttributes>
  emailChanges: ProvisionalMemberEmailAddress[]
  phoneChanges: ProvisionalMemberPhoneNumber[]
}

type EditFooterProps = ActionButtonHandlers & EditChangeData

const EditFooter: React.FC<EditFooterProps> = ({
  update,
  emailChanges,
  phoneChanges,
  onDiscard,
  onSaveChanges,
  onSaveAndExit,
}) => {
  const numUpdateChanges = Object.keys(update).length
  const numEmailChanges = emailChanges.length
  const numPhoneChanges = phoneChanges.length
  const numTotalChanges = numUpdateChanges + numEmailChanges + numPhoneChanges

  const [showConfirm, setShowConfirm] = useState(false)
  const [expand, setExpand] = useState(false)

  const handleShowConfirm: React.MouseEventHandler<HTMLButtonElement> = (e) => {
    if (numTotalChanges > 0) {
      setShowConfirm(true)
    } else {
      onDiscard(e)
    }
  }
  const handleNevermind = () => setShowConfirm(false)
  const handleExpand = () => setExpand(!expand)

  return (
    <section className="edit-member-footer">
      <section className="edit-member-footer-action-bar">
        {showConfirm ? (
          <ConfirmDiscard
            numTotalChanges={numTotalChanges}
            onDiscard={onDiscard}
            onNevermind={handleNevermind}
          />
        ) : (
          <>
            <EnumerateChangeSummary
              numTotalChanges={numTotalChanges}
              expanded={expand}
              onExpand={handleExpand}
            />
            <ActionButtonGroup
              numTotalChanges={numTotalChanges}
              onDiscard={handleShowConfirm}
              onSaveChanges={onSaveChanges}
              onSaveAndExit={onSaveAndExit}
            />
          </>
        )}
      </section>
      {expand && (
        <DescribeChanges
          update={update}
          emailChanges={emailChanges}
          phoneChanges={phoneChanges}
        />
      )}
    </section>
  )
}

interface ConfirmDiscardProps extends EnumerateChanges {
  onDiscard: React.MouseEventHandler<HTMLButtonElement>
  onNevermind: React.MouseEventHandler<HTMLButtonElement>
}

const ConfirmDiscard: React.FC<ConfirmDiscardProps> = ({
  numTotalChanges,
  onDiscard,
  onNevermind,
}) => {
  return (
    <>
      <span className="confirm-message">
        <strong>
          You're about to lose {numTotalChanges}{' '}
          {pluralize('change', numTotalChanges)}!
        </strong>
      </span>
      <Button variant="outline-primary" onClick={onDiscard}>
        Discard {numTotalChanges} {pluralize('change', numTotalChanges)}
      </Button>
      <Button variant="primary" onClick={onNevermind}>
        Keep editing
      </Button>
    </>
  )
}

interface EnumerateChanges {
  numTotalChanges: number
}

interface EnumerateChangeSummaryProps extends EnumerateChanges {
  onExpand: React.MouseEventHandler<HTMLButtonElement>
  expanded: boolean
}
const EnumerateChangeSummary: React.FC<EnumerateChangeSummaryProps> = ({
  numTotalChanges,
  expanded,
  onExpand,
}) =>
  numTotalChanges > 0 ? (
    <section className="describe-changes-wrapper changes-found">
      <Button variant="link" className="total-change-count" onClick={onExpand}>
        Waiting to save {numTotalChanges} {pluralize('change', numTotalChanges)}{' '}
        {expanded ? <FiChevronUp /> : <FiChevronDown />}
      </Button>
    </section>
  ) : (
    <section className="describe-changes-wrapper no-changes">
      No changes yet
    </section>
  )

type ActionButtonGroupProps = EnumerateChanges & ActionButtonHandlers

const ActionButtonGroup: React.FC<ActionButtonGroupProps> = ({
  numTotalChanges,
  onDiscard,
  onSaveChanges,
  onSaveAndExit,
}) => {
  if (numTotalChanges === 0) {
    return (
      <Button variant="secondary" onClick={onDiscard}>
        Close
      </Button>
    )
  }

  return (
    <>
      <Button variant="secondary" onClick={onDiscard}>
        Discard
      </Button>
      <Button variant="outline-primary" onClick={onSaveChanges}>
        Save changes
      </Button>
      <Button variant="primary" onClick={onSaveAndExit}>
        Save changes and close
      </Button>
    </>
  )
}

type DescribeChangesProps = EditChangeData

const DescribeChanges: React.FC<DescribeChangesProps> = ({
  update,
  emailChanges,
  phoneChanges,
}) => (
  <section className="describe-changes">
    {Object.keys(update).length > 0 && (
      <>
        <section className="update-changes">
          <PageHeading level={3}>Member info changes</PageHeading>
          <ListGroup>
            {Object.keys(update).map((key) => (
              <ListGroupItem key={key}>
                Updating {key} to{' '}
                <strong>
                  <pre>{JSON.stringify(update[key])}</pre>
                </strong>
              </ListGroupItem>
            ))}
          </ListGroup>
        </section>
        <hr />
      </>
    )}
    {emailChanges.length > 0 && (
      <>
        <section className="email-changes">
          <PageHeading level={3}>Email address changes</PageHeading>
          <ListGroup>
            {emailChanges.map((change) => (
              <ListGroupItem key={change.emailAddress.email_address}>
                {capitalize(change.action!)} email address{' '}
                <strong>{change.emailAddress.email_address}</strong>
              </ListGroupItem>
            ))}
          </ListGroup>
        </section>
        <hr />
      </>
    )}
    {phoneChanges.length > 0 && (
      <section className="phone-changes">
        <PageHeading level={3}>Phone number changes</PageHeading>
        <ListGroup>
          {phoneChanges.map((change) => (
            <ListGroupItem key={change.phoneNumber.phone_number}>
              {capitalize(change.action!)} phone number{' '}
              <strong>{change.phoneNumber.phone_number}</strong>
            </ListGroupItem>
          ))}
        </ListGroup>
      </section>
    )}
  </section>
)

export default EditMemberModal
