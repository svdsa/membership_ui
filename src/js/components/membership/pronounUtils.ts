import { flatten, isEqual, uniqBy } from 'lodash/fp'
import { MemberPronounBundle, MemberPronouns } from 'src/js/client/MemberClient'

export interface PronounDeclensions {
  subject: string
  object: string
  possessiveDeterminer: string
  possessive: string
  reflexive: string
}

/**
 * The top 6 most used pronouns, inspired by pronoun.is, and some collections
 */
export const KnownDeclensions: {
  [key: string]: PronounDeclensions[]
} = {
  'she/her': [
    {
      subject: 'she',
      object: 'her',
      possessiveDeterminer: 'her',
      possessive: 'hers',
      reflexive: 'herself',
    },
  ],
  'she/her/hers': [
    {
      subject: 'she',
      object: 'her',
      possessiveDeterminer: 'her',
      possessive: 'hers',
      reflexive: 'herself',
    },
  ],
  'he/him': [
    {
      subject: 'he',
      object: 'him',
      possessiveDeterminer: 'his',
      possessive: 'his',
      reflexive: 'himself',
    },
  ],
  'he/him/his': [
    {
      subject: 'he',
      object: 'him',
      possessiveDeterminer: 'his',
      possessive: 'his',
      reflexive: 'himself',
    },
  ],
  'they/them': [
    {
      subject: 'they',
      object: 'them',
      possessiveDeterminer: 'their',
      possessive: 'theirs',
      reflexive: 'themselves',
    },
  ],
  'they/them/theirs': [
    {
      subject: 'they',
      object: 'them',
      possessiveDeterminer: 'their',
      possessive: 'theirs',
      reflexive: 'themselves',
    },
  ],
  'he/they': [
    {
      subject: 'he',
      object: 'him',
      possessiveDeterminer: 'his',
      possessive: 'his',
      reflexive: 'himself',
    },
    {
      subject: 'they',
      object: 'them',
      possessiveDeterminer: 'their',
      possessive: 'theirs',
      reflexive: 'themselves',
    },
  ],
  'she/they': [
    {
      subject: 'she',
      object: 'her',
      possessiveDeterminer: 'her',
      possessive: 'hers',
      reflexive: 'herself',
    },
    {
      subject: 'they',
      object: 'them',
      possessiveDeterminer: 'their',
      possessive: 'theirs',
      reflexive: 'themselves',
    },
  ],
  'hey/they/she': [
    {
      subject: 'he',
      object: 'him',
      possessiveDeterminer: 'his',
      possessive: 'his',
      reflexive: 'himself',
    },
    {
      subject: 'they',
      object: 'them',
      possessiveDeterminer: 'their',
      possessive: 'theirs',
      reflexive: 'themselves',
    },
    {
      subject: 'she',
      object: 'her',
      possessiveDeterminer: 'her',
      possessive: 'hers',
      reflexive: 'herself',
    },
  ],
  'she/they/he': [
    {
      subject: 'she',
      object: 'her',
      possessiveDeterminer: 'her',
      possessive: 'hers',
      reflexive: 'herself',
    },
    {
      subject: 'they',
      object: 'them',
      possessiveDeterminer: 'their',
      possessive: 'theirs',
      reflexive: 'themselves',
    },
    {
      subject: 'he',
      object: 'him',
      possessiveDeterminer: 'his',
      possessive: 'his',
      reflexive: 'himself',
    },
  ],
  'ze/hir': [
    {
      subject: 'ze',
      object: 'hir',
      possessiveDeterminer: 'hir',
      possessive: 'hirs',
      reflexive: 'hirself',
    },
  ],
  'ze/zir': [
    {
      subject: 'ze',
      object: 'zir',
      possessiveDeterminer: 'zir',
      possessive: 'zirs',
      reflexive: 'zirself',
    },
  ],
  'xey/xem': [
    {
      subject: 'xey',
      object: 'xem',
      possessiveDeterminer: 'xyr',
      possessive: 'xyrs',
      reflexive: 'xemself',
    },
  ],
  'it/its': [
    {
      subject: 'it',
      object: 'it',
      possessiveDeterminer: 'its',
      possessive: 'its',
      reflexive: 'itself',
    },
  ],
}

export const KnownPronouns: MemberPronouns[] = Object.keys(
  KnownDeclensions
) as MemberPronouns[]
export const KnownPronounsSet = new Set(KnownPronouns)

class UnknownPronounsError extends Error {
  pronouns: MemberPronouns
  constructor(message: string, pronouns: MemberPronouns) {
    super(message)
    this.pronouns = pronouns
  }
}

export const unbundlePronouns = (
  bundle: MemberPronounBundle
): MemberPronouns[] => bundle.split(';') as MemberPronouns[]

export const bundlePronouns = (
  pronouns: MemberPronouns[]
): MemberPronounBundle => pronouns.join(';') as MemberPronounBundle

export const renderBundle = (bundle: MemberPronounBundle): string => {
  const splitBundle = unbundlePronouns(bundle)
  return splitBundle.join(' or ')
}

export const matchCommonDeclensions = (
  declensions: PronounDeclensions
): MemberPronouns | null => {
  const found = Object.entries(KnownDeclensions).filter(
    ([key, known]) => known.length === 1 && isEqual(declensions, known[0])
  )

  if (found.length === 0) {
    return null
  }

  const [pronouns] = found[0]
  return pronouns as MemberPronouns
}

export const renderDeclensions = (
  declensions: PronounDeclensions
): MemberPronouns => {
  const match = matchCommonDeclensions(declensions)
  if (match != null) {
    return match
  }

  const { subject, object, possessiveDeterminer, possessive, reflexive } =
    declensions
  return [subject, object, possessiveDeterminer, possessive, reflexive].join(
    '/'
  ) as MemberPronouns
}

export const uniqByPronouns = uniqBy<PronounDeclensions>((decl) =>
  renderDeclensions(decl)
)

export const parsePronounsAsDeclensions = (
  pronounSets: MemberPronouns[]
): PronounDeclensions[] => {
  return uniqByPronouns(
    flatten(
      pronounSets.map((set: MemberPronouns) => {
        if (KnownPronounsSet.has(set)) {
          return KnownDeclensions[set as string]
        }

        const splitIntoDeclensions = set.split('/')
        if (splitIntoDeclensions.length != 5) {
          // TODO: provide failsafe options when parsing pronouns
          console.error('Unknown pronouns', set)
          // throw new UnknownPronounsError('Unknown pronouns', set)
        }

        const [subject, object, possessiveDeterminer, possessive, reflexive] =
          splitIntoDeclensions
        return {
          subject,
          object,
          possessiveDeterminer,
          possessive,
          reflexive,
        } as PronounDeclensions
      })
    )
  )
}

export const parseBundlesToDeclensions = (
  bundle?: MemberPronounBundle
): PronounDeclensions[] => {
  if (bundle == null || bundle === '') {
    return []
  }

  if (KnownPronounsSet.has(bundle as unknown as MemberPronouns)) {
    return KnownDeclensions[bundle as string]
  }

  const multipleDeclensionSets: MemberPronouns[] = bundle.split(
    ';'
  ) as MemberPronouns[]

  return parsePronounsAsDeclensions(multipleDeclensionSets)
}
