import { flatten, sortBy } from 'lodash'
import React, { Component } from 'react'
import { Link } from 'react-router'
import { ElectionResponse } from 'src/js/client/ElectionClient'
import { MemberVote } from 'src/js/client/MemberClient'
import { ElectionById } from 'src/js/redux/reducers/electionReducers'
import { electionStatus } from '../../services/elections'

interface EligibleVotesProps {
  votes: MemberVote[]
  elections: ElectionById
}

export default class EligibleVotes extends Component<EligibleVotesProps> {
  render() {
    const votes = sortBy(this.props.votes, (vote) => -vote.election_id || 0)
    if (votes.length === 0 || !this.props.elections) {
      return <div />
    }

    const eligibleVotes = flatten(
      votes.map((vote, index) => {
        const electionId = vote.election_id
        if (this.props.elections[electionId] == null) {
          return []
        }
        const election = this.props.elections[electionId]
        return [
          <div key={`vote-${index}`}>
            {this.renderElectionDescription(election)}
          </div>,
        ]
      })
    )

    return (
      <div>
        <h2>Eligible Votes</h2>
        {eligibleVotes}
      </div>
    )
  }

  renderElectionDescription(election: ElectionResponse) {
    return (
      <span>
        <Link to={`/elections/${election.id}/`}>{election.name}</Link> (
        {electionStatus(election)})
      </span>
    )
  }
}
