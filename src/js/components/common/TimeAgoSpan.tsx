import moment from 'moment'
import React from 'react'

interface TimeAgoSpanProps {
  date: Date
}

export const TimeAgoSpan: React.FC<TimeAgoSpanProps> = ({ date }) => (
  <span title={date.toLocaleString()}>{moment(date).fromNow()}</span>
)
