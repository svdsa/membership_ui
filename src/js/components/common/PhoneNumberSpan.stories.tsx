import type { Meta, Story } from '@storybook/react'
import React from 'react'
import { IdContact, IdPhoneNumber } from 'src/js/api/schemas'
import {
  PhoneNumberSpan,
  PhoneNumberSpanProps,
} from 'src/js/components/common/PhoneNumberSpan'

export default {
  title: 'Common/PhoneNumberSpan',
  component: PhoneNumberSpan,
} as Meta

const Template: Story<PhoneNumberSpanProps> = (args) => (
  <PhoneNumberSpan {...args} />
)

export const Simple: Story<PhoneNumberSpanProps> = Template.bind({})
Simple.args = {
  phone: {
    object: 'phone_number',
    id: 'phn_bogus' as IdPhoneNumber,
    date_created: new Date('2021-07-01T00:00:00+00:00'),
    date_confirmed: null,
    date_archived: null,
    reason_archived: null,
    value: '+1 650-555-1234',
    name: 'Cell',
    primary: true,
    contact: 'ct_bogus' as IdContact,
    sms_capable: true,
    consent_to_call: true,
    consent_to_text: true,
    consent_to_share: true,
  },
}
