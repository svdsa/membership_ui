import { SerializedError } from '@reduxjs/toolkit'
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query'
import * as React from 'react'
import { Button } from 'react-bootstrap'
import { BiError } from 'react-icons/bi'
import { FaUserAstronaut } from 'react-icons/fa'
import { FiCloudLightning, FiWifiOff } from 'react-icons/fi'
import { IoHammerOutline } from 'react-icons/io5'
import { Link } from 'react-router'
import { c } from 'src/js/config'

export interface ErrorBlockProps {
  title?: string
  message?: string | React.ReactElement
  icon?: React.ReactElement
  error?: FetchBaseQueryError | SerializedError
}

export const ErrorBlock: React.FC<ErrorBlockProps> = ({
  error,
  title = 'Something went wrong',
  message = (
    <>
      <p>Try refreshing the page and trying again.</p>
      <p>If that doesn't work, contact {c('ADMIN_EMAIL')}.</p>
    </>
  ),
  icon = <BiError />,
}) => (
  <section className="d-flex flex-column align-items-center py-5 text-muted">
    <div className="display-2 pb-4">{icon}</div>
    <div className="h2 pb-4">{title}</div>
    <div className="pb-4">{message}</div>
    {error != null && (
      <details>
        <summary>See error details</summary>
        <pre style={{ overflow: 'scroll', maxWidth: '800px' }}>
          {typeof error === 'string' ? error : JSON.stringify(error, null, 2)}
        </pre>
      </details>
    )}
  </section>
)

export const UnderConstruction: React.FC = () => (
  <ErrorBlock
    title="Ey, we're workin' here!"
    message={<p>This part isn't done quite yet. Check back in a little bit.</p>}
    icon={<IoHammerOutline />}
  />
)

export const FetchError: React.FC = () => (
  <ErrorBlock
    title="Connectivity issues"
    message={
      <p>
        We're having trouble reaching our servers. Check your connection and try
        again later.
      </p>
    }
    icon={<FiWifiOff />}
  />
)

export const ParsingError: React.FC<ErrorBlockProps> = ({ error }) => (
  <ErrorBlock
    title="Internal issues"
    message={
      <p>
        We're having issues understanding responses from the portal server.
        Please contact {c('ADMIN_EMAIL')} with the following report:
      </p>
    }
    icon={<FiCloudLightning />}
    error={error}
  />
)

export const NotSignedIn: React.FC = () => (
  <ErrorBlock
    title="It Came From Outer Space"
    message={
      <>
        <p>You're not signed into the portal.</p>
        <p>Click the button below to sign in.</p>
        <div>
          <Link to="/">
            <Button block size="lg" variant="success">
              Sign in
            </Button>
          </Link>
        </div>
      </>
    }
    icon={<FaUserAstronaut />}
  />
)
