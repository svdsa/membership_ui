import cx from 'classnames'
import React from 'react'
import { ListGroup, ListGroupItemProps } from 'react-bootstrap'

type ListGroupHeaderProps = ListGroupItemProps

export const ListGroupHeader: React.FC<ListGroupHeaderProps> = ({
  children,
  className,
  ...rest
}) => (
  <ListGroup.Item
    {...rest}
    className={cx('font-weight-bold text-uppercase py-2 small', className)}
  >
    {children}
  </ListGroup.Item>
)
