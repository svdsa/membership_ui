import type { Meta, Story } from '@storybook/react'
import React from 'react'
import { IdCustomField } from 'src/js/api/schemas'
import {
  CustomFieldCard,
  CustomFieldCardProps,
} from 'src/js/components/common/CustomFieldCard'

export default {
  title: 'Common/CustomFieldCard',
  component: CustomFieldCard,
} as Meta

const Template: Story<CustomFieldCardProps> = (args) => (
  <CustomFieldCard {...args} />
)

export const Text: Story<CustomFieldCardProps> = Template.bind({})
Text.args = {
  definition: {
    id: 'cfd_G6lnAeM13r01a49Z' as IdCustomField,
    object: 'custom_field',
    name: 'Catchphrase',
    description: 'lol',
    field_type: 'text',
    options: null,
    contacts: [],
    tags: [],
    date_created: new Date('2021-07-01T00:00:00+00:00'),
  },
  value: 'Not today, Satan!',
}
