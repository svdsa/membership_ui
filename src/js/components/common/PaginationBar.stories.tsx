import type { Meta, Story } from '@storybook/react'
import React from 'react'
import {
  PaginationBar,
  PaginationBarProps,
} from 'src/js/components/common/PaginationBar'

export default {
  title: 'Common/PaginationBar',
  component: PaginationBar,
} as Meta

const Template: Story<PaginationBarProps> = (args) => (
  <PaginationBar {...args} />
)

export const Simple: Story<PaginationBarProps> = Template.bind({})
Simple.args = {
  numPages: 10,
  pageIndex: 0,
  hasMore: false,
  isFetchingPages: false,
}

export const FewPages: Story<PaginationBarProps> = Template.bind({})
FewPages.args = {
  numPages: 5,
  pageIndex: 0,
  hasMore: false,
  isFetchingPages: false,
}
