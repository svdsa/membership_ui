import cx from 'classnames'
import React from 'react'
import { Badge } from 'react-bootstrap'

interface AppFooterProps {
  className?: string
}

export const AppFooter: React.FC<AppFooterProps> = ({ className }) => (
  <p
    id="version"
    className={cx(
      'small d-flex align-items-center justify-content-between',
      className
    )}
  >
    <span>Membership Portal 🌹 Volunteer labor donated</span>
    <span>
      {process.env.CI_COMMIT_SHA != null && (
        <span className="build-commit">
          Build <code>{process.env.CI_COMMIT_SHA}</code> (
          <code>{process.env.CI_COMMIT_TIMESTAMP}</code>)
        </span>
      )}
      {process.env.NODE_ENV === 'development' && (
        <Badge pill variant="primary" className="ml-2">
          DEV
        </Badge>
      )}
    </span>
  </p>
)
