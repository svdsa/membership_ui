import React from 'react'
import { Card } from 'react-bootstrap'
import { useGetCustomFieldFromIdQuery } from 'src/js/api'
import type {
  CustomFieldResponse,
  CustomFieldTypeUnion,
  IdCustomField,
} from 'src/js/api/schemas'
import ContentStack, {
  DefaultInlineError,
  DefaultInlineLoading,
} from 'src/js/components/common/ContentStack'

interface CustomFieldValueProps {
  type: CustomFieldTypeUnion
  value: unknown
}

const CustomFieldValue: React.FC<CustomFieldValueProps> = ({ type, value }) => {
  switch (type) {
    case 'text':
    case 'textarea':
    case 'number':
      return <span>{value as string}</span>
    case 'date':
      return <span>{(value as Date).toLocaleDateString()}</span>
    case 'url':
      return <span>{value as string}</span>
    default:
      return <span>Not implemented</span>
  }
}

export interface CustomFieldCardProps {
  definition: CustomFieldResponse
  value: unknown
  flush?: boolean
}

export const CustomFieldCard: React.FC<CustomFieldCardProps> = ({
  definition,
  value,
  flush,
}) => {
  const contents = (
    <>
      <div className="small font-weight-semibold">{definition.name}</div>
      <CustomFieldValue type={definition.field_type} value={value} />
    </>
  )

  if (flush) {
    return contents
  }

  return (
    <Card>
      <Card.Body className="p-2">{contents}</Card.Body>
    </Card>
  )
}

export interface CustomFieldCardFromIdProps {
  id: IdCustomField
  value: unknown
  flush?: boolean
}

export const CustomFieldCardFromId: React.FC<CustomFieldCardFromIdProps> = ({
  id,
  value,
  flush,
}) => {
  const { data, isLoading, error } = useGetCustomFieldFromIdQuery(id)

  return (
    <ContentStack
      data={data}
      isLoading={isLoading}
      error={error}
      loadingComponent={DefaultInlineLoading}
      errorComponent={DefaultInlineError}
    >
      {(cf) => <CustomFieldCard definition={cf} value={value} flush={flush} />}
    </ContentStack>
  )
}
