import React, { useCallback, useMemo } from 'react'
import {
  ListGroup,
  ListGroupItemProps,
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap'
import { Variant } from 'react-bootstrap/esm/types'
import { BsCheckCircleFill, BsCircle, BsSlashCircle } from 'react-icons/bs'
import { ChildrenMaybeFunction, resolveChildrenFn } from 'src/js/util/react'

export interface ListGroupSelectableItemChildrenProps {
  selected: boolean
  readOnly: boolean
  onSelect(): void
}

export interface ListGroupSelectableItemCoreProps extends ListGroupItemProps {
  readOnly: boolean
  selected: boolean
  selectedVariant?: Variant
  onSelect(): void
  children: ChildrenMaybeFunction<ListGroupSelectableItemChildrenProps>
}

export const ListGroupSelectableItemCore: React.FC<ListGroupSelectableItemCoreProps> =
  ({ readOnly, onSelect, selectedVariant, selected, children, ...rest }) => {
    const handleSelect = useCallback(() => {
      if (!readOnly) {
        onSelect()
      }
    }, [onSelect, readOnly])

    const resolvedChildren = useMemo(
      () => resolveChildrenFn(children, { readOnly, selected, onSelect }),
      [children, onSelect, readOnly, selected]
    )

    const variant = useMemo(() => {
      if (!selectedVariant) {
        return rest.variant
      }

      if (selected) {
        return selectedVariant
      } else {
        return rest.variant
      }
    }, [selectedVariant, selected, rest.variant])

    return (
      <ListGroup.Item
        {...rest}
        variant={variant}
        action={!readOnly}
        onClick={handleSelect}
      >
        {resolvedChildren}
      </ListGroup.Item>
    )
  }

export interface ListGroupSelectableIconProps {
  iconSelected?: React.ReactElement
  iconUnselected?: React.ReactElement
  iconUnselectable?: React.ReactElement

  selected: boolean
  isSelectActionValid: boolean
}

export const ListGroupSelectionIcon: React.FC<ListGroupSelectableIconProps> = ({
  iconSelected = <BsCheckCircleFill />,
  iconUnselectable = <BsSlashCircle />,
  iconUnselected = <BsCircle />,
  isSelectActionValid,
  selected,
}) => {
  if (!isSelectActionValid) {
    return iconUnselectable
  }

  if (selected) {
    return iconSelected
  } else {
    return iconUnselected
  }
}

export interface ListGroupSelectableItemProps
  extends ListGroupSelectableItemCoreProps,
    ListGroupSelectableIconProps {
  /** A unique id for accessibility */
  id: string

  /** A message to display to the user if selecting this object would lead to an invalid state */
  invalidSelectionMessage?: React.ReactChild

  hideIcons?: boolean
}

export const ListGroupSelectableItem: React.FC<ListGroupSelectableItemProps> = (
  props
) => {
  const {
    hideIcons = false,
    id,
    invalidSelectionMessage = 'Selection not allowed',
    ...rest
  } = props
  const { children, readOnly, onSelect, selected, isSelectActionValid } = rest
  const resolvedChild = useMemo(
    () => resolveChildrenFn(children, { readOnly, onSelect, selected }),
    [children, readOnly, onSelect, selected]
  )
  const wrapperChild = useMemo(() => {
    if (hideIcons) {
      return resolvedChild
    }

    return (
      <div className="d-flex flex-row">
        <div className="flex-fill">{resolvedChild}</div>
        <div className="ml-2">
          <ListGroupSelectionIcon {...rest} />
        </div>
      </div>
    )
  }, [hideIcons, resolvedChild, rest])

  if (!readOnly && !isSelectActionValid) {
    return (
      <OverlayTrigger
        placement="bottom"
        overlay={<Tooltip id={id}>{invalidSelectionMessage}</Tooltip>}
      >
        <ListGroupSelectableItemCore {...rest} readOnly={true}>
          {wrapperChild}
        </ListGroupSelectableItemCore>
      </OverlayTrigger>
    )
  }

  return (
    <ListGroupSelectableItemCore {...rest}>
      {wrapperChild}
    </ListGroupSelectableItemCore>
  )
}
