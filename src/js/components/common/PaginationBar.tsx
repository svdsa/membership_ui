import { range } from 'lodash'
import React, { useCallback } from 'react'
import { Pagination, Spinner } from 'react-bootstrap'
import { BsChevronLeft, BsChevronRight } from 'react-icons/bs'

const DEFAULT_HEAD_PAGES_COUNT = 5
const DEFAULT_TAIL_PAGES_COUNT = 2

export interface PaginationBarProps {
  numPages: number
  pageIndex: number
  hasMore: boolean
  isFetchingPages: boolean
  onSetPageIndex(page: number): void
  onNextPage(): void
  onPreviousPage(): void
  maxNumHeadPages?: number
  maxNumTailPages?: number
}

export const PaginationBar: React.FC<PaginationBarProps> = ({
  numPages,
  pageIndex,
  hasMore,
  isFetchingPages,
  onSetPageIndex: onSetPageIndex,
  onNextPage,
  onPreviousPage,
  maxNumHeadPages = DEFAULT_HEAD_PAGES_COUNT,
  maxNumTailPages = DEFAULT_TAIL_PAGES_COUNT,
}) => {
  const handleSetPage = useCallback(
    (pageIndex: number) => () => onSetPageIndex(pageIndex),
    [onSetPageIndex]
  )

  const isOnFirstPage = pageIndex <= 0
  const disabledPrevious = isOnFirstPage

  const isOnLastPage = !hasMore && pageIndex >= numPages - 1
  const disabledNext = isFetchingPages || isOnLastPage

  const numHeadPages = Math.max(Math.min(numPages, maxNumHeadPages), 0)
  const numTailPages = Math.max(
    Math.min(numPages - numHeadPages, maxNumTailPages),
    0
  )
  const jumpCount = numHeadPages + numTailPages

  return (
    <Pagination>
      <Pagination.Item onClick={onPreviousPage} disabled={disabledPrevious}>
        <span className="mr-2">
          <BsChevronLeft />
        </span>
        Previous
      </Pagination.Item>
      {range(numHeadPages).map((_, i) => (
        <Pagination.Item
          key={i}
          active={pageIndex === i}
          onClick={handleSetPage(i)}
        >
          {i + 1}
        </Pagination.Item>
      ))}
      {hasMore && <Pagination.Ellipsis disabled />}
      {numPages > jumpCount && <Pagination.Ellipsis disabled />}
      {numPages > numHeadPages &&
        numTailPages > 0 &&
        range(numTailPages).map((_, i) => {
          const tailPageIndex = numPages - numTailPages + i
          return (
            <Pagination.Item
              key={i}
              active={pageIndex === tailPageIndex}
              onClick={handleSetPage(tailPageIndex)}
            >
              {tailPageIndex + 1}
            </Pagination.Item>
          )
        })}
      <Pagination.Item disabled={disabledNext} onClick={onNextPage}>
        {isFetchingPages ? (
          <Spinner size="sm" animation="border" />
        ) : (
          <>
            Next
            <span className="ml-2">
              <BsChevronRight />
            </span>
          </>
        )}
      </Pagination.Item>
    </Pagination>
  )
}
