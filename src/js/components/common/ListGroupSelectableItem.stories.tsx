import type { Meta, Story } from '@storybook/react'
import React from 'react'
import { Button } from 'react-bootstrap'
import {
  ListGroupSelectableItem,
  ListGroupSelectableItemProps,
} from 'src/js/components/common/ListGroupSelectableItem'

export default {
  title: 'Bootstrap/ListGroupSelectableItem',
  component: ListGroupSelectableItem,
} as Meta

const Template: Story<ListGroupSelectableItemProps> = (args) => (
  <ListGroupSelectableItem {...args} />
)

export const SelectedWritable: Story<ListGroupSelectableItemProps> =
  Template.bind({})
SelectedWritable.storyName = 'Selected (writable)'
SelectedWritable.args = {
  readOnly: false,
  selected: true,
  isSelectActionValid: true,
  selectedVariant: 'success',
  children: <span>Some text goes here</span>,
}

export const SelectedReadOnly: Story<ListGroupSelectableItemProps> =
  Template.bind({})
SelectedReadOnly.storyName = 'Selected (read only)'
SelectedReadOnly.args = {
  readOnly: true,
  selected: true,
  isSelectActionValid: true,
  selectedVariant: 'success',
  children: <span>Some text goes here</span>,
}

export const UnselectedValidCanModify: Story<ListGroupSelectableItemProps> =
  Template.bind({})
UnselectedValidCanModify.storyName = 'Unselected (writable, valid selection)'
UnselectedValidCanModify.args = {
  readOnly: false,
  selected: false,
  isSelectActionValid: true,
  selectedVariant: 'success',
  children: <span>Some text goes here</span>,
}

export const UnselectedInvalidCanModify: Story<ListGroupSelectableItemProps> =
  Template.bind({})
UnselectedInvalidCanModify.storyName =
  'Unselected (writable, invalid selection)'
UnselectedInvalidCanModify.args = {
  readOnly: false,
  selected: false,
  isSelectActionValid: false,
  invalidSelectionMessage: "Nope, can't do it",
  selectedVariant: 'success',
  children: (
    <span>
      Some text goes here <Button>This button is still clickable</Button>
    </span>
  ),
}

export const UnselectedReadOnly: Story<ListGroupSelectableItemProps> =
  Template.bind({})
UnselectedReadOnly.storyName = 'Unselected (read only)'
UnselectedReadOnly.args = {
  readOnly: true,
  selected: false,
  isSelectActionValid: true,
  invalidSelectionMessage: "Nope, can't do it",
  selectedVariant: 'success',
  children: (
    <span>
      Some text goes here <Button>This button is still clickable</Button>
    </span>
  ),
}

export const UnselectedReadOnlyInvalidSelection: Story<ListGroupSelectableItemProps> =
  Template.bind({})
UnselectedReadOnlyInvalidSelection.storyName =
  'Unselected (read only, invalid selection)'
UnselectedReadOnlyInvalidSelection.args = {
  readOnly: true,
  selected: false,
  isSelectActionValid: false,
  invalidSelectionMessage: "Nope, can't do it",
  selectedVariant: 'success',
  children: (
    <span>
      Some text goes here <Button>This button is still clickable</Button>
    </span>
  ),
}

export const UnselectedReadOnlyNoIconInvalidSelection: Story<ListGroupSelectableItemProps> =
  Template.bind({})
UnselectedReadOnlyNoIconInvalidSelection.storyName =
  'Unselected (read only, no icons, invalid selection)'
UnselectedReadOnlyNoIconInvalidSelection.args = {
  readOnly: true,
  selected: false,
  isSelectActionValid: false,
  hideIcons: true,
  invalidSelectionMessage: "Nope, can't do it",
  selectedVariant: 'success',
  children: (
    <span>
      Some text goes here <Button>This button is still clickable</Button>
    </span>
  ),
}

export const UnselectedReadOnlyNoIcon: Story<ListGroupSelectableItemProps> =
  Template.bind({})
UnselectedReadOnlyNoIcon.storyName = 'Unselected (read only, no icons)'
UnselectedReadOnlyNoIcon.args = {
  readOnly: true,
  selected: false,
  isSelectActionValid: true,
  hideIcons: true,
  invalidSelectionMessage: "Nope, can't do it",
  selectedVariant: 'success',
  children: (
    <span>
      Some text goes here <Button>This button is still clickable</Button>
    </span>
  ),
}
