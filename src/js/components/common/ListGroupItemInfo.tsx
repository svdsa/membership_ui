import React from 'react'
import { ListGroup, ListGroupItemProps } from 'react-bootstrap'

interface ListGroupItemInfoProps extends ListGroupItemProps {
  icon?: React.ReactChild
  title: string
}

export const ListGroupItemInfoHeader: React.FC<ListGroupItemInfoProps> = ({
  icon,
  title,
}) => (
  <div className="text-muted font-weight-semibold small">
    {icon != null && <span className="mr-2">{icon}</span>}
    <span>{title}</span>
  </div>
)

export const ListGroupItemInfo: React.FC<ListGroupItemInfoProps> = ({
  icon,
  title,
  children,
}) => (
  <ListGroup.Item>
    <ListGroupItemInfoHeader icon={icon} title={title} />
    <div>{children}</div>
  </ListGroup.Item>
)
