import cx from 'classnames'
import { nanoid } from 'nanoid'
import React from 'react'
import { PAGE_HEADING_HTML_ID } from '../../config'

interface PageHeadingProps
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLHeadingElement>,
    HTMLHeadingElement
  > {
  level?: number
}

/**
 * Heading for the page with an associated skip link. The app will attempt to
 * focus this skip link after changing routes.
 *
 * This is based on a solution proposed by Gatsby to provide a skip link as a
 * focus anchor when moving between pages in a single-page app.
 *
 * @see https://www.gatsbyjs.org/blog/2019-07-11-user-testing-accessible-client-routing/
 */
const PageHeading: React.FC<PageHeadingProps> = (props) => {
  const baseId = nanoid()
  const { level = 1, children, className } = props
  const cappedLevel = Math.min(6, level)
  const H: keyof JSX.IntrinsicElements = `h${cappedLevel}` as any
  const headingId = PAGE_HEADING_HTML_ID
    ? PAGE_HEADING_HTML_ID
    : `${baseId}_page_heading_h`

  return (
    <H id={headingId} className={cx(className, 'page-heading')} tabIndex={-1}>
      {children}
    </H>
  )
}

export default PageHeading
