import React, { FC } from 'react'
import {
  useGetContactFromIdQuery,
  useGetSecurityPrincipalByIdQuery,
} from 'src/js/api'
import { IdSecurityPrincipal, SecurityPrincipal } from 'src/js/api/schemas'
import { ContactName } from 'src/js/components/common/ContactName'

interface SecurityPrincipalSpanProps {
  sp: SecurityPrincipal
}

export const SecurityPrincipalSpan: FC<SecurityPrincipalSpanProps> = ({
  sp,
}) => {
  if (sp.user == null) {
    return <span>Couldn't load user</span>
  }

  const {
    data: contact,
    isLoading: contactLoading,
    error: contactError,
  } = useGetContactFromIdQuery(sp.user.contact)
  if (contactLoading) {
    return <span>Loading...</span>
  }

  if (contactError) {
    console.error(contactError)
    return <span>Error: couldn't load user</span>
  }

  if (contact == null) {
    return (
      <span>
        Error: invalid contact reference <code>{sp.user.contact}</code>
      </span>
    )
  }

  return <ContactName contact={contact} />
}

interface SecurityPrincipalSpanFromIdProps {
  id: IdSecurityPrincipal
}

export const SecurityPrincipalSpanFromId: FC<SecurityPrincipalSpanFromIdProps> =
  ({ id }) => {
    const {
      data: sp,
      isLoading: spLoading,
      error: spError,
    } = useGetSecurityPrincipalByIdQuery(id)

    if (spLoading) {
      return <span>Loading...</span>
    }

    if (spError) {
      console.error(spError)
      return <span>Error: couldn't load security principal</span>
    }

    if (sp == null) {
      return <span>Error: invalid security principal</span>
    }

    return <SecurityPrincipalSpan sp={sp} />
  }
