import React from 'react'
import { BsArchive } from 'react-icons/bs'
import {
  ErrorBlock,
  ErrorBlockProps,
  FetchError,
  NotSignedIn,
  ParsingError,
} from 'src/js/components/common/ErrorBlock'
import Loading from 'src/js/components/common/Loading'

export interface LoadingComponentProps {}

export interface EmptyComponentProps {}

interface ContentStackProps<TData, TError> {
  data: TData | null | undefined
  isLoading?: boolean
  error: TError | undefined
  errorComponent?: React.FC<ErrorBlockProps>
  loadingComponent?: React.FC<LoadingComponentProps>
  emptyComponent?: React.FC<EmptyComponentProps>
  children(data: TData): React.ReactElement | null
}

const DefaultLoading: React.FC<LoadingComponentProps> = () => (
  <section>
    <Loading />
  </section>
)

export const DefaultEmpty: React.FC = () => (
  <ErrorBlock title="No results" icon={<BsArchive />} message={<p></p>} />
)

const ErrorRouter: React.FC<ErrorBlockProps> = ({ error }) => {
  if (error == null) {
    return <ErrorBlock error={error} />
  }

  if ('status' in error) {
    switch (error.status) {
      case 'FETCH_ERROR':
        return <FetchError />
      case 'PARSING_ERROR':
        return <ParsingError error={error} />
      case 401:
        return <NotSignedIn />
      case 'CUSTOM_ERROR':
      default:
        return <ErrorBlock error={error} />
    }
  } else {
    return <ErrorBlock error={error} />
  }
}

export const DefaultInlineLoading: React.FC<LoadingComponentProps> = () => (
  <span>Loading...</span>
)

export const DefaultInlineError: React.FC<ErrorBlockProps> = () => (
  <span>Error (check console)</span>
)

export default function ContentStack<TData, TError>({
  data,
  isLoading,
  error,
  errorComponent: ErrorComp = ErrorRouter,
  loadingComponent: LoadingComp = DefaultLoading,
  emptyComponent: EmptyComp = DefaultEmpty,
  children,
}: ContentStackProps<TData, TError>): React.ReactElement | null {
  if (error != null) {
    console.error('ContentStack', error)
    return <ErrorComp error={error} />
  } else if (isLoading && data == null) {
    return <LoadingComp />
  } else if (data == null) {
    return <EmptyComp />
  } else if (Array.isArray(data) && data.length === 0) {
    return <EmptyComp />
  }

  return children(data)
}
