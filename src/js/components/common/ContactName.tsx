import * as React from 'react'
import { ContactResponse } from 'src/js/api/schemas'

interface ContactNameProps {
  contact: ContactResponse
}

export const ContactName: React.FC<ContactNameProps> = ({ contact }) => {
  if (contact.display_name != null) {
    return <span>{contact.display_name}</span>
  } else {
    return <span>{contact.full_name}</span>
  }
}
