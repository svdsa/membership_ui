import cx from 'classnames'
import React from 'react'
import { Button, OverlayTrigger, Popover, PopoverProps } from 'react-bootstrap'
import { BsTag } from 'react-icons/bs'
import { useGetTagFromIdQuery } from 'src/js/api'
import { IdTag, TagResponse } from 'src/js/api/schemas'
import ContentStack, {
  DefaultInlineError,
  DefaultInlineLoading,
} from 'src/js/components/common/ContentStack'

interface TagDetailsPopoverProps extends Partial<PopoverProps> {
  tag: TagResponse
}

const TagDetailsPopover = React.forwardRef<
  HTMLDivElement,
  TagDetailsPopoverProps
>(({ tag, ...props }, ref) => (
  <Popover ref={ref} {...props} id={tag.name}>
    <Popover.Content className="p-0">
      Tag search not implemented yet
    </Popover.Content>
  </Popover>
))

export interface TagSpanProps {
  tag: TagResponse
}

export const TagSpan: React.FC<TagSpanProps> = ({ tag }) => (
  <OverlayTrigger
    trigger={['click']}
    placement="bottom"
    rootClose={true}
    overlay={<TagDetailsPopover tag={tag} />}
  >
    <Button
      variant="light"
      className="p-1 py-0"
      onClick={(e) => {
        e.preventDefault()
        e.stopPropagation()
      }}
    >
      <span className="mr-2">
        <BsTag />
      </span>
      <span className={cx('text-underline-dotted', 'text-primary')}>
        {tag.name}
      </span>
    </Button>
  </OverlayTrigger>
)

interface TagSpanFromIdProps {
  tagId: IdTag
}

export const TagSpanFromId: React.FC<TagSpanFromIdProps> = ({ tagId }) => {
  const { data, isLoading, error } = useGetTagFromIdQuery(tagId)

  return (
    <ContentStack
      data={data}
      isLoading={isLoading}
      error={error}
      loadingComponent={DefaultInlineLoading}
      errorComponent={DefaultInlineError}
    >
      {(tag) => <TagSpan tag={tag} />}
    </ContentStack>
  )
}
