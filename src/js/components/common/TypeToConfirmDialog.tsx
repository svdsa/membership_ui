import React, { useState } from 'react'
import { Button, Form, Modal, ModalProps } from 'react-bootstrap'
import { Variant } from 'react-bootstrap/esm/types'
import { LoadingButton } from 'src/js/components/common/LoadingButton'

interface TypeToConfirmDialogOwnProps {
  title: string
  children: React.ReactNode

  /**
   * The instruction text shown above the confirmation text input.
   */
  confirmHintText?: React.ReactChild

  /**
   * The text the user must type in order to be able to confirm the action
   */
  typeToConfirmText: string

  /**
   * The text shown on the button.
   */
  actionText?: string

  /**
   * The variant for the button
   */
  actionVariant?: Variant

  /**
   * Whether to show a spinner on the confirmation box
   */
  isLoading?: boolean

  onConfirm(e: React.FormEvent<HTMLFormElement>): void
}

type TypeToConfirmDialogProps = TypeToConfirmDialogOwnProps & ModalProps

const TypeToConfirmDialog: React.FC<TypeToConfirmDialogProps> = ({
  title,
  children,
  typeToConfirmText,
  confirmHintText = (
    <span>
      To proceed, please type <strong>{typeToConfirmText}</strong> into the box
      below.
    </span>
  ),
  actionText = 'Confirm',
  actionVariant = 'danger',
  onConfirm,
  isLoading,
  ...props
}) => {
  const [textInput, setTextInput] = useState('')
  const normalizedInput = textInput.toLocaleLowerCase()
  const normalizedConfirmText = typeToConfirmText.toLocaleLowerCase()
  const confirmed = normalizedConfirmText === normalizedInput

  const handleVerifyConfirm: React.FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault()
    e.stopPropagation()
    if (confirmed) {
      onConfirm(e)
    }
  }

  return (
    <Modal {...props}>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Form onSubmit={handleVerifyConfirm}>
        <Modal.Body>
          {children}
          <hr />
          <p>{confirmHintText}</p>

          <Form.Control
            onChange={(e) => setTextInput(e.currentTarget.value)}
            value={textInput}
            placeholder="Type here..."
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={props.onHide}>
            Cancel
          </Button>
          <LoadingButton
            className="mt-2"
            type="submit"
            isLoading={!!isLoading}
            disabled={!confirmed}
            disabledVariant={`outline-${actionVariant}`}
            variant={actionVariant}
          >
            {actionText}
          </LoadingButton>
        </Modal.Footer>
      </Form>
    </Modal>
  )
}

export default TypeToConfirmDialog
