import React from 'react'

interface DeveloperInfoProps {
  data?: any | null
}

export const DeveloperInfo: React.FC<DeveloperInfoProps> = ({ data }) => {
  if (data != null) {
    return (
      <details className="mt-2">
        <summary className="text-muted small">Developer info</summary>
        <pre>{JSON.stringify(data, null, 2)}</pre>
      </details>
    )
  } else {
    return null
  }
}
