import { SerializedError } from '@reduxjs/toolkit'
import React, { Component, HTMLAttributes } from 'react'
import { GiExplosiveMaterials } from 'react-icons/gi'
import { ErrorBlock } from 'src/js/components/common/ErrorBlock'
import { c } from 'src/js/config'

interface ErrorBoundaryState {
  hasError: boolean
  error?: SerializedError
}

export type ErrorBoundaryProps = HTMLAttributes<HTMLDivElement>

export class ErrorBoundary extends Component<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  constructor(props: ErrorBoundaryProps) {
    super(props)

    const errorState: ErrorBoundaryState = {
      hasError: false,
      error: undefined,
    }

    this.state = errorState
  }

  componentDidCatch({ message, stack }: Error) {
    // From Elastic UI error boundary component
    // https://github.com/elastic/eui/blob/master/src/components/error_boundary/error_boundary.tsx
    const idx = stack?.indexOf(message) || -1
    const stackStr = idx > -1 ? stack?.substr(idx + message.length + 1) : stack
    const error = {
      message,
      stack,
    }
    this.setState({
      hasError: true,
      error,
    })
  }

  render() {
    const { children, ...rest } = this.props

    if (this.state.hasError) {
      return (
        <div className="error-boundary" {...rest}>
          <ErrorBlock
            title="An unexpected error occurred"
            message={
              <p>
                Something went wrong and we didn't know how to handle the issue.
                Please contact {c('ADMIN_EMAIL')} with the following report:
              </p>
            }
            error={this.state.error}
            icon={<GiExplosiveMaterials />}
          />
        </div>
      )
    }

    return children
  }
}
