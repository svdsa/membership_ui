import type { Meta, Story } from '@storybook/react'
import React from 'react'
import { IdContact, IdEmailAddress } from 'src/js/api/schemas'
import {
  EmailAddressSpan,
  EmailAddressSpanProps,
} from 'src/js/components/common/EmailAddressSpan'

export default {
  title: 'Common/EmailAddressSpan',
  component: EmailAddressSpan,
} as Meta

const Template: Story<EmailAddressSpanProps> = (args) => (
  <EmailAddressSpan {...args} />
)

export const Simple: Story<EmailAddressSpanProps> = Template.bind({})
Simple.args = {
  email: {
    object: 'email_address',
    id: 'eml_bogus' as IdEmailAddress,
    contact: 'ct_bogus' as IdContact,
    date_created: new Date('2021-07-01T00:00:00+00:00'),
    date_confirmed: null,
    date_archived: null,
    reason_archived: null,
    value: 'alicebob@example.com',
    primary: true,
    consent_to_email: true,
    consent_to_share: false,
  },
}
