import React from 'react'
import { Button, ButtonProps, Spinner } from 'react-bootstrap'
import { Variant } from 'react-bootstrap/esm/types'

interface LoadingButtonProps extends ButtonProps {
  isLoading: boolean
  loadingText?: string | React.ReactElement
  disabledVariant?: Variant
}

/**
 * A button with built-in loading animation and option to show a special "disabled variant"
 * when the button is disabled. Based on React-Bootstrap recipe for loading buttons.
 */
export const LoadingButton: React.FC<LoadingButtonProps> = ({
  isLoading,
  loadingText,
  children,
  disabledVariant,
  ...props
}) => (
  <Button
    {...props}
    variant={
      disabledVariant != null
        ? props.disabled
          ? disabledVariant
          : props.variant
        : props.variant
    }
    disabled={isLoading || props.disabled}
  >
    {isLoading ? (
      <span>
        <Spinner animation="border" size="sm" /> {loadingText ?? 'Loading...'}
      </span>
    ) : (
      children
    )}
  </Button>
)
