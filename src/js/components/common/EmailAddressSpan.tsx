import cx from 'classnames'
import React from 'react'
import {
  Button,
  ListGroup,
  OverlayTrigger,
  Popover,
  PopoverProps,
} from 'react-bootstrap'
import {
  BsDashCircle,
  BsDashCircleFill,
  BsPatchCheckFill,
  BsPatchQuestion,
} from 'react-icons/bs'
import { FiSend } from 'react-icons/fi'
import { RiMailForbidLine, RiMailLine } from 'react-icons/ri'
import {
  EmailAddressCreateUnion,
  LaxEmailAddressResponse,
} from 'src/js/api/schemas'
import { TimeAgoSpan } from 'src/js/components/common/TimeAgoSpan'

interface EmailAddressDetailsPopoverProps extends Partial<PopoverProps> {
  email: LaxEmailAddressResponse
}

const EmailAddressDetailsPopover = React.forwardRef<
  HTMLDivElement,
  EmailAddressDetailsPopoverProps
>(({ email, ...props }, ref) => (
  <Popover ref={ref} {...props} id={email.value}>
    {email.date_archived != null && (
      <div className="bg-warning px-3 p-2">
        <div className="font-weight-bold">
          Email address archived <TimeAgoSpan date={email.date_archived} />
        </div>
        {email.reason_archived != null && (
          <span>Reason: {email.reason_archived}</span>
        )}
      </div>
    )}
    <Popover.Title>
      {email.consent_to_email === false ? (
        <Button variant="outline-danger" block>
          <span className="mr-2">
            <BsDashCircle />
          </span>
          Do not send email
        </Button>
      ) : (
        <Button variant="success" block href={`mailto:${email.value}`}>
          <span className="mr-2">
            <FiSend />
          </span>
          Send email
        </Button>
      )}
      {email.consent_to_email === false && (
        <div className="small my-1">
          <span className="text-danger mr-2">
            <BsDashCircleFill />
          </span>
          <span className="text-danger">
            This contact has opted out of emails
          </span>
        </div>
      )}
      {email.date_confirmed != null ? (
        <div className="small my-1">
          <span className="text-primary mr-2">
            <BsPatchCheckFill />
          </span>
          <span className="text-muted">
            Email confirmed <TimeAgoSpan date={email.date_confirmed} />
          </span>
        </div>
      ) : (
        <div className="small my-1">
          <span className="text-muted mr-2">
            <BsPatchQuestion />
          </span>
          <span className="text-muted">Email not confirmed yet</span>
        </div>
      )}
    </Popover.Title>
    <Popover.Content className="p-0">
      <ListGroup variant="flush" as="dl">
        {email.consent_to_share != null && (
          <ListGroup.Item className="p-2 px-3 display-flex flex-row justify-content-between">
            <dt>Consent to share outside of the org?</dt>
            <dd>{email.consent_to_share ? 'Yes' : 'No'}</dd>
          </ListGroup.Item>
        )}
        {email.date_created != null && (
          <ListGroup.Item className="p-2 px-3 display-flex flex-row justify-content-between">
            <dt>Created on</dt>
            <dd>{email.date_created.toLocaleDateString()}</dd>
          </ListGroup.Item>
        )}
        {email.contact != null && (
          <ListGroup.Item className="p-2 px-3 display-flex flex-row justify-content-between">
            <dt>Belongs to</dt>
            <dd>
              <code>{email.contact}</code>
            </dd>
          </ListGroup.Item>
        )}
      </ListGroup>
    </Popover.Content>
  </Popover>
))

export interface EmailAddressSpanProps {
  email: LaxEmailAddressResponse
}

export const EmailAddressSpan: React.FC<EmailAddressSpanProps> = ({
  email,
}) => (
  <OverlayTrigger
    trigger={['click']}
    placement="bottom"
    rootClose={true}
    overlay={<EmailAddressDetailsPopover email={email} />}
  >
    <Button
      variant="light"
      className="p-1 py-0"
      onClick={(e) => {
        e.preventDefault()
        e.stopPropagation()
      }}
    >
      <span className="mr-2">
        {email.consent_to_email === false ? (
          <RiMailForbidLine />
        ) : (
          <RiMailLine />
        )}
      </span>
      <span
        className={cx('text-underline-dotted', 'text-primary', {
          'text-danger': email.consent_to_email === false,
          'text-strikethrough': email.date_archived != null,
        })}
      >
        {email.value}
        {email.date_confirmed != null && (
          <span className="ml-2 text-primary">
            <BsPatchCheckFill />
          </span>
        )}
      </span>
    </Button>
  </OverlayTrigger>
)

export const normalizeEmailAddress = (
  email: LaxEmailAddressResponse | EmailAddressCreateUnion[number]
): LaxEmailAddressResponse => {
  if (typeof email === 'string') {
    return { value: email }
  } else {
    return email
  }
}
