import cx from 'classnames'
import React from 'react'
import {
  Button,
  ListGroup,
  OverlayTrigger,
  Popover,
  PopoverProps,
} from 'react-bootstrap'
import {
  BsDashCircle,
  BsInfoCircle,
  BsPatchCheckFill,
  BsPatchQuestion,
  BsPhone,
} from 'react-icons/bs'
import { FiPhone, FiPhoneOff } from 'react-icons/fi'
import {
  LaxPhoneNumberResponse,
  PhoneNumberCreateUnion,
} from 'src/js/api/schemas'
import { TimeAgoSpan } from 'src/js/components/common/TimeAgoSpan'

interface PhoneNumberDetailsPopoverProps extends Partial<PopoverProps> {
  phone: LaxPhoneNumberResponse
}

const PhoneNumberDetailsPopover = React.forwardRef<
  HTMLDivElement,
  PhoneNumberDetailsPopoverProps
>(({ phone, ...props }, ref) => (
  <Popover ref={ref} {...props} id={phone.value}>
    {phone.date_archived != null && (
      <div className="bg-warning px-3 p-2">
        <div className="font-weight-bold">
          Number archived <TimeAgoSpan date={phone.date_archived} />
        </div>
        {phone.reason_archived != null && (
          <span>Reason: {phone.reason_archived}</span>
        )}
      </div>
    )}
    <Popover.Title>
      {phone.consent_to_call === false ? (
        <Button variant="outline-danger" block>
          <span className="mr-2">
            <BsDashCircle />
          </span>
          Do not call
        </Button>
      ) : (
        <Button variant="success" block href={`tel:${phone.value}`}>
          <span className="mr-2">
            <BsPhone />
          </span>
          Call {phone.value}
        </Button>
      )}
      {phone.sms_capable ? (
        <div className="mt-2">
          {phone.consent_to_text === false ? (
            <Button variant="outline-danger" block>
              <span className="mr-2">
                <BsDashCircle />
              </span>
              Do not text
            </Button>
          ) : (
            <Button variant="primary" block href={`sms:${phone.value}`}>
              <span className="mr-2">
                <BsPhone />
              </span>
              Text {phone.value}
            </Button>
          )}
        </div>
      ) : (
        <div className="small text-muted mt-2">
          <span className="mr-2">
            <BsInfoCircle />
          </span>
          This number can't receive texts
        </div>
      )}
      {phone.date_confirmed != null ? (
        <span className="small">
          <span className="text-primary mr-2">
            <BsPatchCheckFill />
          </span>
          <span className="text-muted">
            Number confirmed <TimeAgoSpan date={phone.date_confirmed} />
          </span>
        </span>
      ) : (
        <span className="small">
          <span className="text-muted mr-2">
            <BsPatchQuestion />
          </span>
          <span className="text-muted">Number not confirmed yet</span>
        </span>
      )}
    </Popover.Title>
    <Popover.Content className="p-0">
      <ListGroup variant="flush" as="dl">
        {phone.consent_to_share != null && (
          <ListGroup.Item className="p-2 px-3 display-flex flex-row justify-content-between">
            <dt>Consent to share outside of the org?</dt>
            <dd>{phone.consent_to_share ? 'Yes' : 'No'}</dd>
          </ListGroup.Item>
        )}
        {phone.contact != null && (
          <ListGroup.Item className="p-2 px-3 display-flex flex-row justify-content-between">
            <dt>Belongs to</dt>
            <dd>
              <code>{phone.contact}</code>
            </dd>
          </ListGroup.Item>
        )}
      </ListGroup>
    </Popover.Content>
  </Popover>
))

export interface PhoneNumberSpanProps {
  phone: LaxPhoneNumberResponse
}

export const PhoneNumberSpan: React.FC<PhoneNumberSpanProps> = ({ phone }) => (
  <OverlayTrigger
    trigger={['click']}
    placement="bottom"
    rootClose={true}
    overlay={<PhoneNumberDetailsPopover phone={phone} />}
  >
    <Button
      variant="light"
      className="p-1 py-0"
      onClick={(e) => {
        e.preventDefault()
        e.stopPropagation()
      }}
    >
      <span className="mr-2">
        {phone.consent_to_call === false ? <FiPhoneOff /> : <FiPhone />}
      </span>
      <span
        className={cx('text-underline-dotted', 'text-primary', {
          'text-danger':
            phone.consent_to_call === false || phone.consent_to_text === false,
          'text-strikethrough': phone.date_archived != null,
        })}
      >
        {phone.value}
      </span>
      {phone.date_confirmed != null && (
        <span className="ml-2 text-primary">
          <BsPatchCheckFill />
        </span>
      )}
    </Button>
  </OverlayTrigger>
)

export const normalizePhoneNumber = (
  phone: LaxPhoneNumberResponse | PhoneNumberCreateUnion[number]
): LaxPhoneNumberResponse => {
  if (typeof phone === 'string') {
    return { value: phone }
  } else {
    return phone
  }
}
