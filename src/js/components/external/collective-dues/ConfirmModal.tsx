import React from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap'
import { FiCalendar } from 'react-icons/fi'
import { IoIosCheckmarkCircle } from 'react-icons/io'
import { formatCurrency } from 'src/js/components/external/collective-dues/utils'
import { c } from 'src/js/config'

interface ConfirmModalProps {
  totalAmount?: number
  email: string
  localAmount?: number
  nationalAmount?: number
}

const ConfirmModal: React.FC<ConfirmModalProps> = ({
  email,
  totalAmount,
  localAmount,
  nationalAmount,
}) => {
  return (
    <Modal.Body className="payment-confirm-modal">
      <div className="payment-confirm-icon">
        <IoIosCheckmarkCircle />
      </div>
      <div className="payment-confirm-text">
        Welcome to {c('CHAPTER_NAME')}!
      </div>
      <div className="payment-confirm-subtext">
        {totalAmount != null &&
        localAmount != null &&
        nationalAmount != null ? (
          <>
            <p>
              Thank you for your generous dues payment of{' '}
              <strong>${formatCurrency(totalAmount)}</strong> ($
              {formatCurrency(localAmount)} to {c('CHAPTER_NAME_SHORT')}, $
              {formatCurrency(nationalAmount)} to national DSA).
            </p>
            <p>
              You should receive a receipt at <strong>{email}</strong> shortly.
            </p>
          </>
        ) : (
          <>
            <p>We're glad you're here!</p>
            <p>
              We'll sort the sponsorship on our end and reach out to you with
              more information shortly.
            </p>
          </>
        )}
      </div>
      <Row>
        <Col>
          <Button
            variant="success"
            href={c('URL_CHAPTER_EVENTS')}
            className="event-action-button"
          >
            <FiCalendar />
            <span>Take action at an upcoming event!</span>
          </Button>
        </Col>
      </Row>
    </Modal.Body>
  )
}

export default ConfirmModal
