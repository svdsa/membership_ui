import { useAsync, useMountEffect } from '@react-hookz/web'
import { Elements } from '@stripe/react-stripe-js'
import React, { useState } from 'react'
import { Alert, Button, Card } from 'react-bootstrap'
import { FiArrowLeftCircle } from 'react-icons/fi'
import { DuesRecurrence, NewMemberForm } from 'src/js/client/PaymentsClient'
import Loading from 'src/js/components/common/Loading'
import PageHeading from 'src/js/components/common/PageHeading'
import DuesForm from 'src/js/components/external/collective-dues/DuesForm'
import NewMemberJoinForm from 'src/js/components/external/collective-dues/NewMemberJoinForm'
import { COLLECTIVE_DUES_STRIPE_API_KEY } from 'src/js/config'

const EMPTY_NEW_MEMBER_FORM: NewMemberForm = {
  first_name: '',
  last_name: '',
  email_address: '',
  phone_number: '',
  pronouns: '',
  address: '',
  city: '',
  zip: '',
}

type DuesFormStep = 'dues' | 'payment'

const CollectiveDuesForm: React.FC = () => {
  const [canPay, setCanPay] = useState<boolean | null>(null)
  const [recurrence, setRecurrence] = useState<DuesRecurrence | null>(null)
  const [duesAmount, setDuesAmount] = useState<number | null>(null)
  const [natlAmount, setNatlAmount] = useState<number>(0)
  const [newMemberForm, setNewMemberForm] = useState<NewMemberForm>(
    EMPTY_NEW_MEMBER_FORM
  )
  const [formStep, setFormStep] = useState<DuesFormStep>('dues')
  const [{ status, result: stripe, error }, { execute }] = useAsync(
    async () => {
      // See https://gitlab.com/DSASanFrancisco/membership_ui/-/merge_requests/362
      // tl;dr statically importing the Stripe library injects the library into the page on every component
      // even if the component using Stripe is unmounted, which is a potential privacy issue.
      // We split and lazy-load the Stripe import to only load the library when this component is mounted
      const { loadStripe } = await import(
        /* webpackChunkName: "stripe" */ '@stripe/stripe-js'
      )
      return await loadStripe(COLLECTIVE_DUES_STRIPE_API_KEY)
    }
  )
  useMountEffect(execute)

  const updateNewMember = <K extends keyof NewMemberForm>(
    formKey: K,
    value: NewMemberForm[K]
  ): void => {
    setNewMemberForm({ ...newMemberForm, [formKey]: value })
  }

  const duesFormComplete =
    canPay === false ||
    (recurrence != null && duesAmount != null && natlAmount != null)

  if (status === 'loading') {
    return <Loading />
  }

  if (error) {
    return <Alert variant="danger">{error.message}</Alert>
  }

  return (
    <Card className="collective-dues-form">
      {formStep === 'dues' && (
        <>
          <DuesForm
            canPay={canPay}
            amount={duesAmount}
            recurrence={recurrence}
            natlAmount={natlAmount}
            onChangeCanPay={setCanPay}
            onChangeAmount={setDuesAmount}
            onChangeNatlAmount={setNatlAmount}
            onChangeRecurrence={setRecurrence}
          />
          {duesFormComplete && (
            <Button
              className="dues-continue-button"
              block
              size="lg"
              variant="success"
              disabled={canPay == null}
              onClick={() => setFormStep('payment')}
            >
              Continue
            </Button>
          )}
        </>
      )}
      {formStep === 'payment' && canPay != null && (
        <>
          <Button
            size="lg"
            variant="outline-primary"
            className="change-dues-button"
            onClick={() => setFormStep('dues')}
          >
            <FiArrowLeftCircle /> Change dues
          </Button>
          <section className="collective-dues-step confirm-step">
            <PageHeading level={3}>Tell us a little about yourself</PageHeading>
            {canPay ? (
              <p>
                Please fill out your personal information and confirm your dues
                info.
              </p>
            ) : (
              <p>Please fill out your personal information.</p>
            )}
            {(canPay === false ||
              (recurrence != null &&
                duesAmount != null &&
                natlAmount != null &&
                stripe != null)) && (
              <Elements stripe={stripe || null}>
                <NewMemberJoinForm
                  value={newMemberForm}
                  onChange={updateNewMember}
                  canPay={canPay}
                  localAmount={duesAmount != null ? duesAmount - natlAmount : 0}
                  nationalAmount={natlAmount}
                  recurrence={recurrence}
                />
              </Elements>
            )}
            <p className="information-security-notice">
              Your information security is of paramount importance to us. We
              only handle member personal information on a "need-to-know" basis
              for chapter administrative and contact needs. We will{' '}
              <strong>never</strong> share your information with anyone outside
              of the Democratic Socialists of America.
            </p>
          </section>
        </>
      )}
    </Card>
  )
}

export default CollectiveDuesForm
