import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js'
import { StripeError } from '@stripe/stripe-js'
import React, { useState } from 'react'
import { Alert, Button, Form, Modal, Spinner } from 'react-bootstrap'
import { FiCreditCard } from 'react-icons/fi'
import { useMutation } from 'react-query'
import { ApiResponseErrorHandlerProps } from 'src/js/client/ApiClient'
import {
  CreateDuesSubscriptionRequest,
  CreateDuesSubscriptionResponse,
  DuesRecurrence,
  NewMemberForm,
  Payments,
} from 'src/js/client/PaymentsClient'
import FieldGroup, { VALID_EMAIL } from 'src/js/components/common/FieldGroup'
import ConfirmModal from 'src/js/components/external/collective-dues/ConfirmModal'
import PaymentForm from 'src/js/components/external/collective-dues/PaymentForm'
import { DuesSubscriptionPaymentInfo } from 'src/js/components/external/collective-dues/utils'
import { PronounSelect } from 'src/js/components/membership/EditPronouns'
import {
  bundlePronouns,
  renderDeclensions,
} from 'src/js/components/membership/pronounUtils'
import { c } from 'src/js/config'

interface DuesSubscriptionPaymentInfoWithId
  extends DuesSubscriptionPaymentInfo {
  stripePaymentMethodId: string
}

interface CreateDuesSubscriptionProps {
  paymentInfo?: DuesSubscriptionPaymentInfoWithId
  newMemberForm: NewMemberForm
}

const createDuesSubscription = async ({
  newMemberForm,
  paymentInfo,
}: CreateDuesSubscriptionProps): Promise<CreateDuesSubscriptionResponse | null> => {
  const request: CreateDuesSubscriptionRequest = {
    email_address: newMemberForm.email_address,
    phone_number: newMemberForm.phone_number || undefined,
    first_name: newMemberForm.first_name,
    last_name: newMemberForm.last_name,
    pronouns: newMemberForm.pronouns || undefined,
    address: newMemberForm.address || undefined,
    city: newMemberForm.city,
    zip_code: newMemberForm.zip,
    payment:
      paymentInfo != null
        ? {
            recurrence: paymentInfo.recurrence,
            total_amount_cents: parseInt(
              (paymentInfo.totalAmount * 100).toFixed(0)
            ),
            national_amount_cents: parseInt(
              (paymentInfo.nationalAmount * 100).toFixed(0)
            ),
            local_amount_cents: parseInt(
              (paymentInfo.localAmount * 100).toFixed(0)
            ),
            stripe_payment_method_id: paymentInfo.stripePaymentMethodId,
          }
        : undefined,
  }
  return Payments.createDuesSubscription(request)
}

interface NewMemberJoinProps {
  value: NewMemberForm
  onChange(formKey: keyof NewMemberForm, value: string): void
  canPay: boolean
  nationalAmount: number
  localAmount: number
  recurrence: DuesRecurrence | null
}

const NewMemberJoinForm: React.FC<NewMemberJoinProps> = ({
  value,
  onChange,
  canPay,
  nationalAmount,
  localAmount,
  recurrence,
}) => {
  const stripe = useStripe()
  const elements = useElements()
  const [paymentError, setPaymentError] = useState<StripeError | null>(null)
  const {
    mutate,
    isLoading: isProcessing,
    error,
    data,
  } = useMutation<
    CreateDuesSubscriptionResponse | null,
    Error | null,
    CreateDuesSubscriptionProps
  >(createDuesSubscription)
  const totalAmount = nationalAmount + localAmount

  const isFormInvalid =
    !value.email_address ||
    !VALID_EMAIL.test(value.email_address) ||
    !value.first_name ||
    !value.last_name

  const isPaymentInvalid = stripe == null || elements == null

  const paymentInfo: DuesSubscriptionPaymentInfo | null =
    canPay != null && recurrence != null
      ? {
          localAmount,
          nationalAmount,
          recurrence,
          totalAmount,
        }
      : null

  const handleSubmit: React.FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault()

    if (isProcessing) {
      return
    }

    if (canPay) {
      if (stripe == null || elements == null) {
        console.warn(
          "Couldn't submit in an invalid state (payment framework not loaded)"
        )
        return
      }

      const cardElement = elements.getElement(CardElement)

      if (cardElement == null) {
        console.warn(
          "Couldn't submit in an invalid state (unable to find card element from payment framework)"
        )
        return
      }

      const paymentResponse = await stripe.createPaymentMethod({
        type: 'card',
        card: cardElement,
        billing_details: {
          name: `${value.first_name} ${value.last_name}`,
          address: {
            line1: value.address || undefined,
            city: value.city,
            country: 'US',
            postal_code: value.zip,
            state: 'CA',
          },
        },
      })

      if (paymentResponse.error != null) {
        setPaymentError(paymentResponse.error)
        return
      } else if (paymentResponse.paymentMethod != null && paymentInfo != null) {
        const stripePaymentMethodId = paymentResponse.paymentMethod.id
        setPaymentError(null)

        try {
          const infoWithId: DuesSubscriptionPaymentInfoWithId = {
            ...paymentInfo,
            stripePaymentMethodId,
          }
          await mutate({
            paymentInfo: infoWithId,
            newMemberForm: value,
          })
        } catch (err) {
          console.error('Submission error', err)
        }
      } else {
        console.warn(
          'Received an invalid response from payment processor (neither payment method nor error data available)'
        )
      }
    } else if (canPay === false) {
      try {
        await mutate({
          newMemberForm: value,
        })
      } catch (err) {
        console.error('Submission error', err)
      }
    }
  }

  return (
    <Form onSubmit={handleSubmit} className="new-member-join-form">
      <FieldGroup
        formKey="email_address"
        componentClass="input"
        type="email"
        title="Email address"
        layout="row"
        placeholder="Email address"
        value={value.email_address}
        onFormValueChange={onChange}
        required
      />
      <FieldGroup
        formKey="phone_number"
        componentClass="input"
        type="tel"
        title="Valid 10 digit phone number"
        placeholder="Phone number (optional)"
        layout="row"
        value={value.phone_number}
        onFormValueChange={onChange}
      />
      <hr />
      <FieldGroup
        formKey="first_name"
        componentClass="input"
        type="text"
        placeholder="First name"
        layout="row"
        value={value.first_name}
        onFormValueChange={onChange}
        required
      />
      <FieldGroup
        formKey="last_name"
        componentClass="input"
        type="text"
        placeholder="Last name"
        layout="row"
        value={value.last_name}
        onFormValueChange={onChange}
        required
      />
      <PronounSelect
        pronouns={[]}
        placeholder="Select pronouns (optional)"
        onChange={(pronouns) =>
          onChange('pronouns', bundlePronouns(pronouns.map(renderDeclensions)))
        }
      />
      <hr />
      <FieldGroup
        formKey="address"
        componentClass="input"
        type="text"
        placeholder="Address (optional; needed to receive magazine)"
        layout="row"
        value={value.address}
        onFormValueChange={onChange}
      />
      <FieldGroup
        formKey="city"
        componentClass="input"
        type="text"
        placeholder="City"
        layout="row"
        value={value.city}
        onFormValueChange={onChange}
        required
      />
      <FieldGroup
        formKey="zip"
        componentClass="input"
        type="text"
        placeholder="ZIP code"
        layout="row"
        value={value.zip}
        onFormValueChange={onChange}
        required
      />

      {paymentInfo != null ? (
        <>
          <PaymentForm paymentInfo={paymentInfo} />
          {(paymentError != null || error != null) && (
            <Alert className="payment-error-alert" variant="danger">
              <Alert.Heading>Couldn't complete your payment</Alert.Heading>
              {paymentError != null && (
                <div className="payment-error">{paymentError.message}</div>
              )}
              {error != null && (
                <div className="request-error">
                  {(error as any as ApiResponseErrorHandlerProps).error.response
                    ?.body.err ?? (
                    <span>
                      We encountered an unexpected error. Please try again in a
                      little while, or email{' '}
                      <a href={`mailto:${c('ADMIN_EMAIL')}`}>
                        {c('ADMIN_EMAIL')}
                      </a>{' '}
                      for help.
                    </span>
                  )}
                </div>
              )}
            </Alert>
          )}
          <Button
            size="lg"
            block
            variant="success"
            type="submit"
            disabled={
              isFormInvalid || isPaymentInvalid || isProcessing || data != null
            }
          >
            {isProcessing ? (
              <Spinner animation="border" variant="light" />
            ) : (
              <>
                <FiCreditCard /> Confirm membership and pay
              </>
            )}
          </Button>
        </>
      ) : (
        <Button
          size="lg"
          block
          variant="success"
          type="submit"
          disabled={isFormInvalid || isProcessing || data != null}
        >
          {isProcessing ? (
            <Spinner animation="border" variant="light" />
          ) : (
            'Confirm membership and join'
          )}
        </Button>
      )}

      {canPay &&
        data != null &&
        data.subscription != null &&
        data.subscription.status === 'active' &&
        error == null &&
        paymentError == null && (
          <Modal show={true} centered size="lg" backdrop="static">
            <ConfirmModal
              totalAmount={data.subscription.amount / 100}
              email={data.email_address}
              localAmount={data.subscription.items.local.amount / 100}
              nationalAmount={data.subscription.items.national.amount / 100}
            />
          </Modal>
        )}

      {canPay === false &&
        data != null &&
        error == null &&
        paymentError == null && (
          <Modal show={true} centered size="lg" backdrop="static">
            <ConfirmModal email={data.email_address} />
          </Modal>
        )}
    </Form>
  )
}

export default NewMemberJoinForm
