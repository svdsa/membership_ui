import { DuesRecurrence } from 'src/js/client/PaymentsClient'

export interface DuesSubscriptionPaymentInfo {
  recurrence: DuesRecurrence
  totalAmount: number
  nationalAmount: number
  localAmount: number
}

export const ABBR_TIME_UNIT: { [K in DuesRecurrence]: string } = {
  'one-time': 'once',
  monthly: 'mo',
  annually: 'yr',
}

export const TIME_UNIT: { [K in DuesRecurrence]: string } = {
  'one-time': 'once',
  monthly: 'month',
  annually: 'year',
}

export const formatCurrency = (amount: number): string => {
  if (amount % 1 == 0) {
    return amount.toFixed(0)
  } else {
    return amount.toFixed(2)
  }
}

export const roundToFixed = (amount: number, decimals: number): number => {
  const numPlaces = Math.pow(10, decimals)
  return Math.round((amount + Number.EPSILON) * numPlaces) / numPlaces
}
