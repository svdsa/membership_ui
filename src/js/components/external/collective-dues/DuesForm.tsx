import { useDebouncedEffect } from '@react-hookz/web'
import React, { useState } from 'react'
import {
  Alert,
  Button,
  ButtonGroup,
  Form,
  InputGroup,
  ToggleButton,
  ToggleButtonGroup,
} from 'react-bootstrap'
import { DuesRecurrence } from 'src/js/client/PaymentsClient'
import PageHeading from 'src/js/components/common/PageHeading'
import {
  ABBR_TIME_UNIT,
  formatCurrency,
  roundToFixed,
} from 'src/js/components/external/collective-dues/utils'
import { c } from 'src/js/config'

const MINIMUM_NATIONAL_AMOUNT: { [K in DuesRecurrence]: number } = {
  'one-time': 20,
  annually: 20,
  monthly: 5,
}

const RECOMMENDED_MONTHLY_THRESHOLD = 4.2

const PresetAmounts: { [K in DuesRecurrence]: number[] } = {
  'one-time': [5, 10, 27, 50, 127],
  monthly: [5, 10, 15, 27, 50],
  annually: [5, 10, 27, 50, 127],
}

const isFunnyNumber = (amount: number | null): boolean =>
  amount === 4.2 || amount === 69 || amount === 420 || amount === 420.69

const computeAnnualDuesSavings = (amount: number): number =>
  amount * 0.29 * 12 + 0.3 * 12 - (amount * 12 * 0.29 + 0.3)

interface DuesFormProps {
  canPay: boolean | null
  amount: number | null
  natlAmount: number
  recurrence: DuesRecurrence | null
  onChangeCanPay(canPay: boolean): void
  onChangeAmount(amount: number | null): void
  onChangeNatlAmount(amount: number): void
  onChangeRecurrence(recurrence: DuesRecurrence): void
}

const DuesForm: React.FC<DuesFormProps> = ({
  canPay,
  amount,
  natlAmount,
  recurrence,
  onChangeCanPay,
  onChangeAmount,
  onChangeNatlAmount,
  onChangeRecurrence,
}) => {
  const handleChangeRecurrence = (recurrence: DuesRecurrence): void => {
    onChangeRecurrence(recurrence)
    onChangeAmount(null)
  }

  const handleChangeDues = (amount: number | null): void => {
    if (recurrence != null && amount != null && amount >= 0) {
      onChangeNatlAmount(Math.min(MINIMUM_NATIONAL_AMOUNT[recurrence], amount))
    }

    if (amount != null && amount < natlAmount) {
      onChangeNatlAmount(amount)
    }

    onChangeAmount(amount)
  }

  const handleCanPay = (canPay: 'yes' | 'no'): void =>
    onChangeCanPay(canPay == 'yes')

  return (
    <section className="dues-form">
      <section className="collective-dues-step waiver-step">
        <PageHeading level={3}>Can you pay any amount of dues?</PageHeading>
        <ToggleButtonGroup
          name="pay-any-amount"
          type="radio"
          onChange={handleCanPay}
        >
          <ToggleButton value="yes">Yes, I'm able to pay</ToggleButton>
          <ToggleButton value="no">No, I would like sponsorship</ToggleButton>
        </ToggleButtonGroup>
      </section>
      {canPay && (
        <section className="collective-dues-step recurring-step">
          <PageHeading level={3}>
            How <strong>often</strong> do you want to pay?
          </PageHeading>
          <ToggleButtonGroup
            name="recurring"
            type="radio"
            onChange={handleChangeRecurrence}
            value={recurrence || undefined}
            vertical
          >
            <ToggleButton value="monthly">
              Every month (recommended)
            </ToggleButton>
            <ToggleButton value="annually">Every year</ToggleButton>
            <ToggleButton value="one-time">
              Just once (for a one year membership)
            </ToggleButton>
          </ToggleButtonGroup>
        </section>
      )}
      {canPay && recurrence != null && (
        <DuesAmountForm
          key={recurrence}
          recurrence={recurrence}
          amount={amount}
          onChange={handleChangeDues}
        />
      )}
      {canPay === false && (
        <section className="collective-dues-step waiver-confirmation">
          <PageHeading level={3}>We'll sponsor you.</PageHeading>
          <p>
            If you live or work in or near {c('CHAPTER_AREA_NAME')}, the chapter
            will sponsor your dues to national DSA.
          </p>
        </section>
      )}
      {canPay &&
        recurrence === 'monthly' &&
        amount != null &&
        amount < RECOMMENDED_MONTHLY_THRESHOLD && (
          <ProcessingFeeNotice amount={amount} />
        )}
      {canPay && recurrence != null && amount != null && (
        <DuesSplitForm
          amount={amount}
          nationalAmount={natlAmount}
          recurrence={recurrence}
          onChange={onChangeNatlAmount}
        />
      )}
    </section>
  )
}

interface DuesAmountFormProps {
  recurrence: DuesRecurrence
  amount: number | null
  onChange(amount: number | null): void
}

const DuesAmountForm: React.FC<DuesAmountFormProps> = ({
  recurrence,
  amount,
  onChange,
}) => {
  const [showCustomInput, setShowCustomInput] = useState(false)
  const [customValue, setCustomValue] = useState('')
  const [error, setError] = useState<string | null>(null)

  const handleParseCustomValue = (customValue: string): void => {
    const parsed = roundToFixed(parseFloat(customValue), 2)
    if (!Number.isNaN(parsed) && parsed >= 1) {
      onChange(parsed)
      setError(null)
    } else {
      if (Number.isNaN(parsed)) {
        setError('Please type a number with at most two decimal places')
      } else if (parsed < 1 && parsed >= 0) {
        setError(
          "If you can't pay any dues, please choose 'I would like sponsorship' instead"
        )
      } else if (parsed < 0) {
        setError(
          "As much as we'd like to redistribute wealth, we can't pay you to become a member just yet 😏"
        )
      }
      onChange(null)
    }
  }

  useDebouncedEffect(
    () => {
      if (customValue != null && customValue.length > 0) {
        handleParseCustomValue(customValue)
      }
    },
    [customValue],
    500,
    1000
  )

  const handleChangeCustomValue: React.ChangeEventHandler<HTMLInputElement> = (
    e
  ) => setCustomValue(e.target.value)

  const handleChooseValue = (amount: 'other' | number): void => {
    if (amount === 'other') {
      setShowCustomInput(true)

      if (customValue) {
        handleParseCustomValue(customValue)
      }
    } else {
      setShowCustomInput(false)
      onChange(amount)
    }
  }

  return (
    <section className={`collective-dues-step ${recurrence}-amount-step`}>
      <PageHeading level={3}>
        How <strong>much</strong> do you want to pay {recurrence}?
      </PageHeading>
      <div className="dues-choice">
        <ToggleButtonGroup
          name={`amount-${recurrence}`}
          type="radio"
          value={amount || undefined}
          onChange={handleChooseValue}
        >
          {PresetAmounts[recurrence].map((amountOption) => (
            <ToggleButton
              key={`${recurrence}-${amountOption}`}
              value={amountOption}
            >
              ${formatCurrency(amountOption)}
            </ToggleButton>
          ))}
          {showCustomInput === false && (
            <ToggleButton value="other">Other $</ToggleButton>
          )}
        </ToggleButtonGroup>
        {showCustomInput && (
          <InputGroup>
            <InputGroup.Text>Other $</InputGroup.Text>
            <Form.Control
              onChange={handleChangeCustomValue}
              value={customValue}
              type="text"
              inputMode="numeric"
              placeholder="3.14"
              isValid={customValue.length > 0 && error == null}
            />
          </InputGroup>
        )}
      </div>
      {error && (
        <Alert variant="warning" className="other-amount-error-alert">
          {error}
        </Alert>
      )}
      {isFunnyNumber(amount) && <span>nice</span>}
    </section>
  )
}

interface DuesSplitFormProps {
  amount: number
  nationalAmount: number
  recurrence: DuesRecurrence
  onChange(amount: number): void
}

const DuesSplitForm: React.FC<DuesSplitFormProps> = ({
  amount,
  nationalAmount,
  recurrence,
  onChange,
}) => {
  const handleChangeRangeValue: React.ChangeEventHandler<HTMLInputElement> = (
    e
  ) => {
    const value = parseInt(e.currentTarget.value)
    onChange(value)
  }

  const setNationalSplit: React.MouseEventHandler<HTMLButtonElement> = (e) =>
    onChange(amount)
  const setRecommendedSplit: React.MouseEventHandler<HTMLButtonElement> = (e) =>
    onChange(Math.min(amount, MINIMUM_NATIONAL_AMOUNT[recurrence]))
  const setLocalSplit: React.MouseEventHandler<HTMLButtonElement> = (e) =>
    onChange(0)

  return (
    <section className="collective-dues-step dues-split-step">
      <PageHeading level={3}>Choose your local/national dues split</PageHeading>
      <Form.Control
        type="range"
        className="custom-range"
        min={0}
        max={amount}
        step={1}
        value={nationalAmount.toFixed(0)}
        onChange={handleChangeRangeValue}
      />
      <div className="split">
        <section className="local-split">
          <header>Local dues</header>
          <span className="local-amount">
            <span className="amount-value">
              ${formatCurrency(amount - nationalAmount)}
            </span>
            {recurrence !== 'one-time' && (
              <span className="amount-period">
                /{ABBR_TIME_UNIT[recurrence]}
              </span>
            )}
          </span>
        </section>
        <section className="national-split">
          <header>National dues</header>
          <span className="national-amount">
            <span className="amount-value">
              ${formatCurrency(nationalAmount)}
            </span>
            {recurrence !== 'one-time' && (
              <span className="amount-period">
                /{ABBR_TIME_UNIT[recurrence]}
              </span>
            )}
          </span>
        </section>
      </div>
      <ButtonGroup className="presets">
        <Button variant="outline-primary" onClick={setLocalSplit}>
          100% local
        </Button>
        <Button variant="outline-primary" onClick={setRecommendedSplit}>
          Recommended split
        </Button>
      </ButtonGroup>
    </section>
  )
}

interface ProcessingFeeNoticeProps {
  amount: number
}

const ProcessingFeeNotice: React.FC<ProcessingFeeNoticeProps> = ({
  amount,
}) => {
  const annualEquivalentAmount = amount * 12
  const savings = computeAnnualDuesSavings(amount)
  const savingPct = (savings / annualEquivalentAmount) * 100
  return (
    <Alert variant="info" className="processing-fee-notice-alert">
      If you make a{' '}
      <strong>
        ${formatCurrency(annualEquivalentAmount)} annual contribution
      </strong>{' '}
      instead of your current{' '}
      <strong>${formatCurrency(amount)} monthly contribution</strong>, you'll
      save the chapter <strong>${formatCurrency(savings)}</strong> (
      {formatCurrency(savingPct)}
      %) on processing fees. 💸
    </Alert>
  )
}

export default DuesForm
