import React, { useState } from 'react'
import { Helmet } from 'react-helmet'
import { RouteComponentProps } from 'react-router'
import {
  Container,
  Row,
  Col,
  InputGroup,
  Form,
  Button,
  Spinner,
  Alert,
  Card,
} from 'react-bootstrap'
import PageHeading from 'src/js/components/common/PageHeading'
import Meetings, { VerifyTokenResponse } from 'src/js/client/MeetingClient'
import { useMutation } from 'react-query'
import { ApiResponseErrorHandlerProps } from 'src/js/client/ApiClient'
import { MemberInfo } from 'src/js/client/MemberClient'
import { c } from 'src/js/config'

interface VerifyEmailParamProps {
  token: string
}

interface VerifyEmailRouteParamProps {}

type VerifyEmailProps = RouteComponentProps<
  VerifyEmailParamProps,
  VerifyEmailRouteParamProps
>

interface SubmitTokenProps {
  token: string
}

const submitToken = ({ token }: SubmitTokenProps) => Meetings.verifyToken(token)

export const VerifyEmail: React.FC<VerifyEmailProps> = ({
  params: { token },
}) => {
  const [tokenInput, setTokenInput] = useState<string>(token || '')
  const [onLoadSubmitted, setOnLoadSubmitted] = useState(false)
  const { mutate, isLoading, error, data } = useMutation(submitToken)

  const handleChangeTokenInput: React.ChangeEventHandler<HTMLInputElement> = (
    e
  ) => setTokenInput(e.currentTarget.value)

  const handleFormSubmit: React.FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault()
    mutate({ token: tokenInput })
  }

  if (!onLoadSubmitted && token != null && token.length > 0) {
    mutate({ token })
    setOnLoadSubmitted(true)
  }

  if (error) {
    console.error(error)
  }

  const errorResponse = error as unknown as ApiResponseErrorHandlerProps | null
  const notFound = errorResponse != null && errorResponse.error.status == 404

  if (notFound && errorResponse?.error.response != null) {
    return (
      <Container className="external-screen verify-email-screen">
        <main className="verify-failed">
          <span className="failed-icon">🙀</span>
          <span className="failed-text">
            {errorResponse.error.response.body.err}
          </span>
          <span className="failed-guidance">Please try signing in again.</span>
        </main>
      </Container>
    )
  }

  return (
    <Container className="external-screen verify-email-screen">
      <Helmet>
        <title>Verify your attendance</title>
      </Helmet>
      <main>
        <Row>
          <Col md={{ span: 8, offset: 2 }}>
            <header>
              <PageHeading level={1}>Verify meeting attendance</PageHeading>
            </header>
            {errorResponse != null &&
              errorResponse.error.response != null &&
              notFound && (
                <Alert variant="warning">
                  {errorResponse.error.response.body.err}
                </Alert>
              )}
            {data != null && <VerifyTokenSuccess member={data.member} />}
            {data == null && (
              <Form className="token" onSubmit={handleFormSubmit}>
                <InputGroup size="lg">
                  <InputGroup.Prepend>
                    <InputGroup.Text>Token</InputGroup.Text>
                  </InputGroup.Prepend>
                  <Form.Control
                    onChange={handleChangeTokenInput}
                    value={tokenInput}
                  />
                </InputGroup>
                <Button
                  block
                  size="lg"
                  className="token-submit"
                  type="submit"
                  disabled={tokenInput.length == 0 || isLoading}
                >
                  {isLoading ? <Spinner animation="border" /> : 'Verify'}
                </Button>
              </Form>
            )}
          </Col>
        </Row>
      </main>
    </Container>
  )
}

interface VerifyTokenSuccessProps {
  member: MemberInfo
}

const VerifyTokenSuccess: React.FC<VerifyTokenSuccessProps> = ({ member }) => {
  return (
    <Card className="verify-token-success-card">
      <Card.Body>
        <Row>
          <Col md={{ span: 10, offset: 1 }}>
            <header>
              <PageHeading level={2}>
                Thank you for verifying your email!
              </PageHeading>
              <p>Please take a moment to make sure your info is up to date.</p>
            </header>
            <Card body className="member-details-card">
              <dl>
                <dt>Name</dt>
                <dd>
                  {member.first_name} {member.last_name}
                </dd>
                {member.pronouns != null && member.pronouns !== '' && (
                  <>
                    <dt>Pronouns</dt>
                    <dd>{member.pronouns}</dd>
                  </>
                )}
                {member.email_address != null && (
                  <>
                    <dt>Email addresses</dt>
                    <dd>
                      <ul>
                        <li>{member.email_address}</li>
                        {member.additional_email_addresses != null &&
                          member.additional_email_addresses.length > 0 &&
                          member.additional_email_addresses.map((email) => (
                            <li key={email.email_address}>
                              {email.email_address}
                            </li>
                          ))}
                      </ul>
                    </dd>
                  </>
                )}

                {member.phone_numbers != null &&
                  member.phone_numbers.length > 0 && (
                    <>
                      <dt>Phone numbers</dt>
                      <dd>
                        <ul>
                          {member.phone_numbers.map((number) => (
                            <li key={number.phone_number}>
                              {number.name}: {number.phone_number}
                            </li>
                          ))}
                        </ul>
                      </dd>
                    </>
                  )}
              </dl>
            </Card>
          </Col>
        </Row>
      </Card.Body>
      <Card.Footer>
        <p>
          If your info is out of date, please ask for help, or email{' '}
          <a href={c('ADMIN_EMAIL')}>{c('ADMIN_EMAIL')}</a> for support.
        </p>
      </Card.Footer>
    </Card>
  )
}
