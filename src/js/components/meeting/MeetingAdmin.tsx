import React, { Component } from 'react'
import { Button, Col, Container, Row, Tab, Tabs } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { EligibleMemberListEntry } from 'src/js/client/MemberClient'
import {
  AttendeeEligibilityRow,
  EmptyEligibilityRow,
} from 'src/js/components/admin/MemberRow'
import Loading from 'src/js/components/common/Loading'
import PageHeading from 'src/js/components/common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import {
  countEligibleAttendees,
  countProxyVotes,
  isGeneralMeeting,
} from 'src/js/services/meetings'
import {
  Meeting,
  Meetings,
  UpdateAttendeeAttributes,
} from '../../client/MeetingClient'
import MemberSearchField from '../common/MemberSearchField'
import EditMeeting from './EditMeeting'
import EditMeetingAgenda from './EditMeetingAgenda'
import MeetingDetail from './MeetingDetail'
import MeetingInvitations from './MeetingInvitations'
import MeetingKiosk from './MeetingKiosk'
import VoteEligiblePopup from './VoteEligiblePopup'

type MeetingAdminStateProps = RootReducer
interface MeetingAdminParamProps {
  meetingId: string
}
interface MeetingAdminRouteParamProps {}
type MeetingAdminProps = MeetingAdminStateProps &
  RouteComponentProps<MeetingAdminParamProps, MeetingAdminRouteParamProps>

interface MeetingAdminState {
  meeting: Meeting | null
  attendees: EligibleMemberListEntry[] | null
  lastAddedAttendee: EligibleMemberListEntry | null
  tabKey: string
}

class MeetingAdmin extends Component<MeetingAdminProps, MeetingAdminState> {
  constructor(props) {
    super(props)
    this.state = {
      meeting: null,
      attendees: null,
      lastAddedAttendee: null,
      tabKey: 'details',
    }
  }

  componentDidMount() {
    this.getMeeting()
    this.getMeetingAttendees()
  }

  render() {
    const attendees = this.state.attendees

    if (this.state.meeting == null) {
      return <Loading />
    }

    return (
      <Container className="meeting-admin">
        <Helmet>
          <title>{this.state.meeting.name} Admin</title>
        </Helmet>
        <PageHeading level={1}>
          {this.state.meeting.name}{' '}
          <small>
            {this.state.meeting.code ? '#' + this.state.meeting.code : ''}
          </small>
        </PageHeading>

        <Row>
          <Col>
            <Tabs
              id="meeting-tabs"
              activeKey={this.state.tabKey}
              onSelect={this.handleChangeTab}
              className="meeting-tabs"
              unmountOnExit
            >
              <Tab eventKey="details" title="Meeting details">
                <EditMeeting meeting={this.state.meeting} />
              </Tab>
              <Tab eventKey="agenda" title="Agenda">
                <EditMeetingAgenda meeting={this.state.meeting} />
              </Tab>
              <Tab eventKey="attachments" title="Attachments">
                <MeetingDetail
                  meetingId={this.props.params.meetingId}
                  meeting={this.state.meeting}
                  hideSummary
                />
              </Tab>
              <Tab eventKey="kiosk" title="Kiosk">
                <MeetingKiosk
                  meetingId={this.props.params.meetingId}
                  meeting={this.state.meeting}
                />
              </Tab>
              <Tab eventKey="signin" title="Assisted sign-in">
                <h3>Sign In</h3>
                <div className="meeting-member-search-container">
                  <MemberSearchField
                    onMemberSelected={(e) => this.onMemberSelected(e)}
                  />
                  <VoteEligiblePopup
                    numVotes={
                      this.state.lastAddedAttendee
                        ? this.state.lastAddedAttendee.eligibility.num_votes
                        : 0
                    }
                    visible={this.state.lastAddedAttendee != null}
                  />
                </div>
                <AttendeesList
                  attendees={attendees}
                  meeting={this.state.meeting}
                />
              </Tab>
              <Tab eventKey="invitations" title="Invitations">
                <MeetingInvitations meeting={this.state.meeting} />
              </Tab>
            </Tabs>
          </Col>
        </Row>
      </Container>
    )
  }

  handleChangeTab = (tabKey: string) => {
    this.setState({ tabKey })
  }

  async onMemberSelected(member: EligibleMemberListEntry) {
    await Meetings.addAttendee(parseInt(this.props.params.meetingId), member.id)
    this.setState({ lastAddedAttendee: member })
    setTimeout(() => {
      this.setState({ lastAddedAttendee: null })
    }, 5000)
    this.getMeetingAttendees()
  }

  async getMeetingAttendees() {
    await this.getMeeting()

    const meetingId = parseInt(this.props.params.meetingId)
    const results = await Meetings.getAttendees(meetingId)
    this.setState({ attendees: results.reverse() })
  }

  async getMeeting() {
    const meeting = await Meetings.get(parseInt(this.props.params.meetingId))

    if (meeting != null) {
      this.setState({ meeting: meeting })
    }
  }
}

const updateAttendeeClicked = async (
  member: EligibleMemberListEntry,
  meeting: Meeting,
  attributes: UpdateAttendeeAttributes
) => {
  await Meetings.updateAttendee(meeting.id, member.id, attributes)
  // TODO refactor this
  // getMeetingAttendees()
}

const removeAttendeeClicked = async (
  member: EligibleMemberListEntry,
  meeting: Meeting
) => {
  if (meeting != null) {
    const meetingName = meeting.name
    if (
      confirm(
        `Are you sure you want to remove ${member.name} from the list of attendees for ${meetingName}?`
      )
    ) {
      await Meetings.removeAttendee(meeting.id, member.id)
      // this.getMeetingAttendees()
    }
  }
}

interface AttendeeEntryProps {
  attendee: EligibleMemberListEntry
  meeting: Meeting
}

const AttendeeEntry: React.FC<AttendeeEntryProps> = ({ attendee, meeting }) => (
  <div className="attendee-entry">
    <span>{attendee.name}</span>
    <Button
      className="attendee-remove"
      onClick={(e) => removeAttendeeClicked(attendee, meeting)}
      variant="danger"
    >
      remove
    </Button>
    {meeting != null && (
      <Button
        className="attendee-update-eligibility-to-vote"
        onClick={(e) =>
          updateAttendeeClicked(attendee, meeting, {
            update_eligibility_to_vote: true,
          })
        }
        variant="outline-info"
      >
        mark as eligible to vote
      </Button>
    )}
    {isGeneralMeeting(meeting) ? (
      attendee.eligibility ? (
        <AttendeeEligibilityRow
          isPersonallyEligible={attendee.eligibility.is_eligible}
          numVotes={
            attendee.eligibility.num_votes ||
            // if num_votes was not passed, assume is_eligible counts as 1
            attendee.eligibility.is_eligible
              ? 1
              : 0
          }
        />
      ) : (
        <EmptyEligibilityRow />
      )
    ) : null}
  </div>
)

interface AttendeesListProps {
  attendees: EligibleMemberListEntry[] | null
  meeting: Meeting | null
}

const AttendeesList: React.FC<AttendeesListProps> = ({
  attendees,
  meeting,
}) => {
  if (attendees == null || meeting == null || attendees.length === 0) {
    return null
  }

  return (
    <section className="attendees-list">
      <h3>{'Attendees ' + (attendees ? '(' + attendees.length + ')' : '')}</h3>
      {isGeneralMeeting(meeting) && attendees != null ? (
        <small>
          <i>Eligible voters: {countEligibleAttendees(attendees)}</i>
          <br />
          <i>
            Number of proxy votes: {countProxyVotes(attendees)} (already
            included in Eligible voter count)
          </i>
        </small>
      ) : null}
      {attendees.map((attendee) => {
        return <AttendeeEntry attendee={attendee} meeting={meeting} />
      })}
    </section>
  )
}

export default connect<MeetingAdminStateProps, null, {}, RootReducer>(
  (state) => state
)(MeetingAdmin)
