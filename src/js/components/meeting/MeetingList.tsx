import { find } from 'lodash'
import { compact } from 'lodash/fp'
import React, { Component, useMemo } from 'react'
import { Table } from 'react-bootstrap'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Column, useTable } from 'react-table'
import { bindActionCreators, Dispatch } from 'redux'
import { CommitteesState } from 'src/js/redux/reducers/committeeReducers'
import { MemberState } from 'src/js/redux/reducers/memberReducers'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { intersection } from 'src/js/util/setOperations'
import { Meeting } from '../../client/MeetingClient'
import { fetchCommittees } from '../../redux/actions/committeeActions'
import {
  claimMeetingCode,
  fetchAllMeetings,
} from '../../redux/actions/meetingActions'
import { isAdmin, isCommitteeAdmin } from '../../services/members'

type MeetingListPassedProps = {
  committeeFilter: string | null
  ownerFilter: string | null
  sortLatestFirst: boolean
}

type MeetingListProps = MeetingListDispatchProps &
  MeetingListStateProps &
  MeetingListPassedProps

class MeetingList extends Component<MeetingListProps> {
  componentWillMount() {
    this.props.fetchAllMeetings()
    this.props.fetchCommittees()
  }

  render() {
    const meetings = (this.props.meetings.byId as Meeting[]).sort(
      (meeting: Meeting) => -meeting.id || 0
    )
    const activeRoles = new Set(
      (this.props.member.user.data?.roles || [])
        .filter((role) => role.role === 'active')
        .map((role) => role.committee_id)
    )
    const filteredMeetings = meetings
      .filter((meeting) => {
        const committeeId = meeting.committee_id || 'general'
        const committee = this.props.committees.byId[committeeId] || {}
        const canAdmin =
          isAdmin(this.props.member) ||
          isCommitteeAdmin(this.props.member, committee.name)
        const hasActiveRole =
          committeeId === 'general' ||
          intersection(activeRoles, new Set([committeeId])).size
        if (!(canAdmin || hasActiveRole)) {
          return false
        }
        if (
          this.props.committeeFilter &&
          this.props.committeeFilter !== `${committeeId}`
        ) {
          return false
        }
        const owner = meeting.owner
        if (this.props.ownerFilter && this.props.ownerFilter !== owner) {
          return false
        }
        return true
      })
      .sort((a, b) => {
        if (a.start_time != null && b.start_time != null) {
          return a.start_time.valueOf() - b.start_time.valueOf()
        } else if (a.start_time == null && b.start_time != null) {
          return b.start_time.valueOf()
        } else {
          return a.start_time!.valueOf()
        }
      })
    const sortedFilteredMeetings = this.props.sortLatestFirst
      ? filteredMeetings.reverse()
      : filteredMeetings

    return (
      <div>
        {[...sortedFilteredMeetings].length ? (
          <MeetingsTable
            meetings={sortedFilteredMeetings}
            committees={this.props.committees}
            member={this.props.member}
          />
        ) : (
          'No meetings'
        )}
      </div>
    )
  }
}

interface MeetingsTableProps {
  meetings: Meeting[]
  committees: CommitteesState
  member: MemberState
}

const MeetingsTable: React.FC<MeetingsTableProps> = ({
  meetings,
  committees,
  member,
}) => {
  const memberIsAdmin = isAdmin(member)
  const meetingsAsJs = meetings

  const canEditMeeting = (meeting) => {
    if (memberIsAdmin) {
      return true
    }
    const committeeId = meeting.committee_id
    const committee = committees.byId[committeeId] || {}
    return isCommitteeAdmin(member, committee.name)
  }
  const canEditAny = find(meetingsAsJs, canEditMeeting)

  const displayProxyLink = (meeting) =>
    meeting.end_time && new Date() < meeting.end_time
  const displayAnyProxyLinks = find(meetingsAsJs, displayProxyLink)

  const columns = useMemo<Column<Meeting>[]>(
    () =>
      compact([
        {
          Header: 'Name',
          id: 'name',
          accessor: (meeting) => meeting.name,
          Cell: ({ row: { original } }) => (
            <Link to={`/meetings/${original.id}/details`}>{original.name}</Link>
          ),
        },
        {
          Header: 'Time',
          id: 'time',
          accessor: (meeting) => {
            const startString = meeting.start_time?.toLocaleString(undefined, {
              year: 'numeric',
              month: 'short',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric',
            })
            const startDateString = meeting.start_time?.toDateString()
            const endDateString = meeting.end_time?.toDateString()
            const endOptions = {
              year: startDateString === endDateString ? undefined : 'numeric',
              month: startDateString === endDateString ? undefined : 'short',
              day: startDateString === endDateString ? undefined : 'numeric',
              hour: 'numeric',
              minute: 'numeric',
            }
            const endString = meeting.end_time?.toLocaleString(
              undefined,
              endOptions
            )
            if (!startString) {
              return ''
            } else if (!endString) {
              return startString
            }
            return `${startString} - ${endString}`
          },
        },
        displayAnyProxyLinks && {
          Header: 'Nominate a Proxy',
          id: 'nominate',
          accessor: displayProxyLink,
          Cell: ({ row: { original, values } }) =>
            values.nominate ? (
              <Link to={`/meetings/${original.id}/proxy-token`}>Nominate</Link>
            ) : null,
        },
        canEditAny && {
          Header: 'Edit',
          id: 'edit',
          accessor: canEditMeeting,
          Cell: ({ row: { original, values } }) =>
            values.edit ? (
              <Link to={`/meetings/${original.id}/admin`}>Admin</Link>
            ) : null,
        },
      ]),
    []
  )

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({
      columns,
      data: meetings,
    })

  return (
    <Table
      striped
      bordered
      hover
      {...getTableProps()}
      className="meetings-table"
    >
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </Table>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      claimMeetingCode,
      fetchAllMeetings,
      fetchCommittees,
    },
    dispatch
  )

type MeetingListStateProps = RootReducer
type MeetingListDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MeetingListStateProps,
  MeetingListDispatchProps,
  {},
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(MeetingList)
