import React, { useEffect } from 'react'
import { Helmet } from 'react-helmet'
import { useDispatch, useSelector } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import Loading from 'src/js/components/common/Loading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { fetchAllMeetings } from '../../redux/actions/meetingActions'
import MeetingDetail from './MeetingDetail'

interface MeetingDetailParamProps {
  meetingId: string
}
interface MeetingDetailRouteParamProps {}
type MeetingDetailProps = RouteComponentProps<
  MeetingDetailParamProps,
  MeetingDetailRouteParamProps
>

const MeetingDetailPage: React.FC<MeetingDetailProps> = (props) => {
  const meetings = useSelector((state: RootReducer) => state.meetings)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchAllMeetings())
  }, [])

  if (!meetings) {
    return <Loading />
  }
  const meeting = meetings.byId[parseInt(props.params.meetingId, 10)]
  const meetingName = meeting.name

  return (
    <>
      <Helmet>
        <title>{meetingName}</title>
      </Helmet>
      <MeetingDetail meetingId={props.params.meetingId} meeting={meeting} />
    </>
  )
}

export default MeetingDetailPage
