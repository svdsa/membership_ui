import dateFormat from 'dateformat'
import { filter, sortBy } from 'lodash'
import { fromPairs } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, Container } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import {
  Meeting,
  MeetingInvitation,
  Meetings,
} from 'src/js/client/MeetingClient'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { fetchAllMeetings } from '../../redux/actions/meetingActions'
import { isMemberLoaded } from '../../services/members'
import Loading from '../common/Loading'
import PageHeading from '../common/PageHeading'

const MEETING_DETAIL_TIMEFORMAT = 'mmmm d, yyyy h:MM TT'

type MyMeetingsProps = MyMeetingsDispatchProps & MyMeetingsStateProps

interface MyMeetingsState {
  invitations: { [id: number]: MeetingInvitation } | null
  inSubmission: boolean
  showDeclinedMeetings: boolean
}

class MyMeetings extends Component<MyMeetingsProps, MyMeetingsState> {
  constructor(props) {
    super(props)
    this.state = {
      invitations: null,
      inSubmission: false,
      showDeclinedMeetings: false,
    }
  }

  componentDidMount() {
    this.props.fetchAllMeetings()
    this.fetchMyInvitations()
  }

  componentDidUpdate(prevProps: MyMeetingsProps) {
    const prevUser = prevProps.member.user.data
    const currentUser = this.props.member.user.data

    if (prevUser !== currentUser) {
      this.props.fetchAllMeetings()
      this.fetchMyInvitations()
    }
  }

  async fetchMyInvitations() {
    const invitations = (await Meetings.getMyInvitations()) || []
    const invitationsByMeetingId = fromPairs(
      invitations.map((i: MeetingInvitation) => [i.meeting_id, i])
    )
    this.setState({ invitations: invitationsByMeetingId })
  }

  async sendRsvp(meetingId, canAttend) {
    this.setState({ inSubmission: true })
    try {
      const invitation = await Meetings.sendRsvp(meetingId, canAttend)

      if (invitation != null) {
        const invitations = {
          ...(this.state.invitations || {}),
          [invitation.meeting_id]: invitation,
        }
        this.setState({ invitations: invitations })
      } else {
        throw new Error("Couldn't send RSVP")
      }
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  render() {
    if (!isMemberLoaded(this.props.member) || this.state.invitations === null) {
      return <Loading />
    }

    return (
      <Container>
        <Helmet>
          <title>My Meetings</title>
        </Helmet>
        <PageHeading level={1}>My Meetings</PageHeading>
        <div style={{ marginBottom: '2rem' }}>
          <h3>What meetings are coming up?</h3>
          {this.renderUpcoming()}
        </div>
        <div style={{ marginBottom: '2rem' }}>
          <h3>What meetings have I been invited to?</h3>
          {this.renderInvitations()}
        </div>
        <div style={{ marginBottom: '2rem' }}>
          <h3>What meetings have I attended?</h3>
          {this.renderAttendance()}
        </div>
        <div style={{ marginBottom: '2rem' }}>
          <h3>Where can I find the rest of the meetings?</h3>
          <p>
            Check out our <Link to={`/meetings`}>full list of meetings</Link>.
          </p>
        </div>
      </Container>
    )
  }

  renderAttendance() {
    const memberData = this.props.member.user.data

    if (memberData == null) {
      return <p>We couldn't look up your meeting attendance.</p>
    }

    const meetings = memberData.meetings.reverse().map((meeting) => {
      const meetingId = meeting.meeting_id
      const meetingName = meeting.name

      return (
        <div key={`meeting-${meetingId}`}>
          <a href={`meetings/${meetingId}/details`}>{meetingName}</a>
        </div>
      )
    })

    if (meetings.length === 0) {
      return (
        <p>
          You haven't attended any chapter meetings yet. To find our upcoming
          events, check out{' '}
          <a href={c('URL_CHAPTER_EVENTS')}>our events page</a>.
        </p>
      )
    } else {
      return (
        <div>
          <p>You've attended the following meetings so far:</p>
          {meetings}
        </div>
      )
    }
  }

  upcomingMeetings(): Meeting[] {
    const memberData = this.props.member.user.data
    if (memberData == null) {
      return []
    }

    const memberRoles = memberData.roles
    return sortBy(
      filter(this.props.meetings.byId, (meeting: Meeting) => {
        if (!meeting) return false

        // Only show meetings in the future
        const startTime = meeting.start_time
        if (!startTime || startTime.getTime() < Date.now()) {
          return false
        }

        // Only show published meetings
        if (!meeting.published) {
          return false
        }

        // If the user accepted the invite, show it here (declined meetings will be collapsed)
        const invitations = this.state.invitations
        const hasAcceptedInvite =
          invitations?.[meeting.id]?.status === 'ACCEPTED'
        if (hasAcceptedInvite) {
          return true
        }
        // If there's an invite but the user has declined or hasn't responded, don't show it here
        const hasUnacceptedInvite =
          invitations?.[meeting.id] &&
          invitations?.[meeting.id]?.status !== 'ACCEPTED'
        if (hasUnacceptedInvite) {
          return false
        }

        const committeeId = meeting.committee_id
        // If committee_id is null, this is a general meeting; show it
        if (committeeId === null) {
          return true
        }
        // Otherwise, show the meeting if it's for a committee that the user is a member of
        const forMyCommittees = memberRoles.find(
          (role) => role.committee_id === committeeId
        )
        if (forMyCommittees) {
          return true
        }

        return false
      }),
      (meeting: Meeting) => meeting.start_time
    )
  }

  unacceptedInvitesForOtherMeetings() {
    const upcomingMeetingsById = fromPairs(
      this.upcomingMeetings().map((m: Meeting) => [m.id, m])
    )
    return sortBy(
      filter(this.props.meetings.byId, (meeting: Meeting) => {
        if (!meeting) return false

        // Only show meetings in the future
        const startTime = meeting.start_time
        if (!startTime || startTime.getTime() < Date.now()) {
          return false
        }

        // Only show published meetings
        if (!meeting.published) {
          return false
        }

        // If this is shown in the "upcoming meetings" section, don't show it here
        if (upcomingMeetingsById[meeting.id]) {
          return false
        }

        // If the user has an invite, respect the provided "withAnInvite" argument
        const hasUnacceptedInvite =
          this.state.invitations?.[meeting.id] &&
          this.state.invitations?.[meeting.id]?.status !== 'ACCEPTED'

        return hasUnacceptedInvite || false
      }),
      (meeting: Meeting) => meeting.start_time
    )
  }

  renderInvitations() {
    const unacceptedInvitesForOtherMeetings =
      this.unacceptedInvitesForOtherMeetings()

    if (unacceptedInvitesForOtherMeetings.length === 0) {
      return <p>You haven't been invited to any other upcoming meetings.</p>
    }
    const noResponseMeetings = unacceptedInvitesForOtherMeetings
      .filter(
        (m: Meeting) => this.state.invitations?.[m.id]?.status === 'NO_RESPONSE'
      )
      .map((meeting: Meeting) => {
        const meetingId = meeting.id
        const meetingName = meeting.name
        const meetingStartTime = meeting.start_time

        return (
          <div key={`meeting-${meetingId}`} style={{ marginBottom: '1rem' }}>
            <a href={`meetings/${meetingId}/details`}>{meetingName}</a> -{' '}
            {dateFormat(meetingStartTime, MEETING_DETAIL_TIMEFORMAT)}{' '}
            <div style={{ display: 'inline-block' }}>
              <Button
                disabled={this.state.inSubmission}
                onClick={() => this.sendRsvp(meetingId, true)}
              >
                Attending
              </Button>{' '}
              <Button
                disabled={this.state.inSubmission}
                onClick={() => this.sendRsvp(meetingId, false)}
              >
                Not Attending
              </Button>
            </div>
          </div>
        )
      })

    const declinedMeetings = unacceptedInvitesForOtherMeetings.filter(
      (m: Meeting) => this.state.invitations?.[m.id]?.status === 'DECLINED'
    )
    const declinedMeetingRows =
      declinedMeetings.length > 0 ? (
        this.state.showDeclinedMeetings ? (
          <div>
            {noResponseMeetings.length > 0 ? (
              <hr style={{ marginTop: '1rem' }} />
            ) : null}
            <Button
              onClick={() => this.setState({ showDeclinedMeetings: false })}
              style={{ marginBottom: '1rem' }}
            >
              Hide Declined Meetings
            </Button>
            {declinedMeetings.map((meeting: Meeting) => {
              const meetingId = meeting.id
              const meetingName = meeting.name
              const meetingStartTime = meeting.start_time

              return (
                <div
                  key={`meeting-${meetingId}`}
                  style={{ marginBottom: '1rem' }}
                >
                  <a href={`meetings/${meetingId}/details`}>{meetingName}</a> -{' '}
                  {dateFormat(meetingStartTime, MEETING_DETAIL_TIMEFORMAT)}{' '}
                  <div style={{ display: 'inline-block' }}>
                    <Button
                      disabled={this.state.inSubmission}
                      onClick={() => this.sendRsvp(meetingId, true)}
                    >
                      Change Response to Attending
                    </Button>
                  </div>
                </div>
              )
            })}
          </div>
        ) : (
          <Button onClick={() => this.setState({ showDeclinedMeetings: true })}>
            Show Declined Meetings
          </Button>
        )
      ) : null

    return (
      <>
        {noResponseMeetings}
        {declinedMeetingRows}
      </>
    )
  }

  renderUpcoming() {
    const upcomingMeetings = this.upcomingMeetings()

    if (upcomingMeetings.length === 0) {
      return (
        <p>
          There are no scheduled meetings here. To find our public upcoming
          events, check out{' '}
          <a href={c('URL_CHAPTER_EVENTS')}>our events page</a>.
        </p>
      )
    }

    return upcomingMeetings.map((meeting: Meeting) => {
      const meetingId = meeting.id
      const meetingName = meeting.name
      const meetingStartTime = meeting.start_time

      return (
        <div key={`meeting-${meetingId}`}>
          <a href={`meetings/${meetingId}/details`}>{meetingName}</a> -{' '}
          {dateFormat(meetingStartTime, MEETING_DETAIL_TIMEFORMAT)}
        </div>
      )
    })
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchAllMeetings }, dispatch)

type MyMeetingsStateProps = RootReducer
type MyMeetingsDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MyMeetingsStateProps,
  MyMeetingsDispatchProps,
  Record<string, never>,
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(MyMeetings)
