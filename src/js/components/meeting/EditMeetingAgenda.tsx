import React, { useEffect, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Meeting, MeetingAgenda, Meetings } from 'src/js/client/MeetingClient'
import { logError } from 'src/js/util/util'
import Loading from '../common/Loading'
import SimpleMarkdownEditor from '../common/SimpleMarkdownEditor'

interface EditMeetingAgendaOwnProps {
  meeting: Meeting
}
type EditMeetingAgendaProps = EditMeetingAgendaOwnProps

const EditMeetingAgenda: React.FC<EditMeetingAgendaProps> = (props) => {
  const [agenda, setAgenda] = useState<MeetingAgenda | null>(null)
  const [inSubmission, setInSubmission] = useState(false)

  const getMeetingAgenda = async () => {
    try {
      const agendaResult = await Meetings.getAgenda(props.meeting.id)
      setAgenda(agendaResult)
    } catch (err) {
      return logError(`Error loading meeting agenda ${props.meeting.id}`, err)
    }
  }
  useEffect(() => {
    getMeetingAgenda()
  }, [])

  if (agenda === null) {
    return <Loading />
  }

  const saveMeetingAgenda = async (text) => {
    try {
      setInSubmission(true)
      const agendaResult = await Meetings.editAgenda(
        props.meeting.id,
        text,
        agenda.updated_at
      )
      setAgenda(agendaResult)
    } catch (err) {
      return console.error(
        `Error editing meeting agenda ${props.meeting.id}`,
        err
      )
    } finally {
      setInSubmission(false)
    }
  }

  return (
    <Container>
      <Row>
        <Col xs={12} lg={{ span: 10, offset: 1 }}>
          <SimpleMarkdownEditor
            initialValue={agenda.text}
            onClickSave={saveMeetingAgenda}
            buttonDisabled={inSubmission}
          />
        </Col>
      </Row>
    </Container>
  )
}

export default EditMeetingAgenda
