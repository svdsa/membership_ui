import { sortBy } from 'lodash/fp'
import React, { Component } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { ResolvedAsset } from 'src/js/client/AssetClient'
import Meetings, { Meeting } from 'src/js/client/MeetingClient'
import { EligibleMemberListEntry } from 'src/js/client/MemberClient'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import {
  countEligibleAttendees,
  countProxyVotes,
  isGeneralMeeting,
} from 'src/js/services/meetings'
import { searchAssets } from '../../redux/actions/assetActions'
import { fetchCommittees } from '../../redux/actions/committeeActions'
import {
  deleteMeetingAsset,
  saveMeetingAsset,
} from '../../redux/actions/meetingActions'
import { isAdmin, isCommitteeAdmin } from '../../services/members'
import EditableAsset, { AssetForm, OnSaveHandler } from '../asset/EditableAsset'
import PageHeading from '../common/PageHeading'
import MeetingSummary from './MeetingSummary'

interface MeetingDetailOwnProps {
  meetingId: string
  meeting: Meeting | null
  hideSummary?: boolean
}

type MeetingDetailProps = MeetingDetailOwnProps &
  MeetingDetailStateProps &
  MeetingDetailDispatchProps

interface MeetingDetailState {
  attendees: EligibleMemberListEntry[] | null
  numAttendees: number | null
  numEligibleAttendees: number | null
  numVotesIncludingProxies: number | null
  numRsvpYes: number | null
  numRsvpNo: number | null
  numInvitedNoResponse: number | null
}

class MeetingDetail extends Component<MeetingDetailProps, MeetingDetailState> {
  meetingId: number
  meetingLabels: string[]
  assetPrefix: string

  constructor(props) {
    super(props)
    this.meetingId = parseInt(this.props.meetingId)
    this.meetingLabels = [`meeting_id!${this.meetingId}`]
    this.assetPrefix = `meeting/${this.meetingId}/`
    this.state = {
      attendees: null,
      numAttendees: null,
      numEligibleAttendees: null,
      numVotesIncludingProxies: null,
      numRsvpYes: null,
      numRsvpNo: null,
      numInvitedNoResponse: null,
    }
  }

  componentWillMount() {
    this.props.fetchCommittees()
    this.props.searchAssets({ labels: this.meetingLabels })
    this.getMeetingAttendees()
    this.getMeetingInvitations()
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.meeting == null &&
      this.props.meeting != null &&
      this.state.attendees !== null
    ) {
      if (isGeneralMeeting(this.props.meeting)) {
        this.setState({
          numEligibleAttendees: countEligibleAttendees(this.state.attendees),
          numVotesIncludingProxies: countProxyVotes(this.state.attendees),
        })
      }
    }
  }

  async getMeetingAttendees() {
    const results = await Meetings.getAttendees(this.meetingId)
    if (!results) {
      return
    }
    this.setState({
      attendees: results,
      numAttendees: results.length,
    })
    if (this.props.meeting != null && isGeneralMeeting(this.props.meeting)) {
      this.setState({
        numEligibleAttendees: countEligibleAttendees(results),
        numVotesIncludingProxies: countProxyVotes(results),
      })
    }
  }

  async getMeetingInvitations() {
    const results = await Meetings.getInvitatitons(this.meetingId)
    if (!results) {
      return
    }
    let numRsvpYes = 0,
      numRsvpNo = 0,
      numInvitedNoResponse = 0
    results?.forEach((invitation) => {
      switch (invitation.status) {
        case 'ACCEPTED':
          numRsvpYes++
          break
        case 'DECLINED':
          numRsvpNo++
          break
        case 'NO_RESPONSE':
        default:
          numInvitedNoResponse++
          break
      }
    })
    this.setState({ numRsvpYes, numRsvpNo, numInvitedNoResponse })
  }

  renderInvitationsAndAttendees() {
    if (
      this.props.meeting == null ||
      (this.state.numAttendees === null && this.state.numRsvpYes === null)
    ) {
      return <div style={{ margin: '2rem 0' }}>Loading...</div>
    }
    return (
      <div style={{ margin: '2rem 0' }}>
        {this.state.numRsvpYes !== null ? (
          <>
            <p>
              <b>RSVP yes:</b> {this.state.numRsvpYes}
            </p>
            <p>
              <b>Invited, no response:</b> {this.state.numInvitedNoResponse}
            </p>
          </>
        ) : null}
        {this.state.numAttendees !== null ? (
          <>
            <p>
              <b>Attendees:</b> {this.state.numAttendees}
            </p>
            {isGeneralMeeting(this.props.meeting) ? (
              <>
                <p>
                  <b>Eligible voters:</b> {this.state.numEligibleAttendees}
                </p>
                <p>
                  <b>Number of proxy votes:</b>{' '}
                  {this.state.numVotesIncludingProxies} (already included in
                  Eligible voter count)
                </p>
              </>
            ) : null}
          </>
        ) : null}
      </div>
    )
  }

  createMeetingAsset = async (asset: AssetForm, file: File) => {
    const request: AssetForm = {
      ...asset,
      labels: this.meetingLabels,
    }
    const response = await this.props.saveMeetingAsset(request, file)
    if (response != null) {
      return response
    } else {
      throw new Error("Couldn't create meeting asset")
    }
  }

  updateMeetingAsset = async (asset: AssetForm, file: File) => {
    const request = { ...asset, labels: this.meetingLabels }
    await this.props.saveMeetingAsset(request, file)
    return asset
  }

  canEdit = () => {
    const meeting = this.props.meeting
    if (meeting == null) {
      return false
    }

    const committeeId = meeting.committee_id
    if (committeeId == null) {
      return false
    }

    const committee: string = this.props.committees.byId[committeeId].name

    return (
      isAdmin(this.props.member) ||
      isCommitteeAdmin(this.props.member, [committee])
    )
  }

  renderExistingAsset = (asset: ResolvedAsset, onSave?: OnSaveHandler) => {
    const assetId = asset.id
    const assetType = asset.content_type
    return assetId != null ? (
      <div key={`asset-${assetId}`} style={{ margin: '2rem 0' }}>
        <EditableAsset
          asset={asset}
          alt={`${assetType} #${assetId} of meeting #${this.meetingId}`}
          prefix={this.assetPrefix}
          labels={this.meetingLabels}
          editable={this.canEdit()}
          onSave={onSave}
          onDelete={(asset) => this.props.deleteMeetingAsset(assetId)}
        />
      </div>
    ) : null
  }

  renderContent() {
    if (this.props.assets == null) {
      return null
    }

    const sortByAssetId = sortBy<ResolvedAsset>((a) => a.id)
    const sortedAssets = sortByAssetId(Object.values(this.props.assets.byId))

    const videos = sortedAssets
      .filter((asset) => asset.content_type === 'video')
      .map((asset) => this.renderExistingAsset(asset))

    const audio = sortedAssets
      .filter((asset) => asset.content_type === 'audio')
      .map((asset) => this.renderExistingAsset(asset))

    const images = sortedAssets
      .filter((asset) => asset.content_type === 'image')
      .map((asset) => this.renderExistingAsset(asset))

    const editContent = (
      <div style={{ marginBottom: '4rem' }}>
        <PageHeading level={3}>Add Content</PageHeading>
        <EditableAsset
          prefix={this.assetPrefix}
          labels={this.meetingLabels}
          onSave={this.createMeetingAsset}
          onDelete={(asset) => this.props.deleteMeetingAsset(asset.id)}
        />
      </div>
    )

    const viewContent = (
      <div>
        <PageHeading level={2}>Content</PageHeading>
        <p>
          As part of our meetings, we aim to always inform members if they may
          be photographed or filmed as well as how as that content will be used.
          To that end, please be mindful before sharing content. Always request
          permission from the subject(s) of a photo or video before sharing it
          externally.
        </p>
        <p>
          Check out our{' '}
          <a href={c('URL_CHAPTER_CODE_OF_CONDUCT')}>Code of Conduct</a> for
          more information.
        </p>
        {videos.length === 0 ? null : (
          <>
            <h3>Video</h3>
            <div style={{ marginTop: '-1rem' }}>{videos}</div>
          </>
        )}
        {audio.length === 0 ? null : (
          <>
            <h3>Audio</h3>
            <div style={{ marginTop: '-1rem' }}>{audio}</div>
          </>
        )}
        {images.length === 0 ? null : (
          <>
            <h3>Images</h3>
            <div style={{ marginTop: '-1rem' }}>{images}</div>
          </>
        )}
      </div>
    )

    const emptyContent = (
      <section className="content-list empty-content-list non-ideal-content-list">
        <PageHeading level={2}>Content</PageHeading>
        <PageHeading level={3}>😿</PageHeading>
        <p>
          Nobody has uploaded content for this meeting yet.
          <br />
          Be the change you want to see in the world!
        </p>
      </section>
    )

    return (
      <div style={this.props.hideSummary ? { marginTop: '-20px' } : {}}>
        {Object.keys(this.props.assets.byId).length === 0
          ? emptyContent
          : viewContent}
        {this.canEdit() && editContent}
      </div>
    )
  }

  render() {
    const meeting = this.props.meeting
    if (meeting == null) {
      return <span>Loading...</span>
    }
    const committeeName =
      meeting.committee_id != null
        ? this.props.committees.byId[meeting.committee_id].name
        : null

    return (
      <Container>
        <Row>
          <Col sm={8} lg={6}>
            {this.props.hideSummary ? null : (
              <MeetingSummary
                meeting={meeting}
                committeeName={committeeName}
                showAdmin={this.canEdit()}
              />
            )}
            {this.renderInvitationsAndAttendees()}
            {this.renderContent()}
          </Col>
        </Row>
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      deleteMeetingAsset,
      fetchCommittees,
      saveMeetingAsset,
      searchAssets,
    },
    dispatch
  )

type MeetingDetailStateProps = RootReducer
type MeetingDetailDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MeetingDetailStateProps,
  MeetingDetailDispatchProps,
  MeetingDetailOwnProps,
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(MeetingDetail)
