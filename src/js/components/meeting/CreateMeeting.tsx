import { capitalize, filter } from 'lodash/fp'
import moment from 'moment'
import React, { Component } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import { CommitteeDetailed } from 'src/js/client/CommitteeClient'
import { Meeting } from 'src/js/client/MeetingClient'
import { mapCommitteesToName } from 'src/js/components/committee/utils'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import * as committees from '../../redux/actions/committeeActions'
import * as meetings from '../../redux/actions/meetingActions'
import { isAdmin, isCommitteeAdmin } from '../../services/members'
import FieldGroup from '../common/FieldGroup'
import PageHeading from '../common/PageHeading'

interface CreateMeetingParamProps {}
interface CreateMeetingRouteParamProps {}

type CreateMeetingOtherProps = RouteComponentProps<
  CreateMeetingParamProps,
  CreateMeetingRouteParamProps
>
type CreateMeetingProps = CreateMeetingStateProps &
  CreateMeetingDispatchProps &
  CreateMeetingOtherProps

interface CreateMeetingState {
  newMeeting: Meeting
}

const DEFAULT_GENERAL_COMMITTEE = 'General (no committee)'

class CreateMeeting extends Component<CreateMeetingProps, CreateMeetingState> {
  constructor(props) {
    super(props)
    this.state = {
      newMeeting: {
        id: 0,
        name: '',
        committee_id: null,
        landing_url: '',
        code: null,
        owner: null,
        published: true,
        start_time: moment().minute(0).add(1, 'hour').toDate(),
        end_time: moment().minute(0).add(2, 'hour').toDate(),
      },
    }
  }

  componentWillMount() {
    this.props.fetchCommittees()
  }

  canCreateMeeting() {
    return (
      this.state.newMeeting.name.length > 0 &&
        !this.props.meetings.form.create.inSubmission,
      false
    )
  }

  createMeeting: React.MouseEventHandler<HTMLButtonElement> = () => {
    this.props.createMeeting(this.state.newMeeting).then(() => {
      history.back()
    })
  }

  render() {
    const isGeneralAdmin = isAdmin(this.props.member)
    if (!isGeneralAdmin && !isCommitteeAdmin(this.props.member)) {
      this.props.router.replace('/')
      return null
    }

    const committeeAdminIds = new Set(
      (this.props.member.user.data?.roles || [])
        .filter((role) => role.role === 'admin' && role.committee_id)
        .map((role) => role.committee_id)
    )

    let committeesById = this.props.committees.byId
    if (!isGeneralAdmin) {
      const filterByAdmin = filter<CommitteeDetailed>((committee) =>
        committeeAdminIds.has(committee.id)
      )
      committeesById = filterByAdmin(committeesById)
    }
    const committeeIdsByName = {
      ...mapCommitteesToName(committeesById),
      [0]: DEFAULT_GENERAL_COMMITTEE,
    }

    const newMeetingCommitteeId = this.state.newMeeting.committee_id
    const newMeetingCommitteeName =
      newMeetingCommitteeId != null
        ? committeeIdsByName[newMeetingCommitteeId]
        : committeeIdsByName[0] || DEFAULT_GENERAL_COMMITTEE

    return (
      <Container className="create-meeting-page">
        <Row>
          <Col sm={8} lg={6}>
            <PageHeading level={2}>Create Meeting</PageHeading>
            <Form onSubmit={(e) => e.preventDefault()}>
              <FieldGroup
                formKey="name"
                componentClass="input"
                type="text"
                label="Meeting name"
                value={this.state.newMeeting.name}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: { ...this.state.newMeeting, [formKey]: value },
                  })
                }
                required
              />
              <FieldGroup
                formKey="committee_id"
                componentClass="select"
                options={committeeIdsByName}
                label={capitalize(c('GROUP_NAME_SINGULAR'))}
                value={this.mapCommitteeIdToSelectValue(
                  newMeetingCommitteeId
                ).toString()}
                onFormValueChange={(formKey, value) => {
                  this.setState({
                    newMeeting: {
                      ...this.state.newMeeting,
                      [formKey]: this.mapSelectValueToCommitteeId(value),
                    },
                  })
                }}
              />
              <FieldGroup
                formKey="landing_url"
                componentClass="input"
                type="text"
                label="Landing URL"
                helptext="An important link for your meeting. Examples include agenda docs or handout links."
                value={this.state.newMeeting.landing_url || ''}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: { ...this.state.newMeeting, [formKey]: value },
                  })
                }
              />
              <FieldGroup
                formKey="start_time"
                componentClass="datetime"
                label="Start Time"
                value={this.state.newMeeting.start_time}
                onFormValueChange={(formKey, value) =>
                  this.setState({
                    newMeeting: { ...this.state.newMeeting, [formKey]: value },
                  })
                }
              />
              <FieldGroup
                formKey="end_time"
                componentClass="datetime"
                label="End Time"
                value={this.state.newMeeting.end_time}
                onFormValueChange={(formKey, value) => {
                  const startTime = this.state.newMeeting.start_time
                  if (
                    startTime != null &&
                    moment(value).isBefore(moment(startTime))
                  ) {
                    alert(
                      'The end time of the meeting must not be before the start time!'
                    )
                  } else {
                    this.setState({
                      newMeeting: {
                        ...this.state.newMeeting,
                        [formKey]: value,
                      },
                    })
                  }
                }}
              />
              <FieldGroup
                formKey="published"
                componentClass="select"
                options={{ true: 'Published', false: 'Unpublished' }}
                label="Published"
                value={(this.state.newMeeting.published || true).toString()}
                onFormValueChange={(formKey, value) => {
                  this.setState({
                    newMeeting: {
                      ...this.state.newMeeting,
                      [formKey]: value === 'true',
                    },
                  })
                }}
              />
              <Button
                type="submit"
                disabled={!this.canCreateMeeting()}
                onClick={this.createMeeting}
              >
                Create Meeting
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }

  mapCommitteeIdToSelectValue(newMeetingCommitteeId: number | null): number {
    if (newMeetingCommitteeId == null) {
      return 0
    } else {
      return newMeetingCommitteeId
    }
  }

  mapSelectValueToCommitteeId(selectValue: string): number | null {
    const parsed = parseInt(selectValue)
    if (parsed <= 0) {
      return null
    } else {
      return parsed
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      createMeeting: meetings.createMeeting,
      fetchCommittees: committees.fetchCommittees,
    },
    dispatch
  )

type CreateMeetingStateProps = RootReducer
type CreateMeetingDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  CreateMeetingStateProps,
  CreateMeetingDispatchProps,
  {},
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(CreateMeeting)
