import React, { Component } from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import {
  Meeting,
  MeetingInvitation,
  Meetings,
} from 'src/js/client/MeetingClient'
import { fetchCommittees } from 'src/js/redux/actions/committeeActions'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import Loading from '../common/Loading'
import MeetingSummary from './MeetingSummary'

interface MeetingRsvpParamProps {}
interface MeetingRsvpRouteParamProps {
  meetingId: string
  token: string
}
type MeetingRsvpOwnProps = RouteComponentProps<
  MeetingRsvpParamProps,
  MeetingRsvpRouteParamProps
>
type MeetingRsvpProps = MeetingRsvpOwnProps &
  MeetingRsvpStateProps &
  MeetingRsvpDispatchProps

interface MeetingRsvpState {
  meeting: Meeting | null
  inSubmission: boolean
  invitation: MeetingInvitation | null
  invitationFailed: boolean
}

class MeetingRsvp extends Component<MeetingRsvpProps, MeetingRsvpState> {
  meetingId: number

  constructor(props: MeetingRsvpProps) {
    super(props)
    this.state = {
      meeting: null,
      inSubmission: false,
      invitation: null,
      invitationFailed: false,
    }
    this.meetingId = parseInt(this.props.params.meetingId, 10)
  }

  componentWillMount() {
    this.fetchInvitation()
    this.fetchMeeting()
    this.props.fetchCommittees()
  }

  async fetchInvitation() {
    try {
      const invitation = await Meetings.getInvitation(this.meetingId)

      if (invitation != null) {
        this.setState({ invitation: invitation })
      } else {
        throw new Error("Couldn't load invitation")
      }
    } catch (err) {
      this.setState({ invitationFailed: true })
    }
  }

  async fetchMeeting() {
    const meeting = await Meetings.get(this.meetingId)

    if (meeting != null) {
      this.setState({ meeting: meeting })
    } else {
      throw new Error("Couldn't load meeting")
    }
  }

  async sendRsvp(canAttend) {
    this.setState({ inSubmission: true })
    try {
      const invitation = await Meetings.sendRsvp(this.meetingId, canAttend)

      if (invitation != null) {
        this.setState({ invitation: invitation })
      } else {
        throw new Error("Couldn't send RSVP")
      }
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  render() {
    if (this.state.invitationFailed) {
      return (
        <Container>
          <Row>
            <Col xs={12}>
              <p>
                We were not able to find that invitation. Please check the link
                and try again, or contact tech support.
              </p>
            </Col>
          </Row>
        </Container>
      )
    }

    const invitation = this.state.invitation
    const meeting = this.state.meeting
    const committeeName =
      meeting != null && meeting.committee_id != null
        ? this.props.committees.byId[meeting.committee_id].name
        : null

    return (
      <Container>
        <Row>
          <Col sm={8} lg={6}>
            {meeting ? (
              <MeetingSummary meeting={meeting} committeeName={committeeName} />
            ) : (
              <Loading />
            )}
          </Col>
        </Row>
        <Row>
          {invitation && invitation.status !== 'NO_RESPONSE' ? (
            <Col xs={12}>
              <p>
                You've replied saying you{' '}
                {invitation.status === 'ACCEPTED' ? 'can' : 'cannot'} attend.
                <br />
                You can change your response if you'd like.
              </p>
            </Col>
          ) : null}
          <Col xs={12}>Can you attend?</Col>
          <Col xs={6}>
            {this.state.inSubmission ? (
              <Loading />
            ) : (
              <>
                <Button onClick={() => this.sendRsvp(true)}>Yes</Button>{' '}
                <Button onClick={() => this.sendRsvp(false)}>No</Button>
              </>
            )}
          </Col>
        </Row>
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchCommittees }, dispatch)

type MeetingRsvpStateProps = RootReducer
type MeetingRsvpDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MeetingRsvpStateProps,
  MeetingRsvpDispatchProps,
  MeetingRsvpProps,
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(MeetingRsvp)
