import { get } from 'lodash'
import React, { Component, ReactNode, useState } from 'react'
import { Alert, Button, Card, Col, Container, Form, Row } from 'react-bootstrap'
import { IoIosCheckmarkCircle } from 'react-icons/io'
import { connect } from 'react-redux'
import Meetings, { Meeting } from 'src/js/client/MeetingClient'
import LabeledSwitch from 'src/js/components/common/LabeledSwitch'
import Loading from 'src/js/components/common/Loading'
import PageHeading from 'src/js/components/common/PageHeading'
import { PronounSelect } from 'src/js/components/membership/EditPronouns'
import {
  bundlePronouns,
  renderDeclensions,
} from 'src/js/components/membership/pronounUtils'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { membershipApi } from '../../services/membership'
import { serviceLocator } from '../../util/serviceLocator'
import { HTTP_POST, logError } from '../../util/util'
import FieldGroup, { VALID_EMAIL } from '../common/FieldGroup'

interface AttendeeForm {
  first_name: string
  last_name: string
  email_address: string
  phone_number: string
  pronouns: string
  emergency_phone_number: string
  guardian_phone_number: string
  verified: boolean
}

const EMPTY_ATTENDEE: AttendeeForm = {
  first_name: '',
  last_name: '',
  email_address: '',
  phone_number: '',
  pronouns: '',
  emergency_phone_number: '',
  guardian_phone_number: '',
  verified: false,
}

type MeetingKioskStateProps = RootReducer
interface MeetingKioskOwnProps {
  meetingId: string
  meeting: Meeting | null
}
type MeetingKioskProps = MeetingKioskStateProps & MeetingKioskOwnProps

interface MeetingKioskState {
  meeting: Meeting | null
  attendee: AttendeeForm
  inSubmission: boolean
  error: Error | null
  path: KioskPath | null
  confirmed: boolean
}

class MeetingKiosk extends Component<MeetingKioskProps, MeetingKioskState> {
  constructor(props) {
    super(props)
    this.state = {
      meeting: props.meeting,
      attendee: EMPTY_ATTENDEE,
      path: null,
      inSubmission: false,
      error: null,
      confirmed: false,
    }
  }

  componentDidMount(): void {
    if (!this.state.meeting) {
      this.getMeeting()
    }
  }

  updateAttendee = <K extends keyof AttendeeForm>(
    formKey: K,
    value: AttendeeForm[K]
  ): void => {
    if (this.state.inSubmission) {
      return
    }

    this.setState({ attendee: { ...this.state.attendee, [formKey]: value } })
  }

  handleClose = () =>
    this.setState({
      confirmed: false,
      attendee: EMPTY_ATTENDEE,
      path: null,
    })

  handleSubmit: React.MouseEventHandler<HTMLButtonElement> = async (e) => {
    try {
      await this.submitForm(
        e,
        'attendee',
        `/meetings/${this.props.meetingId}/attendee`
      )

      this.setState({ confirmed: true })
      setTimeout(this.handleClose, 30000)
    } catch (err) {
      logError(`Error checking in to ${this.props.meetingId}`, err)
      this.setState({ error: err })
    }
  }

  choosePath = (path: KioskPath) =>
    this.setState({
      path,
      attendee: { ...this.state.attendee, verified: false },
    })

  resetPath = () => this.setState({ path: null, error: null })

  render(): ReactNode {
    if (this.state.meeting == null) {
      return <Loading />
    }

    return (
      <Container className="kiosk-screen">
        <Card className="kiosk-card">
          <Card.Header>
            <Row>
              <Col lg={6} md={12} className="meeting-title">
                <PageHeading level={2}>
                  Sign into {this.state.meeting.name}
                </PageHeading>
              </Col>
              <Col lg={6} md={12} className="meeting-info">
                <MeetingTimeDisplay
                  startTime={this.state.meeting.start_time}
                  endTime={this.state.meeting.end_time}
                />
              </Col>
            </Row>
          </Card.Header>
          <Card.Body>
            {!this.state.confirmed && this.state.path != null && (
              <div className="go-back">
                <Button variant="link" onClick={this.resetPath}>
                  {'<'} I'm a different kind of member
                </Button>
              </div>
            )}
            {this.state.confirmed && (
              <AttendMeetingConfirmation
                icon={this.state.path == 'new' ? '🎉' : '💌'}
                verificationRequired={true}
                onClose={this.handleClose}
              />
            )}
            {!this.state.confirmed && this.state.path == null && (
              <KioskPathPrompt onChoosePath={this.choosePath} />
            )}
            {this.state.error != null && (
              <Alert variant="danger">
                Couldn't sign you in:{' '}
                {get(
                  this.state.error,
                  ['response', 'body', 'err'],
                  this.state.error.toString()
                )}
              </Alert>
            )}
            {!this.state.confirmed && this.state.path === 'returning' && (
              <ReturningMemberSignInForm
                value={this.state.attendee}
                onChange={this.updateAttendee}
                onSubmit={this.handleSubmit}
              />
            )}
            {!this.state.confirmed && this.state.path === 'new' && (
              <NewMemberSignInForm
                value={this.state.attendee}
                onChange={this.updateAttendee}
                onSubmit={this.handleSubmit}
              />
            )}
          </Card.Body>
        </Card>
        <footer>
          <p>
            <em>
              We use your email address to check your meeting voting eligibility
              and to send you occasional information emails about upcoming DSA
              events.
            </em>
          </p>
          <p>
            <em>
              Your information's security is paramount to us. We do not share
              this information with anyone.
            </em>
          </p>
        </footer>
      </Container>
    )
  }

  async getMeeting() {
    try {
      const meeting = await Meetings.get(parseInt(this.props.meetingId))
      this.setState({ meeting })
    } catch (err) {
      return logError(`Error loading meeting ${this.props.meetingId}`, err)
    }
  }

  async submitForm(
    e: React.MouseEvent<HTMLButtonElement>,
    key: string,
    endpoint: string
  ) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true, error: null })
    try {
      await membershipApi(HTTP_POST, endpoint, this.state[key])
      this.setState({
        attendee: EMPTY_ATTENDEE,
      })
      serviceLocator.notificationSystem?.current?.addNotification({
        title: 'Welcome to the meeting!',
        level: 'success',
      })
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

type KioskPath = 'new' | 'returning'

interface KioskPathPromptProps {
  onChoosePath(path: KioskPath): void
}

const KioskPathPrompt: React.FC<KioskPathPromptProps> = ({ onChoosePath }) => (
  <section className="kiosk-path-prompt">
    <header>
      <PageHeading level={3}>Are you new to {c('CHAPTER_NAME')}?</PageHeading>
    </header>
    <Row className="path-prompt-buttons">
      <Col md={{ span: 5, offset: 1 }} sm={12}>
        <Button
          variant="outline-primary"
          onClick={() => onChoosePath('new')}
          block
        >
          <div className="path-button-icon">🐣</div>
          <div className="path-button-text">Yes, this is my first time</div>
          <div className="path-button-description">
            This is your first {c('CHAPTER_NAME')} meeting
          </div>
        </Button>
      </Col>
      <Col md={5} sm={12}>
        <Button
          variant="outline-primary"
          onClick={() => onChoosePath('returning')}
          block
        >
          <div className="path-button-icon">🌹</div>
          <div className="path-button-text">No, I'm returning</div>
          <div className="path-button-description">
            You've attended a {c('CHAPTER_NAME')} meeting before
          </div>
        </Button>
      </Col>
    </Row>
  </section>
)

interface NewMemberSignInFormProps {
  value: AttendeeForm
  onChange(formKey: keyof AttendeeForm, value: string): void
  onSubmit: React.MouseEventHandler<HTMLButtonElement>
}

const NewMemberSignInForm: React.FC<NewMemberSignInFormProps> = ({
  value,
  onChange,
  onSubmit,
}) => {
  return (
    <Row>
      <Col
        lg={{
          span: 6,
          offset: 3,
        }}
      >
        <Card className="new-member-form sign-in-form">
          <Card.Body>
            <header className="form-header">
              <PageHeading level={3}>🐣</PageHeading>
              <PageHeading level={3}>It's great to meet you!</PageHeading>
              <p>
                We'd love to stay in touch. Please tell us a little about
                yourself. We'll add you to our email list and call or text
                occasionally about events and volunteer opportunities.
              </p>
            </header>
            <FullSignInForm
              value={value}
              onChange={onChange}
              onSubmit={onSubmit}
            />
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}

interface ReturningMemberSignInProps extends FullSignInFormProps {}

const ReturningMemberSignInForm: React.FC<ReturningMemberSignInProps> = ({
  value,
  onChange,
  onSubmit,
}) => {
  const [showFullForm, setShowFullForm] = useState(false)

  const isInvalid =
    !value.email_address || !VALID_EMAIL.test(value.email_address)

  const toggleShowAllFields: React.MouseEventHandler<HTMLButtonElement> = (e) =>
    setShowFullForm(!showFullForm)

  return (
    <Row>
      <Col
        lg={{
          span: 6,
          offset: 3,
        }}
      >
        <Card className="returning-member-form sign-in-form">
          <Card.Body>
            <header className="form-header">
              <PageHeading level={3}>🌹</PageHeading>
              <PageHeading level={3}>Welcome back!</PageHeading>
              {showFullForm ? (
                <>
                  <p>Please fill in your updated information below.</p>
                  <p>
                    We use your email to update our records. If you need to
                    change your email, please ask for help.
                  </p>
                </>
              ) : (
                <p>
                  Sign in with your email address. You'll receive a confirmation
                  email with a link to confirm your attendance.
                </p>
              )}
            </header>
            {showFullForm ? (
              <FullSignInForm
                value={value}
                onChange={onChange}
                onSubmit={onSubmit}
              />
            ) : (
              <Form
                onSubmit={(e) => e.preventDefault()}
                className="sign-in-form"
              >
                <FieldGroup
                  formKey="email_address"
                  componentClass="input"
                  type="email"
                  title="Email address"
                  layout="row"
                  placeholder="Email address"
                  value={value.email_address}
                  onFormValueChange={onChange}
                  required
                />
                <Button
                  block
                  type="submit"
                  variant={isInvalid ? 'outline-primary' : 'primary'}
                  disabled={isInvalid}
                  onClick={onSubmit}
                >
                  Join meeting
                </Button>
              </Form>
            )}
          </Card.Body>
          <Card.Footer>
            <Button block variant="secondary" onClick={toggleShowAllFields}>
              {showFullForm
                ? 'Never mind, just join by email'
                : 'Need to update your contact or personal info?'}
            </Button>
          </Card.Footer>
        </Card>
      </Col>
    </Row>
  )
}

interface FullSignInFormProps {
  value: AttendeeForm
  onChange(formKey: keyof AttendeeForm, value: string): void
  onSubmit: React.MouseEventHandler<HTMLButtonElement>
}

const FullSignInForm: React.FC<FullSignInFormProps> = ({
  value,
  onChange,
  onSubmit,
}) => {
  const [showMinorContactForm, setShowMinorContactForm] = useState(false)

  const isInvalid =
    !value.email_address ||
    !VALID_EMAIL.test(value.email_address) ||
    !value.first_name ||
    !value.last_name

  return (
    <Form onSubmit={(e) => e.preventDefault()} className="sign-in-form">
      <FieldGroup
        formKey="email_address"
        componentClass="input"
        type="email"
        title="Email address"
        layout="row"
        placeholder="Email address"
        value={value.email_address}
        onFormValueChange={onChange}
        required
      />
      <FieldGroup
        formKey="phone_number"
        componentClass="input"
        type="tel"
        title="Valid 10 digit phone number"
        placeholder="Phone number (optional)"
        layout="row"
        value={value.phone_number}
        onFormValueChange={onChange}
      />
      <hr />
      <FieldGroup
        formKey="first_name"
        componentClass="input"
        type="text"
        placeholder="First name"
        layout="row"
        value={value.first_name}
        onFormValueChange={onChange}
        required
      />
      <FieldGroup
        formKey="last_name"
        componentClass="input"
        type="text"
        placeholder="Last name"
        layout="row"
        value={value.last_name}
        onFormValueChange={onChange}
        required
      />
      <PronounSelect
        pronouns={[]}
        placeholder="Select pronouns (optional)"
        onChange={(pronouns) =>
          onChange('pronouns', bundlePronouns(pronouns.map(renderDeclensions)))
        }
      />
      <hr />
      <Form.Label>
        <h5>Are you under 18?</h5>
        <span>
          We collect emergency contact information for minors attending our
          meetings.
        </span>
      </Form.Label>
      <LabeledSwitch
        value={showMinorContactForm}
        trueLabel="Yes"
        falseLabel="No"
        onChange={setShowMinorContactForm}
      />
      {showMinorContactForm && (
        <section className="minor-contact-form">
          <FieldGroup
            formKey="guardian_phone_number"
            componentClass="input"
            type="tel"
            title="Valid parent or guardian phone number"
            placeholder="Parent or guardian phone number"
            layout="row"
            value={value.guardian_phone_number}
            onFormValueChange={onChange}
            required={showMinorContactForm}
          />
          <FieldGroup
            formKey="emergency_phone_number"
            componentClass="input"
            type="tel"
            title="Valid emergency phone number"
            placeholder="Other emergency phone number (optional)"
            layout="row"
            value={value.emergency_phone_number}
            onFormValueChange={onChange}
          />
        </section>
      )}
      <hr />
      <Button
        block
        type="submit"
        variant={isInvalid ? 'outline-primary' : 'primary'}
        disabled={isInvalid}
        onClick={onSubmit}
      >
        Join meeting
      </Button>
    </Form>
  )
}

interface AttendMeetingConfirmationProps {
  icon: React.ReactChild
  verificationRequired: boolean
  onClose(): void
}

const AttendMeetingConfirmation: React.FC<AttendMeetingConfirmationProps> = ({
  icon,
  verificationRequired,
  onClose,
}) => (
  <Row>
    <Col md={{ span: 6, offset: 3 }}>
      <div className="attend-confirmation">
        <div className="attend-icon">{icon || <IoIosCheckmarkCircle />}</div>

        {verificationRequired ? (
          <>
            <div className="attend-text">Check your email!</div>
            <div className="attend-subtext">
              <p>We sent you an email to verify your attendance.</p>
              <p>
                If you don't receive an email, check your junk folder, or try
                signing in again with a different email.
              </p>
            </div>
          </>
        ) : (
          <>
            <div className="attend-text">Welcome to the meeting!</div>
          </>
        )}
        <Button
          size="lg"
          block
          variant="success"
          onClick={onClose}
          className="attend-button"
        >
          Got it
        </Button>
      </div>
    </Col>
  </Row>
)

interface MeetingTimeDisplayProps {
  startTime: Date | null
  endTime: Date | null
}

const MeetingTimeDisplay: React.FC<MeetingTimeDisplayProps> = ({
  startTime,
  endTime,
}) => {
  if (startTime == null || endTime == null) {
    return (
      <div className="meeting-time-display no-times">
        No time information available
      </div>
    )
  }

  if (startTime.toLocaleDateString() === endTime.toLocaleDateString()) {
    return (
      <div className="meeting-time-display same-dates">
        <div className="meeting-date">
          {startTime.toLocaleDateString([], {
            month: 'long',
            day: 'numeric',
            year: 'numeric',
          })}
        </div>
        <div className="meeting-times">
          <span className="meeting-start">
            🟢{' '}
            <strong>
              {startTime.toLocaleTimeString([], {
                hour: '2-digit',
                minute: '2-digit',
              })}
            </strong>
          </span>{' '}
          to{' '}
          <span className="meeting-end">
            🏁{' '}
            <strong>
              {endTime.toLocaleTimeString([], {
                hour: '2-digit',
                minute: '2-digit',
              })}
            </strong>
          </span>
        </div>
      </div>
    )
  } else {
    return (
      <div className="meeting-time-display different-dates">
        <div className="meeting-start">
          🟢{' '}
          {startTime.toLocaleString([], {
            month: 'long',
            day: 'numeric',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
          })}
        </div>
        <div className="meeting-end">
          to 🏁{' '}
          {endTime.toLocaleString([], {
            month: 'long',
            day: 'numeric',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
          })}
        </div>
      </div>
    )
  }
}

export default connect<MeetingKioskStateProps, null, {}, RootReducer>(
  (state) => state
)(MeetingKiosk)
