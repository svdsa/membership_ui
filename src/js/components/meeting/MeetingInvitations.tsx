import React, { Component } from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import Meetings, {
  Meeting,
  MeetingInvitation,
  MeetingInvitationStatus,
} from 'src/js/client/MeetingClient'
import { EligibleMemberListEntry } from 'src/js/client/MemberClient'
import Loading from 'src/js/components/common/Loading'
import { isGeneralMeeting } from 'src/js/services/meetings'
import { logError } from 'src/js/util/util'
import MemberSearchField from '../common/MemberSearchField'

interface MeetingInvitationsOwnProps {
  meeting: Meeting
}
type MeetingInvitationsProps = MeetingInvitationsOwnProps

interface MeetingInvitationsState {
  invitations: MeetingInvitation[] | null
  invitationMember: EligibleMemberListEntry | null
  inSubmission: boolean
}

class MeetingInvitations extends Component<
  MeetingInvitationsProps,
  MeetingInvitationsState
> {
  constructor(props) {
    super(props)
    this.state = {
      invitations: null,
      invitationMember: null,
      inSubmission: false,
    }
  }

  componentWillMount() {
    this.fetchInvitations()
  }

  async fetchInvitations() {
    const meetingId = this.props.meeting.id
    try {
      const invitations = await Meetings.getInvitatitons(meetingId)
      this.setState({ invitations })
    } catch (err) {
      return logError(`Error loading invitations for meeting ${meetingId}`, err)
    }
  }

  onMemberSelected(member: EligibleMemberListEntry) {
    this.setState({ invitationMember: member })
  }

  async sendInvitation() {
    if (this.state.inSubmission) {
      return
    }
    const memberId = this.state.invitationMember?.id
    if (memberId === null || memberId === undefined) {
      return
    }
    const meetingId = this.props.meeting.id
    try {
      this.setState({ inSubmission: true })
      const invitation = await Meetings.sendInvitation(meetingId, memberId)
      if (!invitation) {
        return
      }
      this.setState((currentState: MeetingInvitationsState) => ({
        invitations: [...(currentState.invitations || []), invitation],
        invitationMember: null,
      }))
    } catch (err) {
      return logError(
        `Error sending invitation to ${memberId} for meeting ${meetingId}`,
        err
      )
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  async bulkInviteCommitteeMembers() {
    if (this.state.inSubmission) {
      return
    }
    const meetingId = this.props.meeting.id
    try {
      this.setState({ inSubmission: true })
      const invitations = await Meetings.bulkInviteCommitteeMembers(meetingId)
      if (!invitations || !invitations.length) {
        return
      }
      this.setState((currentState: MeetingInvitationsState) => ({
        invitations: [...(currentState.invitations || []), ...invitations],
      }))
    } catch (err) {
      return logError(
        `Error sending bulk invitations for meeting ${meetingId}`,
        err
      )
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  stringForStatus(status: MeetingInvitationStatus): string {
    return {
      SEND_PENDING: 'Pending, will send when meeting published',
      NO_RESPONSE: 'No response',
      ACCEPTED: 'Accepted',
      DECLINED: 'Declined',
    }[status]
  }

  render() {
    const invitations = this.state.invitations
    if (invitations === null) {
      return <Loading />
    }
    return (
      <Container>
        <Row>
          <Col xs={12} style={{ marginTop: '1rem' }}>
            {invitations.length
              ? invitations.map((invitation) => (
                  <div key={invitation.id} style={{ marginBottom: '0.5rem' }}>
                    {invitation.name} ({invitation.email}):{' '}
                    {this.stringForStatus(invitation.status)}
                  </div>
                ))
              : 'No invitations sent yet'}
          </Col>
        </Row>
        <hr style={{ margin: '1.5rem 0' }} />
        {isGeneralMeeting(this.props.meeting) ? null : (
          <Row>
            <Col xs={12} style={{ marginBottom: '1rem' }}>
              Bulk invite all committee members:{' '}
              <Button
                disabled={this.state.inSubmission}
                onClick={() => this.bulkInviteCommitteeMembers()}
              >
                Send Invites
              </Button>
            </Col>
          </Row>
        )}
        <Row>
          <Col xs={12}>
            Send an individual invitation:{' '}
            {this.state.invitationMember ? (
              <div>
                {this.state.invitationMember.name} (
                {this.state.invitationMember.email}){' '}
                <Button
                  disabled={this.state.inSubmission}
                  onClick={() => this.sendInvitation()}
                >
                  Send
                </Button>
              </div>
            ) : this.state.inSubmission ? (
              <Loading />
            ) : (
              <MemberSearchField
                onMemberSelected={(e) => this.onMemberSelected(e)}
              />
            )}
          </Col>
        </Row>
      </Container>
    )
  }
}

export default MeetingInvitations
