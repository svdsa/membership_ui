import React, { Component } from 'react'
import { Button, Container, ListGroup, ListGroupItem } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { Meetings } from '../../client/MeetingClient'
import { isMemberLoaded } from '../../services/members'
import Loading from '../common/Loading'

type ProxyTokenCreateStateProps = RootReducer
interface ProxyTokenCreateParamProps {}
interface ProxyTokenCreateRouteParamProps {
  meetingId: number
}
type ProxyTokenCreateProps = ProxyTokenCreateStateProps &
  RouteComponentProps<
    ProxyTokenCreateParamProps,
    ProxyTokenCreateRouteParamProps
  >

interface ProxyTokenCreateState {
  loading: boolean
  meetingName: string | null
  token: number | null
  errorMessage: string | null
}

class ProxyTokenCreate extends Component<
  ProxyTokenCreateProps,
  ProxyTokenCreateState
> {
  copyableLinkRef = React.createRef<HTMLInputElement>()

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      meetingName: null,
      token: null,
      errorMessage: null,
    }
  }

  componentDidMount() {
    this.getMeeting()
  }

  async getMeeting() {
    const meeting = await Meetings.get(this.props.params.meetingId)

    if (meeting != null) {
      this.setState({ meetingName: meeting.name })
    }
  }

  createProxyToken = async () => {
    const meetingId = this.props.routeParams.meetingId
    try {
      this.setState({ loading: true })
      const result = await Meetings.createProxyToken(meetingId)
      this.setState({
        loading: false,
        token: result.proxy_token_id,
        errorMessage: null,
      })
    } catch (err) {
      let errorMessage = `Error creating proxy token for meeting ${meetingId}`
      try {
        errorMessage = err.error.response.body.err
      } catch (responseParseError) {}
      this.setState({ loading: false, errorMessage })
    }
  }

  render() {
    if (!isMemberLoaded(this.props.member)) {
      return <Loading />
    }

    const { meetingId } = this.props.routeParams
    const { loading, meetingName, token, errorMessage } = this.state

    return (
      <Container>
        <Helmet>
          <title>Nominate a proxy for {meetingName}</title>
        </Helmet>
        <h1>Nominate a Proxy Voter</h1>
        <p>
          Can't make {meetingName}, but still want your voice heard? Nominate
          someone to vote on your behalf!
        </p>
        <ListGroup>
          <ListGroupItem className="list-group-item">
            <strong>1. Generate a link</strong> with the button below. Make sure
            not to lose it!
          </ListGroupItem>
          <ListGroupItem className="list-group-item">
            <strong>2. Send it to your proxy</strong> along with instructions on
            how to cast your vote. Once they respond to your nomination, we'll
            let you know.
          </ListGroupItem>
          <ListGroupItem className="list-group-item">
            <strong>3. When they check in</strong> to the meeting, they'll be
            able vote on your behalf, as long as they accepted your proxy
            nomination. You'll still have an opportunity to participate in any
            online votes, or your proxy can request a paper ballot.
          </ListGroupItem>
        </ListGroup>
        {token ? (
          <div>
            <hr />
            <p>
              Link created! Send the link below to someone whom you've confirmed
              will be attending, and be sure to provide them with instructions,
              specific or general, on how to cast your vote. We won't be
              monitoring individual proxy votes, so we'll be assuming that your
              proxy is following your instructions.
            </p>
            <p>
              Once your nominated proxy visits the link, they can either accept
              or reject your nomination. We'll let you know when they do.
            </p>
            <div style={{ display: 'flex' }}>
              <input
                style={{ flex: '1 0 auto' }}
                type="text"
                value={`${
                  window.location.origin
                }/meetings/${meetingId}/proxy-token/${encodeURIComponent(
                  token
                )}`}
                onFocus={() => {
                  this.copyableLinkRef.current?.select()
                }}
                ref={this.copyableLinkRef}
              />
              <Button
                style={{ flex: '0 0 auto' }}
                onClick={() => {
                  this.copyableLinkRef.current?.select()
                  document.execCommand('copy')
                }}
              >
                Copy Link
              </Button>
            </div>
          </div>
        ) : loading ? (
          <Loading />
        ) : (
          <div>
            <Button onClick={this.createProxyToken}>Create Link</Button>
            {errorMessage ? <p>{errorMessage}</p> : null}
          </div>
        )}
      </Container>
    )
  }
}

export default connect<ProxyTokenCreateStateProps, null, {}, RootReducer>(
  (state) => state
)(ProxyTokenCreate)
