import dateFormat from 'dateformat'
import React from 'react'
import { Meeting } from 'src/js/client/MeetingClient'
import { c } from 'src/js/config'
import PageHeading from '../common/PageHeading'

const MEETING_SUMMARY_TIMEFORMAT = 'mmmm d, yyyy h:MM TT'

const GROUP_NAME_SINGULAR = c('GROUP_NAME_SINGULAR')

interface MeetingSummaryProps {
  meeting: Meeting
  committeeName?: string | null
  showAdmin?: boolean
}

export default (props: MeetingSummaryProps): React.ReactElement => {
  const meeting = props.meeting
  const meetingId = meeting.id
  const meetingName = meeting.name
  const committeeId = meeting.committee_id
  const meetingType = committeeId
    ? props.committeeName
      ? `${GROUP_NAME_SINGULAR} / ${props.committeeName}`
      : GROUP_NAME_SINGULAR
    : 'Chapter Meeting'
  const landingPage = meeting.landing_url
  const startTime = meeting.start_time
  const endTime = meeting.end_time

  return (
    <>
      <PageHeading level={1}>{meetingName}</PageHeading>
      <p>
        <b>Meeting Type:</b> {meetingType}
      </p>
      {startTime && (
        <p>
          <b>Start Time:</b> {dateFormat(startTime, MEETING_SUMMARY_TIMEFORMAT)}
        </p>
      )}
      {endTime && (
        <p>
          <b>End Time:</b> {dateFormat(endTime, MEETING_SUMMARY_TIMEFORMAT)}
        </p>
      )}
      <p>
        <b>Check-in:</b> <a href={`/meetings/${meetingId}/kiosk`}>Kiosk</a>
      </p>
      {landingPage && (
        <p>
          <b>Landing Page:</b> <a href={landingPage}>{landingPage}</a>
        </p>
      )}
      {props.showAdmin && (
        <p>
          <a href={`/meetings/${meetingId}/admin`}>Edit</a>
        </p>
      )}
    </>
  )
}
