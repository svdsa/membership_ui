import React from 'react'

interface VoteEligiblePopupProps {
  numVotes: number
  visible: boolean
}

const VoteEligiblePopup: React.FC<VoteEligiblePopupProps> = ({
  numVotes,
  visible,
}) => {
  const isEligible = numVotes > 0
  return (
    <div
      className={`meeting-voter-eligibility-popup ${
        visible ? 'is-activated' : ''
      } ${isEligible ? 'is-eligible' : ''}`}
    >
      {isEligible
        ? `👍 Voter is eligible, give ${numVotes} card${
            numVotes === 1 ? '' : 's'
          }`
        : '🚫 Voter is NOT eligible'}
    </div>
  )
}

export default VoteEligiblePopup
