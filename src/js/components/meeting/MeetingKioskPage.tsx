import React, { useEffect, useState } from 'react'
import { Helmet } from 'react-helmet'
import { RouteComponentProps } from 'react-router'
import Meetings, { Meeting } from 'src/js/client/MeetingClient'
import Loading from 'src/js/components/common/Loading'
import { logError } from '../../util/util'
import MeetingKiosk from './MeetingKiosk'

interface MeetingKioskPageParamProps {
  meetingId: string
}
interface MeetingKioskPageRouteParamProps {}
type MeetingKioskPageProps = RouteComponentProps<
  MeetingKioskPageParamProps,
  MeetingKioskPageRouteParamProps
>

const MeetingKioskPage: React.FC<MeetingKioskPageProps> = (props) => {
  const [meeting, setMeeting] = useState<Meeting | null>(null)

  const getMeeting = async () => {
    try {
      const meetingResult = await Meetings.get(
        parseInt(props.params.meetingId, 10)
      )
      setMeeting(meetingResult)
    } catch (err) {
      return logError(`Error loading meeting ${props.params.meetingId}`, err)
    }
  }
  useEffect(() => {
    getMeeting()
  }, [])

  if (meeting == null) {
    return <Loading />
  }

  return (
    <>
      <Helmet>
        <title>Sign into {meeting.name}</title>
      </Helmet>
      <MeetingKiosk meetingId={props.params.meetingId} meeting={meeting} />
    </>
  )
}

export default MeetingKioskPage
