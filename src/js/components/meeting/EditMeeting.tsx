import { capitalize, flow, mapValues, pick, pickBy } from 'lodash/fp'
import moment, { Moment } from 'moment'
import React, { Component } from 'react'
import { Button, Col, Form, FormControl, Row } from 'react-bootstrap'
import DateTime from 'react-datetime'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { CommitteeDetailed } from 'src/js/client/CommitteeClient'
import { Meeting, Meetings } from 'src/js/client/MeetingClient'
import { c } from 'src/js/config'
import * as committees from 'src/js/redux/actions/committeeActions'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { isAdmin } from 'src/js/services/members'

const DEFAULT_GENERAL_COMMITTEE = 'General (no committee)'

interface EditMeetingOwnProps {
  meeting: Meeting
}
type EditMeetingProps = EditMeetingOwnProps &
  RootReducer &
  EditMeetingDispatchProps
interface EditMeetingState {
  meeting: Meeting
  inSubmission: boolean
}

class EditMeeting extends Component<EditMeetingProps, EditMeetingState> {
  constructor(props) {
    super(props)
    this.state = {
      meeting: props.meeting,
      inSubmission: false,
    }
  }

  componentWillMount() {
    this.props.fetchCommittees()
  }

  updateName(name: string) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    this.setState({
      meeting: {
        ...this.state.meeting,
        name,
      },
    })
  }

  mapCommitteeIdToSelectValue(newMeetingCommitteeId: number | null): number {
    if (newMeetingCommitteeId == null) {
      return 0
    } else {
      return newMeetingCommitteeId
    }
  }

  updateCommitteeId(committeeIdString: string) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    const parsed = parseInt(committeeIdString)
    const committeeId = parsed <= 0 ? null : parsed
    this.setState({
      meeting: {
        ...this.state.meeting,
        committee_id: committeeId,
      },
    })
  }

  updateLandingUrl(landingUrl) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    this.setState({
      meeting: {
        ...this.state.meeting,
        landing_url: landingUrl,
      },
    })
  }

  updateStartTime(startTime: string | Moment) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    this.setState({
      meeting: {
        ...this.state.meeting,
        start_time: moment(startTime).toDate(),
      },
    })
  }

  updateEndTime(endTime: string | Moment) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    this.setState({
      meeting: {
        ...this.state.meeting,
        end_time: moment(endTime).toDate(),
      },
    })
  }

  updateCode(codeString: string) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    const digitString = codeString.replace(/[^\d]/gi, '')
    const parsed = digitString ? parseInt(digitString) : 0
    const code = parsed <= 0 ? null : parsed
    this.setState({
      meeting: {
        ...this.state.meeting,
        code,
      },
    })
  }

  updatePublished(publishedStr: string) {
    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }

    this.setState({
      meeting: {
        ...this.state.meeting,
        published: publishedStr === 'true',
      },
    })
  }

  async autogenerateCode(e) {
    e.preventDefault()

    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }
    this.setState({
      inSubmission: true,
    })

    try {
      const updatedCode = (
        await Meetings.autogenerateMeetingCode(this.state.meeting.id)
      ).meeting.code
      const updatedMeeting: Meeting = {
        ...this.state.meeting,
        code: updatedCode,
      }
      this.setState({ meeting: updatedMeeting })
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  async submitMeeting(e) {
    e.preventDefault()

    if (this.state.inSubmission || this.state.meeting == null) {
      return
    }
    this.setState({
      inSubmission: true,
    })

    try {
      const attributeNames = [
        'name',
        'committee_id',
        'landing_url',
        'start_time',
        'end_time',
        'code',
        'published',
      ]
      const pickByAttributes = pick(attributeNames)
      const request = flow(pickByAttributes)(this.state.meeting)

      const response = await Meetings.updateMeeting(
        this.state.meeting.id,
        request
      )
      this.setState({
        meeting: {
          ...this.state.meeting,
          ...response,
        },
      })
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  render() {
    const isGeneralAdmin = isAdmin(this.props.member)
    const committeeAdminIds = new Set(
      (this.props.member.user.data?.roles || [])
        .filter((role) => role.role === 'admin' && role.committee_id)
        .map((role) => role.committee_id)
    )
    let committeesById = this.props.committees.byId
    if (!isGeneralAdmin) {
      const pickByAdmins = pickBy<CommitteeDetailed>((committee) =>
        committeeAdminIds.has(committee.id)
      )
      committeesById = pickByAdmins(committeesById)
    }
    const mapCommitteeAsName = mapValues<CommitteeDetailed, string>(
      (c) => c.name
    )
    const committeeNamesById = {
      ...mapCommitteeAsName(committeesById),
      [0]: DEFAULT_GENERAL_COMMITTEE, // add the empty option
    }

    return (
      <>
        <h3>Edit Meeting</h3>
        <Form
          className="edit-meeting-form"
          onSubmit={(e) => this.submitMeeting(e)}
        >
          <Form.Group as={Row} controlId="formName">
            <Form.Label column sm={2}>
              Meeting name
            </Form.Label>
            <Col sm={6}>
              <FormControl
                type="text"
                value={this.state.meeting.name || undefined}
                onChange={(e) => this.updateName(e.target.value)}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formCommittee">
            <Form.Label column sm={2}>
              {capitalize(c('GROUP_NAME_SINGULAR'))}
            </Form.Label>
            <Col sm={6}>
              <FormControl
                as="select"
                value={(this.state.meeting.committee_id || 0).toString()}
                onChange={(e) =>
                  this.updateCommitteeId((e.target as HTMLSelectElement).value)
                }
              >
                {Object.keys(committeeNamesById).map((id) => (
                  <option value={id} key={id}>
                    {committeeNamesById[id]}
                  </option>
                ))}
              </FormControl>
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formLandingUrl">
            <Form.Label column sm={2}>
              Landing URL
            </Form.Label>
            <Col sm={6}>
              <FormControl
                type="text"
                value={this.state.meeting.landing_url || undefined}
                onChange={(e) => this.updateLandingUrl(e.target.value)}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column sm={2}>
              Start Time
            </Form.Label>
            <Col sm={6}>
              <DateTime
                value={this.state.meeting.start_time || undefined}
                onChange={(m) => this.updateStartTime(m)}
                timeConstraints={{
                  minutes: { min: 0, max: 59, step: 15 },
                }}
              />
            </Col>
          </Form.Group>
          <Form.Group column as={Row}>
            <Form.Label column sm={2}>
              End Time
            </Form.Label>
            <Col sm={6}>
              <DateTime
                value={this.state.meeting.end_time || undefined}
                onChange={(m) => this.updateEndTime(m)}
                timeConstraints={{
                  minutes: { min: 0, max: 59, step: 15 },
                }}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formCode">
            <Form.Label column sm={2}>
              Meeting code
            </Form.Label>
            <Col sm={5}>
              <FormControl
                type="text"
                value={this.state.meeting.code?.toString() || ''}
                onChange={(e) => this.updateCode(e.target.value)}
                minLength={4}
                maxLength={4}
              />
            </Col>
            <Col sm={1}>
              <Button
                disabled={this.state.inSubmission || this.state.meeting == null}
                onClick={(e) => this.autogenerateCode(e)}
              >
                ♻
              </Button>
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formPublished">
            <Form.Label column sm={2}>
              Published?
            </Form.Label>
            <Col sm={6}>
              <select
                value={(this.state.meeting.published || true).toString()}
                onChange={(e) => this.updatePublished(e.target.value)}
              >
                <option value="true">Published</option>
                <option value="false">Unpublished</option>
              </select>
            </Col>
          </Form.Group>
          <Button type="submit" onClick={(e) => this.submitMeeting(e)}>
            Save changes
          </Button>
        </Form>
      </>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      fetchCommittees: committees.fetchCommittees,
    },
    dispatch
  )

type EditMeetingDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<RootReducer, EditMeetingDispatchProps, {}, RootReducer>(
  (state) => state,
  mapDispatchToProps
)(EditMeeting)
