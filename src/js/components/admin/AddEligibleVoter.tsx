import { first } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, Col, Form, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { ElectionResponse } from 'src/js/client/ElectionClient'
import { fetchElections } from '../../redux/actions/electionActions'
import { ElectionsState } from '../../redux/reducers/electionReducers'
import { RootReducer } from '../../redux/reducers/rootReducer'
import { membershipApi } from '../../services/membership'
import { HTTP_POST, logError } from '../../util/util'
import FieldGroup from '../common/FieldGroup'

interface Voter {
  member_id: number
  election_id: number
}

interface AddEligibleVoterOwnProps {
  memberId: number
  refresh: Function
}

interface AddEligibleVoterState {
  inSubmission: boolean
  eligibleVoter: Voter
}

type AddEligibleVoterProps = AddEligibleVoterOwnProps &
  AddEligibleVoterDispatchProps &
  AddEligibleVoterStateProps

class AddEligibleVoter extends Component<
  AddEligibleVoterProps,
  AddEligibleVoterState
> {
  constructor(props) {
    super(props)
    this.state = {
      eligibleVoter: {
        member_id: this.props.memberId,
        election_id: -1,
      },
      inSubmission: false,
    }
  }

  static getDerivedStateFromProps(
    nextProps: AddEligibleVoterProps,
    nextState: AddEligibleVoterState
  ) {
    const elections = Object.values(nextProps.elections.byId)
    if (nextState.eligibleVoter.election_id === -1 && elections.length > 0) {
      const election = first(elections)
      return {
        ...nextState,
        eligibleVoter: {
          ...nextState.eligibleVoter,
          election_id: election.id,
        },
      }
    }
    return nextState
  }

  updateElectionId = <K extends keyof Voter>(formKey: K, value: Voter[K]) => {
    if (this.state.inSubmission) {
      return
    }

    this.setState({
      eligibleVoter: {
        ...this.state.eligibleVoter,
        [formKey]: value,
      },
    })
  }

  render() {
    const electionOptions = Object.values(this.props.elections.byId).map(
      (election: ElectionResponse) => election.name
    )
    const disabled =
      electionOptions.length === 0 ||
      this.state.eligibleVoter.election_id === -1

    return (
      <div>
        <Row>
          <Col sm={4}>
            <Form onSubmit={(e) => e.preventDefault()}>
              <h3>Add Eligible Voter</h3>
              <FieldGroup
                formKey="election_id"
                componentClass="select"
                label="Election"
                options={electionOptions}
                placeholder="Select an election"
                value={this.state.eligibleVoter.election_id.toString()}
                onFormValueChange={(formKey, value) =>
                  this.updateElectionId(formKey, parseInt(value))
                }
                required
              />
              <Button
                type="submit"
                onClick={(e) =>
                  this.submitForm(e, 'eligibleVoter', '/election/voter')
                }
                disabled={disabled}
              >
                Add Voter
              </Button>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }

  async submitForm(e, name, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await membershipApi(HTTP_POST, endpoint, this.state[name])
      this.props.refresh()
    } catch (err) {
      return logError('Error loading test', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchElections }, dispatch)

interface AddEligibleVoterStateProps {
  elections: ElectionsState
}

type AddEligibleVoterDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  AddEligibleVoterStateProps,
  AddEligibleVoterDispatchProps,
  AddEligibleVoterOwnProps,
  RootReducer
>(
  (state) => ({ elections: state.elections }),
  mapDispatchToProps
)(AddEligibleVoter)
