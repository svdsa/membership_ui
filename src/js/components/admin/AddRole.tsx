import { mapValues } from 'lodash'
import { capitalize } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, Form } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import PageHeading from 'src/js/components/common/PageHeading'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { fetchCommittees } from '../../redux/actions/committeeActions'
import { membershipApi } from '../../services/membership'
import { HTTP_POST, logError } from '../../util/util'
import FieldGroup from '../common/FieldGroup'

interface AddRoleOwnProps {
  memberId: number
  refresh: () => void
}

type AddRoleProps = AddRoleOwnProps & AddRoleStateProps & AddRoleDispatchProps

interface NewRoleForm {
  member_id: number
  committee_id: number
  role: string
}

interface AddRoleState {
  newRole: NewRoleForm
  inSubmission: boolean
}

class AddRole extends Component<AddRoleProps, AddRoleState> {
  constructor(props) {
    super(props)
    this.state = {
      newRole: {
        member_id: this.props.memberId,
        committee_id: 0,
        role: 'member',
      },
      inSubmission: false,
    }
  }

  componentDidMount() {
    // TODO(jesse): hide the form behind an edit button to save bandwith for admins
    this.props.fetchCommittees()
  }

  updateNewRole = <K extends keyof NewRoleForm>(
    formKey: K,
    value: NewRoleForm[K]
  ) => {
    if (this.state.inSubmission) {
      return
    }
    this.setState({ newRole: { ...this.state.newRole, [formKey]: value } })
  }

  render() {
    return (
      <section className="add-role-section">
        <PageHeading level={3}>Add role to member</PageHeading>
        <Form onSubmit={(e) => e.preventDefault()}>
          <FieldGroup
            formKey="role"
            componentClass="select"
            options={['admin', 'member', 'co-chair', 'vice-chair']}
            label="Role"
            value={this.state.newRole.role}
            onFormValueChange={this.updateNewRole}
            required
          />
          <FieldGroup
            formKey="committee_id"
            componentClass="select"
            label={capitalize(c('GROUP_NAME_SINGULAR'))}
            options={this.mapCommitteeIdToName()}
            value={this.state.newRole.committee_id}
            onFormValueChange={(formKey, value) =>
              this.updateNewRole(formKey, parseInt(value))
            }
            placeholder={`Select a ${c(
              'GROUP_NAME_SINGULAR'
            )} to add the role to`}
            required
          />
          <Button
            type="submit"
            onClick={(e) => this.submitForm(e, 'newRole', '/member/role')}
          >
            Add Role
          </Button>
        </Form>
      </section>
    )
  }

  mapCommitteeIdToName(): { [id: number]: string } {
    return {
      ...mapValues(this.props.committees.byId, (data, id) => data.name),
      [0]: 'General',
    }
  }

  async submitForm(e, name, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await membershipApi(HTTP_POST, endpoint, this.state[name])
      this.props.refresh()
    } catch (err) {
      return logError('Error loading test', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchCommittees }, dispatch)

type AddRoleStateProps = RootReducer
type AddRoleDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  AddRoleStateProps,
  AddRoleDispatchProps,
  AddRoleOwnProps,
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(AddRole)
