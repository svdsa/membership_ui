import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import * as Actions from '../../../redux/actions/memberSearchActions'
import { MemberSearchState } from '../../../redux/reducers/memberSearchReducer'
import SearchInput from './SearchInput'
import SearchResults from './SearchResults'

type MemberSearchProps = MemberSearchDispatchProps & MemberSearchStateProps

class MemberSearch extends React.Component<MemberSearchProps> {
  constructor(props) {
    super(props)
  }
  handleLoadMore = () => {
    const { loadMoreResults, memberSearch } = this.props
    loadMoreResults(memberSearch.query, memberSearch.cursor)
  }
  render() {
    const {
      memberSearch,
      updateMemberSearchInput,
      clearMemberSearchInput,
      searchMembers,
    } = this.props
    return (
      <div className="admin-member-search">
        <Row>
          <Col sm={4}>
            <SearchInput
              {...memberSearch}
              onChange={updateMemberSearchInput}
              onClear={clearMemberSearchInput}
              onSearch={searchMembers}
            />
          </Col>
        </Row>

        <SearchResults {...memberSearch} loadMore={this.handleLoadMore} />
      </div>
    )
  }
}

const mapStateToProps = (state: RootReducer): MemberSearchStateProps => ({
  memberSearch: state.memberSearch,
})
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(Actions, dispatch)

type MemberSearchDispatchProps = ReturnType<typeof mapDispatchToProps>

interface MemberSearchStateProps {
  memberSearch: MemberSearchState
}

export default connect<MemberSearchStateProps, MemberSearchDispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(MemberSearch)
