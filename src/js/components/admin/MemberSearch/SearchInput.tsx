import React, { Component } from 'react'
import { debounce } from 'lodash'
import { InputGroup, Form } from 'react-bootstrap'
import { TODO } from '../../../typeMigrationShims'

interface SearchInputProps {
  onChange: TODO
  isLoading: TODO
  query: TODO
  onSearch: TODO
  onClear: TODO
}

class SearchInput extends Component<SearchInputProps> {
  constructor(props) {
    super(props)
    this.queryMembers = debounce(this.queryMembers.bind(this), 600)
  }

  handleSearchChange: React.ChangeEventHandler<HTMLInputElement> = ({
    target,
  }) => {
    this.props.onChange(target.value)
    this.queryMembers()
  }

  queryMembers() {
    if (!this.props.isLoading && this.props.query !== '') {
      this.props.onSearch(this.props.query)
    }
  }
  render() {
    const { query, onClear } = this.props
    return (
      <div>
        <InputGroup>
          <Form.Control
            type="text"
            value={query}
            onChange={this.handleSearchChange}
            placeholder="Search users..."
          />
          <InputGroup.Append
            className={`clear-member-search ${query && 'can-clear'}`}
            onClick={onClear}
          >
            <InputGroup.Text>x</InputGroup.Text>
          </InputGroup.Append>
        </InputGroup>
      </div>
    )
  }
}

export default SearchInput
