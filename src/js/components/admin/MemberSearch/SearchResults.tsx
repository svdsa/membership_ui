import React from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { EligibleMemberListEntry } from 'src/js/client/MemberClient'
import SearchResultRow from './SearchResultRow'

interface SearchResultsProps {
  isLoading: boolean
  results: EligibleMemberListEntry[]
  hasMoreResults: boolean
  loadMore(): void
  cursor: number
}

const SearchResults: React.FC<SearchResultsProps> = ({
  isLoading,
  results,
  hasMoreResults,
  loadMore,
}) => (
  <div className="admin-search-list">
    {results.map((member) => (
      <SearchResultRow key={member.id} member={member} />
    ))}
    {isLoading && (
      <Row>
        <Col xs={12}>Loading...</Col>
      </Row>
    )}
    {hasMoreResults && !isLoading && (
      <Row>
        <Col xs={12}>
          <Button className="admin-search-load-more" onClick={loadMore}>
            Load more
          </Button>
        </Col>
      </Row>
    )}
  </div>
)

export default SearchResults
