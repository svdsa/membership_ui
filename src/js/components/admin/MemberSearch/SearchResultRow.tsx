import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { Link } from 'react-router'
import { EligibleMemberListEntry } from 'src/js/client/MemberClient'
import { AttendeeEligibilityRow, EmptyEligibilityRow } from '../MemberRow'

interface SearchResultRowProps {
  member: EligibleMemberListEntry
}

const SearchResultRow: React.FC<SearchResultRowProps> = ({ member }) => {
  return (
    <Row style={{ marginTop: '13px' }}>
      <Col sm={6} md={4}>
        <Link to={`/members/${member.id}`}>
          {`${member.name}: ${member.email}`}
        </Link>
      </Col>
      <Col sm={6} md={8}>
        {member.eligibility.is_eligible != null ? (
          <AttendeeEligibilityRow
            isPersonallyEligible={member.eligibility.is_eligible}
            message={member.eligibility.message}
          />
        ) : (
          <EmptyEligibilityRow />
        )}
      </Col>
    </Row>
  )
}

export default SearchResultRow
