import { compact } from 'lodash/fp'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import {
  FiEdit,
  FiList,
  FiTag,
  FiUploadCloud,
  FiUserPlus,
  FiUsers,
} from 'react-icons/fi'
import { VscOpenPreview } from 'react-icons/vsc'
import { Link } from 'react-router'
import { PAGE_TITLE_TEMPLATE } from 'src/js/config'
import PageHeading from '../common/PageHeading'

export interface NavLinkEntry {
  label: string
  link?: string
  children?: NavLinkEntry[]
  icon?: JSX.Element
}

export const AdminComponents: NavLinkEntry[] = compact([
  {
    label: 'People',
    icon: <FiUsers />,
    children: [
      {
        link: '/admin/contacts',
        label: 'List contacts',
        icon: <FiList />,
      },
      {
        link: '/admin/contacts/new',
        label: 'Add a new contact',
        icon: <FiUserPlus />,
      },
      {
        link: '/admin/imports',
        label: 'Review imports',
        icon: <VscOpenPreview />,
      },
      {
        link: '/admin/import/csv/contacts',
        label: 'Import a CSV',
        icon: <FiUploadCloud />,
      },
      {
        link: '/admin/custom-fields',
        label: 'Custom fields',
        icon: <FiEdit />,
      },
      {
        link: '/admin/tags',
        label: 'Tags',
        icon: <FiTag />,
      },
    ],
  },
  // USE_ELECTIONS && {
  //   label: 'Elections',
  //   icon: <FiCheckSquare />,
  //   children: [
  //     {
  //       link: '/admin/elections',
  //       label: 'Manage elections',
  //       icon: <FiList />,
  //     },
  //     {
  //       link: '/admin/elections/create',
  //       label: 'Create new election',
  //       icon: <FiPlusCircle />,
  //     },
  //   ],
  // },
  // {
  //   label: 'Meetings',
  //   icon: <FiCalendar />,
  //   children: [
  //     {
  //       link: '/admin/meetings',
  //       label: 'Manage meetings',
  //       icon: <FiList />
  //     },
  //     {
  //       link: '/admin/meetings/create',
  //       label: 'Create new meeting',
  //       icon: <FiFilePlus />
  //     }
  //   ]
  // },
  // {
  //   label: capitalize(c('GROUP_NAME_PLURAL')),
  //   icon: <IoIosBonfire />,
  //   children: [
  //     {
  //       link: '/admin/committees',
  //       label: `Add or manage ${c('GROUP_NAME_PLURAL')}`,
  //       icon: <FiPlusCircle />
  //     }
  //   ]
  // },
])

const Admin: React.FC = () => (
  <Container className="admin-screen">
    {/* Needs the title template too, because the Admin page doesn't get the nav shell */}
    <Helmet titleTemplate={`🛡️ ${PAGE_TITLE_TEMPLATE}`}>
      <title>Admin Tools</title>
    </Helmet>
    <Row>
      <Col>
        <PageHeading level={2}>🛡️ Admin Tools</PageHeading>
      </Col>
    </Row>
    <Row>
      {AdminComponents.map((parent) => (
        <Col key={parent.label} md={6}>
          <header className="border-bottom">
            <PageHeading level={3} className="admin-parent-heading">
              <span className="admin-icon mr-3">{parent.icon}</span>
              {parent.label}
            </PageHeading>
          </header>
          <ul className="admin-children">
            {parent.children?.map((child) => (
              <li key={child.label} className="admin-child">
                {child.link != null ? (
                  <Link
                    to={child.link}
                    className="admin-child admin-child-link"
                  >
                    <span className="admin-icon">{child.icon}</span>
                    {child.label}
                  </Link>
                ) : (
                  <span className="admin-child">
                    <span className="admin-icon">{child.icon}</span>
                    {child.label}
                  </span>
                )}
              </li>
            ))}
          </ul>
        </Col>
      ))}
    </Row>
  </Container>
)

export default Admin
