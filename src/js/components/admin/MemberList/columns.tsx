import { compact } from 'lodash/fp'
import React from 'react'
import { Link } from 'react-router'
import { Column } from 'react-table'
import { EligibleMemberListEntry } from 'src/js/client/MemberClient'
import { USE_ELECTIONS } from 'src/js/config'
import { mapMembershipStatusString } from 'src/js/services/members'

export type MemberListColumn = Column<EligibleMemberListEntry>

export const Columns = {
  id: {
    Header: 'ID',
    id: 'id',
    accessor: (member) => member.id,
    width: 80,
  },

  name: {
    Header: 'Name',
    id: 'name',
    accessor: (member) => member.name,
  },

  nameWithLink: {
    Header: 'Name',
    id: 'name',
    accessor: (member) => member.name,
    Cell: ({ row: { original } }) => (
      <Link to={`/members/${original.id}`}>{original.name}</Link>
    ),
  },

  membershipStatus: {
    Header: 'Membership Status',
    id: 'membership_status',
    accessor: (member) => mapMembershipStatusString(member.membership.status),
  },

  eligibility: {
    Header: 'Eligible',
    id: 'eligibility',
    accessor: (member) => (member.eligibility.is_eligible ? 'Yes' : 'No'),
  },

  email: {
    Header: 'Email',
    id: 'email',
    accessor: (member) => member.email,
  },

  phoneNumbers: {
    Header: 'Phone numbers',
    id: 'phone_numbers',
    accessor: (member) => member.phone_numbers || null,
    Cell: ({ row: { original } }) =>
      original.phone_numbers != null
        ? original.phone_numbers.map((pn) => <div key={pn}>{pn}</div>)
        : null,
  },

  joinDate: {
    Header: 'Join date',
    id: 'join_date',
    accessor: (member) => member.membership.join_date,
    Cell: ({
      cell: {
        row: { original },
      },
    }) =>
      original.membership.join_date?.toLocaleDateString() || 'No membership',
  },

  duesPaidUntil: {
    Header: 'Dues paid until',
    id: 'dues_paid_until',
    accessor: (member) => member.membership.dues_paid_until,
    Cell: ({
      cell: {
        row: { original },
      },
    }) => original.membership.dues_paid_until?.toLocaleDateString() || 'N/A',
  },

  city: {
    Header: 'City',
    id: 'city',
    accessor: (member) => member.city,
  },

  zipcode: {
    Header: 'ZIP code',
    id: 'zipcode',
    accessor: (member) => member.zipcode,
  },
}

export const DEFAULT_COLUMNS: MemberListColumn[] = compact([
  Columns.id,
  Columns.nameWithLink,
  Columns.membershipStatus,
  USE_ELECTIONS ? Columns.eligibility : null,
  Columns.email,
  Columns.phoneNumbers,
  Columns.joinDate,
  Columns.duesPaidUntil,
  Columns.zipcode,
])
