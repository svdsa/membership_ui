import React from 'react'
import { Card, Col, Container, Nav, Row } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { LinkContainer } from 'react-router-bootstrap'
import { useGetUserMeQuery } from 'src/js/api'
import { AdminComponents } from 'src/js/components/admin/Admin'
import ContentStack from 'src/js/components/common/ContentStack'
import { ErrorBoundary } from 'src/js/components/common/ErrorBoundary'
import { PAGE_TITLE_TEMPLATE } from 'src/js/config'

const AdminNavBar: React.FC = () => (
  <Card className="shadow-sm">
    <Card.Header>Admin tools</Card.Header>
    <Nav className="flex-column">
      {AdminComponents.map((parent, i) => {
        if (parent.children != null && parent.children.length > 0) {
          const group = (
            <section key={parent.label} className="pb-2 border-bottom">
              <Nav.Item>
                <span className="admin-nav-entry py-1 mx-3 mt-2 small text-uppercase font-weight-bold">
                  <span className="admin-icon">{parent.icon}</span>{' '}
                  <span className="admin-label">{parent.label}</span>
                </span>
              </Nav.Item>
              {parent.children.map((child) => (
                <Nav.Item key={child.link}>
                  <LinkContainer to={child.link ?? ''} key={child.label}>
                    <Nav.Link>
                      <span className="admin-nav-dropdown-entry">
                        <span className="admin-icon">{child.icon}</span>
                        {child.label}
                      </span>
                    </Nav.Link>
                  </LinkContainer>
                </Nav.Item>
              ))}
            </section>
          )

          if (parent.link != null) {
            return (
              <LinkContainer key={parent.label} to={parent.link}>
                {group}
              </LinkContainer>
            )
          } else {
            return group
          }
        } else {
          if (parent.link != null) {
            return (
              <LinkContainer key={parent.label} to={parent.link}>
                <Nav.Link>
                  <span className="admin-icon">{parent.icon}</span>{' '}
                  {parent.label}
                </Nav.Link>
              </LinkContainer>
            )
          } else {
            return (
              <Nav.Item key={parent.label}>
                <span className="admin-icon">{parent.icon}</span> {parent.label}
              </Nav.Item>
            )
          }
        }
      })}
    </Nav>
  </Card>
)

const AdminNavShell: React.FC = ({ children }) => {
  const { data: user, isError, isLoading, error } = useGetUserMeQuery()

  return (
    <ContentStack data={user} isLoading={isLoading} error={error}>
      {(data) => (
        <>
          <Helmet titleTemplate={`🛡️ ${PAGE_TITLE_TEMPLATE}`} />
          <Container fluid>
            <Row className="mt-3">
              <Col lg={2} md={3}>
                <div className="mb-3">
                  <AdminNavBar />
                </div>
              </Col>
              <Col lg={10} md={9}>
                <ErrorBoundary>{children}</ErrorBoundary>
              </Col>
            </Row>
          </Container>
        </>
      )}
    </ContentStack>
  )
}

export default AdminNavShell
