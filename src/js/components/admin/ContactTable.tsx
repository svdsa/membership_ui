import { compact } from 'lodash'
import React from 'react'
import { Button, Table } from 'react-bootstrap'
import { BsSlashCircle } from 'react-icons/bs'
import { Link } from 'react-router'
import { Column, useTable } from 'react-table'
import { ContactResponse, IdCustomField } from 'src/js/api/schemas'
import { CustomFieldCardFromId } from 'src/js/components/common/CustomFieldCard'
import { EmailAddressSpan } from 'src/js/components/common/EmailAddressSpan'
import {
  ErrorBlock,
  ErrorBlockProps,
} from 'src/js/components/common/ErrorBlock'
import Loading from 'src/js/components/common/Loading'
import { PhoneNumberSpan } from 'src/js/components/common/PhoneNumberSpan'
import { TagSpanFromId } from 'src/js/components/common/TagSpan'
import { TimeAgoSpan } from 'src/js/components/common/TimeAgoSpan'
import { ProfilePicture } from 'src/js/components/contacts/ProfilePicture'
import { PronounBadgeListFromBundle } from 'src/js/components/contacts/PronounBadgeList'
import { c } from 'src/js/config'
import PageHeading from '../common/PageHeading'

export const CONTACT_COLUMNS: { [key: string]: Column<ContactResponse> } = {
  profile: {
    Header: 'Profile',
    id: 'profile',
    accessor: (contact: ContactResponse) => contact.full_name,
    Cell: ({ row: { original } }) => (
      <div className="d-flex flex-row align-items-center">
        <div className="mr-3">
          <ProfilePicture contact={original} size="sm" />
        </div>
        <div>
          <div>
            <Link
              to={`/admin/contacts/${original.id}`}
              className="font-weight-semibold mr-1"
            >
              {original.full_name}
            </Link>
            {original.display_name && (
              <span className="mr-1">({original.display_name})</span>
            )}
            {original.pronouns && (
              <PronounBadgeListFromBundle bundle={original.pronouns} />
            )}
          </div>
          <div className="small text-muted">
            Created <TimeAgoSpan date={original.date_created} />
          </div>
        </div>
      </div>
    ),
  },

  emailAddresses: {
    Header: 'Email addresses',
    id: 'email_addresses',
    accessor: (contact) => contact.email_addresses.data,
    Cell: ({ row: { original } }) => {
      const emailAddresses = original.email_addresses.data
      if (emailAddresses == null) {
        return null
      }

      if (emailAddresses.length === 0) {
        return (
          <span className="text-muted" title="No email addresses provided">
            <BsSlashCircle />
          </span>
        )
      }

      return (
        <>
          {emailAddresses.map((ea) => (
            <EmailAddressSpan email={ea} key={ea.id} />
          ))}
        </>
      )
    },
  },

  phoneNumbers: {
    Header: 'Phone numbers',
    id: 'phone_numbers',
    accessor: (contact) => contact.phone_numbers.data,
    Cell: ({ row: { original } }) => {
      const phoneNumbers = original.phone_numbers.data
      if (phoneNumbers == null) {
        return null
      }

      if (phoneNumbers.length === 0) {
        return (
          <span className="text-muted" title="No phone numbers provided">
            <BsSlashCircle />
          </span>
        )
      }

      return (
        <>
          {phoneNumbers.map((pn) => (
            <PhoneNumberSpan phone={pn} key={pn.id} />
          ))}
        </>
      )
    },
  },

  tags: {
    Header: 'Tags',
    id: 'tags',
    accessor: (contact) => contact.tags,
    Cell: ({ row: { original } }) => {
      const tags = original.tags
      if (tags == null) {
        return (
          <span className="text-muted">
            <em>Tags unavailable</em>
          </span>
        )
      }

      if (tags.length === 0) {
        return (
          <span className="text-muted" title="No tags provided">
            <BsSlashCircle />
          </span>
        )
      }

      return (
        <>
          {tags.map((tagId) => (
            <TagSpanFromId key={tagId} tagId={tagId} />
          ))}
        </>
      )
    },
  },

  customFields: {
    Header: 'Custom fields',
    id: 'custom_fields',
    accessor: (contact) => contact.custom_fields,
    Cell: ({ row: { original } }) => {
      const customFields = original.custom_fields
      if (customFields == null) {
        return (
          <span className="text-muted">
            <em>Custom fields unavailable</em>
          </span>
        )
      }

      if (Object.keys(customFields).length === 0) {
        return (
          <span className="text-muted" title="No custom fields provided">
            <BsSlashCircle />
          </span>
        )
      }

      return (
        <>
          {Object.entries(customFields).map(([cfid, value]) => (
            <CustomFieldCardFromId
              key={cfid}
              id={cfid as IdCustomField}
              value={value}
            />
          ))}
        </>
      )
    },
  },

  city: {
    Header: 'City',
    id: 'city',
    accessor: (contact) => contact.city,
    Cell: ({ row: { original } }) => {
      if (!original.city) {
        return (
          <span className="text-muted" title="No city provided">
            <BsSlashCircle />
          </span>
        )
      }

      return <span>{original.city}</span>
    },
  },

  zipcode: {
    Header: 'ZIP code',
    id: 'zipcode',
    accessor: (contact) => contact.zipcode,
    Cell: ({ row: { original } }) => {
      if (!original.zipcode) {
        return (
          <span className="text-muted" title="No ZIP code provided">
            <BsSlashCircle />
          </span>
        )
      }

      return <span>{original.zipcode}</span>
    },
  },
}

export const DEFAULT_CONTACT_COLUMNS: Column<ContactResponse>[] = compact([
  CONTACT_COLUMNS.profile,
  CONTACT_COLUMNS.emailAddresses,
  CONTACT_COLUMNS.phoneNumbers,
  CONTACT_COLUMNS.city,
  CONTACT_COLUMNS.zipcode,
  CONTACT_COLUMNS.tags,
  CONTACT_COLUMNS.customFields,
])

interface ContactTableProps {
  columns?: Column<ContactResponse>[]
  contacts: ContactResponse[]
}

export const ContactTable: React.FC<ContactTableProps> = ({
  columns = DEFAULT_CONTACT_COLUMNS,
  contacts: data,
}) => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({
      columns,
      data,
    })

  return (
    <Table
      striped
      bordered
      hover
      {...getTableProps()}
      className="contact-list-table"
    >
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </Table>
  )
}

export const LoadingContactTable: React.FC = () => (
  <section className="loading-contact-list non-ideal-contact-list">
    <Loading />
  </section>
)

export const EmptyContactTable: React.FC = () => (
  <section className="empty-contact-list non-ideal-contact-list">
    <PageHeading level={2}>No contacts 😿</PageHeading>
    <p>Maybe you should add some!</p>
    <p>
      <Link to={`/admin/contacts/new`}>
        <Button>Add contact</Button>
      </Link>
      <Link to={`/import/csv/contacts`}>
        <Button>Import a CSV</Button>
      </Link>
    </p>
  </section>
)

export const ErrorContactTable: React.FC<Pick<ErrorBlockProps, 'error'>> = ({
  error,
}) => (
  <ErrorBlock
    error={error}
    title="Couldn't load contacts"
    message={
      <>
        <p>Try refreshing the page and trying again.</p>
        <p>If that doesn't work, contact {c('ADMIN_EMAIL')}.</p>
      </>
    }
  />
)
