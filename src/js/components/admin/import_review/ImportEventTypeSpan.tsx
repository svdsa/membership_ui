import React from 'react'
import { ImportEventResponse, ImportEventType } from 'src/js/api/schemas'

export const ImportEventTypeToFriendlyNameMap: {
  [key in ImportEventType]: string
} = {
  csv_import: 'a CSV file',
  dsausa_roster: 'the National DSA roster',
  newsletter_signup: 'newsletter signups',
  zoom_webhook: 'Zoom registrations',
  website_signup: 'website signups',
}

interface ImportEventTypeSpanProps {
  event: ImportEventResponse
}

export const ImportEventTypeSpan: React.FC<ImportEventTypeSpanProps> = ({
  event,
}) => {
  return <span>{ImportEventTypeToFriendlyNameMap[event.event_type]}</span>
}
