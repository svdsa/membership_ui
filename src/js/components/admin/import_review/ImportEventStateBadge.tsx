import React, { FC } from 'react'
import { Badge } from 'react-bootstrap'
import { ImportEventResponse } from 'src/js/api/schemas'

export interface ImportEventStateBadgeProps {
  importEvent: ImportEventResponse
}

const ImportEventStateBadge: FC<ImportEventStateBadgeProps> = ({
  importEvent,
}) => {
  if (importEvent.date_reviewed == null) {
    return (
      <span className="h5">
        <Badge variant="success">Open</Badge>
      </span>
    )
  } else {
    return (
      <span className="h5">
        <Badge variant="primary">Reviewed</Badge>
      </span>
    )
  }
}

export default ImportEventStateBadge
