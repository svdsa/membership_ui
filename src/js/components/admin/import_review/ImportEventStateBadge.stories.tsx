import type { Meta, Story } from '@storybook/react'
import React from 'react'
import { IdImportEvent, IdSecurityPrincipal } from 'src/js/api/schemas'
import ImportEventStateBadge, {
  ImportEventStateBadgeProps,
} from 'src/js/components/admin/import_review/ImportEventStateBadge'
export default {
  title: 'Admin/Import Events/ImportEventStateBadge',
  component: ImportEventStateBadge,
} as Meta

const Template: Story<ImportEventStateBadgeProps> = (args) => (
  <ImportEventStateBadge {...args} />
)

export const Open: Story<ImportEventStateBadgeProps> = Template.bind({})
Open.args = {
  importEvent: {
    object: 'import_event',
    id: 'ime_bogus' as IdImportEvent,
    creator: 'sec_bogus' as IdSecurityPrincipal,
    date_created: new Date('2021-04-21T00:00:00+00:00'),
    title: 'Placeholder title',
    event_type: 'csv_import',
    event_info: {
      location: 'Online',
      date_started: new Date('2021-04-20T18:00:00-07:00'),
      event_id: null,
      date_ended: new Date('2021-04-20T19:00:00-07:00'),
      organizer: 'Some Organization',
    },
    reviewer: null,
    date_reviewed: null,
    review_action_counts: { proposed_create: 2 },
    review_actions: {
      object: 'list',
      url: '/imports/ime_ZP9BEa0l40kpdjLW/review_actions',
      has_more: false,
      data: [],
    },
  },
}

export const Reviewed: Story<ImportEventStateBadgeProps> = Template.bind({})
Reviewed.args = {
  importEvent: {
    object: 'import_event',
    id: 'ime_bogus' as IdImportEvent,
    creator: 'sec_bogus' as IdSecurityPrincipal,
    date_created: new Date('2021-04-21T00:00:00+00:00'),
    title: 'Placeholder title',
    event_type: 'csv_import',
    event_info: {
      location: 'Online',
      date_started: new Date('2021-04-20T18:00:00-07:00'),
      event_id: null,
      date_ended: new Date('2021-04-20T19:00:00-07:00'),
      organizer: 'Some Organization',
    },
    reviewer: 'sec_bogus' as IdSecurityPrincipal,
    date_reviewed: new Date('2021-04-21T00:00:00+00:00'),
    review_action_counts: { proposed_create: 2 },
    review_actions: {
      object: 'list',
      url: '/imports/ime_ZP9BEa0l40kpdjLW/review_actions',
      has_more: false,
      data: [],
    },
  },
}
