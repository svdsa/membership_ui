import type { Meta, Story } from '@storybook/react'
import React from 'react'
import {
  ImportCandidateCard,
  ImportCandidateCardProps,
} from 'src/js/components/admin/import_review/ImportCandidateCard'
import { FAKE_contactCreateRequestNullable } from 'src/js/stories/data'

export default {
  title: 'Admin/Import Events/ImportCandidateCard',
  component: ImportCandidateCard,
} as Meta

const ImportCandidateTemplate: Story<ImportCandidateCardProps> = (args) => (
  <ImportCandidateCard {...args} />
)

export const NoTarget: Story<ImportCandidateCardProps> =
  ImportCandidateTemplate.bind({})
NoTarget.args = {
  contact: FAKE_contactCreateRequestNullable,
  mergeDirectives: {},
  onSelect: () => () => null,
  mergeTargetLoading: false,
  readOnly: true,
}
