import { SerializedError } from '@reduxjs/toolkit'
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query'
import React from 'react'
import { Button } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import PageHeading from 'src/js/components/common/PageHeading'
import { c } from 'src/js/config'

export const ReviewEventError: React.FC<{
  error?: FetchBaseQueryError | SerializedError
}> = ({ error }) => (
  <section>
    <PageHeading level={2}>Error loading event for review 🆘</PageHeading>
    <p>Try refreshing the page and trying again.</p>
    <p>If that doesn't work, contact {c('ADMIN_EMAIL')}.</p>
    {error != null && <code>{JSON.stringify(error)}</code>}
  </section>
)

export const ReviewEventEmpty: React.FC = () => (
  <section>
    <PageHeading level={2}>No participants in event</PageHeading>
    <p>This is probably an error. Please contact {c('ADMIN_EMAIL')}.</p>
    <LinkContainer to="/attendance">
      <Button>Go back</Button>
    </LinkContainer>
  </section>
)
