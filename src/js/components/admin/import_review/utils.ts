import { filter, groupBy, partition } from 'lodash/fp'
import {
  ContactCreateRequestNullable,
  ImportEventReviewAction,
  ImportEventReviewActionType,
  USER_REVIEW_ACTIONS,
} from 'src/js/api/schemas'

export const filterByManualActions = filter<ImportEventReviewAction>((a) =>
  USER_REVIEW_ACTIONS.has(a.action)
)

export const partitionByManualActions = partition<ImportEventReviewActionType>(
  (a) => USER_REVIEW_ACTIONS.has(a)
)

export const getNameQuery = (
  participant: ContactCreateRequestNullable
): string => `${participant.full_name}`

export const groupReviewsByAction = groupBy<ImportEventReviewAction>(
  (a) => a.action
)
