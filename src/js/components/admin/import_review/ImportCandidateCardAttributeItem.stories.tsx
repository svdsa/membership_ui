import type { Meta, Story } from '@storybook/react'
import React from 'react'
import {
  ImportCandidateCardAttributeItem,
  ImportCandidateCardAttributeItemProps,
} from 'src/js/components/admin/import_review/ImportCandidateCard'
import {
  FAKE_contactCreateRequestNullable,
  FAKE_contactResponse,
} from 'src/js/stories/data'

export default {
  title:
    'Admin/Import Events/ImportCandidateCard/ImportCandidateCardAttributeItem',
  component: ImportCandidateCardAttributeItem,
} as Meta

const Template: Story<ImportCandidateCardAttributeItemProps> = (args) => (
  <ImportCandidateCardAttributeItem {...args} />
)

export const UnselectedReadOnly: Story<ImportCandidateCardAttributeItemProps> =
  Template.bind({})
UnselectedReadOnly.storyName = 'Unselected (read-only)'
UnselectedReadOnly.args = {
  candidate: FAKE_contactCreateRequestNullable,
  target: FAKE_contactResponse,
  attribute: 'full_name',
  selected: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  onSelect: () => () => {},
  readOnly: true,
  hideIcons: false,
  children: ({ attribute, candidate, target }) => (
    <span className="h5">{candidate.full_name}</span>
  ),
}

export const SelectedReadOnly: Story<ImportCandidateCardAttributeItemProps> =
  Template.bind({})
UnselectedReadOnly.storyName = 'Selected (read-only)'
UnselectedReadOnly.args = {
  candidate: FAKE_contactCreateRequestNullable,
  target: FAKE_contactResponse,
  attribute: 'full_name',
  selected: true,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  onSelect: () => () => {},
  readOnly: true,
  hideIcons: false,
  children: ({ attribute, candidate, target }) => (
    <span className="h5">{candidate.full_name}</span>
  ),
}
