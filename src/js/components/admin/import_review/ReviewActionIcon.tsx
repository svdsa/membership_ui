import React from 'react'
import { BsPatchQuestion } from 'react-icons/bs'
import { FiUserCheck, FiUserPlus } from 'react-icons/fi'
import { RiGhostLine } from 'react-icons/ri'
import { ImportEventReviewActionType } from 'src/js/api/schemas'

interface ReviewActionIconProps {
  action: ImportEventReviewActionType
}

export const ReviewActionIcon: React.FC<ReviewActionIconProps> = ({
  action,
}) => {
  switch (action) {
    case 'create_new_contact':
    case 'proposed_create':
      return <FiUserPlus />
    case 'match_auto':
    case 'match_manual':
    case 'proposed_match':
      return <FiUserCheck />
    case 'ignore_once':
      return <RiGhostLine />
    default:
      return <BsPatchQuestion />
  }
}
