import React from 'react'
import type { ImportEventResponse } from 'src/js/api/schemas'
import { SecurityPrincipalSpanFromId } from 'src/js/components/common/SecurityPrincipalSpan'
import { TimeAgoSpan } from 'src/js/components/common/TimeAgoSpan'

interface ImportEventCreatedTimeAgoProps {
  event: ImportEventResponse
  className?: string
}

export const ImportEventCreatedTimeAgo: React.FC<ImportEventCreatedTimeAgoProps> =
  ({ event, className }) => (
    <span className={className}>
      Created <TimeAgoSpan date={event.date_created} /> by{' '}
      <SecurityPrincipalSpanFromId id={event.creator} />
    </span>
  )
