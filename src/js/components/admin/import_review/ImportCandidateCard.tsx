import { mapValues } from 'lodash'
import React, { useMemo } from 'react'
import { Card, ListGroup } from 'react-bootstrap'
import { Variant } from 'react-bootstrap/esm/types'
import {
  BsBookHalf,
  BsCheckCircle,
  BsCheckCircleFill,
  BsExclamationTriangleFill,
  BsInputCursorText,
  BsPersonBoundingBox,
  BsPlusCircle,
  BsPlusCircleFill,
  BsSlashSquare,
  BsStickyFill,
} from 'react-icons/bs'
import {
  FiEdit,
  FiMail,
  FiMapPin,
  FiMessageCircle,
  FiPhone,
  FiSmile,
  FiTag,
} from 'react-icons/fi'
import {
  ContactCreateRelationshipKeysSet,
  ContactCreateRequestNullable,
  ContactCustomFieldList,
  ContactResponse,
  EmailAddressCreateUnion,
  IdCustomField,
  IdTag,
  ImportEventReviewMerge,
  ImportReviewMergeActionType,
  PhoneNumberCreateUnion,
  PostalAddressCreateRequest,
} from 'src/js/api/schemas'
import { CustomFieldCardFromId } from 'src/js/components/common/CustomFieldCard'
import { EmailAddressSpan } from 'src/js/components/common/EmailAddressSpan'
import { ListGroupHeader } from 'src/js/components/common/ListGroupHeader'
import { ListGroupItemInfoHeader } from 'src/js/components/common/ListGroupItemInfo'
import { ListGroupSelectableItem } from 'src/js/components/common/ListGroupSelectableItem'
import { PhoneNumberSpan } from 'src/js/components/common/PhoneNumberSpan'
import { TagSpanFromId } from 'src/js/components/common/TagSpan'
import { PostalAddressCard } from 'src/js/components/contacts/PostalAddressCard'

const SELECT_ACTION_FOR_ATTRIBUTES: {
  [key in keyof Required<ImportEventReviewMerge>]: ImportReviewMergeActionType
} = {
  admin_notes: 'take_incoming',
  biography: 'take_incoming',
  display_name: 'take_incoming',
  full_name: 'take_incoming',
  profile_pic: 'take_incoming',
  pronouns: 'take_incoming',
  custom_fields: 'add_new',
  email_addresses: 'add_new',
  mailing_addresses: 'add_new',
  phone_numbers: 'add_new',
  tags: 'add_new',
}

const TITLE_FOR_ATTRIBUTES: {
  [key in keyof Required<ImportEventReviewMerge>]: string
} = {
  admin_notes: 'Admin notes',
  biography: 'Bio',
  display_name: 'Display name',
  full_name: 'Full name',
  profile_pic: 'Profile picture',
  pronouns: 'Pronouns',
  custom_fields: 'Custom fields',
  email_addresses: 'Email addresses',
  mailing_addresses: 'Locations',
  phone_numbers: 'Phone numbers',
  tags: 'Tags',
}

const HEADER_FOR_ATTRIBUTES: {
  [key in keyof Required<ImportEventReviewMerge>]: React.ReactElement
} = {
  admin_notes: (
    <ListGroupItemInfoHeader
      icon={<BsStickyFill />}
      title={TITLE_FOR_ATTRIBUTES['admin_notes']}
    />
  ),
  biography: (
    <ListGroupItemInfoHeader
      icon={<BsBookHalf />}
      title={TITLE_FOR_ATTRIBUTES['biography']}
    />
  ),
  display_name: (
    <ListGroupItemInfoHeader
      icon={<BsInputCursorText />}
      title="Display name"
    />
  ),
  full_name: (
    <ListGroupItemInfoHeader
      icon={<FiSmile />}
      title={TITLE_FOR_ATTRIBUTES['full_name']}
    />
  ),
  profile_pic: (
    <ListGroupItemInfoHeader
      icon={<BsPersonBoundingBox />}
      title={TITLE_FOR_ATTRIBUTES['profile_pic']}
    />
  ),
  pronouns: (
    <ListGroupItemInfoHeader
      icon={<FiMessageCircle />}
      title={TITLE_FOR_ATTRIBUTES['pronouns']}
    />
  ),
  custom_fields: (
    <ListGroupItemInfoHeader
      icon={<FiEdit />}
      title={TITLE_FOR_ATTRIBUTES['custom_fields']}
    />
  ),
  email_addresses: (
    <ListGroupItemInfoHeader
      icon={<FiMail />}
      title={TITLE_FOR_ATTRIBUTES['email_addresses']}
    />
  ),
  mailing_addresses: (
    <ListGroupItemInfoHeader
      icon={<FiMapPin />}
      title={TITLE_FOR_ATTRIBUTES['mailing_addresses']}
    />
  ),
  phone_numbers: (
    <ListGroupItemInfoHeader
      icon={<FiPhone />}
      title={TITLE_FOR_ATTRIBUTES['phone_numbers']}
    />
  ),
  tags: (
    <ListGroupItemInfoHeader
      icon={<FiTag />}
      title={TITLE_FOR_ATTRIBUTES['tags']}
    />
  ),
}

export interface ImportCandidateCardAttributeItemProps {
  candidate: ContactCreateRequestNullable
  target: ContactResponse | undefined
  attribute: keyof Required<ImportEventReviewMerge>
  readOnly: boolean
  hideIcons: boolean
  selected: boolean

  onSelect(
    attribute: keyof Required<ImportEventReviewMerge>,
    action?: ImportReviewMergeActionType
  ): () => void
  children({
    attribute,
    candidate,
    target,
  }: {
    attribute: keyof Required<ImportEventReviewMerge>
    candidate: ContactCreateRequestNullable
    target: ContactResponse | undefined
  }): React.ReactChild
}

export const ImportCandidateCardAttributeItem: React.FC<ImportCandidateCardAttributeItemProps> =
  ({
    candidate,
    target,
    attribute,
    readOnly,
    hideIcons,
    selected,
    onSelect,
    children,
  }) => {
    const memoizedOnSelect = useMemo(
      () => onSelect(attribute, SELECT_ACTION_FOR_ATTRIBUTES[attribute]),
      [attribute, onSelect]
    )

    const memoizedChildren = useMemo(
      () => children({ attribute, candidate, target }),
      [attribute, candidate, target, children]
    )

    const attributeIsArray = Array.isArray(candidate[attribute])
    const defaultIcon = attributeIsArray ? <BsPlusCircle /> : <BsCheckCircle />
    const selectedIcon = attributeIsArray ? (
      <BsPlusCircleFill />
    ) : (
      <BsCheckCircleFill />
    )

    const { isSelectActionValid, iconUnselectable, invalidSelectionMessage } =
      useMemo(() => {
        const disabledTargetNotAccessible = target == null
        const disabledIdenticalValues =
          target != null && candidate[attribute] === target[attribute]
        const disabledEmpty =
          ContactCreateRelationshipKeysSet.has(attribute) &&
          (candidate[attribute] == null ||
            (candidate[attribute] as unknown[]).length === 0)
        const iconNotAccessible = <BsExclamationTriangleFill />
        const iconIdentical = <BsSlashSquare />
        const iconEmpty = <BsSlashSquare />

        const isSelectActionValid = !(
          disabledTargetNotAccessible ||
          disabledIdenticalValues ||
          disabledEmpty
        )

        if (disabledTargetNotAccessible) {
          return {
            isSelectActionValid,
            iconUnselectable: iconNotAccessible,
            invalidSelectionMessage: (
              <span>
                <span className="font-weight-semibold">
                  Can't import field:{' '}
                </span>
                can't load target contact
              </span>
            ),
          }
        } else if (disabledEmpty) {
          return {
            isSelectActionValid,
            iconUnselectable: iconEmpty,
            invalidSelectionMessage: (
              <span>
                <span className="font-weight-semibold">
                  Choosing this won't do anything:{' '}
                </span>
                this list is empty
              </span>
            ),
          }
        } else if (disabledIdenticalValues) {
          return {
            isSelectActionValid,
            iconUnselectable: iconIdentical,
            invalidSelectionMessage: (
              <span>
                <span className="font-weight-semibold">
                  Choosing this won't do anything:{' '}
                </span>
                the values are the same
              </span>
            ),
          }
        } else {
          return {
            isSelectActionValid,
            iconUnselectable: <span />,
            invalidSelectionMessage: <span />,
          }
        }
      }, [target, candidate, attribute])

    return (
      <ListGroupSelectableItem
        id={attribute}
        iconUnselectable={iconUnselectable}
        iconUnselected={defaultIcon}
        iconSelected={selectedIcon}
        hideIcons={hideIcons}
        invalidSelectionMessage={invalidSelectionMessage}
        readOnly={readOnly}
        isSelectActionValid={isSelectActionValid}
        selected={selected}
        onSelect={memoizedOnSelect}
        selectedVariant="success"
      >
        {HEADER_FOR_ATTRIBUTES[attribute]}
        {memoizedChildren}
      </ListGroupSelectableItem>
    )
  }

export interface ImportCandidateCardProps {
  contact: ContactCreateRequestNullable
  variant?: Variant
  prefix?: {
    icon: React.ReactElement
    title: string
  }
  onSelect(
    attribute: keyof ImportEventReviewMerge,
    action?: ImportReviewMergeActionType
  ): () => void
  mergeDirectives: ImportEventReviewMerge
  mergeTarget: ContactResponse | undefined
  mergeTargetLoading: boolean
  readOnly: boolean
  hideIcons: boolean
}

export const ImportCandidateCard: React.FC<ImportCandidateCardProps> = ({
  contact,
  prefix,
  variant,
  mergeDirectives,
  mergeTarget,
  readOnly,
  hideIcons,
  onSelect,
}) => {
  const takeToggles = useMemo(
    () =>
      mapValues(
        mergeDirectives,
        (directive) => directive?.merge_action !== 'keep_existing'
      ),
    [mergeDirectives]
  )

  return (
    <Card border={variant}>
      {prefix != null && (
        <ListGroup variant="flush">
          <ListGroup.Item variant={variant}>
            <span className="mr-2">{prefix.icon}</span>
            <span>{prefix.title}</span>
          </ListGroup.Item>
        </ListGroup>
      )}
      <ListGroup variant="flush">
        <ImportCandidateCardAttributeItem
          candidate={contact}
          target={mergeTarget}
          attribute="full_name"
          selected={takeToggles.full_name || false}
          onSelect={onSelect}
          readOnly={readOnly}
          hideIcons={hideIcons}
        >
          {({ attribute, candidate }) => (
            <span className="h5">{candidate[attribute]}</span>
          )}
        </ImportCandidateCardAttributeItem>
        <ImportCandidateCardAttributeItem
          candidate={contact}
          target={mergeTarget}
          attribute="display_name"
          selected={takeToggles.display_name || false}
          onSelect={onSelect}
          readOnly={readOnly}
          hideIcons={hideIcons}
        >
          {({ attribute, candidate }) => (
            <span>{candidate[attribute] ?? 'No display name provided'}</span>
          )}
        </ImportCandidateCardAttributeItem>
        <ListGroupHeader variant={variant}>
          Contact and identifying info
        </ListGroupHeader>
        <ImportCandidateCardAttributeItem
          candidate={contact}
          target={mergeTarget}
          attribute="email_addresses"
          selected={takeToggles.email_addresses || false}
          onSelect={onSelect}
          readOnly={readOnly}
          hideIcons={hideIcons}
        >
          {({ attribute, candidate }) => {
            // HACK typescript issue https://github.com/microsoft/TypeScript/issues/10530
            const emailAddresses = candidate[
              attribute
            ] as EmailAddressCreateUnion
            if (emailAddresses == null || emailAddresses.length === 0) {
              return 'No email addresses provided'
            }

            return (
              <ul className="list-unstyled m-0">
                {emailAddresses.map((email) => {
                  if (typeof email === 'string') {
                    return (
                      <li key={email} className="mb-1">
                        <EmailAddressSpan email={{ value: email }} />
                      </li>
                    )
                  } else {
                    return (
                      <li key={email.value}>
                        <EmailAddressSpan email={email} />
                      </li>
                    )
                  }
                })}
              </ul>
            )
          }}
        </ImportCandidateCardAttributeItem>
        <ImportCandidateCardAttributeItem
          candidate={contact}
          target={mergeTarget}
          attribute="phone_numbers"
          selected={takeToggles.phone_numbers || false}
          onSelect={onSelect}
          readOnly={readOnly}
          hideIcons={hideIcons}
        >
          {({ attribute, candidate }) => {
            // HACK typescript issue https://github.com/microsoft/TypeScript/issues/10530
            const phoneNumbers = candidate[attribute] as PhoneNumberCreateUnion
            if (phoneNumbers == null || phoneNumbers.length === 0) {
              return 'No phone numbers provided'
            }

            return (
              <ul className="list-unstyled m-0">
                {phoneNumbers.map((phone) => {
                  if (typeof phone === 'string') {
                    return (
                      <li key={phone} className="mb-1">
                        <PhoneNumberSpan phone={{ value: phone }} />
                      </li>
                    )
                  } else {
                    return (
                      <li key={phone.value}>
                        <PhoneNumberSpan phone={phone} />
                      </li>
                    )
                  }
                })}
              </ul>
            )
          }}
        </ImportCandidateCardAttributeItem>
        <ImportCandidateCardAttributeItem
          candidate={contact}
          target={mergeTarget}
          attribute="mailing_addresses"
          selected={takeToggles.mailing_addresses || false}
          onSelect={onSelect}
          readOnly={readOnly}
          hideIcons={hideIcons}
        >
          {({ attribute, candidate }) => {
            // HACK typescript issue https://github.com/microsoft/TypeScript/issues/10530
            const mailingAddresses = candidate[
              attribute
            ] as PostalAddressCreateRequest[]
            if (mailingAddresses == null || mailingAddresses.length === 0) {
              return 'No mailing addresses provided'
            }

            return (
              <ul className="list-unstyled m-0">
                {mailingAddresses.map((address, i) => (
                  <li key={i} className="mb-1">
                    <PostalAddressCard address={address} />
                  </li>
                ))}
              </ul>
            )
          }}
        </ImportCandidateCardAttributeItem>
        <ListGroupHeader variant={variant}>Additional info</ListGroupHeader>
        <ImportCandidateCardAttributeItem
          candidate={contact}
          target={mergeTarget}
          attribute="tags"
          selected={takeToggles.tags || false}
          onSelect={onSelect}
          readOnly={readOnly}
          hideIcons={hideIcons}
        >
          {({ attribute, candidate }) => {
            // HACK typescript issue https://github.com/microsoft/TypeScript/issues/10530
            const tags = candidate[attribute] as IdTag[]
            if (tags == null || tags.length === 0) {
              return 'No tags provided'
            }

            return (
              <ul className="list-unstyled m-0">
                {tags.map((tagId, i) => (
                  <li key={i} className="mb-1">
                    <TagSpanFromId tagId={tagId} />
                  </li>
                ))}
              </ul>
            )
          }}
        </ImportCandidateCardAttributeItem>
        <ImportCandidateCardAttributeItem
          candidate={contact}
          target={mergeTarget}
          attribute="custom_fields"
          selected={takeToggles.custom_fields || false}
          onSelect={onSelect}
          readOnly={readOnly}
          hideIcons={hideIcons}
        >
          {({ attribute, candidate }) => {
            // HACK typescript issue https://github.com/microsoft/TypeScript/issues/10530
            const customFields = candidate[attribute] as ContactCustomFieldList
            if (
              customFields == null ||
              Object.keys(customFields).length === 0
            ) {
              return 'No custom fields provided'
            }

            return (
              <ul className="list-unstyled m-0 mt-2">
                {Object.entries(customFields).map(([id, value]) => (
                  <li key={id} className="mb-1">
                    <CustomFieldCardFromId
                      id={id as IdCustomField}
                      value={value}
                    />
                  </li>
                ))}
              </ul>
            )
          }}
        </ImportCandidateCardAttributeItem>
      </ListGroup>
    </Card>
  )
}
