import React from 'react'
import { Col, ListGroup, Row } from 'react-bootstrap'
import { Link } from 'react-router'
import {
  ImportEventResponse,
  ImportEventReviewActionType,
} from 'src/js/api/schemas'
import { ImportEventCreatedTimeAgo } from 'src/js/components/admin/import_review/ImportEventCreatedTimeAgo'
import { ImportEventReviewedTimeAgo } from 'src/js/components/admin/import_review/ImportEventReviewedTimeAgo'
import { ReviewActionIcon } from 'src/js/components/admin/import_review/ReviewActionIcon'
import { ReviewStateIcon } from 'src/js/components/admin/import_review/ReviewStateIcon'

interface ImportEventEntryProps {
  event: ImportEventResponse
}

const ImportEventEntry: React.FC<ImportEventEntryProps> = ({ event }) => {
  return (
    <ListGroup.Item className="mb-1">
      <Row>
        <Col xs="auto">
          <ReviewStateIcon event={event} />
        </Col>
        <Col xs={12} sm="auto" className="pl-0 flex-fill">
          <div className="mb-1">
            <span className="h5 font-weight-bold">
              <Link to={`/admin/imports/${event.id}`}>{event.title}</Link>
            </span>
          </div>
          <div className="text-muted">
            {event.date_reviewed != null ? (
              <ImportEventReviewedTimeAgo event={event} />
            ) : (
              <ImportEventCreatedTimeAgo event={event} />
            )}
          </div>
        </Col>
        <Col sm={3}>
          <ListGroup
            variant="flush"
            horizontal
            className="justify-content-end align-items-baseline"
          >
            {Object.keys(event.review_action_counts).map((key) => (
              <ListGroup.Item>
                <span className="mr-1">
                  <ReviewActionIcon
                    action={key as ImportEventReviewActionType}
                  />
                </span>
                <span>{event.review_action_counts[key]}</span>
              </ListGroup.Item>
            ))}
          </ListGroup>
        </Col>
      </Row>
    </ListGroup.Item>
  )
}

export default ImportEventEntry
