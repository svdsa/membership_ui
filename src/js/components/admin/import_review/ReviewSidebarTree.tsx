import { keyBy, mapValues, sum } from 'lodash'
import React, { useMemo, useState } from 'react'
import {
  ControlledTreeEnvironment,
  Tree,
  TreeItem,
  TreeItemIndex,
} from 'react-complex-tree'
import {
  IdImportEventReviewAction,
  ImportEventReviewAction,
  ImportEventReviewActionType,
} from 'src/js/api/schemas'
import {
  groupReviewsByAction,
  partitionByManualActions,
} from 'src/js/components/admin/import_review/utils'

function renderActionLabel(
  type: ImportEventReviewActionType,
  count: number
): string {
  switch (type) {
    case 'create_new_contact':
    case 'proposed_create': {
      return `Create new (${count})`
    }
    case 'match_auto':
    case 'match_manual':
    case 'proposed_match': {
      return `Match with existing (${count})`
    }
    case 'ignore_once': {
      return `Skip (${count})`
    }
  }
}

export interface ReviewActionTreeData {
  label: string
  id?: IdImportEventReviewAction
}

interface ReviewSidebarTreeProps {
  actions: ImportEventReviewAction[]
  selectedActions: ImportEventReviewAction[]
  onChooseTreeItems(items: TreeItemIndex[], treeId: string): void
}

export const ReviewSidebarTree: React.FC<ReviewSidebarTreeProps> = ({
  actions,
  selectedActions,
  onChooseTreeItems,
}) => {
  const actionsByType = useMemo(() => groupReviewsByAction(actions), [actions])
  const [manualActionTypes, autoActionTypes] = useMemo(
    () => partitionByManualActions(Object.keys(actionsByType)),
    [actionsByType]
  )
  const actionEntries: Record<string, TreeItem<ReviewActionTreeData>> = useMemo(
    () =>
      keyBy(
        actions.map((action) => ({
          data: { label: action.participant.full_name, id: action.id },
          index: action.id,
        })),
        (item) => item.index
      ),
    [actions]
  )
  const treeEntries: Record<
    TreeItemIndex,
    TreeItem<ReviewActionTreeData>
  > = useMemo(
    () => ({
      root: {
        data: { label: 'root' },
        index: 'root',
        hasChildren: true,
        children: ['auto_actions', 'manual_actions'],
      },
      manual_actions: {
        data: {
          label: `Reviewed actions (${sum(
            manualActionTypes.map((type) => actionsByType[type].length)
          )})`,
        },
        index: 'manual_actions',
        hasChildren: manualActionTypes.length > 0,
        children: manualActionTypes,
      },
      auto_actions: {
        data: {
          label: `Auto actions (${sum(
            autoActionTypes.map((type) => actionsByType[type].length)
          )})`,
        },
        index: 'auto_actions',
        hasChildren: autoActionTypes.length > 0,
        children: autoActionTypes,
      },
      ...mapValues<
        Record<string, ImportEventReviewAction[]>,
        TreeItem<ReviewActionTreeData>
      >(actionsByType, (group, type) => ({
        data: {
          label: renderActionLabel(
            type as ImportEventReviewActionType,
            group.length
          ),
        },
        index: type,
        hasChildren: true,
        children: group.map((action) => action.id),
      })),
      ...actionEntries,
    }),
    [actionsByType, manualActionTypes, autoActionTypes]
  )

  const [focusedItem, setFocusedItem] = useState<TreeItemIndex | undefined>()
  const [expandedItems, setExpandedItems] = useState<TreeItemIndex[]>([
    'auto_actions',
    ...autoActionTypes,
  ])
  const selectedItems = useMemo(() => {
    return selectedActions.map((action) => action.id)
  }, [actions, selectedActions])

  return (
    <ControlledTreeEnvironment<ReviewActionTreeData>
      items={treeEntries}
      getItemTitle={(item) => item.data.label}
      viewState={{
        'import-event-sidebar': {
          focusedItem,
          expandedItems,
          selectedItems,
        },
      }}
      onFocusItem={(item) => setFocusedItem(item.index)}
      onExpandItem={(item) => setExpandedItems([...expandedItems, item.index])}
      onCollapseItem={(item) =>
        setExpandedItems(
          expandedItems.filter(
            (expandedItemIndex) => expandedItemIndex !== item.index
          )
        )
      }
      onSelectItems={onChooseTreeItems}
    >
      <Tree
        treeId="import-event-sidebar"
        rootItem="root"
        treeLabel="Contacts"
      />
    </ControlledTreeEnvironment>
  )
}
