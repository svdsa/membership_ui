import React from 'react'
import { BsCheckCircle, BsExclamationDiamond } from 'react-icons/bs'
import { ImportEventResponse } from 'src/js/api/schemas'

interface ReviewStateIconProps {
  event: ImportEventResponse
}

export const ReviewStateIcon: React.FC<ReviewStateIconProps> = ({ event }) => {
  if (event.date_reviewed == null) {
    return (
      <span className="text-success">
        <BsExclamationDiamond />
      </span>
    )
  } else {
    return (
      <span className="text-primary">
        <BsCheckCircle />
      </span>
    )
  }
}
