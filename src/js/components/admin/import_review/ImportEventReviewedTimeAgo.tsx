import React from 'react'
import type { ImportEventResponse } from 'src/js/api/schemas'
import { SecurityPrincipalSpanFromId } from 'src/js/components/common/SecurityPrincipalSpan'
import { TimeAgoSpan } from 'src/js/components/common/TimeAgoSpan'

interface ImportEventReviewedTimeAgoProps {
  event: ImportEventResponse
  className?: string
}

export const ImportEventReviewedTimeAgo: React.FC<ImportEventReviewedTimeAgoProps> =
  ({ event, className }) => {
    if (event.date_reviewed == null || event.reviewer == null) {
      console.trace(
        'ImportEventReviewedTimeAgo called with null review state, check implementation'
      )
      return <span>Error: invalid review state</span>
    }

    return (
      <span className={className}>
        Review confirmed <TimeAgoSpan date={event.date_reviewed} /> by{' '}
        <SecurityPrincipalSpanFromId id={event.reviewer} />
      </span>
    )
  }
