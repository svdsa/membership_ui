import { SerializedError } from '@reduxjs/toolkit'
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query'
import { keyBy, mapValues } from 'lodash'
import React, { useMemo } from 'react'
import { Card, ListGroup } from 'react-bootstrap'
import { Variant } from 'react-bootstrap/esm/types'
import { BiRename } from 'react-icons/bi'
import { BsArrowRightShort, BsPlusLg, BsSlashCircle } from 'react-icons/bs'
import {
  FiEdit,
  FiMail,
  FiMapPin,
  FiPhone,
  FiSmile,
  FiTag,
} from 'react-icons/fi'
import {
  ContactCreateRequestNullable,
  ContactResponse,
  IdCustomField,
  ImportEventReviewMerge,
} from 'src/js/api/schemas'
import ContentStack from 'src/js/components/common/ContentStack'
import { CustomFieldCardFromId } from 'src/js/components/common/CustomFieldCard'
import {
  EmailAddressSpan,
  normalizeEmailAddress,
} from 'src/js/components/common/EmailAddressSpan'
import { ListGroupHeader } from 'src/js/components/common/ListGroupHeader'
import { ListGroupItemInfo } from 'src/js/components/common/ListGroupItemInfo'
import {
  normalizePhoneNumber,
  PhoneNumberSpan,
} from 'src/js/components/common/PhoneNumberSpan'
import { TagSpanFromId } from 'src/js/components/common/TagSpan'
import { PostalAddressCard } from 'src/js/components/contacts/PostalAddressCard'
import { difference } from 'src/js/util/setOperations'
export interface AttributeDiffProps {
  candidateAttr: string | null
  targetAttr: string | null
}

export const AttributeDiff: React.FC<AttributeDiffProps> = ({
  candidateAttr,
  targetAttr,
}) => {
  if (targetAttr == null) {
    if (candidateAttr != null) {
      return (
        <span>
          <BsSlashCircle /> <span className="small text-muted">Blank</span>{' '}
          <BsArrowRightShort />{' '}
          <span className="highlight-success">{candidateAttr}</span>
        </span>
      )
    } else {
      return <span>This field will remain blank</span>
    }
  } else if (candidateAttr == null) {
    return (
      <span>
        <span className="highlight-danger text-strikethrough">
          {targetAttr}
        </span>{' '}
        <BsArrowRightShort /> <BsSlashCircle />{' '}
        <span className="small text-muted">Blank</span>
      </span>
    )
  } else {
    return (
      <span>
        <span className="highlight-danger text-strikethrough">
          {targetAttr}
        </span>{' '}
        <BsArrowRightShort />{' '}
        <span className="highlight-success">{candidateAttr}</span>
      </span>
    )
  }
}

function takeDifference<T>(
  candidate: T[] | undefined,
  target: T[] | undefined,
  takeCandidate: boolean | undefined,
  keyByFn: (item: T) => string
): T[] {
  if (!takeCandidate) {
    return []
  }

  if (candidate == null || candidate.length === 0) {
    return []
  }

  if (target == null || target.length === 0) {
    return candidate
  }

  const candidateKeyed = keyBy(candidate, keyByFn)
  const targetKeyed = keyBy(target, keyByFn)
  const keyDiff = difference(
    new Set(Object.keys(candidateKeyed)),
    new Set(Object.keys(targetKeyed))
  )
  return [...keyDiff].map((key) => candidateKeyed[key])
}

export interface MatchTargetCardProps {
  variant?: Variant
  mergeDirectives: ImportEventReviewMerge
  candidate: ContactCreateRequestNullable
  mergeTarget: ContactResponse | undefined
  mergeTargetLoading: boolean
  mergeTargetError: FetchBaseQueryError | SerializedError | undefined
  header: React.ReactChild
}

export const MatchTargetCard: React.FC<MatchTargetCardProps> = ({
  variant,
  mergeDirectives,
  candidate,
  mergeTarget,
  mergeTargetLoading,
  mergeTargetError,
  header,
}) => {
  const takeToggles = useMemo(
    () =>
      mapValues(
        mergeDirectives,
        (directive) =>
          directive != null && directive.merge_action !== 'keep_existing'
      ),
    [mergeDirectives]
  )

  const newEmails = useMemo(() => {
    const candidateEmails = candidate.email_addresses?.map(
      normalizeEmailAddress
    )
    const targetEmails = mergeTarget?.email_addresses.data?.map(
      normalizeEmailAddress
    )

    const takeEmails = takeToggles.email_addresses

    return takeDifference(
      candidateEmails,
      targetEmails,
      takeEmails,
      (item) => item.value
    )
  }, [candidate, mergeTarget, takeToggles])

  const newPhones = useMemo(() => {
    const candidatePhones = candidate.phone_numbers?.map(normalizePhoneNumber)
    const targetPhones =
      mergeTarget?.phone_numbers.data?.map(normalizePhoneNumber)
    const takePhones = takeToggles.phone_numbers

    return takeDifference(
      candidatePhones,
      targetPhones,
      takePhones,
      (item) => item.value
    )
  }, [candidate, mergeTarget, takeToggles])

  const newPostals = useMemo(() => {
    const candidatePostals = candidate.mailing_addresses ?? undefined
    const targetPostals = mergeTarget?.mailing_addresses.data
    const takePostals = takeToggles.mailing_addresses

    return takeDifference(
      candidatePostals,
      targetPostals,
      takePostals,
      (item) =>
        JSON.stringify({
          line1: item.line1,
          line2: item.line2,
          city: item.city,
          state: item.state,
          zipcode: item.zipcode,
        })
    )
  }, [candidate, mergeTarget, takeToggles])

  const newTags = useMemo(() => {
    const candidateTags = candidate.tags ?? undefined
    const targetTags = mergeTarget?.tags
    const takeTags = takeToggles.tags

    return takeDifference(candidateTags, targetTags, takeTags, (item) => item)
  }, [candidate, mergeTarget, takeToggles])

  const newCustomFields = useMemo(() => {
    const candidateCustomFields = candidate.custom_fields ?? {}
    const targetCustomFields = mergeTarget?.custom_fields ?? {}
    const takeCustomFields = takeToggles.custom_fields

    return takeDifference(
      Object.entries(candidateCustomFields),
      Object.entries(targetCustomFields),
      takeCustomFields,
      ([key, value]) => key
    )
  }, [candidate, mergeTarget, takeToggles])

  return (
    <Card border={variant}>
      <ListGroup variant="flush">
        <ListGroup.Item variant={variant} className="px-2 py-2">
          {header}
        </ListGroup.Item>
      </ListGroup>
      <ContentStack
        data={mergeTarget}
        isLoading={mergeTargetLoading}
        error={mergeTargetError}
      >
        {(contact) => (
          <ListGroup variant="flush">
            <ListGroupItemInfo icon={<FiSmile />} title="Full name">
              {candidate.full_name != null && takeToggles.full_name ? (
                <span className="h5">
                  <AttributeDiff
                    targetAttr={contact.full_name}
                    candidateAttr={candidate.full_name}
                  />
                </span>
              ) : (
                <span className="h5">{contact.full_name}</span>
              )}
            </ListGroupItemInfo>
            <ListGroupItemInfo icon={<BiRename />} title="Display name">
              {takeToggles.display_name ? (
                <AttributeDiff
                  targetAttr={contact.display_name}
                  candidateAttr={candidate.display_name}
                />
              ) : (
                contact.display_name ?? 'No display name provided'
              )}
            </ListGroupItemInfo>
            <ListGroupHeader variant={variant}>
              Contact and identifying info
            </ListGroupHeader>
            <ListGroupItemInfo icon={<FiMail />} title="Email addresses">
              {contact.email_addresses != null &&
              contact.email_addresses.data.length > 0 ? (
                <ul className="list-unstyled m-0">
                  {contact.email_addresses.data.map((email) => (
                    <li key={email.value} className="mb-1">
                      <EmailAddressSpan email={email} />
                    </li>
                  ))}
                </ul>
              ) : (
                'Contact has no email addresses'
              )}
              {newEmails != null && newEmails.length > 0 && (
                <>
                  <div className="p-1 mt-2 font-weight-semibold small highlight-success">
                    <span className="mr-2">
                      <BsPlusLg />
                    </span>{' '}
                    New emails
                  </div>
                  <ul className="list-unstyled m-0 highlight-success">
                    {newEmails.map((email) => (
                      <li key={email.value} className="p-1">
                        <EmailAddressSpan email={email} />
                      </li>
                    ))}
                  </ul>
                </>
              )}
            </ListGroupItemInfo>
            <ListGroupItemInfo icon={<FiPhone />} title="Phone numbers">
              {contact.phone_numbers != null &&
              contact.phone_numbers.data.length > 0 ? (
                <ul className="list-unstyled m-0">
                  {contact.phone_numbers.data.map((phone) => (
                    <li key={phone.value} className="mb-1">
                      <PhoneNumberSpan phone={phone} />
                    </li>
                  ))}
                </ul>
              ) : (
                'Contact has no phone numbers'
              )}
              {newPhones != null && newPhones.length > 0 && (
                <>
                  <div className="p-1 mt-2 font-weight-semibold small highlight-success">
                    <span className="mr-2">
                      <BsPlusLg />
                    </span>{' '}
                    New phone numbers
                  </div>
                  <ul className="list-unstyled m-0 highlight-success">
                    {newPhones.map((phone) => (
                      <li key={phone.value} className="p-1">
                        <PhoneNumberSpan phone={phone} />
                      </li>
                    ))}
                  </ul>
                </>
              )}
            </ListGroupItemInfo>
            <ListGroupItemInfo icon={<FiMapPin />} title="Locations">
              {contact.mailing_addresses != null &&
              contact.mailing_addresses.data.length > 0 ? (
                <ul className="list-unstyled m-0">
                  {contact.mailing_addresses.data.map((address, i) => (
                    <li key={i} className="mb-1">
                      <PostalAddressCard address={address} />
                    </li>
                  ))}
                </ul>
              ) : (
                'Contact has no mailing addresses'
              )}
              {newPostals != null && newPostals.length > 0 && (
                <>
                  <div className="p-1 mt-2 font-weight-semibold small highlight-success">
                    <span className="mr-2">
                      <BsPlusLg />
                    </span>{' '}
                    New mailing addresses
                  </div>
                  <ul className="list-unstyled m-0 highlight-success">
                    {newPostals.map((address, i) => (
                      <li key={i} className="p-1">
                        <PostalAddressCard address={address} />
                      </li>
                    ))}
                  </ul>
                </>
              )}
            </ListGroupItemInfo>
            <ListGroupHeader variant={variant}>Additional info</ListGroupHeader>
            <ListGroupItemInfo icon={<FiTag />} title="Tags">
              {contact.tags != null && contact.tags.length > 0 ? (
                <ul className="list-unstyled m-0">
                  {contact.tags.map((tag, i) => (
                    <li key={i} className="mb-1">
                      <TagSpanFromId tagId={tag} />
                    </li>
                  ))}
                </ul>
              ) : (
                'Contact has no tags'
              )}
              {newTags != null && newTags.length > 0 && (
                <>
                  <div className="p-1 mt-2 font-weight-semibold small highlight-success">
                    <span className="mr-2">
                      <BsPlusLg />
                    </span>{' '}
                    New tags
                  </div>
                  <ul className="list-unstyled m-0 highlight-success">
                    {newTags.map((tagId, i) => (
                      <li key={i} className="p-1">
                        <TagSpanFromId tagId={tagId} />
                      </li>
                    ))}
                  </ul>
                </>
              )}
            </ListGroupItemInfo>
            <ListGroupItemInfo icon={<FiEdit />} title="Custom fields">
              {contact.custom_fields != null &&
              Object.keys(contact.custom_fields).length > 0 ? (
                <ul className="list-unstyled m-0 mt-2">
                  {Object.entries(contact.custom_fields).map(([id, value]) => (
                    <li key={id} className="mb-1">
                      <CustomFieldCardFromId
                        id={id as IdCustomField}
                        value={value}
                      />
                    </li>
                  ))}
                </ul>
              ) : (
                'Contact has no custom fields'
              )}
              {newCustomFields != null && newCustomFields.length > 0 && (
                <>
                  <div className="p-1 mt-2 font-weight-semibold small highlight-success">
                    <span className="mr-2">
                      <BsPlusLg />
                    </span>{' '}
                    New custom fields
                  </div>
                  <ul className="list-unstyled m-0 highlight-success">
                    {newCustomFields.map(([id, value]) => (
                      <li key={id} className="p-1">
                        <CustomFieldCardFromId
                          id={id as IdCustomField}
                          value={value}
                        />
                      </li>
                    ))}
                  </ul>
                </>
              )}
            </ListGroupItemInfo>
          </ListGroup>
        )}
      </ContentStack>
    </Card>
  )
}
