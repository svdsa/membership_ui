import type { Meta, Story } from '@storybook/react'
import React from 'react'
import {
  ReviewMergeActionChooser,
  ReviewMergeActionChooserProps,
} from 'src/js/components/admin/import_review/ReviewMergeActionChooser'

export default {
  title: 'Admin/Import Events/ReviewMergeActionChooser',
  component: ReviewMergeActionChooser,
} as Meta

const Template: Story<ReviewMergeActionChooserProps> = (args) => (
  <ReviewMergeActionChooser {...args} />
)

export const Auto: Story<ReviewMergeActionChooserProps> = Template.bind({})
Auto.args = {
  actionType: 'match_auto',
}
