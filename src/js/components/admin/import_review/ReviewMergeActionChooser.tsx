import React from 'react'
import { ToggleButton, ToggleButtonGroup } from 'react-bootstrap'
import { FiUserPlus, FiUsers, FiUserX } from 'react-icons/fi'
import { ImportEventReviewActionType } from 'src/js/api/schemas'

export interface ReviewMergeActionChooserProps {
  actionType: ImportEventReviewActionType
  onChangeActionType(action: ImportEventReviewActionType): void
}

function flattenActionType(
  action: ImportEventReviewActionType
): ImportEventReviewActionType {
  switch (action) {
    case 'create_new_contact':
    case 'proposed_create':
      return 'create_new_contact'
    case 'proposed_match':
    case 'match_auto':
    case 'match_manual':
      return 'match_manual'
    case 'ignore_once':
    default:
      return 'ignore_once'
  }
}

export const ReviewMergeActionChooser: React.FC<ReviewMergeActionChooserProps> =
  ({ actionType, onChangeActionType }) => {
    const flattenedAction = flattenActionType(actionType)

    return (
      <div className="d-flex flex-row">
        <ToggleButtonGroup
          type="radio"
          name="merge_action"
          value={flattenedAction}
          onChange={onChangeActionType}
        >
          <ToggleButton value="create_new_contact" variant="outline-primary">
            <div className="d-flex flex-column align-items-center">
              <span className="h2">
                <FiUserPlus />
              </span>
              <span>Create</span>
            </div>
          </ToggleButton>
          <ToggleButton value="match_manual" variant="outline-primary">
            <div className="d-flex flex-column align-items-center">
              <span className="h2">
                <FiUsers />
              </span>
              <span>Match</span>
            </div>
          </ToggleButton>
          <ToggleButton value="ignore_once" variant="outline-primary">
            <div className="d-flex flex-column align-items-center">
              <span className="h2">
                <FiUserX />
              </span>
              <span>Ignore</span>
            </div>
          </ToggleButton>
        </ToggleButtonGroup>
      </div>
    )
  }
