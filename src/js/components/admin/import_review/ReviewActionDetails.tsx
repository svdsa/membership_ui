import { skipToken } from '@reduxjs/toolkit/dist/query'
import { compact } from 'lodash'
import React, { useCallback, useMemo, useState } from 'react'
import { Alert, Card, Col, ListGroup, Row } from 'react-bootstrap'
import {
  BsCheckCircle,
  BsCheckCircleFill,
  BsPersonSquare,
  BsThreeDots,
} from 'react-icons/bs'
import {
  FiArrowRight,
  FiSearch,
  FiUser,
  FiUserPlus,
  FiUserX,
} from 'react-icons/fi'
import {
  useGetContactFromIdQuery,
  useUpdateReviewActionsMutation,
} from 'src/js/api'
import {
  ContactResponse,
  CREATE_ACTIONS,
  IdContact,
  ImportEventReviewAction,
  ImportEventReviewActionType,
  ImportEventReviewActionUpdate,
  ImportEventReviewMerge,
  ImportReviewMergeActionType,
  MATCH_ACTIONS,
} from 'src/js/api/schemas'
import { ImportCandidateCard } from 'src/js/components/admin/import_review/ImportCandidateCard'
import { MatchTargetCard } from 'src/js/components/admin/import_review/MatchTargetCard'
import { ReviewMergeActionChooser } from 'src/js/components/admin/import_review/ReviewMergeActionChooser'
import { LoadingButton } from 'src/js/components/common/LoadingButton'
import { ContactSearchAutosuggest } from 'src/js/components/contacts/ContactSearchAutosuggest'

interface UpdateValidationError {
  message: string
}

interface ReviewActionDetailsProps {
  action: ImportEventReviewAction
  onDirty(dirty: boolean): void
  onUpdate(update: ImportEventReviewActionUpdate): void
  readOnly: boolean
}

export const ReviewActionDetails: React.FC<ReviewActionDetailsProps> = ({
  action,
  onDirty,
  onUpdate,
  readOnly,
}) => {
  const [dirtyType, setDirtyType] =
    useState<ImportEventReviewActionType | null>(null)
  const [dirtyMergeTarget, setDirtyMergeTarget] = useState<IdContact | null>(
    null
  )
  const [dirtyMergeDirectives, setDirtyMergeDirectives] =
    useState<ImportEventReviewMerge | null>(null)
  const dirty =
    dirtyType != null ||
    dirtyMergeTarget != null ||
    dirtyMergeDirectives != null

  const resolvedType = useMemo(
    () => dirtyType ?? action.action,
    [action, dirtyType]
  )
  const resolvedMergeTargetId = useMemo(
    () => dirtyMergeTarget ?? action.merge_into_contact_id,
    [action, dirtyMergeTarget]
  )
  const resolvedMergeDirectives = useMemo(
    () => ({ ...action.merge, ...(dirtyMergeDirectives ?? {}) }),
    [action, dirtyMergeDirectives]
  )
  const updatePatch: ImportEventReviewActionUpdate | null = useMemo(() => {
    if (!dirty) {
      return null
    }

    if (!MATCH_ACTIONS.has(resolvedType)) {
      return {
        id: action.id,
        action: resolvedType,
      }
    }

    return {
      id: action.id,
      action: resolvedType,
      merge_into_contact_id: dirtyMergeTarget ?? action.merge_into_contact_id,
      merge: dirtyMergeDirectives ?? undefined,
    }
  }, [action, dirtyMergeTarget, dirtyMergeDirectives, dirty, resolvedType])
  const [isUpdateValid, updateValidationErrors] = useMemo<
    [boolean, UpdateValidationError[] | null]
  >(() => {
    if (updatePatch == null) {
      return [false, null]
    }

    if (
      !MATCH_ACTIONS.has(updatePatch.action) &&
      (updatePatch.merge != null || updatePatch.merge_into_contact_id != null)
    ) {
      return [
        false,
        [
          {
            message:
              'Invalid state: update is not a matching action, but contains merge directive info',
          },
        ],
      ]
    }

    if (MATCH_ACTIONS.has(updatePatch.action)) {
      if (updatePatch.merge_into_contact_id == null) {
        return [
          false,
          [
            {
              message: 'Choose a contact to match with',
            },
          ],
        ]
      }

      if (updatePatch.merge == null) {
        return [
          false,
          [
            {
              message:
                "You didn't choose any fields to import, so this import will be ignored. Choose some fields to import above, or choose 'Ignore' to skip importing this contact",
            },
          ],
        ]
      }
    }

    if (
      updatePatch.merge != null &&
      updatePatch.merge_into_contact_id == null
    ) {
      return [
        false,
        [
          {
            message:
              'Invalid state: merge target ID not present but merge directives exist',
          },
        ],
      ]
    }

    if (updatePatch.merge_into_contact_id) {
      // check for merge validity
      if (updatePatch.merge == null) {
        // match action type, but not taking any fields
        return [
          false,
          [
            {
              message:
                "You didn't choose any fields to import, so this import will be ignored. Choose some fields to import above, or choose 'Ignore' to skip importing this contact",
            },
          ],
        ]
      }

      if (
        compact(Object.values(updatePatch.merge)).every(
          (item) => item.merge_action === 'keep_existing'
        )
      ) {
        // match action type, but all fields are "keep_existing" noops
        return [
          false,
          [
            {
              message:
                "You didn't choose any fields to import, so this import will be ignored. Choose some fields to import above, or choose 'Ignore' to skip importing this contact",
            },
          ],
        ]
      }
    }

    return [true, null]
  }, [updatePatch])

  const {
    data: resolvedMergeTarget,
    isLoading: mergeTargetLoading,
    error: mergeTargetError,
  } = useGetContactFromIdQuery(resolvedMergeTargetId ?? skipToken)

  const [
    submitUpdate,
    { data: updatedReviewAction, isLoading: updating, error: updateError },
  ] = useUpdateReviewActionsMutation()

  const handleChangeAction = useCallback(
    (type: ImportEventReviewActionType) => {
      onDirty(true)
      setDirtyType(type)
    },
    [onDirty, setDirtyType]
  )

  const handleTakeDirective = useCallback(
    (
        name: keyof ImportEventReviewMerge,
        mergeAction: ImportReviewMergeActionType = 'take_incoming'
      ) =>
      () => {
        const directivesOrEmpty = dirtyMergeDirectives ?? {}

        onDirty(true)
        setDirtyMergeDirectives({
          ...directivesOrEmpty,
          [name]: {
            attribute: name,
            merge_action:
              directivesOrEmpty[name] == null ||
              directivesOrEmpty[name]?.merge_action == 'keep_existing'
                ? mergeAction
                : 'keep_existing',
          },
        })
      },
    [dirtyMergeDirectives, setDirtyMergeDirectives, onDirty]
  )

  const handleSelectMatchTarget = (contact: ContactResponse | null) => {
    setDirtyMergeTarget(contact?.id ?? null)
  }

  const handleResetState = () => {
    setDirtyType(null)
    setDirtyMergeTarget(null)
    setDirtyMergeDirectives(null)
    onDirty(false)
  }

  const handleSubmitUpdate = async () => {
    if (updatePatch == null) {
      // Do nothing
      return
    }

    await submitUpdate({
      id: action.import_event_id,
      body: { actions: [updatePatch] },
    })
    onUpdate(updatePatch)
    handleResetState()
  }

  const matchReady =
    MATCH_ACTIONS.has(resolvedType) && resolvedMergeTarget != null

  return (
    <section>
      {!readOnly && (
        <Row className="mb-2">
          <Col className="d-flex flex-column align-items-center">
            <div>
              <ReviewMergeActionChooser
                actionType={resolvedType}
                onChangeActionType={handleChangeAction}
              />
            </div>
          </Col>
        </Row>
      )}
      {/* {!MATCH_ACTIONS.has(resolvedType) && ( */}
      <Row>
        <Col md={6} xl={5} className="mb-3">
          <ImportCandidateCard
            contact={action.participant}
            prefix={{
              icon: <FiUser />,
              title: readOnly
                ? 'Source info imported'
                : 'Choose fields to import',
            }}
            variant="warning"
            mergeDirectives={resolvedMergeDirectives}
            mergeTarget={resolvedMergeTarget}
            mergeTargetLoading={mergeTargetLoading}
            onSelect={handleTakeDirective}
            readOnly={readOnly || !matchReady}
            hideIcons={!matchReady}
          />
        </Col>
        <Col
          lg={2}
          className="d-none d-xl-flex flex-column align-items-center justify-content-center"
        >
          {(CREATE_ACTIONS.has(resolvedType) ||
            resolvedType === 'ignore_once') && (
            <span className="display-2 text-muted">
              <FiArrowRight />
            </span>
          )}
          {MATCH_ACTIONS.has(resolvedType) && resolvedMergeTargetId == null && (
            <span className="display-2 text-muted">
              <BsThreeDots />
            </span>
          )}
          {MATCH_ACTIONS.has(resolvedType) && resolvedMergeTargetId != null && (
            <div className="d-flex flex-column align-items-center">
              <span className="display-2 text-muted">
                <FiArrowRight />
              </span>
              <span className="text-center">Import into</span>
            </div>
          )}
        </Col>
        <Col
          md={6}
          xl={5}
          className="d-flex flex-column justify-content-stretch"
        >
          {CREATE_ACTIONS.has(resolvedType) && (
            <Card border="primary" className="flex-fill">
              <Card.Body className="d-flex text-primary flex-column align-items-center justify-content-center text-center">
                <div className="display-2 mb-2">
                  <FiUserPlus />
                </div>
                <div>A new contact will be created</div>
              </Card.Body>
            </Card>
          )}
          {resolvedType === 'ignore_once' && (
            <Card border="danger" className="flex-fill">
              <Card.Body className="d-flex text-danger flex-column align-items-center justify-content-center text-center">
                <div className="display-2 mb-2">
                  <FiUserX />
                </div>
                <div>This contact won't be imported</div>
              </Card.Body>
            </Card>
          )}
          {MATCH_ACTIONS.has(resolvedType) && resolvedMergeTargetId == null && (
            <Card border="info" className="flex-fill">
              <ListGroup variant="flush">
                <ListGroup.Item variant="info" className="px-2 py-2">
                  <ContactSearchAutosuggest
                    onContactSelected={handleSelectMatchTarget}
                  />
                </ListGroup.Item>
              </ListGroup>
              <Card.Body className="d-flex text-info flex-column justify-content-center align-items-center flex-fill">
                <span className="display-2 mb-2">
                  <FiSearch />
                </span>
                <div>Choose a contact to match with</div>
              </Card.Body>
            </Card>
          )}
          {MATCH_ACTIONS.has(resolvedType) && resolvedMergeTargetId != null && (
            <MatchTargetCard
              variant="info"
              header={
                readOnly ? (
                  <div className="mx-2 my-1">
                    <span className="mr-2">
                      <BsPersonSquare />
                    </span>{' '}
                    Destination contact
                  </div>
                ) : (
                  <ContactSearchAutosuggest
                    onContactSelected={handleSelectMatchTarget}
                    value={resolvedMergeTarget}
                  />
                )
              }
              mergeDirectives={resolvedMergeDirectives}
              mergeTarget={resolvedMergeTarget}
              mergeTargetLoading={mergeTargetLoading}
              mergeTargetError={mergeTargetError}
              candidate={action.participant}
            />
          )}
        </Col>
      </Row>
      {/* {MATCH_ACTIONS.has(resolvedType) && resolvedMergeTargetId != null && (
        <ReviewActionMatchCard
          action={action}
          contactId={resolvedMergeTargetId}
        />
      )} */}
      <hr />
      {!readOnly && (
        <>
          {updateValidationErrors != null && updateValidationErrors.length > 0 && (
            <Alert variant="warning">
              <Alert.Heading className="h5">Before we proceed</Alert.Heading>
              <ul className="mb-0">
                {updateValidationErrors.map((error, i) => (
                  <li key={i}>{error.message}</li>
                ))}
              </ul>
            </Alert>
          )}
          {isUpdateValid && updateValidationErrors == null && (
            <p className="font-weight-semibold">
              <span className="text-success mr-2">
                <BsCheckCircleFill />
              </span>{' '}
              Everything looks good!
            </p>
          )}
          {!isUpdateValid && updateValidationErrors == null && (
            <p className="font-weight-semibold">
              <span className="text-secondary mr-2">
                <BsCheckCircle />
              </span>{' '}
              No changes yet.
            </p>
          )}
          <LoadingButton
            variant="success"
            isLoading={updating}
            disabled={!isUpdateValid}
            disabledVariant="outline-success"
            onClick={handleSubmitUpdate}
          >
            Update
          </LoadingButton>
          <hr />
        </>
      )}
      <details>
        <summary>Developer info</summary>
        <Row>
          <Col md={6}>
            <h3>Update request</h3>
            <pre>{JSON.stringify(updatePatch, null, 2)}</pre>
          </Col>
          <Col md={6}>
            <h3>Review action</h3>
            <pre>{JSON.stringify(action, null, 2)}</pre>
          </Col>
        </Row>
      </details>
    </section>
  )
}
