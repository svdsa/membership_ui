import pluralize from 'pluralize'
import React from 'react'
import type { ImportEventReviewActionType } from 'src/js/api/schemas'

interface ReviewActionIconProps {
  action: ImportEventReviewActionType
  count: number

  /** enabling this will use the `x"ing"` tense of the verb */
  presentParticiple?: boolean
}

export const ReviewCountActionsSpan: React.FC<ReviewActionIconProps> = ({
  action,
  count,
  presentParticiple,
}) => {
  const pluralizedItem = pluralize('contact', count)
  switch (action) {
    case 'create_new_contact':
    case 'proposed_create': {
      return (
        <span>
          {presentParticiple ? 'Creating' : 'Create'} {count} {pluralizedItem}
        </span>
      )
    }
    case 'match_auto':
    case 'match_manual':
    case 'proposed_match':
      return (
        <span>
          {presentParticiple ? 'Matching' : 'Match'} {count} {pluralizedItem}{' '}
          with existing contacts
        </span>
      )
    case 'ignore_once':
      return (
        <span>
          {presentParticiple ? 'Skipping' : 'Skip'} {count} {pluralizedItem}{' '}
          (for this import)
        </span>
      )
  }
}
