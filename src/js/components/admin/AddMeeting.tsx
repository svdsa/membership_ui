import { fromPairs } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, Form } from 'react-bootstrap'
import { Api } from 'src/js/client/ApiClient'
import PageHeading from 'src/js/components/common/PageHeading'
import { MeetingClient, Meetings } from '../../client/MeetingClient'
import { logError } from '../../util/util'
import FieldGroup from '../common/FieldGroup'

interface AddMeetingProps {
  memberId: number
  refresh: () => void
}

interface MeetingAttendeeForm {
  member_id: number
  meeting_id: number
}

interface AddMeetingState {
  meetings: { [key: number]: string }
  attendee: MeetingAttendeeForm
  inSubmission: boolean
}

export default class AddMeeting extends Component<
  AddMeetingProps,
  AddMeetingState
> {
  constructor(props: AddMeetingProps) {
    super(props)
    this.state = {
      meetings: {},
      attendee: { member_id: this.props.memberId, meeting_id: 0 },
      inSubmission: false,
    }
  }

  componentDidMount() {
    // TODO(jesse): hide the form behind an edit button to save bandwith for admins
    this.getMeetings()
  }

  updateAttendee = (formKey: string, value: string) => {
    if (this.state.inSubmission) {
      return
    }

    this.setState({
      attendee: {
        ...this.state.attendee,
        [formKey]: value,
      },
    })
  }

  render() {
    return (
      <section className="add-meeting-attendance-section">
        <PageHeading level={3}>Add meeting attendance</PageHeading>
        <Form onSubmit={(e) => e.preventDefault()}>
          <FieldGroup
            formKey="meeting_id"
            label="Meeting to attend"
            componentClass="select"
            options={this.state.meetings}
            placeholder="Select a meeting"
            value={this.state.attendee.meeting_id}
            onFormValueChange={this.updateAttendee}
            required
          />
          <Button
            type="submit"
            onClick={(e) => this.submitForm(e, this.addAttendee)}
            disabled={this.state.attendee.meeting_id === 0}
          >
            Attend this meeting
          </Button>
        </Form>
      </section>
    )
  }

  async getMeetings(): Promise<void> {
    try {
      const results = await Meetings.all()
      const meetingNamesById = results.map((m) => [m.id, m.name])
      this.setState({ meetings: fromPairs(meetingNamesById) })
    } catch (err) {
      logError('Error loading meetings', err)
    }
  }

  addAttendee: () => Promise<void> = async () => {
    const meetings = new MeetingClient(Api) // TODO: Replace with redux
    const attendee: MeetingAttendeeForm = this.state.attendee
    await meetings.addAttendee(attendee.meeting_id, attendee.member_id)
  }

  async submitForm(
    e: React.MouseEvent<HTMLElement>,
    action: () => Promise<unknown>
  ): Promise<void> {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await action()
      this.props.refresh()
    } catch (err) {
      logError('Error adding attendee', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}
