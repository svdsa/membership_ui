import { fromPairs } from 'lodash/fp'
import React, { useState } from 'react'
import {
  Button,
  Col,
  Dropdown,
  DropdownButton,
  Form,
  InputGroup,
  OverlayTrigger,
  Popover,
  Row,
  Spinner,
  Table,
} from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { FiCheckCircle, FiHelpCircle } from 'react-icons/fi'
import { useQuery } from 'react-query'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Column, useTable } from 'react-table'
import {
  EligibleMemberListEntry,
  Members,
  MemberSearchResponse,
} from 'src/js/client/MemberClient'
import {
  DEFAULT_COLUMNS,
  MemberListColumn,
} from 'src/js/components/admin/MemberList/columns'
import Loading from 'src/js/components/common/Loading'
import NotFound from 'src/js/components/common/NotFound'
import { c } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { useDebounce } from 'src/js/util/hooks'
import { isAdmin } from '../../services/members'
import PageHeading from '../common/PageHeading'

const PAGE_SIZE = 25

type MemberListStateProps = RootReducer
type MemberListPageProps = MemberListStateProps

export type MembershipSearchOperator = 'none' | 'valid' | 'expired' | 'new'

interface SearchOperators {
  is: string
  membership: MembershipSearchOperator
  location: string
  city: string
  zip: string
}

interface MemberQueryProps {
  query: string
  cursor?: number
}

async function fetchMemberList({
  query,
  cursor,
}: MemberQueryProps): Promise<MemberSearchResponse> {
  const response = await Members.search(query, PAGE_SIZE, cursor)
  return response
}

const MEMBERSHIP_QUERY_REGEX = /membership:(\w*)|is:(\w*)/

const MemberListPage: React.FC<MemberListPageProps> = (props) => {
  if (!isAdmin(props.member)) {
    return <NotFound />
  }

  return (
    <>
      <Helmet>
        <title>Member roster</title>
      </Helmet>
      <Row>
        <Col>
          <PageHeading className="mt-0" level={2}>
            Member roster
          </PageHeading>
        </Col>
      </Row>
      <MemberList
        additionalControls={
          <Link to={`/members/create`}>
            <Button>Add member</Button>
          </Link>
        }
      />
    </>
  )
}

interface MemberListProps {
  placeholder?: string
  columns?: MemberListColumn[]
  additionalControls?: JSX.Element
  query?: string
}

export const MemberList: React.FC<MemberListProps> = ({
  placeholder = 'Search members...',
  columns = DEFAULT_COLUMNS,
  additionalControls,
  query: originalQuery,
}) => {
  const [query, setQuery] = useState(originalQuery || '')
  const debouncedQuery = useDebounce(query, 250)
  const [cursor, setCursor] = useState(0)
  const [hasMore, setHasMore] = useState(false)
  const { data, isLoading, error } = useQuery(
    ['member-list', { query: debouncedQuery, cursor }],
    () => fetchMemberList({ query, cursor }),
    {
      keepPreviousData: true,
    }
  )

  const loadMoreList = async () => {
    const cursor = data?.cursor
    const hasMore = data?.has_more

    console.log('Last page cursor:', cursor)

    if (data != null && cursor != null && hasMore != null) {
      setCursor(cursor)
    }

    setHasMore(hasMore || false)
  }

  const parseSearchOperators = (query: string): { [key: string]: string } => {
    const matched = query.matchAll(/(\w*):(\w*)/g)
    return fromPairs(
      [...matched].map(([_, operator, qualifier]) => [operator, qualifier])
    )
  }

  const handleToggleMembership =
    (membershipState: MembershipSearchOperator) => (e) => {
      const operators = parseSearchOperators(query)
      const strippedQuery = query
        .replace(/membership:(\w*)/, '')
        .replace(/is:(\w*)/, '')

      console.log(strippedQuery, operators, 'replace with', membershipState)

      if (
        operators['membership'] === membershipState ||
        operators['is'] === membershipState
      ) {
        // Remove the operator from the query
        setQuery(strippedQuery.trim())
      } else {
        // Add the new operator in the query
        setQuery(`membership:${membershipState} ${strippedQuery}`.trim())
      }
    }

  const handleFetchMore: React.MouseEventHandler<HTMLButtonElement> = () =>
    loadMoreList()

  return (
    <>
      <header className="mb-2">
        <Row noGutters={true}>
          <Col>
            <InputGroup>
              <Form.Control
                type="text"
                value={query}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  setQuery(e.target.value)
                }
                placeholder={placeholder}
              />
              <InputGroup.Append
                className={`clear-member-search ${query && 'can-clear'}`}
                onClick={() => setQuery('')}
              >
                <InputGroup.Text>x</InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
          </Col>
          <Col lg="auto" className="d-flex flex-row justify-content-around">
            <DropdownButton
              id="membership-dropdown"
              title="Membership"
              variant="outline-secondary"
              className="ml-2"
            >
              <Dropdown.Item onClick={handleToggleMembership('valid')}>
                {query.indexOf('membership:valid') >= 0 && <FiCheckCircle />}{' '}
                Active
              </Dropdown.Item>
              <Dropdown.Item onClick={handleToggleMembership('expired')}>
                {query.indexOf('membership:expired') >= 0 && <FiCheckCircle />}{' '}
                Expired
              </Dropdown.Item>
              <Dropdown.Item onClick={handleToggleMembership('new')}>
                {query.indexOf('membership:new') >= 0 && <FiCheckCircle />} New
              </Dropdown.Item>
              <Dropdown.Item onClick={handleToggleMembership('none')}>
                {query.indexOf('membership:none') >= 0 && <FiCheckCircle />}{' '}
                Missing
              </Dropdown.Item>
            </DropdownButton>
            <OverlayTrigger
              trigger="click"
              placement="bottom"
              overlay={
                <Popover id="help-popover">
                  <Popover.Title as="h3">Search help</Popover.Title>
                  <Popover.Content className="help-popover-content">
                    <section>
                      <p>
                        To search for members by their name or email, just type
                        into the search box.
                      </p>
                      <h4>Membership status</h4>
                      <p>
                        To search for members by membership status, use the
                        Membership dropdown, or type:
                      </p>
                      <ul>
                        <li>
                          <code>membership:valid</code> to find all members with
                          paid dues
                        </li>
                        <li>
                          <code>membership:expired</code> to find members with
                          lapsed dues{' '}
                        </li>
                        <li>
                          <code>membership:new</code> to find members that
                          joined recently (within the past few weeks)
                        </li>
                        <li>
                          <code>membership:none</code> to find users with no
                          membership information
                        </li>
                      </ul>
                      <h4>Location</h4>
                      <p>To search for members by location, type:</p>
                      <ul>
                        <li>
                          <code>location:[city or zip]</code>
                        </li>
                        <li>
                          <code>zip:[zip code]</code>, or
                        </li>
                        <li>
                          <code>city:[city]</code>
                        </li>
                      </ul>
                    </section>
                  </Popover.Content>
                </Popover>
              }
            >
              <Button variant="outline-secondary" className="mx-2">
                <FiHelpCircle /> Help
              </Button>
            </OverlayTrigger>
            {additionalControls}
          </Col>
        </Row>
      </header>
      <section className="member-list-contents">
        <MemberListTableStack
          debouncedQuery={debouncedQuery}
          members={data?.members || null}
          columns={columns}
          isLoading={isLoading}
          error={error as Error | null}
        />
        <div className="member-list-bottom-controls">
          <Button
            className="load-more"
            disabled={!hasMore}
            onClick={handleFetchMore}
            variant="secondary"
          >
            {isLoading ? <Spinner animation="border" /> : 'Load more...'}
          </Button>
        </div>
      </section>
      {data != null && (
        <section className="member-list-footer">
          <div className="member-list-table-indicators">
            <span className="records-indicator">
              Showing {cursor} to {data.members.length + cursor} members
            </span>
          </div>
        </section>
      )}
    </>
  )
}

const LoadingMemberListTable: React.FC = () => (
  <section className="loading-member-list non-ideal-member-list">
    <Loading />
  </section>
)

const EmptyMemberListTable: React.FC = () => (
  <section className="empty-member-list non-ideal-member-list">
    <PageHeading level={2}>No members 😿</PageHeading>
    <p>Maybe you should add some!</p>
    <p>
      <Link to={`/members/create`}>
        <Button>Add member</Button>
      </Link>
      <Link to={`/import/national`}>
        <Button>Import a National DSA roster</Button>
      </Link>
    </p>
  </section>
)

const NoSearchResultsMemberListTable: React.FC = () => (
  <section className="empty-member-list non-ideal-member-list">
    <PageHeading level={2}>No results 🙈</PageHeading>
  </section>
)

const ErrorMemberListTable: React.FC<{ error: Error }> = ({ error }) => (
  <section className="error-member-list non-ideal-member-list">
    <PageHeading level={2}>Error loading members 🆘</PageHeading>
    <p>Try refreshing the page and trying again.</p>
    <p>If that doesn't work, contact {c('ADMIN_EMAIL')}.</p>
    <code>{JSON.stringify(error)}</code>
  </section>
)

interface MemberListTableStackProps {
  debouncedQuery: string
  members: EligibleMemberListEntry[] | null
  columns: Column<EligibleMemberListEntry>[]
  isLoading?: boolean
  error: Error | null
}

export const MemberListTableStack: React.FC<MemberListTableStackProps> = ({
  debouncedQuery,
  members,
  columns,
  isLoading,
  error,
}) => {
  if (error != null) {
    return <ErrorMemberListTable error={error} />
  } else if (isLoading && (members == null || members.length === 0)) {
    return <LoadingMemberListTable />
  } else if (members == null || members.length === 0) {
    if (debouncedQuery === '') {
      return <EmptyMemberListTable />
    } else {
      return <NoSearchResultsMemberListTable />
    }
  } else {
    return <MemberListTable members={members} columns={columns} />
  }
}

interface TableProps {
  columns: Column<EligibleMemberListEntry>[]
  data: EligibleMemberListEntry[]
}

const MemberTable: React.FC<TableProps> = ({ columns, data }) => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({
      columns,
      data,
    })

  return (
    <Table
      striped
      bordered
      hover
      {...getTableProps()}
      className="member-list-table"
    >
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </Table>
  )
}

interface MemberListTableProps {
  members: EligibleMemberListEntry[]
  columns: Column<EligibleMemberListEntry>[]
}

const MemberListTable: React.FC<MemberListTableProps> = ({
  members,
  columns,
}) => {
  return <MemberTable columns={columns} data={members} />
}

export default connect<MemberListStateProps, null, null, RootReducer>(
  (state) => state
)(MemberListPage)
