import dateFormat from 'dateformat'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { ElectionDetailsResponse } from 'src/js/client/ElectionClient'
import { MemberVote } from 'src/js/client/MemberClient'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { electionStatus } from '../../services/elections'

type UpcomingElectionsStateProps = RootReducer
type UpcomingElectionsProps = UpcomingElectionsStateProps

class UpcomingElections extends Component<UpcomingElectionsProps> {
  render() {
    const allVotes: MemberVote[] = this.props.member.user.data?.votes || []
    const futureElections = allVotes
      .map((vote) => this.getElection(vote.election_id))
      .filter((election) => election != null)
      .filter((election) => this.isFutureElection(election))

    if (futureElections.length === 0) {
      return <p>There are no upcoming elections scheduled for you.</p>
    } else {
      return (
        <div>
          {futureElections.map((election) => this.renderElection(election))}
        </div>
      )
    }
  }

  renderElection(election: ElectionDetailsResponse) {
    const startTime = new Date(election.voting_begins_epoch_millis)

    return (
      <p key={`election-${election.id}`}>
        Voting on{' '}
        <strong>
          <Link to={`/elections/${election.id}`}>{election.name}</Link>
        </strong>{' '}
        begins {dateFormat(startTime, "dddd, mmmm dS 'at' h:MM TT")}.
      </p>
    )
  }

  getElection(electionId: number): ElectionDetailsResponse {
    return this.props.elections.byId[electionId]
  }

  isFutureElection(election: ElectionDetailsResponse): boolean {
    return electionStatus(election) === 'polls not open'
  }
}

export default connect<
  UpcomingElectionsStateProps,
  null,
  Record<string, never>,
  RootReducer
>((state) => state)(UpcomingElections)
