import classNames from 'classnames'
import {
  extend as _extend,
  forEach as _forEach,
  get as _get,
  isObject as _isObject,
  map as _map,
  merge as _merge,
  orderBy as _orderBy,
  range as _range,
  set as _set,
} from 'lodash'
import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import {
  Bar,
  BarChart,
  Label,
  LabelList,
  ReferenceLine,
  XAxis,
  YAxis,
} from 'recharts'
import {
  ElectionCount,
  VoteCount,
  VoteCountByCandidate,
} from 'src/js/client/ElectionClient'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { membershipApi } from '../../services/membership'
import { TODO } from '../../typeMigrationShims'
import { arrayOf } from '../../util/getOfType'
import { isFullscreen } from '../../util/isFullscreen'
import '../../util/rounding'
import { round10 } from '../../util/rounding'
import { HTTP_GET, logError } from '../../util/util'

interface RCVConstants extends ElectionCount {
  electionId: number
  maxVotes: TODO
}

interface RCVSettings {
  roundDivisor: TODO
  barSize: number
  width: number
  yAxisWidth: number
  height: number
  refLineSize: string
  refLineWidth: number
  refLineOffset: number
  fullscreen: boolean
  roundDesc: TODO
  margins: TODO
  colors: TODO
  title: TODO
}

type RCVStateProps = RootReducer
type RCVProps = RCVStateProps
interface RCVState {
  currentRound: number | null
  subRound: number | null
}

export class RankedChoiceVisualization extends Component<RCVProps, RCVState> {
  constants: Partial<RCVConstants>
  settings: Partial<RCVSettings>

  constructor(props) {
    super(props)

    let colors = {
      totalVotes: '#ec1e24',
      excessVotes: '#ccc',
      eliminationVotes: '#666',
      blank: '#fff',
      quotaStroke: 'rgba(0,0,0,.25)',
      totalVotesText: '#fff',
    }

    let settings: Partial<RCVSettings> = {
      barSize: 20,
      width: 800,
      yAxisWidth: 200,
      height: 400,
      refLineSize: '5 3',
      refLineWidth: 1,
      refLineOffset: 10,
    }

    const fullscreenLarge = null

    const fullscreen = isFullscreen(
      _get(this.props, ['location', 'pathname'], '')
    )

    if (fullscreen) {
      colors = _merge({}, colors, {
        blank: '#ec1e24',
        excessVotes: '#666',
        eliminationVotes: '#333',
        totalVotes: '#fff',
        totalVotesText: '#ec1e24',
      })

      settings = _merge({}, settings, {
        barSize: 24,
        width: 720,
        yAxisWidth: 210,
        height: 500,
        refLineSize: '5 3',
        refLineWidth: 4,
        refLineOffset: 10,
      })
    }

    if (fullscreenLarge) {
      colors = _merge({}, colors, {
        blank: '#ec1e24',
        excessVotes: '#666',
        eliminationVotes: '#333',
        totalVotes: '#fff',
        totalVotesText: '#ec1e24',
      })

      settings = _merge({}, settings, {
        barSize: 30,
        width: 1000,
        yAxisWidth: 240,
        height: 600,
        refLineSize: '8 5',
        refLineWidth: 5,
        refLineOffset: 20,
      })
    }

    const electionId = _get(this.props, ['params', 'electionId'])

    if (electionId) {
      this.constants = { electionId: parseInt(electionId, 10) }
    }

    this.settings = _merge({}, settings, {
      fullscreen,
      margins: { top: 0, right: 20, bottom: 0, left: 0 },
      title: 'Election Results',
      colors,
      roundDesc: [
        'Starting Total Votes',
        {
          excess: 'Transfer of Excess Votes',
          elimination: 'Transfer of Eliminated Candidate Votes',
        },
        'Ending Vote Total',
      ],
      roundDivisor: 3,
      totalWinners: 5,
    })

    this.state = {
      currentRound: null,
      subRound: null,
    }
  }

  async componentDidMount() {
    const { electionId } = this.constants
    let electionResults = null

    if (electionId) {
      electionResults = await this.getVotes(electionId)
    }

    if (electionResults) {
      this.constants = _merge({}, this.constants, electionResults)

      const { round_information: roundInformation } = this.constants

      if (roundInformation) {
        const roundVotes: VoteCountByCandidate[] = []
        _forEach(roundInformation, (round, key) => {
          roundVotes.push(_get(round, 'votes'))
        })

        const maxVotesRound: VoteCountByCandidate =
          _orderBy<VoteCountByCandidate>(
            roundVotes,
            [(o: VoteCount) => o.starting_votes + o.votes_received]['desc']
          )[0]
        const maxVotesRecord = _orderBy(
          maxVotesRound,
          (o) => o.starting_votes + o.votes_received,
          ['desc']
        )[0]
        const maxVotes =
          round10(maxVotesRecord.starting_votes) +
          round10(maxVotesRecord.votes_received)

        this.constants = _merge({}, this.constants, {
          maxVotes,
        })
        this.setState({
          currentRound: 0,
          subRound: 1,
        })
      }
    }
  }

  async getVotes(electionId) {
    try {
      return await membershipApi(HTTP_GET, '/election/count', {
        id: electionId,
        visualization: 1,
      })
    } catch (err) {
      return logError('Error counting votes', err)
    }
  }

  incrementRound = () => {
    const { currentRound, subRound } = this.state
    const { roundDivisor } = this.settings

    if (currentRound == null || subRound == null) {
      throw new Error('Bad current round when trying to increment round')
    }

    let nextState = {}

    if (subRound % roundDivisor === 0) {
      nextState = {
        subRound: 1,
        currentRound: currentRound + 1,
      }
    } else {
      nextState = {
        subRound: subRound + 1,
      }
    }
    this.setState(_merge({}, this.state, nextState))
  }

  decrementRound = () => {
    const { currentRound, subRound } = this.state
    const { roundDivisor } = this.settings

    if (currentRound == null || subRound == null) {
      throw new Error('Bad current round when trying to decrement round')
    }

    let nextState = {}

    if (subRound > 1) {
      nextState = {
        subRound: subRound - 1,
      }
    } else {
      nextState = {
        currentRound: currentRound - 1,
        subRound: roundDivisor,
      }
    }

    this.setState(_merge({}, this.state, nextState))
  }

  getOrderedData = (data) => {
    const { candidates } = this.constants
    _map(data, (datum, key) => {
      _set(datum, 'candidateId', parseInt(key))
      _set(datum, 'candidate', _get(candidates, [parseInt(key), 'name']))
    })
    return _orderBy(data, [(o) => o.starting_votes], ['desc'])
  }

  getRoundDesc = () => {
    const { currentRound, subRound } = this.state
    const { roundDesc } = this.settings
    const { round_information: roundInformation } = this.constants

    if (currentRound == null || subRound == null) {
      throw new Error('Bad rounds when trying to get round description')
    }

    const transferType = _get(roundInformation, [currentRound, 'transfer_type'])

    if (transferType == null) {
      throw new Error('Bad transfer type')
    }

    const returnVal = roundDesc[subRound - 1]

    if (!_isObject(returnVal)) {
      return returnVal
    }

    return _get(roundDesc, [subRound - 1, transferType], '')
  }

  getChart = () => {
    const {
      round_information: roundInformation,
      maxVotes,
      quota,
    } = this.constants
    const { currentRound, subRound } = this.state

    if (roundInformation == undefined) {
      return null
    }

    if (currentRound == null || subRound == null) {
      throw new Error('Bad round when getting chart')
    }

    if (roundInformation.length > 0) {
      const transferType = _get(roundInformation, [
        currentRound,
        'transfer_type',
      ])
      const {
        barSize,
        refLineOffset,
        refLineSize,
        refLineWidth,
        roundDivisor,
        fullscreen,
        margins,
        width,
        height,
        yAxisWidth,
        colors,
      } = this.settings
      let data = this.getOrderedData(
        _get(roundInformation, [currentRound, 'votes'])
      )
      data = data.reduce((newData, datum) => {
        const starting_votes = round10(datum.starting_votes) // eslint-disable-line camelcase
        const votes_received = round10(datum.votes_received) // eslint-disable-line camelcase
        const sum = starting_votes + votes_received // eslint-disable-line camelcase
        newData.push(
          _extend({}, datum, {
            starting_votes,
            votes_received,
            sum,
          })
        )
        return newData
      }, [])

      const labelFormatter = (value) => {
        if (value > 0) {
          return value
        }
        return null
      }

      let transferColor = colors.blank
      let transferLabelList: JSX.Element | null = null
      let totalLabelList: JSX.Element | null = null
      const transferClass = 'onGrey'
      let totalClass = 'onRed'

      if (fullscreen) {
        totalClass = 'onWhite'
      }
      const containerClasses = classNames('chart', {
        fullscreen,
      })

      if (subRound % roundDivisor === 0) {
        transferColor = colors.totalVotes
        transferLabelList = (
          <LabelList
            dataKey="sum"
            className={totalClass}
            position="insideRight"
          />
        )
      } else if (subRound === 2) {
        transferColor = _get(colors, `${transferType}Votes`)
        transferLabelList = (
          <LabelList
            formatter={labelFormatter}
            dataKey="votes_received"
            position="insideRight"
            className={transferClass}
          />
        )
        totalLabelList = (
          <LabelList
            dataKey="starting_votes"
            className={totalClass}
            position="insideRight"
          />
        )
      } else {
        totalLabelList = (
          <LabelList
            dataKey="starting_votes"
            className={totalClass}
            position="insideRight"
          />
        )
      }

      const ticks = _range(0, maxVotes, 5)

      return (
        <div className={containerClasses}>
          <BarChart
            layout="vertical"
            width={width}
            height={height}
            data={data}
            margin={margins}
          >
            <XAxis type="number" axisLine={false} ticks={ticks} />
            <YAxis
              dataKey="candidate"
              width={yAxisWidth}
              axisLine={false}
              type="category"
            />
            <Bar
              dataKey="starting_votes"
              stackId="a"
              barSize={barSize}
              fill={colors.totalVotes}
            >
              {totalLabelList}
            </Bar>
            <Bar
              dataKey="votes_received"
              stackId="a"
              barSize={barSize}
              fill={transferColor}
            >
              {transferLabelList}
            </Bar>
            <ReferenceLine
              x={quota}
              label={
                <Label
                  value={`Threshold to win: ${quota}  votes`}
                  angle={-90}
                  position="left"
                  offset={refLineOffset}
                />
              }
              stroke={colors.quotaStroke}
              strokeDasharray={refLineSize}
              strokeWidth={refLineWidth}
            />
          </BarChart>
        </div>
      )
    }

    return null
  }

  getDisplayWinners = () => {
    const { winners, candidates } = this.constants
    const { currentRound } = this.state

    if (currentRound == null) {
      return null
    }

    if (winners) {
      return (
        <ol>
          {winners.map((winner, id) => (
            <li key={`winner_${id}`}>
              {_get(winner, 'round') <= currentRound + 1
                ? _get(candidates, [_get(winner, 'candidate'), 'name'])
                : ''}
            </li>
          ))}
        </ol>
      )
    }
    return null
  }

  render() {
    let chart: JSX.Element | null = null
    const { currentRound, subRound } = this.state
    const { round_information: roundInformation } = this.constants

    if (currentRound == null || subRound == null) {
      if (roundInformation) {
        if (roundInformation.length > 0) {
          throw new Error('Bad current round when rendering')
        } else {
          return (
            <div>
              This election is not yet finalized. Please check back when voting
              is concluded.
            </div>
          )
        }
      }
      return <div>Loading...</div>
    }

    if (roundInformation) {
      chart = this.getChart()
    }
    const displayRound = currentRound + 1

    const displayWinners = this.getDisplayWinners()

    let previous = (
      <Button disabled aria-label="Previous">
        &laquo;
      </Button>
    )

    if (currentRound > 0 || subRound > 1) {
      previous = (
        <Button onClick={this.decrementRound} aria-label="Previous">
          &laquo;
        </Button>
      )
    }

    let next = (
      <Button disabled aria-label="Next">
        &raquo;
      </Button>
    )

    if (currentRound < arrayOf(roundInformation).length - 1) {
      next = (
        <Button onClick={this.incrementRound} aria-label="Next">
          &raquo;
        </Button>
      )
    }

    return (
      <div>
        <Helmet>
          <title>Election Results</title>
        </Helmet>
        <div className="titleBox">
          <h2 className="roundTitle">
            Round {displayRound} <small>{this.getRoundDesc()}</small>
          </h2>
          <h3 className="title rose">{this.settings.title}</h3>
        </div>
        <div className="chartBox">
          {chart}
          <div className="infoBox">
            <ul className="legend">
              <li>
                <i
                  style={{
                    backgroundColor: this.settings.colors.totalVotes,
                  }}
                />
                <strong>Total Votes</strong>
              </li>
              <li>
                <i
                  style={{
                    backgroundColor: this.settings.colors.excessVotes,
                  }}
                />
                <strong>Excess Votes</strong>
              </li>
              <li>
                <i
                  style={{
                    backgroundColor: this.settings.colors.eliminationVotes,
                  }}
                />
                <strong>Elimination Votes</strong>
              </li>
            </ul>
            <div className="winnerBox">
              <h4>Winners</h4>
              {displayWinners}
            </div>
          </div>
        </div>
        <div className="buttonBox">
          {previous}
          {next}
        </div>
      </div>
    )
  }
}

export default connect<RCVStateProps, null, {}, RootReducer>((state) => state)(
  RankedChoiceVisualization
)
