import { sortBy } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import { ElectionResponse } from 'src/js/client/ElectionClient'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { fetchElections } from '../../redux/actions/electionActions'
import { isAdmin } from '../../services/members'
import PageHeading from '../common/PageHeading'

type ElectionsProps = ElectionsStateProps & ElectionsDispatchProps

class Elections extends Component<ElectionsProps> {
  componentDidMount() {
    this.props.fetchElections()
  }

  render() {
    const admin = isAdmin(this.props.member)
    const sortByElectionIdDesc = sortBy<ElectionResponse>(
      (election) => -election.id
    )
    const elections = sortByElectionIdDesc(
      Object.values(this.props.elections.byId)
    )
    const electionRows = elections.map((election) => {
      const id = election.id
      const displayStartTime = election.voting_begins_epoch_millis !== null
      const displayEndTime =
        displayStartTime && election.voting_begins_epoch_millis !== null
      const startTime =
        displayStartTime && new Date(election.voting_begins_epoch_millis)
      const endTime =
        displayEndTime && new Date(election.voting_ends_epoch_millis)

      return (
        <Row
          key={`election-toolbox-${id}`}
          className="election-toolbox"
          style={{ marginTop: '13px' }}
        >
          <Col sm={3}>
            <Link to={`/elections/${id}/`}>{election.name}</Link>
          </Col>
          {admin && (
            <Col sm={9}>
              <Link to={`/elections/${id}/print`}>Print Ballots</Link>
              &nbsp;&nbsp;
              <Link
                key={`enter-ballots-${id}`}
                to={`/admin/elections/${id}/vote`}
              >
                Enter Ballots
              </Link>
              &nbsp;&nbsp;
              <Link to={`/elections/${id}/signin`}>Sign In Kiosk</Link>
              &nbsp;&nbsp;
              <Link to={`/elections/${id}/edit`}>Admin</Link>
            </Col>
          )}
        </Row>
      )
    })
    return (
      <Container>
        <Helmet>
          <title>Elections</title>
        </Helmet>
        <PageHeading level={2}>Elections</PageHeading>
        <div style={{ marginBottom: '2rem' }}>
          <Link to={`/elections/create`}>
            <Button>Create Election</Button>
          </Link>
        </div>
        {electionRows}
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchElections }, dispatch)

type ElectionsStateProps = RootReducer
type ElectionsDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  ElectionsStateProps,
  ElectionsDispatchProps,
  null,
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(Elections)
