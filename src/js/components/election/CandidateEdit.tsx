import { mapValues, pickBy } from 'lodash'
import React, { Component } from 'react'
import { Button, Form } from 'react-bootstrap'
import { connect } from 'react-redux'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { Candidate, Candidates } from '../../client/CandidateClient'
import { isAdmin } from '../../services/members'
import FieldGroup from '../common/FieldGroup'

interface CandidateEditOwnProps {
  name?: string
  imageUrl?: string
  height?: number
  electionId: number
  candidateId?: number
  onCreate(candidate: Candidate): void
  onDelete?(candidateId: number): void
}

type CandidateForm = Partial<Candidate>

type CandidateEditStateProps = RootReducer

type CandidateEditProps = CandidateEditOwnProps & CandidateEditStateProps

interface CandidateEditState {
  inSubmission: boolean
  lastSubmittedImageUrl?: string
  candidate: CandidateForm
}

const DEFAULT_CONTROL_HEIGHT = 60

class CandidateEdit extends Component<CandidateEditProps, CandidateEditState> {
  constructor(props: CandidateEditProps) {
    super(props)
    this.state = {
      candidate: {
        id: props.candidateId,
        name: props.name,
        image_url: props.imageUrl,
      },
      inSubmission: false,
      lastSubmittedImageUrl: props.imageUrl,
    }
  }

  updateCandidateFromForm = <K extends keyof CandidateForm>(
    formKey: K,
    value: CandidateForm[K]
  ) => {
    if (this.state.inSubmission) {
      return
    }
    this.setState({
      candidate: { ...this.state.candidate, [formKey]: value },
    })
  }

  render() {
    const admin = isAdmin(this.props.member)
    if (!admin) {
      return null
    }

    const candidate = this.state.candidate
    const icon = this.state.lastSubmittedImageUrl ? (
      <img
        src={this.state.lastSubmittedImageUrl}
        style={{
          width: 'auto',
          height: this.props.height || DEFAULT_CONTROL_HEIGHT,
          marginRight: 30,
        }}
        alt={this.props.name}
      />
    ) : null

    return (
      <div style={{ marginBottom: '2rem' }}>
        <Form onSubmit={(e) => e.preventDefault()}>
          {icon}
          <FieldGroup
            formKey="name"
            componentClass="input"
            type="text"
            label="Name"
            value={candidate.name || ''}
            onFormValueChange={this.updateCandidateFromForm}
          />
          <FieldGroup
            formKey="image_url"
            componentClass="input"
            type="text"
            label="Image URL"
            value={candidate.image_url || ''}
            onFormValueChange={this.updateCandidateFromForm}
          />
          <Button
            type="submit"
            onClick={(e) =>
              candidate.id ? this.updateCandidate(e) : this.createCandidate(e)
            }
          >
            Submit
          </Button>
          {candidate.id ? (
            <div style={{ display: 'inline-block' }}>
              &nbsp;&nbsp;
              <Button type="submit" onClick={this.deleteCandidate}>
                Delete
              </Button>
            </div>
          ) : null}
        </Form>
      </div>
    )
  }

  attributesForAPI(): Candidate {
    const attributeNames = ['name', 'image_url']
    const filtered: Pick<CandidateForm, 'name' | 'image_url'> = pickBy(
      this.state.candidate,
      (_, key) => key != null && attributeNames.includes(key)
    )
    const nulled: Candidate = mapValues(
      filtered,
      (val) => val || null
    ) as Candidate

    return nulled
  }

  createCandidate: React.MouseEventHandler<HTMLElement> = async (e) => {
    e.preventDefault()

    if (this.state.inSubmission) {
      return
    }
    this.setState({
      inSubmission: true,
    })

    const candidateAttributes = this.attributesForAPI()
    const newCandidate = await Candidates.create(
      this.props.electionId,
      candidateAttributes
    )
    this.props.onCreate(newCandidate)
    this.setState({
      candidate: {},
      inSubmission: false,
      lastSubmittedImageUrl: undefined,
    })
  }

  updateCandidate: React.MouseEventHandler<HTMLElement> = async (e) => {
    e.preventDefault()

    if (this.state.inSubmission) {
      return
    }
    this.setState({
      inSubmission: true,
    })

    try {
      const updatedAttributes = this.attributesForAPI()

      if (this.state.candidate.id != null) {
        const result = await Candidates.update(
          this.props.electionId,
          this.state.candidate.id,
          updatedAttributes
        )
        const updatedCandidate = { ...this.state.candidate, ...result }
        this.setState({
          candidate: updatedCandidate,
          lastSubmittedImageUrl: updatedCandidate.image_url || undefined,
        })
      }
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  deleteCandidate: React.MouseEventHandler<HTMLElement> = async (e) => {
    e.preventDefault()

    if (this.state.inSubmission) {
      return
    }
    this.setState({
      inSubmission: true,
    })

    if (this.state.candidate.id != null) {
      await Candidates.delete(this.props.electionId, this.state.candidate.id)

      if (this.props.onDelete != null) {
        this.props.onDelete(this.state.candidate.id)
      }
    }
  }
}

export default connect<
  CandidateEditStateProps,
  null,
  CandidateEditOwnProps,
  RootReducer
>((state) => state)(CandidateEdit)
