import React, { Component } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { fetchElections } from '../../redux/actions/electionActions'
import { isAdmin } from '../../services/members'
import { membershipApi } from '../../services/membership'
import { HTTP_POST, logError } from '../../util/util'
import FieldGroup from '../common/FieldGroup'
import PageHeading from '../common/PageHeading'

type ElectionsProps = ElectionsStateProps & ElectionsDispatchProps

interface ElectionsForm {
  name: string
  candidate_list: string[]
  number_winners: number
  start_time: Date
  end_time: Date
  description: string
  description_img: string
}

interface ElectionsState {
  election: ElectionsForm
  inSubmission: boolean
}

class Elections extends Component<ElectionsProps, ElectionsState> {
  constructor(props: ElectionsProps) {
    super(props)
    this.state = {
      election: {
        name: '',
        candidate_list: [],
        number_winners: 1,
        start_time: new Date(),
        end_time: new Date(),
        description: '',
        description_img: '',
      },
      inSubmission: false,
    }
  }

  componentDidMount() {
    this.props.fetchElections()
  }

  updateElectionForm = <K extends keyof ElectionsForm>(
    formKey: K,
    value: ElectionsForm[K]
  ) => {
    if (this.state.inSubmission) {
      return
    }

    this.setState({ election: { ...this.state.election, [formKey]: value } })
  }

  render() {
    const admin = isAdmin(this.props.member)
    if (!admin) {
      return null
    }
    return (
      <Container>
        <Row>
          <Col sm={8} lg={8}>
            <PageHeading level={2}>Create Election</PageHeading>
            <Form onSubmit={(e) => e.preventDefault()}>
              <FieldGroup
                formKey="name"
                componentClass="input"
                type="text"
                label="Election Name"
                value={this.state.election.name}
                onFormValueChange={this.updateElectionForm}
                required
              />
              <FieldGroup
                formKey="start_time"
                componentClass="datetime"
                label="Start Time"
                value={this.state.election.start_time}
                onFormValueChange={this.updateElectionForm}
              />
              <FieldGroup
                formKey="end_time"
                componentClass="datetime"
                label="End Time"
                value={this.state.election.end_time}
                onFormValueChange={this.updateElectionForm}
              />
              <FieldGroup
                formKey="number_winners"
                componentClass="input"
                type="number"
                step={1}
                min={1}
                label="Number of winners"
                value={this.state.election.number_winners}
                onFormValueChange={this.updateElectionForm}
                required
              />
              <FieldGroup
                formKey="candidate_list"
                componentClass="input"
                type="text"
                label="Candidate Names (comma-separated)"
                onFormValueChange={(formKey, value) =>
                  this.updateElectionForm(
                    formKey,
                    value.split(',').map((s) => s.trim())
                  )
                }
                required
              />
              <FieldGroup
                formKey="description"
                componentClass="input"
                type="text"
                rows={5}
                label="Description (truncated to 2048 characters)"
                onFormValueChange={(formKey, value) =>
                  this.updateElectionForm(formKey, value)
                }
                value={this.state.election.description}
              />
              <FieldGroup
                formKey="description_img"
                componentClass="input"
                type="text"
                label="Link to a description image"
                onFormValueChange={(formKey, value) =>
                  this.updateElectionForm(formKey, value)
                }
                value={this.state.election.description_img}
              />
              <Button
                type="submit"
                onClick={(e) => this.submitForm(e, 'election', '/election')}
              >
                Add Election
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }

  submitForm = async (
    e: React.MouseEvent<HTMLElement>,
    name: keyof ElectionsState,
    endpoint: string
  ) => {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await membershipApi(HTTP_POST, endpoint, this.state[name])
      this.props.fetchElections()
      history.back()
    } catch (err) {
      this.setState({ inSubmission: false })
      return logError('Error creating election', err)
    }
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchElections }, dispatch)

type ElectionsStateProps = RootReducer
type ElectionsDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  ElectionsStateProps,
  ElectionsDispatchProps,
  null,
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(Elections)
