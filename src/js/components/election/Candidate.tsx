import React, { Component } from 'react'

interface CandidateProps {
  name: string
  imageUrl?: string
  height?: number
}

const DEFAULT_CONTROL_HEIGHT = 60

export default class Candidate extends Component<CandidateProps> {
  render() {
    return (
      <div>
        {this.getIcon()}
        <span style={{ fontSize: 36 }}>{this.props.name}</span>
      </div>
    )
  }

  getIcon() {
    if (this.props.imageUrl) {
      return (
        <img
          src={this.props.imageUrl}
          style={{
            width: 'auto',
            height: this.props.height || DEFAULT_CONTROL_HEIGHT,
            marginRight: 30,
          }}
          alt={this.props.name}
        />
      )
    } else {
      return null
    }
  }
}
