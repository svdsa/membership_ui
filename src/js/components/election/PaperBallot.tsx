import { flatMap, range } from 'lodash'
import {
  filter,
  flow,
  fromPairs,
  groupBy,
  isFunction,
  mapValues,
  shuffle,
  unset,
} from 'lodash/fp'
import React, { Component } from 'react'
import { FormControl } from 'react-bootstrap'
import { connect } from 'react-redux'
import { ElectionDetailsResponse } from 'src/js/client/ElectionClient'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'

interface PaperBallotOwnProps {
  editable?: boolean
  election: ElectionDetailsResponse
  ballotKey: string
  onRankingChange?(ranking: { [id: number]: number }): void
}
type PaperBallotStateProps = RootReducer
type PaperBallotProps = PaperBallotStateProps & PaperBallotOwnProps

interface PaperBallotState {
  editable: boolean
  ranking: { [id: number]: number }
  warnings: { [id: number]: string }
}

class PaperBallot extends Component<PaperBallotProps, PaperBallotState> {
  electionId: number
  shuffledCandidates: Candidate[]

  constructor(props) {
    super(props)
    this.state = {
      editable: !!props.editable,
      ranking: {},
      warnings: {},
    }
    this.electionId = this.props.election.id
    const candidates = this.props.election.candidates
    const seed = this.electionId + '_' + String(this.props.ballotKey)
    // Use Fisher-Yates shuffle rather than sorting by RNG
    this.shuffledCandidates = shuffle(candidates)
  }

  static validateBallot(ranking: { [id: number]: number }): {
    [rank: number]: string
  } {
    // Collect all candidates with the same rank

    // 1. Group candidate ids by rank on the form
    const groupByRank = groupBy<[number, number]>(([id, rank]) => rank)
    const mapCandidateEntryToId = mapValues<[number, number], number>(
      ([id, rank]) => id
    )

    // 2. Filter the candidate id groups that contain more than 1 candidate id with the same ranking
    const filterByMultiples = filter<number[]>((cids) => cids.length > 1)
    const candidateIdsWithMultipleRanks: { [rank: string]: number[] } = flow(
      groupByRank,
      mapCandidateEntryToId,
      filterByMultiples
    )(Object.entries(ranking))

    // 3. flatten to a set of candidate ids that have errors
    const errors = flatMap(candidateIdsWithMultipleRanks, (cids, rank) =>
      cids.map((cid) => [cid, `Duplicate rank ${rank}`])
    )

    return fromPairs(errors)
  }

  getCandidateList(candidates: Candidate[]) {
    return range(0, candidates.length, 2).map((ridx) => (
      <tr key={`row-${ridx}`}>
        {range(ridx, Math.min(ridx + 2, candidates.length)).map((cidx) => {
          const candidate = candidates[cidx]
          const id = candidate.id
          const rank = this.state.ranking[id] || '' // either a number or empty string
          if (!this.props.editable) {
            return (
              <td key={`col-${cidx}`} className="candidate">
                <span
                  className={`rank-box ${
                    this.state.warnings[id] != null ? 'warning' : ''
                  }`}
                >
                  <label>{rank}</label>
                </span>
                <span>{candidate.name}</span>
              </td>
            )
          } else {
            return (
              <td
                key={`election-${this.electionId}_candidate-${cidx}`}
                className="candidate"
              >
                <span
                  className={`rank-box ${
                    this.state.warnings[id] != null ? 'warning' : ''
                  }`}
                >
                  <FormControl
                    type="text"
                    maxLength={5}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      const newRank = e.target.value
                        ? parseInt(e.target.value, 10)
                        : null
                      const updatedRank = newRank
                        ? { ...this.state.ranking, [id]: newRank }
                        : unset(id)(this.state.ranking)
                      this.setState({
                        ranking: updatedRank as { [id: number]: number },
                        warnings: PaperBallot.validateBallot(updatedRank),
                      })
                      if (isFunction(this.props.onRankingChange)) {
                        this.props.onRankingChange(updatedRank)
                      }
                    }}
                    value={rank.toString()}
                  />
                </span>
                <span>{candidate.name}</span>
              </td>
            )
          }
        })}
      </tr>
    ))
  }

  render() {
    return (
      <div className="printPage">
        <h2>
          {' '}
          {this.props.election.name || 'Unnamed'} Election Ballot #
          {this.props.ballotKey}{' '}
        </h2>
        <p>
          Rank the candidates in the order of your choice. Rank as many or as
          few as you wish. Be sure not to use the same rank for more than one
          candidate.
        </p>
        <table className="ballotCandidates">
          <tbody>{this.getCandidateList(this.shuffledCandidates)}</tbody>
        </table>
      </div>
    )
  }
}

export default connect<
  PaperBallotStateProps,
  null,
  PaperBallotOwnProps,
  RootReducer
>((state) => state)(PaperBallot)
