import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { MemberVote } from 'src/js/client/MemberClient'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { electionStatus } from '../../services/elections'

type PastElectionsStateProps = RootReducer
type PastElectionsProps = PastElectionsStateProps

class PastElections extends Component<PastElectionsProps> {
  render() {
    if (this.props.member.user.data == null) {
      return 'Unable to load member data (location: part elections)'
    }

    const allVotes = this.props.member.user.data.votes

    const pastVotes = allVotes.filter((vote) =>
      this.isPastElection(vote.election_id)
    )

    if (pastVotes.length === 0) {
      return <p>You haven't been eligible to vote on anything before.</p>
    } else {
      return (
        <div>{pastVotes.reverse().map((vote) => this.renderVote(vote))}</div>
      )
    }
  }

  renderVote(vote: MemberVote) {
    const electionId = vote.election_id
    const electionName = this.getElection(electionId).name

    const electionLink = (
      <Link to={`/elections/${electionId}`}>{electionName}</Link>
    )

    if (vote.voted) {
      return (
        <p key={`election-${electionId}`}>
          You voted on <strong>{electionLink}</strong>.
        </p>
      )
    } else {
      return (
        <p key={`election-${electionId}`}>
          There was a vote on <strong>{electionLink}</strong>.
        </p>
      )
    }
  }

  getElection(electionId: number) {
    return this.props.elections.byId[electionId]
  }

  isPastElection(electionId: number) {
    const election = this.getElection(electionId)
    return (
      election &&
      (electionStatus(election) === 'final' ||
        electionStatus(election) === 'polls closed')
    )
  }
}

export default connect<PastElectionsStateProps, null, {}, RootReducer>(
  (state) => state
)(PastElections)
