import { compact } from 'lodash'
import { flow, pick } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { mapValuesOrNull } from 'src/js/util/fp'
import { Candidate } from '../../client/CandidateClient'
import { ElectionDetailsResponse, Elections } from '../../client/ElectionClient'
import { isAdmin } from '../../services/members'
import { logError } from '../../util/util'
import FieldGroup from '../common/FieldGroup'
import PageHeading from '../common/PageHeading'
import CandidateDisplay from './Candidate'
import CandidateEdit from './CandidateEdit'

interface ElectionEditParamProps {
  electionId: string
}
interface ElectionEditRouteParamProps {}
type ElectionEditStateProps = RootReducer
type ElectionEditOtherProps = RouteComponentProps<
  ElectionEditParamProps,
  ElectionEditRouteParamProps
>
type ElectionEditProps = ElectionEditStateProps & ElectionEditOtherProps

interface ElectionEditState {
  election: ElectionDetailsResponse | null
  inSubmission: boolean
}

type ElectionDetailsUpdateAttributes = Pick<
  ElectionDetailsResponse,
  'voting_begins_epoch_millis' | 'voting_ends_epoch_millis' | 'number_winners'
>

class ElectionEdit extends Component<ElectionEditProps, ElectionEditState> {
  constructor(props) {
    super(props)
    this.state = {
      election: null,
      inSubmission: false,
    }
  }

  componentDidMount() {
    this.getElectionDetails()
  }

  updateStartTime = (startTime: Date) => {
    if (this.state.inSubmission || this.state.election == null) {
      return
    }

    this.setState({
      election: {
        ...this.state.election,
        voting_begins_epoch_millis: new Date(startTime).getTime(),
      },
    })
  }

  updateEndTime = (endTime: Date) => {
    if (this.state.inSubmission || this.state.election == null) {
      return
    }

    this.setState({
      election: {
        ...this.state.election,
        voting_ends_epoch_millis: new Date(endTime).getTime(),
      },
    })
  }

  updateNumberOfWinners = (numberOfWinners: number) => {
    if (this.state.inSubmission || this.state.election == null) {
      return
    }

    this.setState({
      election: { ...this.state.election, number_winners: numberOfWinners },
    })
  }

  updateElection: React.MouseEventHandler<HTMLButtonElement> = async (e) => {
    e.preventDefault()

    if (this.state.inSubmission || this.state.election == null) {
      return
    }
    this.setState({
      inSubmission: true,
    })

    try {
      const attributeNames = [
        'voting_begins_epoch_millis',
        'voting_ends_epoch_millis',
        'number_winners',
      ]
      const pickByAttributes = pick(attributeNames)
      const request: ElectionDetailsResponse = flow(
        pickByAttributes,
        mapValuesOrNull,
        this.cleanupDates
      )(this.state.election)

      const result = await Elections.updateElection(
        parseInt(this.props.params.electionId),
        request
      )
      const election = result.election
      const updatedElection: ElectionDetailsResponse = {
        ...this.state.election,
        ...election,
      }
      this.setState({ election: updatedElection })
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  cleanupDates = (updatedAttributes: ElectionDetailsResponse) => {
    const startTime = updatedAttributes.voting_begins_epoch_millis
    const endTime = updatedAttributes.voting_ends_epoch_millis

    return {
      ...updatedAttributes,
      end_time: new Date(endTime).toISOString(),
      start_time: new Date(startTime).toISOString(),
    }
  }

  onCreateCandidate = (newCandidate: Candidate) => {
    if (this.state.election != null) {
      this.setState({
        election: {
          ...this.state.election,
          candidates: [...this.state.election.candidates, newCandidate],
        },
      })
    }
  }

  onDeleteCandidate = (deletedCandidateId: number) => {
    if (this.state.election != null) {
      const candidatesAfter = this.state.election.candidates.filter(
        (candidate) => candidate.id !== deletedCandidateId
      )
      this.setState({
        election: { ...this.state.election, candidates: candidatesAfter },
      })
    }
  }

  render() {
    const admin = isAdmin(this.props.member)
    if (!admin || this.state.election == null) {
      return <div />
    }

    const status = this.state.election.status
    const canEdit = status === 'draft'
    const existingCandidates = this.state.election.candidates

    const candidateElements = this.renderCandidates(
      this.state.election.id,
      existingCandidates,
      canEdit
    )

    const transitions = this.state.election.transitions.map((t) => (
      <li
        key={t}
        style={{
          display: 'inline-block',
          marginRight: 10,
        }}
      >
        <Button
          type="submit"
          value={t}
          onClick={(e) => this.submitTransition(e)}
        >
          {t}
        </Button>
      </li>
    ))

    return (
      <Container>
        <Helmet>
          <title>{this.state.election.name}</title>
        </Helmet>
        <Row>
          <Col sm={8} lg={6}>
            <PageHeading level={1}>
              {this.state.election.name} ({status})
            </PageHeading>
          </Col>
        </Row>
        <Row>
          <Col sm={8} lg={6}>
            <h3>{canEdit ? 'Edit Details' : 'Details'}</h3>
            <Form onSubmit={(e) => e.preventDefault()}>
              <FieldGroup
                formKey="start_time"
                componentClass="datetime"
                label="Start Time"
                value={new Date(this.state.election.voting_begins_epoch_millis)}
                onFormValueChange={(_, value) => this.updateStartTime(value)}
                disabled={!canEdit}
              />
              <FieldGroup
                formKey="end_time"
                componentClass="datetime"
                label="End Time"
                value={new Date(this.state.election.voting_ends_epoch_millis)}
                onFormValueChange={(_, value) => this.updateEndTime(value)}
                disabled={!canEdit}
              />
              <FieldGroup
                formKey="landingUrl"
                componentClass="input"
                type="number"
                label="Number of Positions"
                value={this.state.election.number_winners}
                onFormValueChange={(_, value) =>
                  this.updateNumberOfWinners(value)
                }
                disabled={!canEdit}
              />
              {canEdit ? (
                <Button type="submit" onClick={this.updateElection}>
                  Submit
                </Button>
              ) : null}
            </Form>
          </Col>
        </Row>
        <Row>
          <Col sm={8} lg={6}>
            <h3>{canEdit ? 'Edit Candidates' : 'Candidates'}</h3>
            {candidateElements}
          </Col>
        </Row>
        <Row>
          <Col sm={8} lg={6}>
            <h3>Update Election Status</h3>
            <ul
              style={{
                listStyleType: 'none',
                paddingLeft: 0,
              }}
            >
              {transitions}
            </ul>
          </Col>
        </Row>
      </Container>
    )
  }

  renderCandidates = (
    electionId: number,
    existingCandidates: Candidate[],
    canEdit: boolean
  ): JSX.Element[] => {
    const existingCandidateEntries = existingCandidates.map((candidate) =>
      canEdit ? (
        <CandidateEdit
          key={candidate.id || 'new'}
          imageUrl={candidate.image_url}
          name={candidate.name}
          electionId={electionId}
          candidateId={candidate.id}
          onCreate={this.onCreateCandidate}
          onDelete={() => this.onDeleteCandidate(candidate.id)}
        />
      ) : (
        <CandidateDisplay
          key={candidate.id}
          imageUrl={candidate.image_url}
          name={candidate.name}
        />
      )
    )

    return compact([
      ...existingCandidateEntries,
      canEdit ? (
        <CandidateEdit
          key="new-candidate"
          electionId={electionId}
          onCreate={this.onCreateCandidate}
        />
      ) : null,
    ])
  }

  async getElectionDetails() {
    try {
      const results = await Elections.getElection(
        parseInt(this.props.params.electionId)
      )
      this.setState({ election: results })
    } catch (err) {
      return logError('Error fetching election', err)
    }
  }

  async submitTransition(e) {
    e.preventDefault()
    if (this.state.inSubmission || this.state.election == null) {
      return
    }
    const transition = e.target.value
    const confirmation = confirm(
      `Are you sure you want to ${transition} the election?`
    )
    if (!confirmation) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      const results = await Elections.submitTransition(
        parseInt(this.props.params.electionId),
        transition
      )
      const newElection: ElectionDetailsResponse = {
        ...this.state.election,
        status: results.election_status,
        transitions: results.transitions,
      }

      this.setState({ election: newElection })
    } catch (err) {
      return logError('Error changing election state', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<ElectionEditStateProps, null, null, RootReducer>(
  (state) => state
)(ElectionEdit)
