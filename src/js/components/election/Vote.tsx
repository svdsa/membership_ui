import { isEqual, shuffle } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, ButtonGroup, Container, Form } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { interleave } from 'src/js/util/arrays'
import {
  ElectionDetailsResponse,
  Elections,
  SubmitVoteResponse,
} from '../../client/ElectionClient'
import { logError } from '../../util/util'
import PageHeading from '../common/PageHeading'

type VoteStateProps = RootReducer
interface VoteParamsProps {
  electionId: number
}
interface VoteRouteParamsProps {}
type VoteProps = VoteStateProps &
  RouteComponentProps<VoteParamsProps, VoteRouteParamsProps>

interface VoteHighlight {
  idx: number
  type: 'anchor' | 'candidate'
}

interface VoteState {
  election: ElectionDetailsResponse | null
  unranked: Candidate[]
  ranked: Candidate[]
  highlighted: VoteHighlight | null
  inSubmission: boolean
  result: SubmitVoteResponse | null
}

class Vote extends Component<VoteProps, VoteState> {
  constructor(props) {
    super(props)
    this.state = {
      election: null,
      unranked: [],
      ranked: [],
      highlighted: null,
      inSubmission: false,
      result: null,
    }
  }

  componentDidMount() {
    this.getElectionDetails()
  }

  insertAtAnchor({ anchorIdx, candidate, ranked = this.state.ranked }) {
    this.setState({
      ranked: [
        ...this.state.ranked.slice(0, anchorIdx),
        candidate,
        ...this.state.ranked.slice(anchorIdx),
      ],
      highlighted: null, // undo highlighting since either anchor or candidate was highlighted
    })
  }

  selectRanked(selected: VoteHighlight) {
    const highlighted = this.state.highlighted
    if (selected && highlighted) {
      const ranked = this.state.ranked
      const reinsertAtAnchor = ({ anchorIdx, candidateIdx }) => {
        // skip anchors that are above or below the candidate, since they won't move
        if (candidateIdx === anchorIdx || candidateIdx === anchorIdx - 1) {
          this.setState({ highlighted: null })
        } else {
          this.insertAtAnchor({
            // move the anchor up one if it comes after the candidate being moved
            anchorIdx: anchorIdx > candidateIdx ? anchorIdx - 1 : anchorIdx,
            candidate: ranked[candidateIdx],
            ranked: ranked.splice(candidateIdx, 1),
          })
        }
      }
      switch (highlighted.type) {
        case 'anchor':
          switch (selected.type) {
            case 'anchor':
              // swap highlighted anchors or toggle anchor highlighting if clicked twice
              this.setState({
                highlighted: isEqual(highlighted, selected) ? null : selected,
              })
              break
            case 'candidate':
              // reinsert ranked candidate at highlighted anchor
              reinsertAtAnchor({
                anchorIdx: highlighted.idx,
                candidateIdx: selected.idx,
              })
              break
          }
          break
        case 'candidate':
          const highlightedCandidateIdx = highlighted.idx
          const highlightedCandidate = ranked[highlightedCandidateIdx]

          if (highlightedCandidate != null) {
            switch (selected.type) {
              case 'anchor':
                // reinsert highlighted ranked candidate at anchor
                reinsertAtAnchor({
                  anchorIdx: selected.idx,
                  candidateIdx: highlightedCandidateIdx,
                })
                break
              case 'candidate':
                // swap both ranked candidates
                const candidateArray = ranked
                const secondCandidateIdx = selected.idx
                const candidateClicked = candidateArray[secondCandidateIdx]
                candidateArray[highlightedCandidateIdx] = candidateClicked
                candidateArray[secondCandidateIdx] = highlightedCandidate
                const updatedRankings = candidateArray
                this.setState({
                  ranked: updatedRankings,
                  highlighted: null,
                })
                break
            }
          } else {
            throw new Error(
              'Bad highlighted candidate when swapping candidates'
            )
          }
          break
      }
    } else if (selected) {
      this.setState({ highlighted: selected })
    }
  }

  addToRanked(unrankedIdx: number) {
    let anchorIdx: number | null = null
    if (this.state.highlighted) {
      const { type, idx } = this.state.highlighted
      if (type === 'anchor') {
        anchorIdx = idx
      }
    }
    const candidate = this.state.unranked[unrankedIdx]
    if (anchorIdx !== null) {
      this.insertAtAnchor({ anchorIdx, candidate })
    } else if (candidate != null) {
      this.setState({
        ranked: [...this.state.ranked, candidate],
      })
    } else {
      throw new Error('Bad index when ranking candidate')
    }
    this.setState({
      unranked: this.state.unranked.splice(unrankedIdx, 1),
    })
  }

  removeFromRanked(rankedIdx: number) {
    const candidate = this.state.ranked[rankedIdx]

    if (candidate != null) {
      this.setState({
        unranked: [...this.state.unranked, candidate],
        ranked: this.state.ranked.splice(rankedIdx, 1),
      })
    } else {
      throw new Error('Bad index when removing ranking')
    }
  }

  render() {
    return this.renderForMobile()
  }

  renderForMobile() {
    if (this.state.election == null) {
      return null
    }

    const highlighted = this.state.highlighted || {}
    const ranked = this.state.ranked
    const rankedCandidates = ranked.map((candidate, idx) => {
      const current: VoteHighlight = {
        type: 'candidate',
        idx,
      }
      return (
        <div key={`ranked-${idx}`}>
          <div style={{ display: 'flex' }}>
            {Vote.candidateImage(candidate)}
            <ButtonGroup size="lg" className="justify-content-between">
              <a
                id={`swap-candidate-${candidate.id}`}
                className={`btn btn-success ${
                  isEqual(current, highlighted) ? 'active hover' : ''
                }`}
                onClick={(e) => {
                  this.selectRanked(current)
                }}
              >
                {candidate.name}
              </a>
              <a
                className="btn btn-danger"
                id={`drop-candidate-${candidate.id}`}
                onClick={(e) => {
                  this.removeFromRanked(idx)
                }}
              >
                Unrank
              </a>
            </ButtonGroup>
          </div>
        </div>
      )
    })
    const anchor = (idx: number) => {
      const current: VoteHighlight = {
        type: 'anchor',
        idx,
      }
      return (
        <div
          key={`anchor-${idx}`}
          className={`anchor ${
            isEqual(current, highlighted) ? 'highlighted' : ''
          }`}
          onClick={(e) => this.selectRanked(current)}
        >
          <hr />
        </div>
      )
    }
    const anchors = ranked.map((_, idx) => anchor(idx + 1))
    const rankedCandidatesAndAnchors = [
      anchor(0),
      ...interleave(rankedCandidates, anchors),
    ]
    const rankedList = (
      <div id="ranked-candidates">{rankedCandidatesAndAnchors}</div>
    )
    const unrankedList = (
      <div id="unranked-candidates">
        {this.state.unranked.map((candidate, index) => (
          <div key={`unranked-${index}`}>
            <div style={{ display: 'flex' }}>
              {Vote.candidateImage(candidate)}
              <Button block size="lg" onClick={(e) => this.addToRanked(index)}>
                {candidate.name}
              </Button>
            </div>
          </div>
        ))}
      </div>
    )
    return (
      <Container>
        <Helmet>
          <title>{this.state.election.name} ballot</title>
        </Helmet>
        <PageHeading level={1}>{this.state.election.name} ballot</PageHeading>
        <ul>
          <li>Click unranked candidates to add them in order</li>
          <li>Swap 2 ranked candidates by clicking on their names</li>
          <li>Rank as many as you wish</li>
        </ul>
        <Form onSubmit={(e) => e.stopPropagation()}>
          <div className="text-center">
            <strong>Ranked</strong>
          </div>
          {rankedList}
          <div className="text-center">
            <strong>Unranked</strong>
          </div>
          {unrankedList}
          {this.state.result == null ? (
            <Button
              id="vote"
              disabled={this.state.inSubmission}
              onClick={(e) => {
                e.preventDefault()
                return this.vote()
              }}
            >
              VOTE
            </Button>
          ) : (
            <p>
              Congratulations. You've successfully voted. Your confirmation
              number is
              <strong> {this.state.result.ballot_id}</strong>. Save this number
              if you'd like to confirm your vote was counted correctly after the
              election, but keep it private or everybody will be able to see how
              you voted.
            </p>
          )}
        </Form>
        <div className="footer-padding">
          {/* A fix for some mobile browsers not being able to click the submit button */}
        </div>
      </Container>
    )
  }

  static candidateImage(candidate) {
    const imageUrl = candidate.image_url

    if (imageUrl) {
      return (
        <img
          style={{
            height: 50,
            width: 'auto',
            marginRight: 20,
          }}
          src={imageUrl}
          alt={candidate.name}
        />
      )
    } else {
      return null
    }
  }

  async getElectionDetails() {
    try {
      const response = await Elections.getElection(this.props.params.electionId)
      if (response == null) {
        throw new Error("Couldn't load election")
      }
      this.setState({
        election: response,
        unranked: shuffle(response.candidates),
      })
    } catch (err) {
      return logError('Error loading election', err)
    }
  }

  async vote() {
    if (
      !confirm(
        'Are you sure you want to submit your vote now? This cannot be undone.'
      )
    ) {
      return
    }
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    const params = {
      election_id: this.props.params.electionId,
      rankings: this.state.ranked.map((c) => c.id),
    }
    try {
      const result = await Elections.submitVote(params)
      this.setState({ result: result })
    } catch (err) {
      return logError('Error submitting vote', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<VoteStateProps, null, {}, RootReducer>((state) => state)(
  Vote
)
