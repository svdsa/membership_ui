import React, { Component } from 'react'
import { Button, Container, Form, FormControl } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { sanitizeIntermediateBallotInput } from 'src/js/util/ballotKey'
import { ElectionDetailsResponse, Elections } from '../../client/ElectionClient'
import { membershipApi } from '../../services/membership'
import { HTTP_POST, logError } from '../../util/util'
import PageHeading from '../common/PageHeading'
import PaperBallot from './PaperBallot'

type PrintBallotsStateProps = RootReducer
interface PrintBallotsParamProps {
  electionId: string
}
interface PrintBallotsRouteParamProps {}
type PrintBallotsProps = PrintBallotsStateProps &
  RouteComponentProps<PrintBallotsParamProps, PrintBallotsRouteParamProps>

interface PrintBallotsState {
  numBallotsInput: string
  claimedBallots: string[]
  readyToPrint: boolean
  claimAndPrintForm: {
    election_id: number
    number_ballots: number
  }
  election: ElectionDetailsResponse | null
  inSubmission: boolean
}

class PrintBallots extends Component<PrintBallotsProps, PrintBallotsState> {
  constructor(props) {
    super(props)
    this.state = {
      numBallotsInput: '',
      claimedBallots: [],
      readyToPrint: false,
      claimAndPrintForm: {
        election_id: props.params.electionId,
        number_ballots: 0,
      },
      election: null,
      inSubmission: false,
    }
  }

  componentWillMount() {
    Elections.getElection(parseInt(this.props.params.electionId)).then(
      (results) => {
        this.setState({ election: results })
      }
    )
  }

  render() {
    if (this.state.election == null) {
      return null
    }

    const ballots = this.state.claimedBallots.map((ballotKey) => (
      <PaperBallot
        key={ballotKey}
        election={this.state.election!}
        ballotKey={ballotKey}
      />
    ))

    const pageTitle = `Print Ballots for ${this.state.election.name}`

    return (
      <Container>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <div className="print-hidden">
          <PageHeading level={1}>{pageTitle}</PageHeading>
          <Form>
            <label htmlFor="number_ballots">Number of ballots</label>
            <FormControl
              required
              id="number_ballots"
              type="number"
              maxLength={5}
              onChange={(e) => {
                const numBallotsInput = sanitizeIntermediateBallotInput(
                  e.target.value
                )
                if (numBallotsInput) {
                  const form = this.state.claimAndPrintForm
                  form.number_ballots = parseInt(numBallotsInput, 10)
                  this.setState({
                    claimAndPrintForm: form,
                    numBallotsInput: numBallotsInput,
                  })
                } else if (e.target.value.trim() === '') {
                  this.setState({ numBallotsInput: '' })
                }
              }}
              value={this.state.numBallotsInput}
            />
            <Button
              type="submit"
              disabled={!this.state.numBallotsInput}
              onClick={(e) => {
                this.submitForm(e, 'claimAndPrintForm', '/ballot/claim').then(
                  (ballotKeys) => {
                    this.setState({
                      claimedBallots: ballotKeys,
                      readyToPrint: true,
                    })
                  }
                )
              }}
            >
              Claim and Print
            </Button>
          </Form>
        </div>
        <div id="paper-ballots">{ballots}</div>
      </Container>
    )
  }

  componentDidUpdate() {
    if (this.state.readyToPrint) {
      this.setState({ readyToPrint: false })
      window.print()
    }
  }

  async submitForm(e, formName, endpoint) {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      return await membershipApi(HTTP_POST, endpoint, this.state[formName])
    } catch (err) {
      return logError(`Error submitting form to ${endpoint}`, err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<PrintBallotsStateProps, null, {}, RootReducer>(
  (state) => state
)(PrintBallots)
