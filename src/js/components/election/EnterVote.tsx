import { flow, isEqual, keyBy, map, range, sortBy } from 'lodash/fp'
import React, { Component } from 'react'
import { Button, Container, Form } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import {
  sanitizeBallotKey,
  sanitizeIntermediateBallotInput,
} from 'src/js/util/ballotKey'
import {
  ElectionDetailsResponse,
  Elections,
  GetVoteResponse,
  PaperBallotRequest,
} from '../../client/ElectionClient'
import FieldGroup from '../common/FieldGroup'
import PageHeading from '../common/PageHeading'
import PaperBallot from './PaperBallot'

interface ValidationMessage {
  status: 'info' | 'warning' | 'success' | 'error'
  message: JSX.Element
}

interface EnterVoteParamProps {
  electionId: string
}
interface EnterVoteRouteParamProps {}
type EnterVoteStateProps = RootReducer
type EnterVoteProps = EnterVoteStateProps &
  RouteComponentProps<EnterVoteParamProps, EnterVoteRouteParamProps>

interface ElectionState extends ElectionDetailsResponse {
  candidatesById?: { [key: number]: Candidate }
}

interface EnterVoteState {
  ballotKeyInput: string
  searchingForVote: boolean
  existingVote: GetVoteResponse | null
  vote: PaperBallotRequest | null
  rankings: { [id: number]: number }
  sortedCandidateIds: number[]
  submitMessage: null
  election: ElectionState | null
  validation: ValidationMessage | null
  inSubmission: boolean
}

class EnterVote extends Component<EnterVoteProps, EnterVoteState> {
  static initValidationMessage: ValidationMessage = {
    status: 'info',
    message: (
      <p>
        <strong>Enter a 5 digit ballot key to proceed.</strong>
      </p>
    ),
  }

  static blankBallot(electionId: number, ballotKey: string): GetVoteResponse {
    return {
      election_id: electionId,
      ballot_key: ballotKey,
      rankings: [] as number[],
    }
  }

  searchBallotKeyRef = React.createRef<FieldGroup<'searchBallotKey'>>()

  constructor(props) {
    super(props)
    this.state = {
      ballotKeyInput: '',
      searchingForVote: false,
      existingVote: null,
      vote: null,
      sortedCandidateIds: [],
      rankings: {},
      submitMessage: null,
      election: null,
      validation: EnterVote.initValidationMessage,
      inSubmission: false,
    }
  }

  componentWillMount() {
    Elections.getElection(parseInt(this.props.params.electionId)).then(
      (election) => {
        if (election != null) {
          const keyCandidatesById = keyBy<Candidate>((c) => c.id)
          const sortCandidatesById = sortBy<Candidate>((c) => c.id)
          const candidatesById = keyCandidatesById(election.candidates)
          // TODO standardize IDs on strings
          const sortedCandidateIds = Object.keys(candidatesById)
            .sort()
            .map(parseInt)
          this.setState({
            election: {
              ...(election as ElectionState),
              candidatesById,
            },
            sortedCandidateIds,
          })
        }
      }
    )
    const ballotKey = sanitizeBallotKey(this.state.ballotKeyInput)
    if (ballotKey != null) {
      this.searchForVote(ballotKey).then(() => {
        this.setState({ vote: this.state.existingVote })
      })
    }
  }

  async searchForVote(ballotKey: string) {
    this.setState({ searchingForVote: true })
    let existingVote: GetVoteResponse | null = null
    try {
      existingVote = await Elections.getVote(
        parseInt(this.props.params.electionId),
        ballotKey
      )
    } finally {
      this.setState({
        existingVote,
        searchingForVote: false,
        validation: existingVote
          ? null
          : {
              status: 'warning',
              message: (
                <p>
                  <strong>{`Ballot #${ballotKey} is unclaimed.`}</strong>
                  You must claim it before you can submit this vote
                </p>
              ),
            },
      })
    }
  }

  static sortRankings(rankings: { [id: number]: number }): number[] {
    return flow(
      sortBy<[string, number]>(([_, rank]) => rank),
      map(([cid, _]) => parseInt(cid))
    )(Object.entries(rankings))
  }

  verifyOrSubmit: React.MouseEventHandler<HTMLElement> = async (event) => {
    if (
      this.state.existingVote &&
      Object.keys(this.state.existingVote.rankings).length > 0
    ) {
      this.verifyVote()
    } else if (this.state.vote != null) {
      const ballotKey = this.state.vote.ballot_key
      const resp = confirm(
        `Are you sure you want to submit ballot #${ballotKey}?`
      )
      if (resp) {
        const rankedCandidates = EnterVote.sortRankings(this.state.rankings)
        const voteBody: PaperBallotRequest = {
          ...this.state.vote,
          rankings: rankedCandidates,
        }
        await this.submitVoteAndReset(event, () =>
          Elections.submitPaperBallot(voteBody)
        )
      }
    }
  }

  verifyVote = () => {
    if (this.state.vote == null || this.state.existingVote == null) {
      return
    }

    const rankedCandidates = EnterVote.sortRankings(this.state.rankings)
    const existingRankedCandidates = this.state.existingVote.rankings || []
    if (isEqual(rankedCandidates, existingRankedCandidates)) {
      this.setState({
        sortedCandidateIds: rankedCandidates,
        validation: {
          status: 'success',
          message: (
            <p>
              <strong>Vote matches previously submitted ballot!</strong>
            </p>
          ),
        },
      })
    } else {
      this.setState({
        sortedCandidateIds: rankedCandidates,
        validation: {
          status: 'error',
          message: (
            <p>
              <strong>
                Vote does not match order from previously submitted ballot!
              </strong>
            </p>
          ),
        },
      })
    }
  }

  handleOverrideBallot: React.MouseEventHandler<HTMLButtonElement> = (e) => {
    if (this.state.vote != null) {
      const ballotKey = this.state.vote.ballot_key
      const resp = confirm(
        `Are you sure you want to overwrite ballot #${ballotKey}?`
      )
      if (resp) {
        const rankedCandidates = EnterVote.sortRankings(this.state.rankings)
        const voteBody: PaperBallotRequest = {
          ...this.state.vote,
          rankings: rankedCandidates,
        }
        this.submitVoteAndReset(e, () =>
          Elections.submitPaperBallot(voteBody, true).then(() => {
            this.setState({
              validation: {
                status: 'success',
                message: (
                  <p>
                    <strong>{`Ballot #${ballotKey} updated successfully!`}</strong>
                  </p>
                ),
              },
            })
          })
        )
      }
    }
  }

  handleSearchBallotKey = (formKey: string, value: string) => {
    const searchBallotKey = sanitizeBallotKey(value)

    if (searchBallotKey != null) {
      this.searchForVote(searchBallotKey)
      this.setState({
        ballotKeyInput: searchBallotKey,
        vote: EnterVote.blankBallot(
          parseInt(this.props.params.electionId),
          searchBallotKey
        ),
      })
    } else {
      const input = sanitizeIntermediateBallotInput(value)

      if (input != null) {
        this.setState({ vote: null, ballotKeyInput: input })
      }
    }

    this.setState({ validation: EnterVote.initValidationMessage })
  }

  render() {
    if (this.state.election === null) {
      return (
        <div className="no-election">
          Election doesn't exist in state (not loaded yet?)
        </div>
      )
    }

    if (this.state.sortedCandidateIds == null) {
      return <div className="no-candidates">No candid ates (not loaded?)</div>
    }

    const ballot =
      !this.state.searchingForVote &&
      this.state.vote != null &&
      this.state.existingVote != null ? (
        <PaperBallot
          editable={true}
          election={this.state.election}
          ballotKey={this.state.vote.ballot_key}
          onRankingChange={(rankings) => {
            const sortedCandidates = EnterVote.sortRankings(rankings)
            this.setState({
              rankings,
              sortedCandidateIds: sortedCandidates,
              validation: null,
            })
          }}
        />
      ) : null

    let validationBox: JSX.Element | null = null
    let overrideButton: JSX.Element | null = null
    let validationTable: JSX.Element | null = null
    if (this.state.validation) {
      // We have a validation box to show
      const status = this.state.validation.status
      if (status === 'error') {
        const existingRankings = this.state.existingVote?.rankings || []
        const currentRankings = this.state.sortedCandidateIds
        const totalRange = range(
          0,
          Math.max(existingRankings.length, currentRankings.length)
        )
        validationTable = (
          <table>
            <thead>
              <tr>
                <th>Existing Ballot</th>
                <th>Current Ballot</th>
                <th>Matching?</th>
              </tr>
            </thead>
            <tbody>
              {totalRange.map((i) => {
                if (this.state.election == null) {
                  return (
                    <tr key={i}>
                      <td>Election is null</td>
                    </tr>
                  )
                } else {
                  const existing =
                    this.state.election.candidatesById?.[existingRankings[i]]
                      .name
                  const current =
                    this.state.election.candidatesById?.[currentRankings[i]]
                      .name
                  return (
                    <tr key={`validation-rank-comparison-${i}`}>
                      <td>{existing}</td>
                      <td>{current}</td>
                      <td>{existing === current ? '√' : 'X'}</td>
                    </tr>
                  )
                }
              })}
            </tbody>
          </table>
        )
        // If we are displaying an error with the ballot, we have the option to override the ballot to submit the current one
        overrideButton = (
          <Button className="center-block" onClick={this.handleOverrideBallot}>
            Overwrite Ballot
          </Button>
        )
      }
      const validationClasses = [
        'validation-box',
        'center-block',
        'center-text',
        'alert',
      ]
      switch (this.state.validation.status) {
        case 'success':
          validationClasses.push('alert-success')
          break
        case 'info':
          validationClasses.push('alert-info')
          break
        case 'warning':
          validationClasses.push('alert-warning')
          break
        case 'error':
          validationClasses.push('alert-danger')
          break
        default:
      }
      validationBox = (
        <div className={validationClasses.join(' ')}>
          <span className="text-center">{this.state.validation.message}</span>
          <div className="text-center">{validationTable}</div>
          {overrideButton}
        </div>
      )
    }

    const pageTitle = `Enter Ballot for ${this.state.election.name}`

    return (
      <Container>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <PageHeading level={1}>{pageTitle}</PageHeading>
        <Form onSubmit={(e) => e.preventDefault()}>
          <FieldGroup
            required
            formKey="searchBallotKey"
            id="searchBallotKey"
            ref={this.searchBallotKeyRef}
            componentClass="input"
            type="text"
            label="Ballot Key"
            maxLength={5}
            value={this.state.ballotKeyInput}
            onFormValueChange={this.handleSearchBallotKey}
          />
          {validationBox}
          {ballot ? (
            <div>
              {ballot}
              <Button id="verify" onClick={(e) => this.verifyOrSubmit(e)}>
                {Object.keys(this.state.rankings).length === 0
                  ? 'Submit Vote'
                  : 'Verify Vote'}
              </Button>
            </div>
          ) : null}
        </Form>
      </Container>
    )
  }

  async submitVoteAndReset(
    e: React.MouseEvent<HTMLElement>,
    submitFn: () => Promise<unknown>
  ): Promise<void> {
    await this.submitForm(e, submitFn)
    if (this.state.vote != null) {
      await this.searchForVote(this.state.vote.ballot_key)
    }
  }

  async submitForm<T>(e, call: () => Promise<unknown>): Promise<void> {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await call()
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<EnterVoteStateProps, null, null, RootReducer>(
  (state) => state
)(EnterVote)
