import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { ElectionDetailParamProps } from 'src/js/components/election/ElectionDetail'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { ElectionCount, Elections } from '../../client/ElectionClient'
import { isAdmin } from '../../services/members'
import { logError } from '../../util/util'
import Loading from '../common/Loading'

interface ElectionResultsOwnProps {
  params: ElectionDetailParamProps
}
type ElectionResultsStateProps = RootReducer
type ElectionResultsProps = ElectionResultsStateProps & ElectionResultsOwnProps

interface ElectionResultsState {
  inSubmission: boolean
  results: ElectionCount | null
}

class ElectionResults extends Component<
  ElectionResultsProps,
  ElectionResultsState
> {
  constructor(props: ElectionResultsProps) {
    super(props)
    this.state = {
      inSubmission: false,
      results: null,
    }
  }

  render() {
    const admin = isAdmin(this.props.member)
    const winners = this.state.results?.winners.map((winner, index) => {
      const candidateID = winner.candidate

      if (this.state.results != null) {
        const candidateDetails =
          this.state.results.candidates[candidateID.toString()]

        if (candidateDetails != null) {
          return <div key={index}>{candidateDetails.name}</div>
        } else {
          return <div key={index}>Couldn't show winning candidate</div>
        }
      } else {
        return <div key={index}>No results</div>
      }
    })

    return admin ? (
      <div>
        <div>
          <Button type="submit" onClick={this.countVotes}>
            Count the Vote
          </Button>
        </div>
        {this.state.inSubmission && <Loading />}
        {this.state.results != null && (
          <div>
            <h3>Winners</h3>
            {winners}
            <h3>Counts</h3>
            <div>{this.state.results.ballot_count} ballots cast</div>
          </div>
        )}
      </div>
    ) : (
      <div />
    )
  }

  countVotes = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      const results = await Elections.countResults(
        parseInt(this.props.params.electionId)
      )
      this.setState({ results })
    } catch (err) {
      return logError('Error counting votes', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<
  ElectionResultsStateProps,
  null,
  ElectionResultsOwnProps,
  RootReducer
>((state) => state)(ElectionResults)
