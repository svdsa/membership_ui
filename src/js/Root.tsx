import { History } from 'history'
import React from 'react'
import { hot } from 'react-hot-loader/root'
import { QueryClient, QueryClientProvider } from 'react-query'
import { Provider } from 'react-redux'
import { HistoryUnsubscribe } from 'react-router-redux'
import { Store } from 'redux'
import { RootReducer } from './redux/reducers/rootReducer'
import Routes from './Routes'

interface RootProps {
  store: Store<RootReducer>
  history: History & HistoryUnsubscribe
}

const queryClient = new QueryClient()

const Root: React.FC<RootProps> = ({ store, history }) => (
  <Provider store={store}>
    <QueryClientProvider client={queryClient}>
      <Routes history={history} />
    </QueryClientProvider>
  </Provider>
)

export default hot(Root)
