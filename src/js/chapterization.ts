/**
 * Config and utils to support using this software for multiple DSA chapters.
 */

/**
 * Supported chapters
 */
export const SUPPORTED_CHAPTERS = ['san-francisco', 'silicon-valley'] as const
export type SupportedChapter = typeof SUPPORTED_CHAPTERS[number]

/**
 * Required per-chapter props. A chapter must fill out all of these messages
 * for the portal to work properly.
 */
interface RequiredC12nMessages {
  ADMIN_EMAIL: string
  ELIGIBILITY_EMAIL: string
  CHAPTER_NAME: string
  CHAPTER_NAME_SHORT: string
  URL_CHAPTER_JOIN: string
  URL_CHAPTER_GROUP_INFO: string
  URL_CHAPTER_CODE_OF_CONDUCT: string
  URL_CHAPTER_EVENTS: string
}

/**
 * Optional per-chapter props. A chapter may choose to provide these optional
 * messages. If they don't, the portal will furnish a default message instead.
 */
interface OptionalC12nMessages {
  CHAPTER_AREA_NAME: string
  GROUP_NAME_SINGULAR: string
  GROUP_NAME_PLURAL: string
  COLLECTIVE_DUES_CANCEL_EMAIL: string
  URL_CHAPTER_FAVICON_32: string
  URL_CHAPTER_FAVICON_192: string
}

export const DEFAULT_C18_MESSAGES: OptionalC12nMessages = {
  CHAPTER_AREA_NAME: 'the chapter area',
  GROUP_NAME_SINGULAR: 'group',
  GROUP_NAME_PLURAL: 'groups',
  COLLECTIVE_DUES_CANCEL_EMAIL: 'change_me_in_chapterization_file@example.com',
  URL_CHAPTER_FAVICON_32: '/images/favicon_32.png',
  URL_CHAPTER_FAVICON_192: '/images/favicon_192.png',
}

//
// Typechecking guts below here
//
export type C12nMessages = RequiredC12nMessages & Partial<OptionalC12nMessages>
export type C12nKey = keyof C12nMessages
export type MessagesByChapter = {
  [TChapter in SupportedChapter]: C12nMessages
}

export const verifyCurrentChapter = (
  chapter: string | undefined
): SupportedChapter | null =>
  chapter != null &&
  SUPPORTED_CHAPTERS.indexOf(chapter as unknown as SupportedChapter) >= 0
    ? (chapter as SupportedChapter)
    : null
