import { actions } from '../../util/reduxActionTypes'

function formActions(action) {
  return {
    FAILED: `${action} failed`,
    SUBMIT: `${action} initiated`,
    SUCCESS: `${action} succeeded`,
  }
}

export const ASSETS = actions('ASSETS', {
  DELETE_MULTIPLE: 'One or more assets have been deleted',
  UPDATE_LIST: 'The collection of assets has been updated',
})
export const MEMBERS_CREATED = 'MEMBERS_CREATED'
export const COMMITTEES = actions('COMMITTEES', {
  CREATE: formActions('Create new meeting'),
  UPDATE_LIST: 'The list of committees has been updated',
  FETCH_COMMITTEE: '',
  REQUESTING_COMMITTEE_ACCESS: 'REQUESTING_COMMITTEE_ACCESS',
})
export const MEMBERS_FETCHED = 'MEMBERS_FETCHED'
export const IMPORT_MEMBERS = formActions('Member import')
export const UPDATE_EMAIL_TEMPLATE = 'UPDATE_EMAIL_TEMPLATE'
export const FETCH_EMAIL_TEMPLATES_SUCCEEDED = 'FETCH_EMAIL_TEMPLATES_SUCCEEDED'
export const FETCH_EMAIL_TEMPLATE_SUCCEEDED = 'FETCH_EMAIL_TEMPLATE_SUCCEEDED'
export const SAVE_EMAIL_TEMPLATE = 'SAVE_EMAIL_TEMPLATE'
export const SAVE_EMAIL_TEMPLATE_SUCCEEDED = 'SAVE_EMAIL_TEMPLATE_SUCCEEDED'
export const SAVE_EMAIL_TEMPLATE_FAILED = 'SAVE_EMAIL_TEMPLATE_FAILED'
export const MEETINGS = actions('MEETINGS', {
  ADD_ASSET: formActions('Add some content related to a meeting'),
  CODE: formActions('Update meeting code'),
  CREATE: formActions('New meeting creation'),
  EDIT: 'Edit a meeting',
  UPDATE_LIST: 'Update the list of meetings',
  SET_MEETING_CODE: 'Set the 4 digit meeting code',
})
export const ELECTIONS = actions('ELECTIONS', {
  FETCH_ELECTIONS: 'Fetch all elections',
  FETCH_ELECTION: 'Fetch an election',
})

export const MEMBER_SEARCH_INPUT_UPDATED = 'MEMBER_SEARCH_INPUT_UPDATED'
export const MEMBER_SEARCH_INPUT_CLEARED = 'MEMBER_SEARCH_INPUT_CLEARED'
export const MEMBER_SEARCH_QUERY_REQUESTED = 'MEMBER_SEARCH_QUERY_REQUESTED'
export const MEMBER_SEARCH_QUERY_SUCCEEDED = 'MEMBER_SEARCH_QUERY_SUCCEEDED'
export const MEMBER_SEARCH_QUERY_FAILED = 'MEMBER_SEARCH_QUERY_FAILED'
export const MEMBER_SEARCH_LOAD_MORE_REQUESTED =
  'MEMBER_SEARCH_LOAD_MORE_REQUESTED'
export const MEMBER_SEARCH_LOAD_MORE_SUCCEEDED =
  'MEMBER_SEARCH_LOAD_MORE_SUCCEEDED'
