import auth0, { Auth0DecodedHash, Auth0ParseHashError } from 'auth0-js'
import { nanoid } from 'nanoid'
import { AUTH0_CLIENT_ID, AUTH0_DOMAIN } from '../../../config'

export default class Auth {
  auth0: auth0.WebAuth
  constructor() {
    this.auth0 = new auth0.WebAuth({
      domain: AUTH0_DOMAIN,
      clientID: AUTH0_CLIENT_ID,
      // audience: `https://${params.domain}/userinfo`,
      redirectUri: `${window.location.origin}/process`,
      scope: 'openid email authorization',
      responseType: 'token id_token',
    })
  }

  signin = (
    redirectPath: string | null = null,
    options: auth0.AuthorizeOptions = {}
  ) => {
    if (redirectPath) {
      // Generate a `nonce` per Auth0's docs
      // https://auth0.com/docs/protocols/oauth2/redirect-users
      const nonce = nanoid()
      if (!redirectPath.startsWith('/login')) {
        window.localStorage.setItem(nonce, JSON.stringify({ redirectPath }))
      }
      options.state = nonce
    }
    this.auth0.authorize(options)
  }

  silentAuth = (): Promise<Required<Auth0DecodedHash>> => {
    console.log('Starting silent auth')
    return new Promise<Required<Auth0DecodedHash>>((resolve, reject) =>
      this.auth0.checkSession({}, (err, result) => {
        console.log('Received silent auth response', err, result)
        return this.handleAuthResultWithPromise(err, result, resolve, reject)
      })
    )
  }

  clearUserData = () => {
    // Clear access token and ID token from local storage
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    localStorage.removeItem('id_token')
    localStorage.removeItem('expires_at')
  }

  fullySignOut = () => {
    this.clearUserData()
    this.auth0.logout({ returnTo: window.location.origin })
  }

  handleAuthentication = (): Promise<Required<Auth0DecodedHash>> => {
    return new Promise<Required<auth0.Auth0DecodedHash>>((resolve, reject) =>
      this.auth0.parseHash((err, authResult) =>
        this.handleAuthResultWithPromise(err, authResult, resolve, reject)
      )
    )
  }

  handleAuthResultWithPromise = (
    err: Auth0ParseHashError | null,
    authResult: Auth0DecodedHash | null,
    resolve: (authResult: Required<Auth0DecodedHash>) => void,
    reject: (reason: Auth0ParseHashError) => void
  ) => {
    if (
      authResult != null &&
      authResult.accessToken != null &&
      authResult.idToken != null
    ) {
      const goodResult = authResult as Required<auth0.Auth0DecodedHash>
      this.setSession(goodResult)
      this.sendSessionStartEvent()

      resolve(goodResult)
    } else if (err) {
      reject(err)
    }
  }

  setSession(authResult: Required<auth0.Auth0DecodedHash>) {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    )
    localStorage.setItem('user', JSON.stringify(authResult.idTokenPayload))
    localStorage.setItem('access_token', authResult.accessToken)
    localStorage.setItem('id_token', authResult.idToken)
    localStorage.setItem('expires_at', expiresAt)
  }

  sendSessionStartEvent() {
    document.dispatchEvent(new Event('sessionStarted'))
  }

  isAuthenticated = () => {
    // Check whether the current time is past the
    // access token's expiry time
    const lsExpiresAt = localStorage.getItem('expires_at')

    if (lsExpiresAt != null) {
      const expiresAt = JSON.parse(lsExpiresAt)
      return new Date().getTime() < expiresAt
    } else {
      return false
    }
  }
}

const authSingleton = new Auth()

export { authSingleton as auth }
