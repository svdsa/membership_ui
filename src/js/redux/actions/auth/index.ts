import {
  AUTH_IN_PROGRESS,
  UNAUTH_USER,
  AUTH_ERROR,
  CLEARDOWN,
} from './authTypes'

import Auth from './universalAuth'

const auth = new Auth()

export function signinUser(redirectPath: string | null = null) {
  return function (dispatch) {
    dispatch({ type: AUTH_IN_PROGRESS })
    auth.signin(redirectPath)
  }
}

// Nobody calls this
export function authError(error) {
  const timestamp = Date.now()
  return {
    type: AUTH_ERROR,
    error,
    timestamp,
  }
}

/**
 * Clear the user session from local storage
 */
export function clearUserSession() {
  auth.clearUserData()
  return { type: UNAUTH_USER }
}

/**
 * Fully sign out the user. Clears user session from local storage
 * and redirects to auth0 to clear auth0 cookies.
 */
export function fullySignOut() {
  auth.fullySignOut()

  // Probably never gets here
  return { type: UNAUTH_USER }
}

export function cleardown() {
  return {
    type: CLEARDOWN,
  }
}
