import { unset } from 'lodash/fp'
import { AssetForm } from 'src/js/components/asset/EditableAsset'
import { EXTERNAL_URL, PRESIGNED_URL } from 'src/js/models/assets'
import { TODOAsyncThunkAction } from 'src/js/typeMigrationShims'
import { AssetClient, Assets, ResolvedAsset } from '../../client/AssetClient'
import { Meeting, Meetings } from '../../client/MeetingClient'
import { logError } from '../../util/util'
import { ASSETS, MEETINGS } from '../constants/actionTypes'
import { deleteAsset } from './assetActions'

export const MeetingCodeActions = Object.freeze({
  AUTOGENERATE: 'autogenerate',
  REMOVE: 'remote',
  SET: 'set',
})

export function fetchAllMeetings(): TODOAsyncThunkAction {
  return async (dispatch) => {
    try {
      const meetings = await Meetings.all()
      dispatch({
        type: MEETINGS.UPDATE_LIST,
        payload: meetings,
      })
    } catch (err) {
      logError(JSON.stringify(err))
    }
  }
}

export function createMeeting(meeting: Meeting): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    dispatch({
      type: MEETINGS.CREATE.SUBMIT,
      payload: meeting,
    })
    try {
      const state = getState()
      const result = await Meetings.create(meeting)
      const created = Meetings.parseMeeting(result.meeting)
      dispatch({
        type: MEETINGS.CREATE.SUCCESS,
        payload: created,
      })
    } catch (error) {
      dispatch({
        type: MEETINGS.CREATE.FAILED,
        error,
        payload: meeting,
      })
    }
  }
}

export function claimMeetingCode(
  meetingId: number,
  code: string
): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    const payload = {
      meetingId,
      code,
    }
    dispatch({
      type: MEETINGS.CODE.SUBMIT,
      payload,
    })
    const promise =
      code === MeetingCodeActions.AUTOGENERATE
        ? Meetings.autogenerateMeetingCode(meetingId)
        : Meetings.setMeetingCode(meetingId, code)
    try {
      const result = await promise
      dispatch({
        type: MEETINGS.CODE.SUCCESS,
        payload: {
          meetingId,
          code: result.meeting,
        },
      })
    } catch (error) {
      dispatch({
        type: MEETINGS.CODE.FAILED,
        error,
        payload,
      })
    }
  }
}

export function deleteMeetingAsset(assetId: number): TODOAsyncThunkAction {
  return async (dispatch, getState, extra) => {
    await deleteAsset({ id: assetId })(dispatch, getState, extra) // throws exception on failure
    const state = getState()
    const assetsByIdPostDeletion = unset(assetId)(state.assets.byId)
    dispatch({
      type: ASSETS.UPDATE_LIST,
      payload: assetsByIdPostDeletion || {},
    })
  }
}

export function saveMeetingAsset(
  asset: AssetForm,
  file: File
): TODOAsyncThunkAction<ResolvedAsset | null> {
  return async (dispatch, getState) => {
    dispatch({
      type: MEETINGS.ADD_ASSET.SUBMIT,
      payload: asset,
    })
    const operation = pickMeetingAssetOperation(asset, file, Assets)

    try {
      const created = await operation(asset, file)
      const update = {
        [created.id]: created,
      }
      dispatch({
        type: ASSETS.UPDATE_LIST,
        payload: update,
      })
      dispatch({
        type: MEETINGS.ADD_ASSET.SUCCESS,
      })
      return created
    } catch (ex) {
      dispatch({
        type: MEETINGS.ADD_ASSET.FAILED,
        payload: ex,
      })
    }
    return null
  }
}

function pickMeetingAssetOperation(
  asset: AssetForm,
  file: File,
  client: AssetClient
): (asset: AssetForm, file: File) => Promise<ResolvedAsset> {
  const assetType = asset.asset_type

  if (assetType === EXTERNAL_URL) {
    return client.create.bind(client)
  } else if (assetType === PRESIGNED_URL) {
    if (file) {
      return client.uploadAndCreateAsset.bind(client)
    } else {
      return client.create.bind(client)
    }
  } else {
    throw new Error(`Unrecognized asset_type: '${assetType}'`)
  }
}
