import { AUTH_ERROR, AUTH_USER, UNAUTH_USER } from '../auth/authTypes'
import Auth from '../auth/universalAuth'
import { CALLBACK_COMPLETE, CALLBACK_IN_PROGRESS } from './callbackTypes'

const auth = new Auth()

export function loading() {
  return function (dispatch) {
    dispatch({ type: CALLBACK_IN_PROGRESS })
  }
}

export function handleAuthentication() {
  return function (dispatch) {
    return auth
      .handleAuthentication()
      .then((resp) => {
        dispatch({ type: AUTH_USER })
        dispatch({ type: CALLBACK_COMPLETE })
        return resp
      })
      .catch((err) => {
        dispatch({ type: UNAUTH_USER })
        setTimeout(() => {
          const timestamp = Date.now()
          const error = `Error: ${err.error}, Error Description: ${err.errorDescription}`
          dispatch(
            {
              type: AUTH_ERROR,
              error,
              timestamp,
            },
            1000
          )
        })
        return err
      })
  }
}
