import { Dispatch } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { TODOAction, TODOAsyncThunkAction } from 'src/js/typeMigrationShims'
import Assets, { AssetsById } from '../../client/AssetClient'
import { ASSETS } from '../constants/actionTypes'

interface SearchAssetProps {
  labels: string[]
}
export function searchAssets({
  labels,
}: SearchAssetProps): ThunkAction<
  Promise<AssetsById | null>,
  RootReducer,
  null,
  TODOAction
> {
  return async (dispatch: Dispatch, getState) => {
    const state = getState()
    const assets = await Assets.search({ labels })
    dispatch({
      type: ASSETS.UPDATE_LIST,
      payload: assets,
    })
    return assets
  }
}

interface DeleteAssetProps {
  id: number
}
export function deleteAsset({ id }: DeleteAssetProps): TODOAsyncThunkAction {
  return async (dispatch: Dispatch, getState) => {
    const state = getState()
    await Assets.deleteById(id) // throws exception on failure
    dispatch({
      type: ASSETS.DELETE_MULTIPLE,
      payload: [{ id }],
    })
  }
}
