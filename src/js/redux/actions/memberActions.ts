import { Dispatch } from 'redux'
import {
  TODOAsyncThunkAction,
  TODOThunkAction,
} from 'src/js/typeMigrationShims'
import {
  CreateMemberForm,
  Members,
  UpdateMemberAttributes,
} from '../../client/MemberClient'
import { USE_AUTH } from '../../config'
import { FETCH_DATA_SUCCESS } from '../../redux/actions/fetchDataActions'
import { membershipApi } from '../../services/membership'
import { logError } from '../../util/util'
import { IMPORT_MEMBERS, MEMBERS_CREATED } from '../constants/actionTypes'
import { MEMBER_STORE as store } from '../reducers/memberReducers'
import { fetchData } from './fetchDataActions'
import { searchMembers } from './memberSearchActions'

export function fetchMember(): TODOThunkAction {
  return (dispatch) => {
    const user = localStorage.user
    if (!USE_AUTH || user !== null) {
      dispatch(
        fetchData({
          apiService: membershipApi,
          route: '/member/details',
          store,
          keyPath: ['user'],
        })
      )
    }
  }
}

export function createMember(
  newMember: CreateMemberForm
): TODOAsyncThunkAction {
  return async (dispatch, getState, extra) => {
    const state = getState()
    const body = await Members.create(newMember)
    const m = body.data.member
    const member = {
      id: m.id,
      email: m.info.email_address,
      name: m.info.first_name + ' ' + m.info.last_name,
    }
    dispatch({
      type: MEMBERS_CREATED,
      payload: {
        member,
      },
    })
    // refresh the search results
    searchMembers(state.memberSearch.query)(dispatch, getState, extra)
  }
}

export function updateMember(
  attributes: Partial<UpdateMemberAttributes>
): TODOThunkAction {
  return (dispatch: Dispatch) => {
    Members.update(attributes)
      .then((body) => {
        dispatch({
          store,
          type: FETCH_DATA_SUCCESS,
          keyPath: ['user'],
          data: body.data,
        })
      })
      .catch((err) => {
        logError(err.toString(), err)
      })
  }
}

export function importRoster(file: File): TODOThunkAction {
  return (dispatch: Dispatch) => {
    dispatch({ type: IMPORT_MEMBERS.SUBMIT })
    Members.importRoster(file)
      .then((data) => {
        dispatch({
          type: IMPORT_MEMBERS.SUCCESS,
          payload: data.data,
        })
      })
      .catch((err) => {
        dispatch({
          type: IMPORT_MEMBERS.FAILED,
        })
        logError(err.toString(), err)
      })
  }
}
