import { TODOAsyncThunkAction } from 'src/js/typeMigrationShims'
import Committees, { CreateCommitteeProps } from '../../client/CommitteeClient'
import { Members } from '../../client/MemberClient'
import { COMMITTEES } from '../constants/actionTypes'

// thunk that requests data and handles success and error cases
export function createCommittee(
  committee: CreateCommitteeProps
): TODOAsyncThunkAction {
  return async (dispatch) => {
    dispatch({
      type: COMMITTEES.CREATE.SUBMIT,
      payload: committee,
    })
    const result = await Committees.create(committee)
    if (result.status === 'success') {
      const created = result.created

      dispatch({
        type: COMMITTEES.CREATE.SUCCESS,
        payload: created,
      })
    } else {
      dispatch({
        type: COMMITTEES.CREATE.FAILED,
      })
    }
  }
}

export function fetchCommittees(): TODOAsyncThunkAction {
  return async (dispatch) => {
    const committees = await Committees.all()
    dispatch({
      type: COMMITTEES.UPDATE_LIST,
      payload: committees,
    })
  }
}

export function fetchCommittee(committeeId: number): TODOAsyncThunkAction {
  return async (dispatch) => {
    const committee = await Committees.get(committeeId)
    dispatch({
      type: COMMITTEES.FETCH_COMMITTEE,
      payload: committee,
    })
  }
}

export function addAdmin(
  memberId: number,
  committeeId: number
): TODOAsyncThunkAction {
  return async (dispatch) => {
    await Members.addRole(memberId, 'admin', committeeId)
    const committee = await Committees.get(committeeId)
    dispatch({
      type: COMMITTEES.FETCH_COMMITTEE,
      payload: committee,
    })
  }
}

export function removeAdmin(
  memberId: number,
  committeeId: number
): TODOAsyncThunkAction {
  return async (dispatch) => {
    await Members.removeRole(memberId, 'admin', committeeId)
    const committee = await Committees.get(committeeId)
    dispatch({
      type: COMMITTEES.FETCH_COMMITTEE,
      payload: committee,
    })
  }
}

export function markMemberInactive(
  memberId: number,
  committeeId: number
): TODOAsyncThunkAction {
  return async (dispatch) => {
    await Members.removeRole(memberId, 'active', committeeId)
    const committee = await Committees.get(committeeId)
    dispatch({
      type: COMMITTEES.FETCH_COMMITTEE,
      payload: committee,
    })
  }
}

export function requestCommitteeMembership(
  committeeId: number
): TODOAsyncThunkAction {
  return async (dispatch) => {
    await Committees.requestMembership(committeeId)
    dispatch({
      type: COMMITTEES.REQUESTING_COMMITTEE_ACCESS,
    })
  }
}
