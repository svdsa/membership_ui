import { TODOAsyncThunkAction } from 'src/js/typeMigrationShims'
import { Members, MemberSearchResponse } from '../../client/MemberClient'
import {
  MEMBER_SEARCH_INPUT_CLEARED,
  MEMBER_SEARCH_INPUT_UPDATED,
  MEMBER_SEARCH_LOAD_MORE_REQUESTED,
  MEMBER_SEARCH_LOAD_MORE_SUCCEEDED,
  MEMBER_SEARCH_QUERY_REQUESTED,
  MEMBER_SEARCH_QUERY_SUCCEEDED,
} from '../constants/actionTypes'

const RESULTS_PER_PAGE = 25

export function updateMemberSearchInput(query: string) {
  return {
    type: MEMBER_SEARCH_INPUT_UPDATED,
    payload: query,
  }
}

export function clearMemberSearchInput() {
  return {
    type: MEMBER_SEARCH_INPUT_CLEARED,
  }
}

function requestMembersQuery() {
  return {
    type: MEMBER_SEARCH_QUERY_REQUESTED,
  }
}

function receiveMembersQuery(payload: MemberSearchResponse) {
  return {
    type: MEMBER_SEARCH_QUERY_SUCCEEDED,
    payload,
  }
}

export function searchMembers(query: string): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    dispatch(requestMembersQuery())
    const response = await Members.search(query, RESULTS_PER_PAGE)
    dispatch(receiveMembersQuery(response))
  }
}

function requestMoreResults() {
  return {
    type: MEMBER_SEARCH_LOAD_MORE_REQUESTED,
  }
}

function receiveMoreResults(payload) {
  return {
    type: MEMBER_SEARCH_LOAD_MORE_SUCCEEDED,
    payload,
  }
}

export function loadMoreResults(
  query: string,
  cursor: number
): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    dispatch(requestMoreResults())
    const response = await Members.search(query, RESULTS_PER_PAGE, cursor)
    dispatch(receiveMoreResults(response))
  }
}
