import {
  AUTH_USER,
  UNAUTH_USER,
  AUTH_IN_PROGRESS,
  AUTH_ERROR,
  FORGOT_SUCCESS,
  CLEARDOWN,
} from '../actions/auth/authTypes'
import { AnyAction } from 'redux'
import { Reducer } from 'react'

interface UniversalAuthState {
  forgotMsg: string
  error: string
  timestamp: string
  loading: boolean
  authenticated: boolean
  user: string
  token: string
}

export type UniversalAuthAction = AnyAction

const INIT: UniversalAuthState = {
  forgotMsg: '',
  error: '',
  timestamp: '',
  loading: false,
  authenticated: false,
  user: '',
  token: '',
}

const reducer: Reducer<UniversalAuthState, UniversalAuthAction> = (
  state = INIT,
  action
) => {
  switch (action.type) {
    case CLEARDOWN:
      return { ...state, ...INIT }
    case AUTH_USER:
      return {
        ...state,
        ...INIT,
        authenticated: true,
        user: localStorage.user,
        token: localStorage.id_token,
      }
    case AUTH_IN_PROGRESS:
      return { ...state, ...INIT, loading: true }
    case UNAUTH_USER:
      return { ...state, ...INIT }
    case FORGOT_SUCCESS:
      return { ...state, ...INIT, forgotMsg: action.message }
    case AUTH_ERROR:
      return {
        ...state,
        ...INIT,
        error: action.error,
        timestamp: action.timestamp,
      }
    default:
      return state
  }
}

export default reducer
