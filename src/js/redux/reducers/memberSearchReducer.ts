import { Reducer } from 'react'
import { AnyAction } from 'redux'
import { EligibleMemberListEntry } from 'src/js/client/MemberClient'
import {
  MEMBER_SEARCH_INPUT_CLEARED,
  MEMBER_SEARCH_INPUT_UPDATED,
  MEMBER_SEARCH_LOAD_MORE_REQUESTED,
  MEMBER_SEARCH_LOAD_MORE_SUCCEEDED,
  MEMBER_SEARCH_QUERY_REQUESTED,
  MEMBER_SEARCH_QUERY_SUCCEEDED,
} from '../constants/actionTypes'

export interface MemberSearchState {
  query: string
  results: EligibleMemberListEntry[]
  hasMoreResults: boolean
  isLoading: boolean
  cursor: number
}

const reducer: Reducer<MemberSearchState, AnyAction> = (
  state = {
    query: '',
    results: [],
    hasMoreResults: false,
    isLoading: false,
    cursor: 0,
  },
  action
) => {
  switch (action.type) {
    case MEMBER_SEARCH_INPUT_UPDATED:
      if (action.payload === '') {
        return { ...state, query: '', results: [], hasMoreResults: false }
      }
      return { ...state, query: action.payload }
    case MEMBER_SEARCH_INPUT_CLEARED:
      return { ...state, query: '', results: [], hasMoreResults: false }
    case MEMBER_SEARCH_QUERY_REQUESTED:
      return { ...state, isLoading: true }
    case MEMBER_SEARCH_LOAD_MORE_REQUESTED:
      return { ...state, isLoading: true }
    case MEMBER_SEARCH_QUERY_SUCCEEDED:
      return {
        ...state,
        isLoading: false,
        hasMoreResults: action.payload.has_more,
        results: action.payload.members,
        cursor: action.payload.cursor,
      }
    case MEMBER_SEARCH_LOAD_MORE_SUCCEEDED:
      const results = [...state.results.concat(action.payload.members)]

      return {
        ...state,
        isLoading: false,
        hasMoreResults: action.payload.has_more,
        results,
        cursor: action.payload.cursor,
      }
    default:
      return state
  }
}

export default reducer
