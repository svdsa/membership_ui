import produce from 'immer'
import { set } from 'lodash'
import { AnyAction, Reducer } from 'redux'
import {
  FETCH_DATA_ERROR,
  FETCH_DATA_REQUEST,
  FETCH_DATA_SUCCESS,
  SET_DATA,
} from '../actions/fetchDataActions'

const VALID_ACTIONS = new Set([
  FETCH_DATA_REQUEST,
  FETCH_DATA_SUCCESS,
  FETCH_DATA_ERROR,
  SET_DATA,
])

/**
 * Higher order reducer for fetching data that populates store data for fetch data  API request
 * where store contains Map with loading, data, & err properties
 *
 * @deprecated This stores the error into the state tree. Instead you should write your own logic to handle errors.
 */
export function fetchDataHandler<
  TState extends {},
  TAction extends AnyAction,
  TReducer extends Reducer<TState, TAction> = Reducer<TState, TAction>,
  TStore extends string = string
>(reducer: TReducer, store: TStore): Reducer<TState, TAction> {
  return (state: TState, action: TAction): TState => {
    if (action.store === store && VALID_ACTIONS.has(action.type)) {
      const keyPath = action.keyPath
      const loadingKeyPath = action.keyPath.concat('loading')

      switch (action.type) {
        case FETCH_DATA_REQUEST:
          return produce((draft) => {
            set(draft, loadingKeyPath, true)
          })(state)

        case FETCH_DATA_SUCCESS:
          return produce((draft) => {
            set(draft, loadingKeyPath, false)
            set(draft, keyPath.concat('data'), action.data)
            set(draft, keyPath.concat('err'), null)
          })(state)

        case FETCH_DATA_ERROR:
          return produce((draft) => {
            set(draft, loadingKeyPath, false)
            set(draft, keyPath.concat('err'), action.err)
          })(state)

        case SET_DATA:
          return produce((draft) => {
            set(draft, action.keyPath, action.value)
          })(state)
      }
    }
    return reducer(state, action)
  }
}
