import produce from 'immer'
import { Reducer } from 'react'
import { AnyAction } from 'redux'
import { MemberDetailed } from 'src/js/client/MemberClient'
import { TODO } from '../../typeMigrationShims'
import { FETCH_DATA_SUCCESS } from '../actions/fetchDataActions'
import { fetchDataHandler } from './decorators'

export interface MemberState {
  isAdmin: boolean
  user: {
    data: MemberDetailed | null
    loading: boolean
    err: TODO | null
  }
}

export type MemberAction = AnyAction

export const MEMBER_STORE = 'member'

export const INITIAL_STATE: MemberState = {
  isAdmin: false,
  user: {
    data: null as MemberDetailed | null,
    loading: false,
    err: null,
  },
}

const member: Reducer<MemberState, MemberAction> = (
  state = INITIAL_STATE,
  action
) => {
  if (action.store !== MEMBER_STORE) {
    return state
  }

  switch (action.type) {
    case FETCH_DATA_SUCCESS:
      return produce((draft) => {
        draft.isAdmin = draft.user.data.roles.some(
          (r) => r.role === 'admin' && r.committee === 'general'
        )
      })(state)

    default:
      return state
  }
}

export default fetchDataHandler<MemberState, MemberAction>(member, MEMBER_STORE)
