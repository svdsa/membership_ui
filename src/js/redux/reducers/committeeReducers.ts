import { produce } from 'immer'
import { fromPairs } from 'lodash/fp'
import { Reducer } from 'react'
import { AnyAction } from 'redux'
import { CommitteesById } from 'src/js/client/CommitteeClient'
import { showNotification } from '../../util/util'
import { COMMITTEES } from '../constants/actionTypes'

export interface Committee {
  id: string
  name: string
  inactive: boolean
}

export interface CommitteesState {
  byId: CommitteesById
  form: {
    create: {
      name: string
      inSubmission: boolean
    }
  }
}
export type CommitteesAction = AnyAction

const INITIAL_STATE: CommitteesState = {
  byId: {},
  form: {
    create: {
      name: '',
      inSubmission: false,
    },
  },
}

const committees: Reducer<CommitteesState, CommitteesAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case COMMITTEES.UPDATE_LIST:
      return addCommitteesById(state, action.payload)
    case COMMITTEES.FETCH_COMMITTEE:
      return produce<CommitteesState>((draft) => {
        draft.byId[action.payload.id] = action.payload
      })(state)
    case COMMITTEES.CREATE.SUBMIT:
      return produce<CommitteesState>((draft) => {
        draft.form.create.inSubmission = true
      })(state)
    case COMMITTEES.CREATE.SUCCESS:
      const committeeId: number | null = action.payload.id
      if (committeeId == null) {
        throw new Error(`Missing committee.id from ${action.payload}`)
      }
      return produce<CommitteesState>((draft) => {
        draft = addCommitteesById(draft, { [committeeId]: action.payload })
        draft.form.create.inSubmission = false
      })(state)
    case COMMITTEES.CREATE.FAILED:
      return produce<CommitteesState>((draft) => {
        draft.form.create.inSubmission = false
      })(state)
    case COMMITTEES.REQUESTING_COMMITTEE_ACCESS:
      showNotification('Sent!', 'Committee request sent.')
    default:
      return state
  }
}

function addCommitteesById(state: CommitteesState, committees: CommitteesById) {
  const meetingsWithCodeClaimed = fromPairs(
    Object.values(committees).map((com) => [com.id, com])
  )
  return {
    ...state,
    byId: {
      ...state.byId,
      ...meetingsWithCodeClaimed,
    },
  }
}

export default committees
