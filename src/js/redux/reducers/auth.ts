export const AUTH_STORE = 'auth'

import { AnyAction } from 'redux'
import { TODO } from 'src/js/typeMigrationShims'
import {
  LOCK_ERROR,
  LOCK_INIT,
  LOCK_SUCCESS,
  LOGOUT,
  SHOW_LOCK,
} from '../../util/util'

interface AuthState {
  authenticating: boolean
  user: TODO
  token: TODO
  isAuthenticated: boolean
  error: TODO
}

export const INITIAL_AUTH_STATE: AuthState = {
  authenticating: false,
  user: null,
  token: null,
  isAuthenticated: false,
  error: null,
}

// TODO: LOCK_EXPIRE

export default function auth(
  state = INITIAL_AUTH_STATE,
  action: AnyAction
): AuthState {
  switch (action.type) {
    case LOCK_INIT:
      return {
        ...state,
        user: action.user,
        token: action.token,
        isAuthenticated: !!(action.user && action.token),
      }

    case SHOW_LOCK:
      return { ...state, authenticating: true }

    case LOCK_SUCCESS:
      return {
        ...state,
        authenticating: false,
        user: action.user,
        token: action.token,
        isAuthenticated: true,
      }

    case LOCK_ERROR:
      return {
        ...state,
        authenticating: false,
        user: null,
        token: null,
        error: action.error,
      }

    case LOGOUT:
      return {
        ...state,
        authenticating: false,
        isAuthenticated: false,
        user: null,
        token: null,
      }

    default:
      return state
  }
}
