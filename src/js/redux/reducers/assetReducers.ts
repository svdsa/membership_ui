import produce from 'immer'
import { Reducer } from 'react'
import { AnyAction } from 'redux'
import { AssetsById } from 'src/js/client/AssetClient'
import { ASSETS } from '../constants/actionTypes'

interface AssetsState {
  byId: AssetsById
}
export type AssetsAction = AnyAction

const INITIAL_STATE: AssetsState = {
  byId: {},
}

const assets: Reducer<AssetsState, AssetsAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case ASSETS.UPDATE_LIST:
      return {
        ...state,
        byId: {
          ...state.byId,
          ...action.payload,
        },
      }
    case ASSETS.DELETE_MULTIPLE:
      produce((draft) => {
        action.payload.forEach((a) => {
          const assetId = a.id
          if (assetId && draft.byId[assetId]) {
            delete draft.byId[assetId]
          }
        })
      })(state)
    default:
      return state
  }
}

export default assets
