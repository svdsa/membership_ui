import { routerReducer as routing } from 'react-router-redux'
import { combineReducers, Reducer } from 'redux'
import { api } from 'src/js/api'
import assets from './assetReducers'
import auth from './auth'
import callback from './callback'
import committees from './committeeReducers'
import elections from './electionReducers'
import meetings from './meetingReducers'
import member from './memberReducers'
import memberSearch from './memberSearchReducer'
import universalAuth from './universalAuthReducer'

const reducers = {
  universalAuth,
  callback,
  assets,
  auth,
  committees,
  elections,
  member,
  meetings,
  memberSearch,
  routing,
  [api.reducerPath]: api.reducer,
}

type Reducers = { [key: string]: Reducer<any> }
type ReducersReturnType<O extends Reducers> = {
  [K in keyof O]: ReturnType<O[K]>
}

export type RootReducer = ReducersReturnType<typeof reducers>

const rootReducer = combineReducers<RootReducer>(reducers)

export default rootReducer
