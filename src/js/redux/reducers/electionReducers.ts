import { fromPairs } from 'lodash/fp'
import { Reducer } from 'react'
import {
  ElectionDetailsResponse,
  ElectionResponse,
} from 'src/js/client/ElectionClient'
import { TODOAction } from '../../typeMigrationShims'
import { ELECTIONS } from '../constants/actionTypes'

export interface ElectionById {
  [id: number]: ElectionDetailsResponse
}

export interface ElectionsState {
  byId: ElectionById
}
export type ElectionsAction = TODOAction

const INITIAL_STATE: ElectionsState = {
  byId: {},
}

const elections: Reducer<ElectionsState, ElectionsAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case ELECTIONS.FETCH_ELECTIONS: {
      const payload: ElectionResponse[] | null = action.payload

      if (payload != null) {
        const elections = fromPairs(
          payload.map((election) => [election.id, election])
        )
        return { ...state, byId: { ...state.byId, ...elections } }
      } else {
        return state
      }
    }
    case ELECTIONS.FETCH_ELECTION:
      const payload = action.payload as ElectionDetailsResponse | null
      if (payload != null) {
        const electionId = payload.id
        return {
          ...state,
          byId: { ...state.byId, [electionId]: action.payload },
        }
      } else {
        return state
      }
    default:
      return state
  }
}

export default elections
