import produce from 'immer'
import { fromPairs } from 'lodash/fp'
import { Reducer } from 'react'
import { AnyAction } from 'redux'
import { ResolvedAsset } from 'src/js/client/AssetClient'
import { Meeting, MeetingsById } from 'src/js/client/MeetingClient'
import { MEETINGS } from '../constants/actionTypes'

export interface MeetingsState {
  byId: MeetingsById
  form: {
    code: {
      inSubmission: boolean
    }
    create: {
      inSubmission: boolean
    }
    upload: {
      asset: ResolvedAsset | null
      inSubmission: boolean
    }
  }
}

export type MeetingsAction = AnyAction

const INITIAL_STATE: MeetingsState = {
  byId: {},
  form: {
    code: {
      inSubmission: false,
    },
    create: {
      inSubmission: false,
    },
    upload: {
      asset: null,
      inSubmission: false,
    },
  },
}

const meetings: Reducer<MeetingsState, MeetingsAction> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case MEETINGS.CREATE.SUBMIT:
      return produce((draft) => {
        state.form.create.inSubmission = true
      })(state)
    case MEETINGS.CREATE.SUCCESS:
      return produce((draft) => {
        state = updateMeetingsById(state, action.payload)
        state.form.create = INITIAL_STATE.form.create
      })(state)
    case MEETINGS.CREATE.FAILED:
      return produce((draft) => {
        state.form.create.inSubmission = false
      })(state)
    case MEETINGS.UPDATE_LIST:
      return produce((draft) => {
        updateMeetingsById(state, action.payload)
      })(state)
    case MEETINGS.CODE.SUBMIT:
      return produce((draft) => {
        state.form.code.inSubmission = true
      })(state)
    case MEETINGS.CODE.SUCCESS:
      const { code, meetingId } = action.payload
      return produce((draft) => {
        setMeetingCode(state, meetingId, code)
      })(state)
    case MEETINGS.CODE.FAILED:
      return produce((draft) => {
        state.form.code.inSubmission = false
      })(state)
    case MEETINGS.ADD_ASSET.SUBMIT:
      return produce((draft) => {
        state.form.upload.inSubmission = true
      })(state)
    case MEETINGS.ADD_ASSET.SUCCESS:
      return produce((draft) => {
        state.form.upload = {
          asset: null,
          inSubmission: false,
        }
      })(state)
    case MEETINGS.ADD_ASSET.FAILED:
      return produce((draft) => {
        state.form.upload.inSubmission = false
      })(state)
    default:
      return state
  }
}

function updateMeetingsById(state: MeetingsState, meetings: Meeting[]) {
  const meetingsWithCodeClaimed = fromPairs(meetings.map((m) => [m.id, m]))
  return { ...state, byId: { ...state.byId, meetingsWithCodeClaimed } }
}

function setMeetingCode(state: MeetingsState, meetingId: string, code: string) {
  const currentMeeting: Meeting = { ...state.byId[meetingId], code }
  return updateMeetingsById(state, [currentMeeting])
}

export default meetings
