import { browserHistory } from 'react-router'
import { routerMiddleware } from 'react-router-redux'
import { applyMiddleware, compose, createStore, Store } from 'redux'
import { logger } from 'redux-logger'
import thunk from 'redux-thunk'
import { api } from 'src/js/api'
import rootReducer, { RootReducer } from '../reducers/rootReducer'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const enhancer = composeEnhancers(
  applyMiddleware(
    thunk,
    routerMiddleware(browserHistory),
    logger,
    api.middleware
  )
)

export default function configureStore(initialState?): Store<RootReducer> {
  const store = createStore(rootReducer, initialState, enhancer)
  if (module.hot) {
    module.hot.accept('../reducers/rootReducer', () => {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const nextRootReducer = require('../reducers/rootReducer').default
      store.replaceReducer(nextRootReducer)
    })
  }
  return store
}
