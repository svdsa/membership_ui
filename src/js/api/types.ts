import { z } from 'zod'

export type ZodExtends<
  A extends z.SomeZodObject,
  B extends z.SomeZodObject
> = z.ZodObject<
  z.extendShape<A['_shape'], B['_shape']>,
  'strip' | 'passthrough' | 'strict', // HACK: UnknownKeysParam is not exported
  z.ZodTypeAny,
  any,
  any
>
