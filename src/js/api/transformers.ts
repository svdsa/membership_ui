import { FetchBaseQueryMeta } from '@reduxjs/toolkit/dist/query'
import parseLinkHeader from 'parse-link-header'
import { ZdPaginatedList } from 'src/js/api/schemas'
import { ZodExtends } from 'src/js/api/types'
import { z } from 'zod'

/**
 * A thunk for `transformResponse` that parses a paginated list response,
 * injects any pagination info from the Links header, and returns the
 * body as parsed using the provided Zod model.
 *
 * @example transformResponse: parsePaginatedHeaders(PaginatedListFoo)
 *
 * @param zodModel a zod model that derives from PaginatedList
 * @returns the paginated list payload parsed using the provided zod model
 */
export const parsePaginatedHeaders =
  <T extends ZodExtends<typeof ZdPaginatedList, z.AnyZodObject>>(zodModel: T) =>
  (elem: z.infer<T>, meta: FetchBaseQueryMeta | undefined): z.infer<T> => {
    const linkHeaderStr = meta?.response?.headers.get('Link')
    if (linkHeaderStr != null) {
      const parsedHeaders = parseLinkHeader(linkHeaderStr)
      if (parsedHeaders != null) {
        elem.links = {
          next: parsedHeaders['next'],
          prev: parsedHeaders['prev'],
        }
      }
    }
    const parsed = zodModel.parse(elem)
    return parsed
  }
