import { compact, mapValues, omit } from 'lodash'
import { difference } from 'src/js/util/setOperations'
import { NonNullishKeys, Nullishly } from 'src/js/util/typeUtils'
import { deepNullable } from 'src/js/util/zod-deep-nullable'
import type { Brand } from 'utility-types'
import { z, ZodIssueCode, ZodNullable, ZodObject } from 'zod'

const validISODateStrRegex =
  /(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))/
const ZdISODateStr = z
  .string()
  .refine((elem) => validISODateStrRegex.test(elem))
  .transform((elem) => new Date(elem))

export type IdContact = Brand<string, 'Contact'>
export const ZdIdContact = z.string().transform((x) => x as IdContact)

export type IdEmailAddress = Brand<string, 'EmailAddress'>
export const ZdIdEmailAddress = z.string().transform((x) => x as IdEmailAddress)

export type IdPhoneNumber = Brand<string, 'PhoneNumber'>
export const ZdIdPhoneNumber = z.string().transform((x) => x as IdPhoneNumber)

export type IdPostalAddress = Brand<string, 'PostalAddress'>
export const ZdIdPostalAddress = z
  .string()
  .transform((x) => x as IdPostalAddress)

export type IdUser = Brand<string, 'User'>
export const ZdIdUser = z.string().transform((x) => x as IdUser)

export type IdSecurityPrincipal = Brand<string, 'SecurityPrincipal'>
export const ZdIdSecurityPrincipal = z
  .string()
  .transform((x) => x as IdSecurityPrincipal)

export type IdSecurityPrincipalRole = Brand<string, 'SecurityPrincipalRole'>
export const ZdIdSecurityPrincipalRole = z
  .string()
  .transform((x) => x as IdSecurityPrincipalRole)

export type IdCustomField = Brand<string, 'CustomField'>
export const ZdIdCustomField = z.string().transform((x) => x as IdCustomField)

export type IdTag = Brand<string, 'Tag'>
export const ZdIdTag = z.string().transform((x) => x as IdTag)

export type IdImportEvent = Brand<string, 'ImportEvent'>
export const ZdIdImportEvent = z.string().transform((x) => x as IdImportEvent)

export type IdImportEventReviewAction = Brand<string, 'ImportEventReviewAction'>
export const ZdIdImportEventReviewAction = z
  .string()
  .transform((x) => x as IdImportEventReviewAction)

export type IdCommittee = Brand<string, 'Committee'>
export const ZdIdCommittee = z.string().transform((x) => x as IdCommittee)

export type IdMeeting = Brand<string, 'Meeting'>
export const ZdIdMeeting = z.string().transform((x) => x as IdMeeting)

export type IdElection = Brand<string, 'Election'>
export const ZdIdElection = z.string().transform((x) => x as IdElection)

export const ZdPaginationQueryArgs = z.object({
  limit: z.number().optional(),
  starting_after: z.string().optional(),
  ending_before: z.string().optional(),
})
export type PaginationQueryArgs = z.infer<typeof ZdPaginationQueryArgs>

export const ZdPaginationLinkArgs = z.object({
  url: z.string(),
  rel: z.string(),
  starting_after: z.string().optional(),
  ending_before: z.string().optional(),
  limit: z.string().optional(),
})
export type PaginationLinkArgs = z.infer<typeof ZdPaginationLinkArgs>

export const ZdPaginatedLinks = z.object({
  next: ZdPaginationLinkArgs.optional(),
  prev: ZdPaginationLinkArgs.optional(),
})
export type PaginatedLinks = z.infer<typeof ZdPaginatedLinks>

export const ZdPaginatedList = z.object({
  object: z.literal('list'),
  url: z.string(),
  has_more: z.boolean(),
  data: z.array(z.any()),
  links: ZdPaginatedLinks.optional(),
})
export type PaginatedList = z.infer<typeof ZdPaginatedList>

const ZdContactTagList = z.array(ZdIdTag)
export type ContactTagList = z.infer<typeof ZdContactTagList>

const ZdContactCustomFieldList = z.object({}).catchall(z.any())
export type ContactCustomFieldList = z.infer<typeof ZdContactCustomFieldList>

export const ZdEmailAddressCreateBase = z.object({
  value: z.string().email(),
})

export const ZdEmailAddressCreateRequest =
  ZdEmailAddressCreateBase.partial().extend({
    date_created: z.date().optional(),
    value: z.string().email(),

    // TODO FLAGS handle this later
    primary: z.boolean().optional(),

    // TODO CONSENT handle this later
    consent_to_email: z.boolean().optional(),
    consent_to_share: z.boolean().optional(),
  })
export type EmailAddressCreateRequest = z.infer<
  typeof ZdEmailAddressCreateRequest
>
export const EmailAddressCreateRequestDefaults: EmailAddressCreateRequest = {
  value: '',

  // HACK these need explicit initializers here because otherwise
  // Formik will convert them into arrays and break form validation
  // https://github.com/jaredpalmer/formik/issues/2240
  // UNHACK remove these initializers and test if form still works
  primary: false,
  consent_to_email: false,
  consent_to_share: false,
}

export const ZdEmailAddressCreateRequestNullable = deepNullable(
  ZdEmailAddressCreateBase
).extend({
  date_created: z.date().nullable(),
  value: z.string().email(),
  // TODO FLAGS handle this later
  primary: z.boolean().nullable(),

  // TODO CONSENT handle this later
  consent_to_email: z.boolean().nullable(),
  consent_to_share: z.boolean().nullable(),
})

export const ZdEmailAddressResponse = z.object({
  object: z.literal('email_address'),
  id: ZdIdEmailAddress,

  date_created: ZdISODateStr,
  date_confirmed: ZdISODateStr.nullable(),
  date_archived: ZdISODateStr.nullable(),
  reason_archived: z.string().nullable(),

  value: z.string().email(),
  primary: z.boolean(),
  consent_to_email: z.boolean(),
  consent_to_share: z.boolean(),

  contact: ZdIdContact,
})
export type EmailAddressResponse = z.infer<typeof ZdEmailAddressResponse>
export type LaxEmailAddressResponse = NonNullishKeys<
  Nullishly<EmailAddressResponse>,
  'value'
>

export const ZdPaginatedListEmailAddress = ZdPaginatedList.extend({
  data: z.array(ZdEmailAddressResponse),
})
export type PaginatedListEmailAddress = z.infer<
  typeof ZdPaginatedListEmailAddress
>

export const ZdEmailAddressArchiveRequest = z.object({
  reason_archived: z.string().optional(),
})
export type EmailAddressArchiveRequest = z.infer<
  typeof ZdEmailAddressArchiveRequest
>

export const ZdEmailAddressUpdateRequest = z.object({
  primary: z.boolean().optional(),
  consent_to_email: z.boolean().optional(),
  consent_to_share: z.boolean().optional(),
})
export type EmailAddressUpdateRequest = z.infer<
  typeof ZdEmailAddressUpdateRequest
>

export interface ContactAttributePaginatedQueryArgs
  extends PaginationQueryArgs {
  contactId: IdContact
}

export const ZdPhoneNumberResponse = z.object({
  object: z.literal('phone_number'),
  id: ZdIdPhoneNumber,

  date_created: ZdISODateStr,
  date_confirmed: ZdISODateStr.nullable(),
  date_archived: ZdISODateStr.nullable(),
  reason_archived: z.string().nullable(),

  value: z.string().min(1),
  name: z.string().nullable(),
  primary: z.boolean(),

  contact: ZdIdContact,

  sms_capable: z.boolean().optional(),
  consent_to_call: z.boolean().optional(),
  consent_to_text: z.boolean().optional(),
  consent_to_share: z.boolean().optional(),
})
export type PhoneNumberResponse = z.infer<typeof ZdPhoneNumberResponse>
export type LaxPhoneNumberResponse = NonNullishKeys<
  Nullishly<PhoneNumberResponse>,
  'value'
>

export const ZdPaginatedListPhoneNumber = ZdPaginatedList.extend({
  data: z.array(ZdPhoneNumberResponse),
})
export type PaginatedListPhoneNumber = z.infer<
  typeof ZdPaginatedListPhoneNumber
>

export const ZdPhoneNumberCreateBase = z.object({
  value: z.string().min(1),
  name: z.string(),
})

export const ZdPhoneNumberCreateRequest =
  ZdPhoneNumberCreateBase.partial().extend({
    date_created: z.date().optional(),
    value: z.string().min(1),

    // TODO FLAGS handle this later
    primary: z.boolean().optional(),
    sms_capable: z.boolean().optional(),

    // TODO CONSENT handle this later
    consent_to_call: z.boolean().optional(),
    consent_to_text: z.boolean().optional(),
    consent_to_share: z.boolean().optional(),
  })
export type PhoneNumberCreateRequest = z.infer<
  typeof ZdPhoneNumberCreateRequest
>
export const PhoneNumberCreateRequestDefaults: PhoneNumberCreateRequest = {
  name: undefined,
  value: '',
  primary: false,
  sms_capable: false,
  consent_to_call: false,
  consent_to_text: false,
  consent_to_share: false,
}

export const ZdPhoneNumberCreateRequestNullable = deepNullable(
  ZdPhoneNumberCreateBase
).extend({
  date_created: z.date().nullable(),
  value: z.string().min(1),

  // TODO FLAGS handle this later
  primary: z.boolean().nullable(),
  sms_capable: z.boolean().nullable(),

  // TODO CONSENT handle this later
  consent_to_call: z.boolean().nullable(),
  consent_to_text: z.boolean().nullable(),
  consent_to_share: z.boolean().nullable(),
})

export const ZdPhoneNumberArchiveRequest = z.object({
  reason_archived: z.string().optional(),
})
export type PhoneNumberArchiveRequest = z.infer<
  typeof ZdPhoneNumberArchiveRequest
>

export const ZdPhoneNumberUpdateRequest = z.object({
  name: z.string().min(1).optional(),
  primary: z.boolean().optional(),
  sms_capable: z.boolean().optional(),
  consent_to_call: z.boolean().optional(),
  consent_to_text: z.boolean().optional(),
  consent_to_share: z.boolean().optional(),
})
export type PhoneNumberUpdateRequest = z.infer<
  typeof ZdPhoneNumberUpdateRequest
>

export interface ContactAttributePaginatedQueryArgs
  extends PaginationQueryArgs {
  contactId: IdContact
}

export const ZdPostalAddressResponse = z.object({
  object: z.literal('postal_address'),
  id: ZdIdPostalAddress,

  line1: z.string().nullable(),
  line2: z.string().nullable(),
  city: z.string().nullable(),
  state: z.string().nullable(),
  zipcode: z.string(),

  date_created: ZdISODateStr,
  source_created: z.string().nullable(),
  date_archived: ZdISODateStr.nullable(),
  reason_archived: z.string().nullable(),

  consent_to_mail: z.boolean(),

  contact: ZdIdContact,
})
export type PostalAddressResponse = z.infer<typeof ZdPostalAddressResponse>
export type LaxPostalAddressResponse = NonNullishKeys<
  Nullishly<PostalAddressResponse>,
  'zipcode'
>

export const ZdPaginatedListPostalAddress = ZdPaginatedList.extend({
  data: z.array(ZdPostalAddressResponse),
})
export type PaginatedListPostalAddress = z.infer<
  typeof ZdPaginatedListPostalAddress
>

export const ZdPostalAddressCreateBase = z.object({
  line1: z.string(),
  line2: z.string(),
  city: z.string(),
  state: z.string(),
  zipcode: z.string().regex(/^\d{5}(?:[-\s]\d{4})?$/),
})
export type PostalAddressCreateBase = z.infer<typeof ZdPostalAddressCreateBase>

export const ZdPostalAddressCreateRequest =
  ZdPostalAddressCreateBase.partial().extend({
    zipcode: z.string().regex(/^\d{5}(?:[-\s]\d{4})?$/),
    date_created: z.date().optional(),
    source_created: z.string().optional(),

    // TODO CONSENT handle this later
    consent_to_mail: z.boolean().optional(),
  })
export type PostalAddressCreateRequest = z.infer<
  typeof ZdPostalAddressCreateRequest
>

export const ZdPostalAddressCreateRequestNullable = deepNullable(
  ZdPostalAddressCreateBase
).extend({
  zipcode: z.string().regex(/^\d{5}(?:[-\s]\d{4})?$/),
  date_created: z.date().nullable(),
  source_created: z.string().nullable(),

  // TODO CONSENT handle this later
  consent_to_mail: z.boolean().nullable(),
})
export type PostalAddressCreateRequestNullable = z.infer<
  typeof ZdPostalAddressCreateRequestNullable
>

const ZdEmailAddressCreateUnion = z.union([
  z.array(z.string().email()),
  z.array(ZdEmailAddressCreateRequest),
])
export type EmailAddressCreateUnion = z.infer<typeof ZdEmailAddressCreateUnion>

const ZdEmailAddressCreateUnionNullable = z.union([
  z.array(z.string().email()),
  z.array(ZdEmailAddressCreateRequestNullable),
])
export type EmailAddressCreateUnionNullable = z.infer<
  typeof ZdEmailAddressCreateUnionNullable
>

const ZdPhoneNumberCreateUnion = z.union([
  z.array(z.string()),
  z.array(ZdPhoneNumberCreateRequest),
])
export type PhoneNumberCreateUnion = z.infer<typeof ZdPhoneNumberCreateUnion>

const ZdPhoneNumberCreateUnionNullable = z.union([
  z.array(z.string()),
  z.array(ZdPhoneNumberCreateRequestNullable),
])
export type PhoneNumberCreateUnionNullable = z.infer<
  typeof ZdPhoneNumberCreateUnionNullable
>

export const ZdContactResponse = z.object({
  object: z.literal('contact'),
  id: ZdIdContact,

  date_created: ZdISODateStr,
  source_created: z.string().nullable(),
  date_archived: ZdISODateStr.nullable(),
  reason_archived: z.string().nullable(),

  full_name: z.string().min(1),
  display_name: z.string().nullable(),
  pronouns: z.string().nullable(),
  biography: z.string().nullable(),
  profile_pic: z
    .string()
    .url({ message: 'Invalid profile picture URL' })
    .nullable(),
  admin_notes: z.string().nullable(),

  email_addresses: ZdPaginatedListEmailAddress,
  phone_numbers: ZdPaginatedListPhoneNumber,
  mailing_addresses: ZdPaginatedListPostalAddress,

  city: z.string().nullable(),
  zipcode: z.string().nullable(),
  tags: ZdContactTagList,
  custom_fields: ZdContactCustomFieldList,
})
export type ContactResponse = z.infer<typeof ZdContactResponse>

export const ZdPaginatedListContact = ZdPaginatedList.extend({
  data: z.array(ZdContactResponse),
})
export type PaginatedListContact = z.infer<typeof ZdPaginatedListContact>

export const ZdContactCreateAttributesBase = z.object({
  full_name: z.string().min(1),
  display_name: z.string().min(1),
  pronouns: z.string().min(1),
  biography: z.string().min(1),
  profile_pic: z.string().url({ message: 'Invalid profile picture URL' }),
  admin_notes: z.string().min(1),
})
export const ContactCreateAttributeKeysSet = new Set(
  Object.keys(ZdContactCreateAttributesBase.shape)
)
export type ContactCreateAttributeKeys =
  keyof typeof ZdContactCreateAttributesBase.shape

export const ZdContactCreateListRelationshipsBase = z.object({
  email_addresses: ZdEmailAddressCreateUnion,
  phone_numbers: ZdPhoneNumberCreateUnion,
  mailing_addresses: z.array(ZdPostalAddressCreateRequest),
})
export type ContactCreateListRelationshipKeys =
  keyof typeof ZdContactCreateListRelationshipsBase.shape

export const ZdContactCreateNullableListRelationships = z.object({
  email_addresses: ZdEmailAddressCreateUnionNullable,
  phone_numbers: ZdPhoneNumberCreateUnionNullable,
  mailing_addresses: z.array(ZdPostalAddressCreateRequestNullable),
})

export const ZdContactCreateForeignRelationshipsBase = z.object({
  tags: ZdContactTagList,
  custom_fields: ZdContactCustomFieldList,
})
export type ContactCreateForeignRelationshipsBase = z.infer<
  typeof ZdContactCreateForeignRelationshipsBase
>
export type ContactCreateForeignRelationshipKeys =
  keyof typeof ZdContactCreateForeignRelationshipsBase

export const ZdContactCreateNullableForeignRelationships = z.object({
  tags: ZdContactTagList.nullable(),
  custom_fields: ZdContactCustomFieldList.nullable(),
})

export const ZdContactCreateRelationshipsBase =
  ZdContactCreateListRelationshipsBase.merge(
    ZdContactCreateForeignRelationshipsBase
  )
export const ContactCreateRelationshipKeysSet = new Set(
  Object.keys(ZdContactCreateRelationshipsBase.shape)
)
export type ContactCreateRelationshipKeys =
  keyof typeof ZdContactCreateRelationshipsBase.shape

export const ZdContactCreateNullableRelationships =
  ZdContactCreateNullableListRelationships.merge(
    ZdContactCreateNullableForeignRelationships
  )

const ZdContactCreateAttributes = ZdContactCreateAttributesBase.merge(
  ZdContactCreateRelationshipsBase
).shape

export const ZdContactCreateRequest = ZdContactCreateAttributesBase.partial()
  .merge(ZdContactCreateRelationshipsBase.partial())
  .extend({
    // override: non-optional
    full_name: z.string().min(1),

    date_created: z.date().optional(),
    source_created: z.string().optional(),
  })
export type ContactCreateRequest = z.infer<typeof ZdContactCreateRequest>
export const ContactCreateRequestDefaults: ContactCreateRequest = {
  full_name: '',
}

const contactCreateRequestMerged = ZdContactCreateAttributesBase.merge(
  ZdContactCreateNullableRelationships
)
const contactCreateRequestNulled = new ZodObject({
  ...contactCreateRequestMerged._def,
  shape: () => mapValues(contactCreateRequestMerged.shape, (v) => v.nullable()),
}) as ZodObject<{
  [k in keyof typeof contactCreateRequestMerged.shape]: ZodNullable<
    typeof contactCreateRequestMerged.shape[k]
  >
}> // HACK not sure if this works

const ZdContactCreateRequestNullable = contactCreateRequestNulled.extend({
  full_name: z.string().min(1), // override: non-optional

  date_created: z.date().nullable(),
  source_created: z.string().nullable(),
})
export type ContactCreateRequestNullable = z.infer<
  typeof ZdContactCreateRequestNullable
>

export const ZdContactArchiveRequest = z.object({
  reason_archived: z.string().optional(),
})
export type ContactArchiveRequest = z.infer<typeof ZdContactArchiveRequest>

export const ZdContactUpdateRequest = ZdContactCreateAttributesBase.partial()
export type ContactUpdateRequest = z.infer<typeof ZdContactUpdateRequest>
export const ZdContactRequestNullish = deepNullable(ZdContactUpdateRequest)
export type ContactRequestNullish = z.infer<typeof ZdContactRequestNullish>

export const ZdPermission = z.object({
  slug: z.string(),
  contact_id: ZdIdContact.nullish(),
  committee_id: ZdIdCommittee.nullish(),
  meeting_id: ZdIdMeeting.nullish(),
  election_id: ZdIdElection.nullish(),
})
export type Permission = z.infer<typeof ZdPermission>

export const ZdUserResponse = z.object({
  object: z.literal('user'),
  id: ZdIdUser,
  contact: ZdIdContact,
  security_principal: ZdIdSecurityPrincipal,
  permissions: z.array(ZdPermission),
})
export type UserResponse = z.infer<typeof ZdUserResponse>

export const CustomFieldTypes = [
  'boolean',
  'choice',
  'date',
  'number',
  'range',
  'text',
  'textarea',
  'url',
  'array',
  'object',
] as const
export type CustomFieldTypeUnion = typeof CustomFieldTypes[number]

export const CustomFieldTypeNames: { [k in CustomFieldTypeUnion]: string } = {
  array: 'List of items',
  boolean: 'Toggle (yes/no, on/off)',
  choice: 'Pick one from multiple choices',
  date: 'Date',
  number: 'Number',
  object: '(advanced) JSON object',
  range: 'Range of numbers',
  text: 'Short text',
  textarea: 'Large text',
  url: 'Link',
}

export const ZdCustomFieldChoiceOptions = z.object({
  choices: z.array(z.object({ name: z.string().nullish(), value: z.string() })),
})
export type CustomFieldChoiceOptions = z.infer<
  typeof ZdCustomFieldChoiceOptions
>
export const ZdCustomFieldRangeOptions = z.object({
  start: z.number(),
  end: z.number(),
  step: z.number().nullish(),
})
export type CustomFieldRangeOptions = z.infer<typeof ZdCustomFieldRangeOptions>
export const ZdCustomFieldOptions = z.union([
  ZdCustomFieldChoiceOptions,
  ZdCustomFieldRangeOptions,
])

export const ZdCustomFieldResponse = z.object({
  object: z.literal('custom_field'),
  id: ZdIdCustomField,
  date_created: ZdISODateStr,
  name: z.string(),
  description: z.string().nullable(),
  field_type: z.enum(CustomFieldTypes),
  options: ZdCustomFieldOptions.nullable(),
  contacts: z.array(z.string()),
  tags: z.array(z.string()),
})
export type CustomFieldResponse = z.infer<typeof ZdCustomFieldResponse>

export const ZdCustomFieldCreateRequest = z.object({
  name: z.string(),
  description: z.string().optional(),
  field_type: z.enum(CustomFieldTypes),
  options: ZdCustomFieldOptions.optional(),
  date_created: z.date().optional(),
})
export type CustomFieldCreateRequest = z.infer<
  typeof ZdCustomFieldCreateRequest
>
export const CustomFieldCreateRequestDefaults: CustomFieldCreateRequest = {
  name: '',
  field_type: 'text',
}
export const CustomFieldChoiceOptionsDefaults: CustomFieldChoiceOptions = {
  choices: [],
}
export const CustomFieldRangeOptionsDefaults: CustomFieldRangeOptions = {
  start: 0,
  end: 0,
  step: 1,
}

export const ZdPaginatedListCustomField = ZdPaginatedList.extend({
  data: z.array(ZdCustomFieldResponse),
})
export type PaginatedListCustomField = z.infer<
  typeof ZdPaginatedListCustomField
>

export const ZdTagResponse = z.object({
  object: z.literal('tag'),
  id: ZdIdTag,
  date_created: ZdISODateStr,
  name: z.string(),
  members: z.array(z.string()).nullish(),
  contacts: z.array(ZdIdContact),
  custom_fields: z.array(ZdIdCustomField),
})
export type TagResponse = z.infer<typeof ZdTagResponse>

export const ZdPaginatedListTag = ZdPaginatedList.extend({
  data: z.array(ZdTagResponse),
})
export type PaginatedListTag = z.infer<typeof ZdPaginatedListTag>

export const ZdTagCreateRequest = z.object({
  name: z.string(),
  contacts: z.array(ZdIdContact).optional(),
  custom_fields: z.array(ZdIdCustomField).optional(),
})
export type TagCreateRequest = z.infer<typeof ZdTagCreateRequest>
export const TagCreateRequestDefaults: TagCreateRequest = {
  name: '',
}

export const ZdSearchQuery = ZdPaginationQueryArgs.extend({
  q: z.string(),
})
export const ZdSearchTagsQuery = ZdSearchQuery
export type SearchTagsQuery = z.infer<typeof ZdSearchTagsQuery>
export const ZdSearchContactsQuery = ZdSearchQuery
export type SearchContactsQuery = z.infer<typeof ZdSearchContactsQuery>
export const SearchContactsQueryDefaults: SearchContactsQuery = { q: '' }

export const ImportEventTypeArray = [
  'zoom_webhook',
  'newsletter_signup',
  'dsausa_roster',
  'website_signup',
  'csv_import',
] as const
export type ImportEventType = typeof ImportEventTypeArray[number]

export const ZdImportEventInfo = z
  .object({
    location: z.string().nullable(),
    date_started: ZdISODateStr.nullable(),
    date_ended: ZdISODateStr.nullable(),
    organizer: z.string().nullable(),
    event_id: z.string().nullable(),
  })
  .passthrough()

export const ImportEventReviewActionTypeArray = [
  'create_new_contact',
  'ignore_once',
  'match_manual',
  'match_auto',
  'proposed_match',
  'proposed_create',
] as const
export type ImportEventReviewActionType =
  typeof ImportEventReviewActionTypeArray[number]

export const USER_REVIEW_ACTIONS: Set<ImportEventReviewActionType> = new Set([
  'create_new_contact',
  'ignore_once',
  'match_manual',
  'match_auto',
])
export const AUTO_REVIEW_ACTIONS: Set<ImportEventReviewActionType> = difference(
  new Set(ImportEventReviewActionTypeArray),
  USER_REVIEW_ACTIONS
)

export const PROPOSED_REVIEW_ACTIONS: Set<ImportEventReviewActionType> =
  new Set(['proposed_match', 'proposed_create'])

export const MATCH_ACTIONS: Set<ImportEventReviewActionType> = new Set([
  'proposed_match',
  'match_auto',
  'match_manual',
])

export const CREATE_ACTIONS: Set<ImportEventReviewActionType> = new Set([
  'proposed_create',
  'create_new_contact',
])

export const ImportReviewMergeActionTypeArray = [
  'take_incoming',
  'keep_existing',
  'manual_edit',
  'add_new',
] as const
export type ImportReviewMergeActionType =
  typeof ImportReviewMergeActionTypeArray[number]

// TODO improve these types
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const importReviewMergeActionGenerator = (value: z.ZodTypeAny) =>
  z.object({
    attribute: z.string(),
    merge_action: z.enum(ImportReviewMergeActionTypeArray),
    edit_value: value.nullish(),
  })

export const ZdImportEventReviewMerge = z.object(
  mapValues(ZdContactCreateAttributes, (value) =>
    importReviewMergeActionGenerator(value).nullish()
  )
)
export type ImportEventReviewMerge = z.infer<typeof ZdImportEventReviewMerge>

export const ZdImportEventMatchJustification = z.object({
  confidence: z.enum(['strong', 'fuzzy']),
  strong_matches: z.array(ZdIdContact),
  fuzzy_matches: z.array(ZdIdContact),
})

export const ZdImportEventReviewAction = z.object({
  object: z.literal('import_event_review_action'),
  id: ZdIdImportEventReviewAction,
  participant: ZdContactCreateRequestNullable,
  import_event_id: ZdIdImportEvent,
  action: z.enum(ImportEventReviewActionTypeArray),
  merge_into_contact_id: ZdIdContact.nullable(),
  merge: ZdImportEventReviewMerge.nullable(),
  match_justification: ZdImportEventMatchJustification.nullable(),
})
export type ImportEventReviewAction = z.infer<typeof ZdImportEventReviewAction>
export const ZdPaginatedListImportEventReviewActions = ZdPaginatedList.extend({
  data: z.array(ZdImportEventReviewAction),
})
export type PaginatedListImportEventReviewActions = z.infer<
  typeof ZdPaginatedListImportEventReviewActions
>

export const ZdImportEventResponse = z.object({
  object: z.literal('import_event'),
  id: ZdIdImportEvent,
  creator: ZdIdSecurityPrincipal,
  date_created: ZdISODateStr,
  reviewer: ZdIdSecurityPrincipal.nullable(),
  date_reviewed: ZdISODateStr.nullable(),
  title: z.string(),
  event_type: z.enum(ImportEventTypeArray),
  event_info: ZdImportEventInfo.nullable(),
  review_actions: ZdPaginatedListImportEventReviewActions,
  review_action_counts: z.record(z.number()),
})
export type ImportEventResponse = z.infer<typeof ZdImportEventResponse>

export const ZdPaginatedListImportEvents = ZdPaginatedList.extend({
  data: z.array(ZdImportEventResponse),
})
export type PaginatedListImportEvents = z.infer<
  typeof ZdPaginatedListImportEvents
>

export const ZdImportEventInfoCreateRequest = z
  .object({
    location: z.string().optional(),
    date_started: z.date().optional(),
    date_ended: z.date().optional(),
    organizer: z.string().optional(),
    event_id: z.string().optional(),
  })
  .passthrough()
export const ZdImportEventCreateRequest = z.object({
  title: z.string(),
  event_type: z.enum(ImportEventTypeArray),
  event_info: ZdImportEventInfoCreateRequest.optional(),
  date_created: z.date().optional(),
  tags: z.array(ZdIdTag).optional(),
  custom_fields: z.array(ZdIdCustomField).optional(),
  participants: z.array(ZdContactCreateRequest),
})
export type ImportEventCreateRequest = z.infer<
  typeof ZdImportEventCreateRequest
>

export const ZdImportContactFieldsBase = z.object({
  first_name: z.string().optional(),
  middle_name: z.string().optional(),
  last_name: z.string().optional(),
  suffix: z.string().optional(),

  full_name: z.string().optional(),
  display_name: z.string().optional(),
})

export const ZdImportContactFields = ZdImportContactFieldsBase.superRefine(
  (val, ctx) => {
    if (val.full_name == null) {
      if (
        val.first_name == null &&
        val.middle_name == null &&
        val.last_name == null
      ) {
        ctx.addIssue({
          code: ZodIssueCode.custom,
          message: 'must provide at least one name',
        })
      }
    } else {
      if (val.first_name != null) {
        ctx.addIssue({
          code: ZodIssueCode.custom,
          message: "can't use 'full name' and first name at the same time",
          path: ['first_name'],
        })
      }

      if (val.middle_name != null) {
        ctx.addIssue({
          code: ZodIssueCode.custom,
          message: "can't use 'full name' and middle name at the same time",
          path: ['middle_name'],
        })
      }

      if (val.last_name != null) {
        ctx.addIssue({
          code: ZodIssueCode.custom,
          message: "can't use 'full name' and last name at the same time",
          path: ['last_name'],
        })
      }

      if (val.suffix != null) {
        ctx.addIssue({
          code: ZodIssueCode.custom,
          message: "can't use 'full name' and suffix at the same time",
          path: ['suffix'],
        })
      }
    }

    if (
      val.first_name == null &&
      val.middle_name == null &&
      val.last_name == null &&
      val.suffix != null
    ) {
      ctx.addIssue({
        code: ZodIssueCode.custom,
        message: 'must provide a name to use with a suffix',
        path: ['suffix'],
      })
    }

    if (val.first_name == null && val.last_name != null) {
      ctx.addIssue({
        code: ZodIssueCode.custom,
        message:
          "must provide a first name to use a last name. If you only have one name, consider using 'full name' instead",
        path: ['last_name'],
      })
    }
  }
).transform((val) => {
  if (val.full_name != null) {
    return val
  }

  const combinedNameFragments = compact([
    val.first_name,
    val.middle_name,
    val.last_name,
    val.suffix,
  ]).join(' ')
  const omitted = omit(val, [
    'first_name',
    'middle_name',
    'last_name',
    'suffix',
  ])
  return { ...omitted, full_name: combinedNameFragments }
})

export type ImportContactFields = z.infer<typeof ZdImportContactFields>

export const ZdImportEventReviewActionUpdate = z.object({
  id: ZdIdImportEventReviewAction,
  action: z.enum(ImportEventReviewActionTypeArray),
  merge_into_contact_id: ZdIdContact.nullish(),
  merge: ZdImportEventReviewMerge.nullish(),
})
export type ImportEventReviewActionUpdate = z.infer<
  typeof ZdImportEventReviewActionUpdate
>

export const ZdImportEventReviewActionsUpdateRequest = z.object({
  actions: z.array(ZdImportEventReviewActionUpdate),
})
export type ImportEventReviewActionsUpdateRequest = z.infer<
  typeof ZdImportEventReviewActionsUpdateRequest
>

export const ZdSecurityPrincipalRole = z.object({
  object: z.literal('security_principal_role'),
  id: ZdIdSecurityPrincipalRole,
  slug: z.string().min(1),
  name: z.string().min(1),
  description: z.string(),
  autogenerated: z.boolean(),
  permissions: z.array(ZdPermission),
})
export type SecurityPrincipalRole = z.infer<typeof ZdSecurityPrincipalRole>

export const ZdPaginatedListSecurityPrincipalRole = ZdPaginatedList.extend({
  data: z.array(ZdSecurityPrincipalRole),
})
export type PaginatedListSecurityPrincipalRole = z.infer<
  typeof ZdPaginatedListSecurityPrincipalRole
>

export const ZdSecurityPrincipal = z.object({
  object: z.literal('security_principal'),
  id: ZdIdSecurityPrincipal,
  user: ZdUserResponse.nullable(),
  roles: ZdPaginatedListSecurityPrincipalRole.nullable(),
})
export type SecurityPrincipal = z.infer<typeof ZdSecurityPrincipal>

export const ZdPaginatedListSecurityPrincipal = ZdPaginatedList.extend({
  data: z.array(ZdSecurityPrincipal),
})
export type PaginatedListSecurityPrincipal = z.infer<
  typeof ZdPaginatedListSecurityPrincipal
>

export const ZdApiError = z.object({
  object: z.literal('error'),
  type: z.string().min(1), // TODO should be Zod enum
  error_code: z.string().min(1),
  status_code: z.number(), // TODO should be Zod enum
  friendly_message: z.string().min(1),
  params: z.union([z.object({}).passthrough(), z.array(z.string())]).nullable(),
})
export type ApiError = z.infer<typeof ZdApiError>

const ZdParameterInvalidErrorParam = z.object({
  loc: z.array(z.union([z.string(), z.number()])),
  msg: z.string(),
  type: z.string(),
  ctx: z.object({}).optional(),
})

export const ZdParameterInvalidError = ZdApiError.extend({
  type: z.literal('parameter_invalid'),
  params: z.array(ZdParameterInvalidErrorParam),
})
export type ParameterInvalidError = z.infer<typeof ZdParameterInvalidError>
