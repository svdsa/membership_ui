import {
  BaseQueryFn,
  createApi,
  FetchArgs,
  fetchBaseQuery,
  FetchBaseQueryError,
} from '@reduxjs/toolkit/query/react'
import { pick } from 'lodash'
import { browserHistory } from 'react-router'
import {
  ContactArchiveRequest,
  ContactAttributePaginatedQueryArgs,
  ContactCreateRequest,
  ContactResponse,
  ContactUpdateRequest,
  CustomFieldCreateRequest,
  CustomFieldResponse,
  EmailAddressArchiveRequest,
  EmailAddressCreateRequest,
  EmailAddressResponse,
  EmailAddressUpdateRequest,
  IdContact,
  IdCustomField,
  IdEmailAddress,
  IdImportEvent,
  IdPhoneNumber,
  IdSecurityPrincipal,
  IdTag,
  ImportEventCreateRequest,
  ImportEventResponse,
  ImportEventReviewActionsUpdateRequest,
  PaginatedListContact,
  PaginatedListCustomField,
  PaginatedListEmailAddress,
  PaginatedListImportEventReviewActions,
  PaginatedListImportEvents,
  PaginatedListPhoneNumber,
  PaginatedListTag,
  PaginationQueryArgs,
  PhoneNumberArchiveRequest,
  PhoneNumberCreateRequest,
  PhoneNumberResponse,
  PhoneNumberUpdateRequest,
  SearchContactsQuery,
  SearchTagsQuery,
  SecurityPrincipal,
  TagCreateRequest,
  TagResponse,
  UserResponse,
  ZdContactResponse,
  ZdCustomFieldResponse,
  ZdImportEventResponse,
  ZdPaginatedListContact,
  ZdPaginatedListCustomField,
  ZdPaginatedListEmailAddress,
  ZdPaginatedListImportEventReviewActions,
  ZdPaginatedListImportEvents,
  ZdPaginatedListPhoneNumber,
  ZdPaginatedListTag,
  ZdPaginationQueryArgs,
  ZdSecurityPrincipal,
  ZdTagResponse,
  ZdUserResponse,
} from 'src/js/api/schemas'
import { parsePaginatedHeaders } from 'src/js/api/transformers'
import { JsonSchema } from 'src/js/client/ApiClient'
import { MEMBERSHIP_API_URL, USE_AUTH } from 'src/js/config'
import { auth } from 'src/js/redux/actions/auth/universalAuth'

const baseQuery = fetchBaseQuery({
  baseUrl: MEMBERSHIP_API_URL,
  prepareHeaders: (headers) => {
    if (USE_AUTH) {
      // TODO use redux for this
      const token = localStorage.getItem('id_token')
      if (token) {
        headers.set('Authorization', `Bearer ${token}`)
      }
    }
    return headers
  },
})

const baseQueryWithAuth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  const result = await baseQuery(args, api, extraOptions)

  if (result.error != null && result.error.status === 401) {
    // Auth0 session expired, attempt silent re-auth
    try {
      await auth.silentAuth()
    } catch (e) {
      // TODO consider dispatching something here to show a modal
      // to the user instead of redirecting them by surprise
      const currentLocation = browserHistory.getCurrentLocation()
      browserHistory.push({
        pathname: '/login',
        query: { redirect: currentLocation.pathname },
      })
    }

    const retry = await baseQuery(args, api, extraOptions)
    return retry
  }
  return result
}

export const api = createApi({
  baseQuery: baseQueryWithAuth,
  tagTypes: [
    'Contact',
    'CustomField',
    'User',
    'Tag',
    'ImportEvent',
    'EmailAddress',
    'PhoneNumber',
  ],
  endpoints: (builder) => ({
    getContactFromId: builder.query<ContactResponse, IdContact>({
      query: (id) => ({ url: `contacts/${id}` }),
      transformResponse: (elem) => ZdContactResponse.parse(elem),
      providesTags: (result) =>
        result ? [{ type: 'Contact', id: result.id }] : [],
    }),
    getContactList: builder.query<
      PaginatedListContact,
      PaginationQueryArgs | null | void
    >({
      query: (args) => ({ url: `contacts`, params: args || undefined }),
      transformResponse: parsePaginatedHeaders(ZdPaginatedListContact),
      providesTags: (result) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'Contact' as const,
                id,
              })),
              { type: 'Contact', id: 'PAGE' },
            ]
          : [{ type: 'Contact', id: 'PAGE' }],
    }),
    getContactMe: builder.query<ContactResponse, void>({
      query: () => ({ url: `contacts/@me` }),
      transformResponse: (elem) => ZdContactResponse.parse(elem),
      providesTags: (result) =>
        result
          ? [
              { type: 'Contact', id: result.id },
              { type: 'Contact', id: '@me' },
            ]
          : [],
    }),
    createContact: builder.mutation<ContactResponse, ContactCreateRequest>({
      query: (body) => ({ url: `contacts`, method: 'POST', body }),
      invalidatesTags: [{ type: 'Contact', id: 'PAGE' }],
    }),
    archiveContact: builder.mutation<
      ContactResponse,
      { id: IdContact; body: ContactArchiveRequest }
    >({
      query: ({ id, body }) => ({
        url: `contacts/${id}/archive`,
        method: 'PUT',
        body,
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'Contact', id: result.id },
              { type: 'Contact', id: 'PAGE' },
            ]
          : [],
    }),
    unarchiveContact: builder.mutation<ContactResponse, IdContact>({
      query: (id) => ({
        url: `contacts/${id}/archive`,
        method: 'DELETE',
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'Contact', id: result.id },
              { type: 'Contact', id: 'PAGE' },
            ]
          : [],
    }),
    updateContact: builder.mutation<
      ContactResponse,
      { id: IdContact; body: ContactUpdateRequest }
    >({
      query: ({ id, body }) => ({
        url: `contacts/${id}`,
        method: 'PATCH',
        body,
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'Contact', id: result.id },
              { type: 'Contact', id: 'PAGE' },
            ]
          : [],
    }),

    getContactEmailAddresses: builder.query<
      PaginatedListEmailAddress,
      ContactAttributePaginatedQueryArgs
    >({
      // HACK this feels weird, consider refactoring args to separate PaginatedQueryArgs
      query: (args) => ({
        url: `contacts/${args.contactId}/email_addresses`,
        params: pick(args, Object.keys(ZdPaginationQueryArgs.shape)),
      }),
      transformResponse: parsePaginatedHeaders(ZdPaginatedListEmailAddress),
      providesTags: (result) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'EmailAddress' as const,
                id,
              })),
              { type: 'EmailAddress', id: 'PAGE' },
            ]
          : [],
    }),
    createContactEmailAddress: builder.mutation<
      EmailAddressResponse,
      { id: IdContact; body: EmailAddressCreateRequest }
    >({
      query: ({ id, body }) => ({
        url: `contacts/${id}/email_addresses`,
        method: 'POST',
        body,
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'EmailAddress', id: result.id },
              { type: 'EmailAddress', id: 'PAGE' },
              { type: 'Contact', id: 'PAGE' },
              { type: 'Contact', id: result.contact },
            ]
          : [],
    }),
    archiveContactEmailAddress: builder.mutation<
      EmailAddressResponse,
      {
        emailAddressId: IdEmailAddress
        contactId: IdContact
        body: EmailAddressArchiveRequest
      }
    >({
      query: ({ emailAddressId, contactId, body }) => ({
        url: `contacts/${contactId}/email_addresses/${emailAddressId}/archive`,
        method: 'PUT',
        body,
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'EmailAddress', id: result.id },
              { type: 'EmailAddress', id: 'PAGE' },
              { type: 'Contact', id: 'PAGE' },
              { type: 'Contact', id: result.contact },
            ]
          : [],
    }),
    updateContactEmailAddress: builder.mutation<
      EmailAddressResponse,
      {
        emailAddressId: IdEmailAddress
        contactId: IdContact
        body: EmailAddressUpdateRequest
      }
    >({
      query: ({ emailAddressId, contactId, body }) => ({
        url: `contacts/${contactId}/email_addresses/${emailAddressId}`,
        method: 'PATCH',
        body,
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'EmailAddress', id: result.id },
              { type: 'EmailAddress', id: 'PAGE' },
              { type: 'Contact', id: 'PAGE' },
              { type: 'Contact', id: result.contact },
            ]
          : [],
    }),

    getContactPhoneNumbers: builder.query<
      PaginatedListPhoneNumber,
      ContactAttributePaginatedQueryArgs
    >({
      // HACK this feels weird, consider refactoring args to separate PaginatedQueryArgs
      query: (args) => ({
        url: `contacts/${args.contactId}/phone_numbers`,
        params: pick(args, Object.keys(ZdPaginationQueryArgs.shape)),
      }),
      transformResponse: parsePaginatedHeaders(ZdPaginatedListPhoneNumber),
      providesTags: (result) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'PhoneNumber' as const,
                id,
              })),
              { type: 'PhoneNumber', id: 'PAGE' },
            ]
          : [],
    }),
    createContactPhoneNumber: builder.mutation<
      PhoneNumberResponse,
      { id: IdContact; body: PhoneNumberCreateRequest }
    >({
      query: ({ id, body }) => ({
        url: `contacts/${id}/phone_numbers`,
        method: 'POST',
        body,
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'PhoneNumber', id: result.id },
              { type: 'PhoneNumber', id: 'PAGE' },
              { type: 'Contact', id: 'PAGE' },
              { type: 'Contact', id: result.contact },
            ]
          : [],
    }),
    archiveContactPhoneNumber: builder.mutation<
      PhoneNumberResponse,
      {
        phoneNumberId: IdPhoneNumber
        contactId: IdContact
        body: PhoneNumberArchiveRequest
      }
    >({
      query: ({ phoneNumberId, contactId, body }) => ({
        url: `contacts/${contactId}/phone_numbers/${phoneNumberId}/archive`,
        method: 'PUT',
        body,
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'PhoneNumber', id: result.id },
              { type: 'PhoneNumber', id: 'PAGE' },
              { type: 'Contact', id: 'PAGE' },
              { type: 'Contact', id: result.contact },
            ]
          : [],
    }),
    updateContactPhoneNumber: builder.mutation<
      PhoneNumberResponse,
      {
        phoneNumberId: IdPhoneNumber
        contactId: IdContact
        body: PhoneNumberUpdateRequest
      }
    >({
      query: ({ phoneNumberId, contactId, body }) => ({
        url: `contacts/${contactId}/phone_numbers/${phoneNumberId}`,
        method: 'PATCH',
        body,
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'PhoneNumber', id: result.id },
              { type: 'PhoneNumber', id: 'PAGE' },
              { type: 'Contact', id: 'PAGE' },
              { type: 'Contact', id: result.contact },
            ]
          : [],
    }),

    // Users
    getUserMe: builder.query<UserResponse, void>({
      query: () => ({ url: `users/@me` }),
      transformResponse: (elem) => ZdUserResponse.parse(elem),
    }),

    // Custom fields
    getCustomFieldsList: builder.query<
      PaginatedListCustomField,
      PaginationQueryArgs | null | void
    >({
      query: (args) => ({ url: `custom_fields`, params: args || undefined }),
      transformResponse: parsePaginatedHeaders(ZdPaginatedListCustomField),
      providesTags: (result) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'CustomField' as const,
                id,
              })),
              { type: 'CustomField', id: 'PAGE' },
            ]
          : [{ type: 'CustomField', id: 'PAGE' }],
    }),
    getCustomFieldFromId: builder.query<CustomFieldResponse, IdCustomField>({
      query: (id) => ({ url: `custom_fields/${id}` }),
      transformResponse: (elem) => ZdCustomFieldResponse.parse(elem),
      providesTags: (result) =>
        result ? [{ type: 'CustomField', id: result.id }] : [],
    }),
    createCustomField: builder.mutation<
      CustomFieldResponse,
      CustomFieldCreateRequest
    >({
      query: (body) => ({
        url: `custom_fields`,
        method: 'POST',
        body,
      }),
      invalidatesTags: [{ type: 'CustomField', id: 'PAGE' }],
    }),
    deleteCustomField: builder.mutation<CustomFieldResponse, IdCustomField>({
      query: (id) => ({
        url: `custom_fields/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'CustomField', id: result.id },
              { type: 'CustomField', id: 'PAGE' },
            ]
          : [],
    }),

    // Tags
    getTagsList: builder.query<
      PaginatedListTag,
      PaginationQueryArgs | null | void
    >({
      query: (args) => ({ url: `tags`, params: args || undefined }),
      transformResponse: parsePaginatedHeaders(ZdPaginatedListTag),
      providesTags: (result) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'Tag' as const,
                id,
              })),
              { type: 'Tag', id: 'PAGE' },
            ]
          : [{ type: 'Tag', id: 'PAGE' }],
    }),
    createTag: builder.mutation<TagResponse, TagCreateRequest>({
      query: (body) => ({
        url: `tags`,
        method: 'POST',
        body,
      }),
      transformResponse: (elem) => ZdTagResponse.parse(elem),
      invalidatesTags: [{ type: 'Tag', id: 'PAGE' }],
    }),
    getTagFromId: builder.query<TagResponse, IdTag>({
      query: (id) => ({ url: `tags/${id}` }),
      transformResponse: (elem) => ZdTagResponse.parse(elem),
      providesTags: (result) =>
        result ? [{ type: 'Tag', id: result.id }] : [],
    }),

    // Search
    getTagsForSearch: builder.query<PaginatedListTag, SearchTagsQuery>({
      query: (args) => ({
        url: `search/tags`,
        params: args || undefined,
      }),
      transformResponse: parsePaginatedHeaders(ZdPaginatedListTag),
    }),
    searchTags: builder.mutation<PaginatedListTag, SearchTagsQuery>({
      query: (args) => ({
        url: `search/tags`,
        params: args || undefined,
      }),
      transformResponse: parsePaginatedHeaders(ZdPaginatedListTag),
    }),
    searchContacts: builder.query<PaginatedListContact, SearchContactsQuery>({
      query: (args) => ({
        url: 'search/contacts',
        params: args || undefined,
      }),
      transformResponse: parsePaginatedHeaders(ZdPaginatedListContact),
    }),
    // Import events
    getImportsList: builder.query<
      PaginatedListImportEvents,
      PaginationQueryArgs | null | void
    >({
      query: (args) => ({ url: `imports`, params: args || undefined }),
      transformResponse: parsePaginatedHeaders(ZdPaginatedListImportEvents),
      providesTags: (result) =>
        result
          ? [
              ...result.data.map(({ id }) => ({
                type: 'ImportEvent' as const,
                id,
              })),
              { type: 'ImportEvent', id: 'PAGE' },
            ]
          : [{ type: 'ImportEvent', id: 'PAGE' }],
    }),
    getImportEventFromId: builder.query<ImportEventResponse, IdImportEvent>({
      query: (id) => ({ url: `imports/${id}` }),
      transformResponse: (elem) => ZdImportEventResponse.parse(elem),
      providesTags: (result) =>
        result ? [{ type: 'ImportEvent', id: result.id }] : [],
    }),
    createImportEvent: builder.mutation<
      ImportEventResponse,
      ImportEventCreateRequest
    >({
      query: (body) => ({ url: 'imports', method: 'POST', body }),
      transformResponse: (elem) => ZdImportEventResponse.parse(elem),
    }),
    updateReviewActions: builder.mutation<
      PaginatedListImportEventReviewActions,
      { id: IdImportEvent; body: ImportEventReviewActionsUpdateRequest }
    >({
      query: ({ id, body }) => ({
        url: `imports/${id}/review_actions`,
        method: 'PATCH',
        body,
      }),
      transformResponse: parsePaginatedHeaders(
        ZdPaginatedListImportEventReviewActions
      ),
      invalidatesTags: (result) => {
        if (result == null) {
          return []
        }

        if (result.data.length === 0) {
          return [{ type: 'ImportEvent', id: 'PAGE' }]
        }

        return [
          { type: 'ImportEvent', id: result.data[0].import_event_id },
          { type: 'ImportEvent', id: 'PAGE' },
        ]
      },
    }),
    confirmImportEvent: builder.mutation<ImportEventResponse, IdImportEvent>({
      query: (id) => ({ url: `imports/${id}/confirm`, method: 'PUT' }),
      transformResponse: (elem) => ZdImportEventResponse.parse(elem),
      invalidatesTags: (result) =>
        result != null
          ? [
              { type: 'ImportEvent', id: result.id },
              { type: 'ImportEvent', id: 'PAGE' },
              { type: 'Contact' },
            ]
          : [],
    }),

    // Schemas
    schemaCreateContact: builder.query<JsonSchema<ContactCreateRequest>, void>({
      query: () => ({ url: 'schemas/contacts', method: 'POST' }),
    }),

    // Security principals
    getSecurityPrincipalById: builder.query<
      SecurityPrincipal,
      IdSecurityPrincipal
    >({
      query: (id) => ({ url: `security_principals/${id}` }),
      transformResponse: (elem) => ZdSecurityPrincipal.parse(elem),
    }),
  }),
})

export const {
  useGetContactMeQuery,
  useGetUserMeQuery,
  useGetContactFromIdQuery,
  useGetContactListQuery,
  useCreateContactMutation,
  useArchiveContactMutation,
  useUnarchiveContactMutation,
  useUpdateContactMutation,
  useGetContactEmailAddressesQuery,
  useCreateContactEmailAddressMutation,
  useArchiveContactEmailAddressMutation,
  useUpdateContactEmailAddressMutation,
  useGetContactPhoneNumbersQuery,
  useCreateContactPhoneNumberMutation,
  useArchiveContactPhoneNumberMutation,
  useUpdateContactPhoneNumberMutation,
  useGetCustomFieldsListQuery,
  useGetCustomFieldFromIdQuery,
  useCreateCustomFieldMutation,
  useDeleteCustomFieldMutation,
  useGetTagsListQuery,
  useCreateTagMutation,
  useGetTagFromIdQuery,
  useSearchTagsMutation,
  useGetTagsForSearchQuery,
  useSearchContactsQuery,
  useGetImportsListQuery,
  useGetImportEventFromIdQuery,
  useCreateImportEventMutation,
  useUpdateReviewActionsMutation,
  useConfirmImportEventMutation,
  useSchemaCreateContactQuery,
  useGetSecurityPrincipalByIdQuery,
} = api
