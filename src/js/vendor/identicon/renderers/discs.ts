import { IdenticonRenderOptions } from 'src/js/vendor/identicon'
import { createColor, encodeColor, rand, seedrand } from './utils'

const renderIcon = (
  opts: IdenticonRenderOptions,
  canvas: HTMLCanvasElement
): HTMLCanvasElement => {
  const seed =
    opts.seed || Math.floor(Math.random() * Math.pow(10, 16)).toString(16)
  seedrand(seed)
  const size = 512
  const minSize = size / 32

  const cc = canvas.getContext('2d')
  if (cc == null) {
    return canvas
  }

  canvas.width = size
  canvas.height = size

  cc.fillStyle = encodeColor({ h: 0, s: 0, l: 100 * rand() })
  cc.fillRect(0, 0, size, size)
  const numDiscs = 3 + rand() * 10
  for (let i = 0; i < numDiscs; i++) {
    cc.fillStyle = encodeColor(createColor())
    cc.beginPath()
    let radius: number
    if (i < 2) {
      radius = rand() * (size - minSize) + minSize
    } else {
      radius = rand() * (size - minSize) * 0.125 + minSize
    }
    cc.arc(rand() * size, rand() * size, radius, 0, 2 * Math.PI)
    cc.fill()
  }

  return canvas
}

export default renderIcon
