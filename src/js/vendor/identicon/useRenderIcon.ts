import React, { useEffect } from 'react'

export default function useRenderIcon(
  renderIcon,
  opts,
  canvasRef: React.RefObject<HTMLCanvasElement>
) {
  useEffect(() => {
    if (canvasRef.current) {
      renderIcon(opts, canvasRef.current)
    }
  })
}
