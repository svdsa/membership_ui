import React, { useRef } from 'react'
import renderBishop from './renderers/bishop'
import renderBlockie from './renderers/blockies'
import renderDiscs from './renderers/discs'
import renderRings from './renderers/rings'
import useRenderIcon from './useRenderIcon'

export interface IdenticonRenderOptions {
  seed?: string
  gridSize?: number
  scale?: number
}

export interface IdenticonProps extends IdenticonRenderOptions {
  renderIcon(
    opts: IdenticonRenderOptions,
    canvasRef: HTMLCanvasElement
  ): HTMLCanvasElement
  className?: string
}

export const Identicon: React.FC<IdenticonProps> = ({
  renderIcon,
  seed,
  gridSize,
  scale,
  className,
}) => {
  const canvasRef = useRef<HTMLCanvasElement>(null)
  useRenderIcon(renderIcon, { seed, gridSize, scale }, canvasRef)

  return (
    <canvas
      className={className}
      ref={canvasRef}
      style={{ width: '100%', height: '100%' }}
    />
  )
}

type IdenticonPresetProps = Omit<IdenticonProps, 'renderIcon'>

export const Blockie: React.FC<IdenticonPresetProps> = (props) => {
  return <Identicon {...props} renderIcon={renderBlockie} />
}

export const Bishop: React.FC<IdenticonPresetProps> = (props) => {
  return <Identicon {...props} renderIcon={renderBishop} />
}

export const Discs: React.FC<IdenticonPresetProps> = (props) => {
  return <Identicon {...props} renderIcon={renderDiscs} />
}

export const Rings: React.FC<IdenticonPresetProps> = (props) => {
  return <Identicon {...props} renderIcon={renderRings} />
}
