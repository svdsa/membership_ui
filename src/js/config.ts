import {
  MessagesByChapter,
  C12nKey,
  SupportedChapter,
  verifyCurrentChapter,
  DEFAULT_C18_MESSAGES,
} from './chapterization'
import { DisableFeatureKey } from 'src/js/features'

export const MEMBERSHIP_API_URL = process.env.MEMBERSHIP_API_URL

function castBool(value: string | undefined, defaultValue: boolean): boolean {
  value = value && value.trim()
  if (value === undefined || value === '') return Boolean(defaultValue)
  if (value === 'true' || value === '1') return true
  if (value === 'false' || value === '0') return false
  throw new Error('castBool unexpected value: ' + value)
}

function useAuth(): boolean {
  return castBool(process.env.USE_AUTH, true)
}

export const AUTH0_CLIENT_ID =
  process.env.AUTH0_CLIENT_ID || '<auth0 clientid unset>'
export const AUTH0_DOMAIN =
  process.env.AUTH0_DOMAIN || 'auth0domainnotset.example.com'
export const USE_AUTH = useAuth()
export const USE_ELECTIONS = !castBool(
  process.env.DISABLE_FEATURE_ELECTIONS,
  false
)
export const USE_COLLECTIVE_DUES = castBool(
  process.env.ENABLE_FEATURE_COLLECTIVE_DUES,
  false
)
export const USE_ANONYMOUS_SIGNUPS = castBool(
  process.env.ENABLE_FEATURE_ANONYMOUS_SIGNUPS,
  false
)
export const COLLECTIVE_DUES_STRIPE_API_KEY =
  process.env.COLLECTIVE_DUES_STRIPE_API_KEY ||
  'pk_test_yourtestpublickeygoeshere' // dummy example key

export const MAIN_HTML_ID = 'main'
export const PAGE_HEADING_HTML_ID = 'page_heading'
export const PAGE_TITLE_TEMPLATE = '%s - DSA Member Portal'

export const CHAPTER_ID: SupportedChapter =
  verifyCurrentChapter(process.env.DSA_CHAPTER_ID) || 'san-francisco'

/**
 * Define per-chapter messages here
 *
 * Edit `RequiredC12nMessages` and `OptionalC12nMessages` in `chapterization.ts` to
 * change the required messages below.
 */
const C12N_MESSAGES: MessagesByChapter = {
  'san-francisco': {
    CHAPTER_NAME: 'DSA San Francisco',
    CHAPTER_NAME_SHORT: 'DSA SF',
    ADMIN_EMAIL: 'tech@dsasf.org',
    ELIGIBILITY_EMAIL: 'eligibility@dsasf.org',
    CHAPTER_AREA_NAME: 'San Francisco',
    GROUP_NAME_SINGULAR: 'committee',
    GROUP_NAME_PLURAL: 'committees',
    URL_CHAPTER_CODE_OF_CONDUCT: 'https://dsasf.org/code_of_conduct',
    URL_CHAPTER_EVENTS: 'https://dsasf.org/events',
    URL_CHAPTER_GROUP_INFO: 'https://dsasf.org/working-groups',
    URL_CHAPTER_JOIN: 'https://dsasf.org/join',
    URL_CHAPTER_FAVICON_32:
      'https://dsasf.org/wp-content/uploads/2017/05/cropped-bridge-32x32.png',
    URL_CHAPTER_FAVICON_192:
      'https://dsasf.org/wp-content/uploads/2017/05/cropped-bridge-192x192.png',
  },
  'silicon-valley': {
    CHAPTER_NAME: 'Silicon Valley DSA',
    CHAPTER_NAME_SHORT: 'SV DSA',
    ADMIN_EMAIL: 'tech-data@siliconvalleydsa.org',
    ELIGIBILITY_EMAIL: 'info@siliconvalleydsa.org',
    COLLECTIVE_DUES_CANCEL_EMAIL: 'treasurer@siliconvalleydsa.org',
    CHAPTER_AREA_NAME: 'Silicon Valley',
    URL_CHAPTER_CODE_OF_CONDUCT: 'http://sv-dsa.org/code-of-conduct',
    URL_CHAPTER_EVENTS: 'https://sv-dsa.org/events',
    URL_CHAPTER_GROUP_INFO: 'https://sv-dsa.org/working-groups',
    URL_CHAPTER_JOIN: 'https://sv-dsa.org/join',
    URL_CHAPTER_FAVICON_192:
      'https://siliconvalleydsa.org/wp-content/uploads/2020/02/svdsa.png',
    URL_CHAPTER_FAVICON_32:
      'https://siliconvalleydsa.org/wp-content/uploads/2020/04/sv_dsa_favicon.png',
  },
}

const renderChapterMessage =
  (chapter: SupportedChapter) =>
  (key: C12nKey): string =>
    process.env[`DSA_${key}`] ||
    C12N_MESSAGES[chapter][key] ||
    DEFAULT_C18_MESSAGES[key]

export const c = renderChapterMessage(CHAPTER_ID)
