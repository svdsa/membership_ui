import React, { useCallback } from 'react'
import { Button, Card, Col, Container, Form, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import { AppFooter } from 'src/js/components/common/AppFooter'
import { c } from 'src/js/config'
import * as actions from 'src/js/redux/actions/auth'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'

interface SigninOwnProps {
  error?: string
}

type SigninProps = SigninOwnProps &
  SigninDispatchProps &
  SigninStateProps &
  RouteComponentProps<
    Record<string, never>,
    Record<string, never>,
    SigninOwnProps,
    { redirect?: string }
  >

const Login: React.FC<SigninProps> = ({
  clearUserSession,
  signinUser,
  location: { query },
}) => {
  const handleLoginClick = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault()
      e.stopPropagation()
      clearUserSession()
      const { redirect } = query
      signinUser(redirect)
    },
    [clearUserSession, window.location, signinUser]
  )

  return (
    <>
      <div className="height-fullscreen width-fullscreen position-absolute d-none d-md-block bg-secondary"></div>
      <Container className="height-fullscreen d-flex flex-column justify-content-center position-relative">
        <Row className="flex-fill justify-content-md-center align-items-center">
          <Col md={6} lg={4}>
            <Card body className="text-center mb-2 p-3">
              <Form onSubmit={handleLoginClick}>
                <div className="display-4 mb-2">🌹</div>
                <Card.Title as="h1" className="h3">
                  {c('CHAPTER_NAME')}
                </Card.Title>
                <Card.Subtitle className="mb-3">Member Portal</Card.Subtitle>
                <Button block variant="primary" type="submit">
                  Sign in
                </Button>
              </Form>
            </Card>
          </Col>
        </Row>
        <footer>
          <AppFooter className="text-light" />
        </footer>
      </Container>
    </>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(actions, dispatch)

type SigninStateProps = RootReducer
type SigninDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  SigninStateProps,
  SigninDispatchProps,
  SigninOwnProps,
  RootReducer
>(
  (state) => state,
  mapDispatchToProps
)(Login)
