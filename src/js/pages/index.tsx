import * as React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router'
import { useGetContactMeQuery } from 'src/js/api'
import { ContactName } from 'src/js/components/common/ContactName'
import ContentStack from 'src/js/components/common/ContentStack'
import PageHeading from 'src/js/components/common/PageHeading'
import iconContact from 'images/mycontactinfo.png'

interface HomePanelProps {
  title: string
  path: string
  imageUrl: string
}

const HomePanel: React.FC<HomePanelProps> = ({ title, path, imageUrl }) => (
  <Link to={path} className="wrapper-link">
    <div className="panel portal-panel">
      <div className="panel-heading text-center">
        <h3>{title}</h3>
      </div>
      <div className="panel-body">
        <img src={imageUrl} alt={title} />
      </div>
    </div>
  </Link>
)

const HomeContents: React.FC = () => {
  const { data, isLoading, error } = useGetContactMeQuery()

  return (
    <ContentStack data={data} error={error} isLoading={isLoading}>
      {(data) => (
        <>
          <Row>
            <Col>
              <PageHeading level={1}>
                Hey <ContactName contact={data} />!
              </PageHeading>
            </Col>
          </Row>
          <Row className="justify-content-md-center">
            <Col sm={6} md={4} lg={3}>
              <HomePanel
                title="Profile"
                path="/profile"
                imageUrl={iconContact}
              />
            </Col>
          </Row>
        </>
      )}
    </ContentStack>
  )
}

const HomeWrapper: React.FC = () => (
  <Container>
    <HomeContents />
  </Container>
)

export default HomeWrapper
