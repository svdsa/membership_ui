import { Formik } from 'formik'
import React from 'react'
import { Col, Row } from 'react-bootstrap'
import Helmet from 'react-helmet'
import { RouteComponentProps } from 'react-router'
import { useCreateContactMutation } from 'src/js/api'
import {
  ApiError,
  ContactCreateRequestDefaults,
  ZdContactCreateRequest,
} from 'src/js/api/schemas'
import PageHeading from 'src/js/components/common/PageHeading'
import { ContactForm } from 'src/js/components/contacts/ContactCreateForm'
import { transformApiErrorsToFormik } from 'src/js/util/formik'
import { toFormikValidationSchema } from 'src/js/util/zod-formik-adapter'

const NewContact: React.FC<
  RouteComponentProps<Record<string, never>, Record<string, never>>
> = ({ router }) => {
  const [submitForm] = useCreateContactMutation()

  return (
    <>
      <Helmet>
        <title>New contact</title>
      </Helmet>
      <Row>
        <Col>
          <PageHeading className="mt-0" level={2}>
            Add a new contact
          </PageHeading>
        </Col>
      </Row>
      <Row>
        <Col sm={12} xl={8}>
          <Formik
            initialValues={ContactCreateRequestDefaults}
            validationSchema={toFormikValidationSchema(ZdContactCreateRequest)}
            onSubmit={async (
              values,
              { setSubmitting, setErrors, setStatus }
            ) => {
              try {
                const results = await submitForm(values).unwrap()
                setSubmitting(false)
                router.push(`/admin/contacts/${results.id}`)
              } catch (err) {
                setSubmitting(false)
                const { status, errors } = transformApiErrorsToFormik(
                  err as { status: number; data: ApiError }
                )

                setStatus(status)
                if (errors != null) {
                  setErrors(errors)
                }
              }
            }}
          >
            {(params) => <ContactForm {...params} />}
          </Formik>
        </Col>
      </Row>
    </>
  )
}

export default NewContact
