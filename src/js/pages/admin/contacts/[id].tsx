import cx from 'classnames'
import { ErrorMessage, Formik } from 'formik'
import { isNil, omitBy, pick } from 'lodash'
import React, { useCallback, useMemo } from 'react'
import {
  Alert,
  Badge,
  Breadcrumb,
  Button,
  Col,
  Form,
  ListGroup,
  Row,
} from 'react-bootstrap'
import Helmet from 'react-helmet'
import { BsArchive } from 'react-icons/bs'
import { RouteComponentProps } from 'react-router'
import { LinkContainer } from 'react-router-bootstrap'
import {
  api,
  useArchiveContactEmailAddressMutation,
  useArchiveContactPhoneNumberMutation,
  useCreateContactEmailAddressMutation,
  useCreateContactPhoneNumberMutation,
  useGetContactFromIdQuery,
  useUpdateContactEmailAddressMutation,
  useUpdateContactMutation,
  useUpdateContactPhoneNumberMutation,
} from 'src/js/api'
import {
  ApiError,
  ContactAttributePaginatedQueryArgs,
  EmailAddressCreateRequestDefaults,
  EmailAddressUpdateRequest,
  IdContact,
  IdEmailAddress,
  IdPhoneNumber,
  PhoneNumberCreateRequestDefaults,
  PhoneNumberUpdateRequest,
  ZdContactUpdateRequest,
  ZdEmailAddressCreateRequest,
  ZdPhoneNumberCreateRequest,
} from 'src/js/api/schemas'
import ContentStack from 'src/js/components/common/ContentStack'
import { DeveloperInfo } from 'src/js/components/common/DeveloperInfo'
import { LoadingButton } from 'src/js/components/common/LoadingButton'
import PageHeading from 'src/js/components/common/PageHeading'
import { ContactArchiveAction } from 'src/js/components/contacts/ContactArchiveAction'
import { ContactUpdateForm } from 'src/js/components/contacts/ContactUpdateForm'
import { c } from 'src/js/config'
import { usePaginatedQuery } from 'src/js/hooks/usePaginatedQuery'
import { transformApiErrorsToFormik } from 'src/js/util/formik'
import { logError, showNotification } from 'src/js/util/util'
import { toFormikValidationSchema } from 'src/js/util/zod-formik-adapter'

const paginationArgs = {
  fetchAllPages: true,
}

const ContactFromId: React.FC<
  RouteComponentProps<{ contactId: string }, Record<string, never>>
> = ({ params: { contactId }, router }) => {
  const {
    data: contact,
    isLoading,
    isError,
    error,
  } = useGetContactFromIdQuery(contactId as IdContact)
  const emailArgs = useMemo(
    () => ({ contactId: contactId as IdContact }),
    [contactId]
  )
  const {
    items: emailAddresses,
    isComplete: isEmailComplete,
    error: emailError,
    refetch: refetchEmailAddrs,
  } = usePaginatedQuery<
    ContactAttributePaginatedQueryArgs,
    typeof api.endpoints.getContactEmailAddresses
  >(api.endpoints.getContactEmailAddresses, emailArgs, paginationArgs)
  const [updateContact] = useUpdateContactMutation()

  const handleArchive = useCallback(() => {
    alert('not implemented yet')
  }, [])

  const [createEmailAddress] = useCreateContactEmailAddressMutation()
  const [archiveEmailAddress] = useArchiveContactEmailAddressMutation()
  const [updateEmailAddress] = useUpdateContactEmailAddressMutation()

  const handleArchiveEmailAddress = useCallback(
    (emailAddressId: string) => async () => {
      try {
        const result = await archiveEmailAddress({
          emailAddressId: emailAddressId as IdEmailAddress,
          contactId: contactId as IdContact,
          body: {},
        })
        refetchEmailAddrs()
      } catch (err) {
        logError('Error archiving email address')
      }
    },
    [archiveEmailAddress, refetchEmailAddrs, contactId]
  )

  const handleEmailConsentChange = useCallback(
    (emailAddressId: string, property: keyof EmailAddressUpdateRequest) =>
      async (e: React.ChangeEvent<HTMLInputElement>) => {
        try {
          const result = await updateEmailAddress({
            emailAddressId: emailAddressId as IdEmailAddress,
            contactId: contactId as IdContact,
            body: { [property]: e.currentTarget.checked },
          })
          refetchEmailAddrs()
        } catch (err) {
          logError('Error updating email address')
        }
      },
    [contactId, updateEmailAddress, refetchEmailAddrs]
  )

  const {
    items: phoneNumbers,
    isLoading: isPhoneLoading,
    error: phoneError,
    refetch: refetchPhoneNumbers,
  } = usePaginatedQuery<
    ContactAttributePaginatedQueryArgs,
    typeof api.endpoints.getContactPhoneNumbers
  >(
    api.endpoints.getContactPhoneNumbers,
    { contactId: contactId as IdContact },
    {
      fetchAllPages: true,
    }
  )
  const [createPhoneNumber] = useCreateContactPhoneNumberMutation()
  const [archivePhoneNumber] = useArchiveContactPhoneNumberMutation()
  const [updatePhoneNumber] = useUpdateContactPhoneNumberMutation()

  const handleArchivePhoneNumber = useCallback(
    (phoneNumberId: string) => async () => {
      try {
        const result = await archivePhoneNumber({
          phoneNumberId: phoneNumberId as IdPhoneNumber,
          contactId: contactId as IdContact,
          body: {},
        })
        refetchPhoneNumbers()
      } catch (err) {
        logError('Error archiving phone number')
      }
    },
    [archivePhoneNumber, contactId, refetchPhoneNumbers]
  )

  const handlePhoneConsentChange = useCallback(
    (phoneNumberId: string, property: keyof PhoneNumberUpdateRequest) =>
      async (e: React.ChangeEvent<HTMLInputElement>) => {
        try {
          await updatePhoneNumber({
            phoneNumberId: phoneNumberId as IdPhoneNumber,
            contactId: contactId as IdContact,
            body: { [property]: e.currentTarget.checked },
          })
          refetchPhoneNumbers()
        } catch (err) {
          console.error(err)
          logError('Error updating phone number')
        }
      },
    [contactId, updatePhoneNumber, refetchPhoneNumbers]
  )

  return (
    <ContentStack data={contact} isLoading={isLoading} error={error}>
      {(data) => (
        <>
          <Helmet>
            <title>{data.full_name}</title>
          </Helmet>
          <Row>
            <Col>
              <Breadcrumb>
                <LinkContainer to={'/admin/contacts'}>
                  <Breadcrumb.Item>Contacts</Breadcrumb.Item>
                </LinkContainer>
                <Breadcrumb.Item active>{data.full_name}</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>
          <Row>
            <Col md={true}>
              <PageHeading className="mt-0" level={2}>
                {data.full_name}
              </PageHeading>
            </Col>
            <Col md="auto">
              <span className="text-muted">
                <code>{data.id}</code>
              </span>
            </Col>
          </Row>
          <Row>
            <Col sm={12} xl={8}>
              <Formik
                initialValues={omitBy(
                  pick(data, Object.keys(ZdContactUpdateRequest.shape)),
                  isNil
                )}
                validationSchema={toFormikValidationSchema(
                  ZdContactUpdateRequest
                )}
                onSubmit={async (
                  values,
                  { setSubmitting, setErrors, setStatus }
                ) => {
                  try {
                    const results = await updateContact({
                      id: data.id,
                      body: values,
                    }).unwrap()
                    setSubmitting(false)
                    setErrors({})
                    setStatus({})
                    showNotification(
                      'Changes saved',
                      'Your changes to this contact have been saved.'
                    )
                  } catch (err) {
                    setSubmitting(false)
                    const { status, errors } = transformApiErrorsToFormik(
                      err as { status: number; data: ApiError }
                    )
                    setStatus(status)
                    if (errors != null) {
                      setErrors(errors)
                    }
                  }
                }}
              >
                {(params) => <ContactUpdateForm {...params} />}
              </Formik>
            </Col>
          </Row>
          <Row>
            <Col sm={12} xl={8}>
              <PageHeading level={3} className="h4 pb-2">
                Contact info
              </PageHeading>

              {/* List of editable email addresses */}
              <section>
                <PageHeading level={4} className="h5 pb-2">
                  Email addresses
                </PageHeading>
                <Row>
                  <Col sm={3}>
                    <span>Current email addresses</span>
                  </Col>
                  <Col sm={9}>
                    <ListGroup>
                      <ContentStack
                        data={emailAddresses}
                        isLoading={!isEmailComplete}
                        error={emailError}
                      >
                        {(data) => (
                          <>
                            {data.map((email) => (
                              <ListGroup.Item key={email.id}>
                                <div className="d-flex justify-content-between align-items-center">
                                  <span
                                    className={cx({
                                      'font-weight-semibold': email.primary,
                                    })}
                                  >
                                    {email.value}
                                    {email.primary && (
                                      <Badge
                                        pill
                                        variant="primary"
                                        className="ml-2"
                                      >
                                        Primary
                                      </Badge>
                                    )}
                                  </span>
                                  <div>
                                    {!email.primary && (
                                      <Button
                                        variant="danger"
                                        onClick={handleArchiveEmailAddress(
                                          email.id
                                        )}
                                      >
                                        <BsArchive />
                                      </Button>
                                    )}
                                  </div>
                                </div>
                                <div className="mt-2">
                                  <span className="font-weight-semibold">
                                    Consent
                                  </span>
                                  <Form.Switch
                                    id={`${email.id}-consent-email-switch`}
                                    checked={email.consent_to_email}
                                    onChange={handleEmailConsentChange(
                                      email.id,
                                      'consent_to_email'
                                    )}
                                    label={`Receive emails from ${c(
                                      'CHAPTER_NAME_SHORT'
                                    )}`}
                                  />
                                  <Form.Switch
                                    id={`${email.id}-consent-share-switch`}
                                    checked={email.consent_to_share}
                                    onChange={handleEmailConsentChange(
                                      email.id,
                                      'consent_to_share'
                                    )}
                                    label={`Share email address outside of ${c(
                                      'CHAPTER_NAME_SHORT'
                                    )}`}
                                  />
                                </div>
                              </ListGroup.Item>
                            ))}
                          </>
                        )}
                      </ContentStack>
                    </ListGroup>
                  </Col>
                </Row>

                <Formik
                  initialValues={EmailAddressCreateRequestDefaults}
                  validationSchema={toFormikValidationSchema(
                    ZdEmailAddressCreateRequest
                  )}
                  onSubmit={async (
                    values,
                    { setSubmitting, setErrors, setStatus, resetForm }
                  ) => {
                    try {
                      const results = await createEmailAddress({
                        id: contactId as IdContact,
                        body: values,
                      }).unwrap()
                      setSubmitting(false)
                      setErrors({})
                      setStatus({})
                      resetForm()
                      showNotification(
                        'Email added',
                        `'${values.value}' added to contact '${data.full_name}'`
                      )
                      refetchEmailAddrs()
                    } catch (err) {
                      setSubmitting(false)
                      const { status, errors } = transformApiErrorsToFormik(
                        err as { status: number; data: ApiError }
                      )
                      setStatus(status)
                      if (errors != null) {
                        setErrors(errors)
                      }
                    }
                  }}
                >
                  {({
                    values,
                    errors,
                    touched,
                    status,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                  }) => (
                    <Form onSubmit={handleSubmit} className="mt-4">
                      <Row>
                        <Form.Label column sm="3">
                          Add an email address
                        </Form.Label>
                        <Col sm="9">
                          <Form.Label htmlFor="newEmail-value" srOnly>
                            Email address
                          </Form.Label>
                          <Form.Control
                            type="text"
                            placeholder="email@example.com"
                            id="newEmail-value"
                            name="value"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.value}
                          />
                          <ErrorMessage
                            name="value"
                            component={Form.Text}
                            className="text-danger pb-2"
                          />
                          <Form.Check
                            className="mt-2"
                            name="primary"
                            type="checkbox"
                            id="newEmail-primary"
                            label="Set as primary email"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            checked={values.primary}
                          />
                          <Form.Check
                            className="mt-2"
                            name="consent_to_email"
                            type="checkbox"
                            id="newEmail-consent_to_email"
                            label="Opt into email communications"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            checked={values.consent_to_email}
                          />
                          <Form.Check
                            className="mt-2"
                            name="consent_to_share"
                            type="checkbox"
                            id="newEmail-consent_to_share"
                            label="Opt into sharing with other orgs"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            checked={values.consent_to_share}
                          />

                          {status?.formError != null && (
                            <Alert className="mt-2" variant="danger">
                              {status.formError.message}
                              <DeveloperInfo
                                data={status.formError.innerError}
                              />
                            </Alert>
                          )}

                          <LoadingButton
                            className="mt-2"
                            type="submit"
                            isLoading={isSubmitting}
                          >
                            Add email
                          </LoadingButton>
                          <DeveloperInfo data={values} />
                        </Col>
                      </Row>
                    </Form>
                  )}
                </Formik>
              </section>

              {/* List of editable phone numbers */}
              <section>
                <PageHeading level={4} className="h5 pb-2">
                  Phone numbers
                </PageHeading>
                <Row>
                  <Col sm={3}>
                    <span>Current phone numbers</span>
                  </Col>
                  <Col sm={9}>
                    <ListGroup>
                      <ContentStack
                        data={phoneNumbers}
                        isLoading={isPhoneLoading}
                        error={phoneError}
                      >
                        {(data) => (
                          <>
                            {data.map((phone) => (
                              <ListGroup.Item key={phone.id}>
                                <div className="d-flex justify-content-between align-items-center">
                                  <span
                                    className={cx({
                                      'font-weight-semibold': phone.primary,
                                    })}
                                  >
                                    {phone.value}
                                    {phone.primary && (
                                      <Badge
                                        pill
                                        variant="primary"
                                        className="ml-2"
                                      >
                                        Primary
                                      </Badge>
                                    )}
                                  </span>
                                  <div>
                                    {!phone.primary && (
                                      <Button
                                        variant="danger"
                                        onClick={handleArchivePhoneNumber(
                                          phone.id
                                        )}
                                      >
                                        <BsArchive />
                                      </Button>
                                    )}
                                  </div>
                                </div>
                                <div className="mt-2">
                                  <span className="font-weight-semibold">
                                    Info
                                  </span>
                                  <Form.Switch
                                    id={`${phone.id}-capability-sms-switch`}
                                    checked={phone.sms_capable}
                                    onChange={handlePhoneConsentChange(
                                      phone.id,
                                      'sms_capable'
                                    )}
                                    label="This number can receive texts"
                                  />
                                </div>
                                <div className="mt-2">
                                  <span className="font-weight-semibold">
                                    Consent
                                  </span>
                                  <Form.Switch
                                    id={`${phone.id}-consent-call-switch`}
                                    checked={phone.consent_to_call}
                                    onChange={handlePhoneConsentChange(
                                      phone.id,
                                      'consent_to_call'
                                    )}
                                    label={`Receive phone calls from ${c(
                                      'CHAPTER_NAME_SHORT'
                                    )}`}
                                  />
                                  <Form.Switch
                                    id={`${phone.id}-consent-text-switch`}
                                    checked={phone.consent_to_call}
                                    onChange={handlePhoneConsentChange(
                                      phone.id,
                                      'consent_to_call'
                                    )}
                                    label={`Receive texts from ${c(
                                      'CHAPTER_NAME_SHORT'
                                    )}`}
                                  />
                                  <Form.Switch
                                    id={`${phone.id}-consent-share-switch`}
                                    checked={phone.consent_to_share}
                                    onChange={handlePhoneConsentChange(
                                      phone.id,
                                      'consent_to_share'
                                    )}
                                    label={`Share phone number outside of ${c(
                                      'CHAPTER_NAME_SHORT'
                                    )}`}
                                  />
                                </div>
                              </ListGroup.Item>
                            ))}
                          </>
                        )}
                      </ContentStack>
                    </ListGroup>
                  </Col>
                </Row>

                <Formik
                  initialValues={PhoneNumberCreateRequestDefaults}
                  validationSchema={toFormikValidationSchema(
                    ZdPhoneNumberCreateRequest
                  )}
                  onSubmit={async (
                    values,
                    { setSubmitting, setErrors, setStatus, resetForm }
                  ) => {
                    try {
                      const results = await createPhoneNumber({
                        id: contactId as IdContact,
                        body: values,
                      }).unwrap()
                      setSubmitting(false)
                      setErrors({})
                      setStatus({})
                      resetForm()
                      showNotification(
                        'Phone number added',
                        "The phone number has been added to the contact. TODO the list won't change until you refresh the page"
                      )
                      refetchPhoneNumbers()
                    } catch (err) {
                      setSubmitting(false)
                      const { status, errors } = transformApiErrorsToFormik(
                        err as { status: number; data: ApiError }
                      )
                      setStatus(status)
                      if (errors != null) {
                        setErrors(errors)
                      }
                    }
                  }}
                >
                  {({
                    values,
                    errors,
                    touched,
                    status,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                  }) => (
                    <Form onSubmit={handleSubmit} className="mt-4">
                      <Row>
                        <Col sm={3}>
                          <PageHeading level={5} className="h6 my-0 py-0">
                            Add a phone number
                          </PageHeading>
                        </Col>
                        <Col sm={9}>
                          <Form.Group controlId="name">
                            <Form.Label className="font-weight-medium">
                              Label
                            </Form.Label>
                            <Form.Control
                              type="text"
                              placeholder="Cell"
                              id="newPhone-name"
                              name="name"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.name}
                            />
                            <ErrorMessage
                              name="name"
                              component={Form.Text}
                              className="text-danger pb-2"
                            />
                          </Form.Group>
                        </Col>
                      </Row>
                      <Form.Group as={Row} controlId="value">
                        <Col sm={3}></Col>
                        <Col sm={9}>
                          <Form.Label className="font-weight-medium">
                            Phone number (required)
                          </Form.Label>
                          <Form.Control
                            type="text"
                            placeholder="415-555-1234"
                            name="value"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.value}
                          />
                          <ErrorMessage
                            name="value"
                            component={Form.Text}
                            className="text-danger pb-2"
                          />
                        </Col>
                      </Form.Group>
                      <Row>
                        <Col md={3}></Col>
                        <Col md={9}>
                          <Form.Label className="font-weight-medium mb-0">
                            Options
                          </Form.Label>
                          <Form.Check
                            className="mt-2"
                            name="primary"
                            type="checkbox"
                            id="newPhone-primary"
                            label="Set as primary phone number"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            checked={values.primary}
                          />
                          <Form.Check
                            className="mt-2"
                            name="consent_to_call"
                            type="checkbox"
                            id="newPhone-consent_to_call"
                            label={`Opt into receiving calls from ${c(
                              'CHAPTER_NAME_SHORT'
                            )}`}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            checked={values.consent_to_call}
                          />
                          <Form.Check
                            className="mt-2"
                            name="consent_to_text"
                            type="checkbox"
                            id="newPhone-consent_to_text"
                            label={`Opt into receiving texts from ${c(
                              'CHAPTER_NAME_SHORT'
                            )}`}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            checked={values.consent_to_text}
                          />
                          <Form.Check
                            className="mt-2"
                            name="consent_to_share"
                            type="checkbox"
                            id="newPhone-consent_to_share"
                            label="Opt into sharing with other orgs"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            checked={values.consent_to_share}
                          />
                        </Col>
                      </Row>

                      <Row className="mt-2">
                        <Col md={3} />
                        <Col md={9}>
                          {status?.formError != null && (
                            <Alert className="mt-2" variant="danger">
                              {status.formError.message}
                              <DeveloperInfo
                                data={status.formError.innerError}
                              />
                            </Alert>
                          )}

                          <LoadingButton
                            className="mt-2"
                            type="submit"
                            isLoading={isSubmitting}
                          >
                            Add phone number
                          </LoadingButton>
                          <DeveloperInfo data={values} />
                        </Col>
                      </Row>
                    </Form>
                  )}
                </Formik>
              </section>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col sm={12} xl={8}>
              <ContactArchiveAction contact={data} onArchive={handleArchive} />
            </Col>
          </Row>
        </>
      )}
    </ContentStack>
  )
}

export default ContactFromId
