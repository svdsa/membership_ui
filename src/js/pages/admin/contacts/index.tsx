import { skipToken } from '@reduxjs/toolkit/dist/query'
import { Formik } from 'formik'
import React, { useState } from 'react'
import { Button, Col, Form, InputGroup, Row, Spinner } from 'react-bootstrap'
import Helmet from 'react-helmet'
import { FiSearch } from 'react-icons/fi'
import { Link } from 'react-router'
import { api } from 'src/js/api'
import {
  SearchContactsQuery,
  SearchContactsQueryDefaults,
} from 'src/js/api/schemas'
import {
  ContactTable,
  EmptyContactTable,
  ErrorContactTable,
  LoadingContactTable,
} from 'src/js/components/admin/ContactTable'
import ContentStack from 'src/js/components/common/ContentStack'
import { ErrorBlock } from 'src/js/components/common/ErrorBlock'
import { LoadingButton } from 'src/js/components/common/LoadingButton'
import PageHeading from 'src/js/components/common/PageHeading'
import { PaginationBar } from 'src/js/components/common/PaginationBar'
import { usePaginatedCsvExport } from 'src/js/hooks/usePagedCsvExport'
import { usePagedData } from 'src/js/hooks/usePagedData'
import { usePaginatedQuery } from 'src/js/hooks/usePaginatedQuery'

interface ContactListProps {
  placeholder?: string
}

export const ContactList: React.FC<ContactListProps> = ({
  placeholder = 'Search contacts...',
}) => {
  const [query, setQuery] = useState('')

  const listQuery = usePaginatedQuery(api.endpoints.getContactList, undefined, {
    fetchAllPages: true,
    limit: 25,
  })
  const listQueryPages = usePagedData(listQuery)

  const searchQuery = usePaginatedQuery<
    SearchContactsQuery,
    typeof api.endpoints.searchContacts
  >(api.endpoints.searchContacts, query.length > 0 ? { q: query } : skipToken, {
    fetchAllPages: true,
    limit: 25,
  })
  const searchQueryPages = usePagedData(searchQuery)

  const activeQuery = query.length > 0 ? searchQuery : listQuery
  const activeQueryPages = query.length > 0 ? searchQueryPages : listQueryPages

  const { pages, error, isLoading, isFetching, hasMore } = activeQuery
  const { page, pageIndex, isLastPage, nextPage, previousPage, setPage } =
    activeQueryPages

  const { downloadCsvFromPaginatedQuery } = usePaginatedCsvExport()

  const handleClickExportCsv = () => {
    downloadCsvFromPaginatedQuery(activeQuery)
  }

  return (
    <>
      <header className="mb-2">
        <Row>
          <Col>
            <Formik
              initialValues={SearchContactsQueryDefaults}
              onSubmit={async (values, { setSubmitting }) => {
                setQuery(values.q)
                setSubmitting(false)
              }}
            >
              {({
                values,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
              }) => (
                <Form onSubmit={handleSubmit}>
                  <InputGroup>
                    <Form.Control
                      type="text"
                      name="q"
                      value={values.q}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder={placeholder}
                    />
                    <InputGroup.Append>
                      <Button type="submit" variant="outline-primary">
                        {isSubmitting ? (
                          <Spinner animation="border" size="sm" />
                        ) : (
                          <FiSearch />
                        )}
                      </Button>
                    </InputGroup.Append>
                  </InputGroup>
                </Form>
              )}
            </Formik>
          </Col>
          <Col lg="auto" className="d-flex flex-row justify-content-around">
            <LoadingButton
              variant="outline-primary"
              className="mr-2"
              isLoading={isLoading}
              onClick={handleClickExportCsv}
            >
              Export as CSV...
            </LoadingButton>
            <Link to={`/admin/contacts/new`}>
              <Button variant="success">Add contact</Button>
            </Link>
          </Col>
        </Row>
      </header>
      {error && (
        <ErrorBlock
          title="Search error"
          message={<p>There was an issue searching for contacts.</p>}
          error={error}
        />
      )}
      <ContentStack
        data={page}
        error={error}
        isLoading={isFetching}
        emptyComponent={EmptyContactTable}
        loadingComponent={LoadingContactTable}
        errorComponent={ErrorContactTable}
      >
        {(page) => (
          <>
            <ContactTable contacts={page} />
            {isLastPage && (
              <>
                <p className="text-center text-muted">
                  <em>That's all, folks</em>
                  <br />
                  <small>(no more contacts)</small>
                </p>
              </>
            )}
            {pages != null && (
              <div className="d-flex flex-column align-items-center">
                <PaginationBar
                  numPages={pages.length}
                  pageIndex={pageIndex}
                  hasMore={hasMore}
                  isFetchingPages={isFetching}
                  onSetPageIndex={setPage}
                  onNextPage={nextPage}
                  onPreviousPage={previousPage}
                />
              </div>
            )}
          </>
        )}
      </ContentStack>
    </>
  )
}

const Contacts: React.FC = () => {
  return (
    <>
      <Helmet>
        <title>Contacts</title>
      </Helmet>
      <Row>
        <Col>
          <PageHeading className="mt-0" level={2}>
            Contacts
          </PageHeading>
        </Col>
      </Row>
      <ContactList />
    </>
  )
}

export default Contacts
