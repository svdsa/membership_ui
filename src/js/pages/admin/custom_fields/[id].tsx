import React, { useEffect, useState } from 'react'
import { Alert, Button, Col, Row } from 'react-bootstrap'
import Helmet from 'react-helmet'
import { RouteComponentProps } from 'react-router'
import {
  useDeleteCustomFieldMutation,
  useGetCustomFieldFromIdQuery,
} from 'src/js/api'
import { IdCustomField } from 'src/js/api/schemas'
import ContentStack from 'src/js/components/common/ContentStack'
import PageHeading from 'src/js/components/common/PageHeading'
import TypeToConfirmDialog from 'src/js/components/common/TypeToConfirmDialog'
import { showNotification } from 'src/js/util/util'

const CustomFieldFromId: React.FC<
  RouteComponentProps<{ customFieldId: string }, {}>
> = ({ params: { customFieldId }, router }) => {
  const { data, isLoading, isError, error } = useGetCustomFieldFromIdQuery(
    customFieldId as IdCustomField
  )
  const [
    deleteCustomField,
    {
      isLoading: isDeleting,
      isError: deleteIsError,
      error: deleteError,
      isSuccess: deleteIsSuccess,
    },
  ] = useDeleteCustomFieldMutation()
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false)

  useEffect(() => {
    if (deleteIsError) {
      setShowDeleteConfirm(false)
    }

    if (deleteIsSuccess) {
      showNotification(
        'Delete successful',
        data != null
          ? `The custom field "${data.name}" was deleted successfully. 🥳`
          : 'The custom field was deleted successfully.'
      )
      router.push('/admin/custom-fields')
    }
  }, [
    deleteIsError,
    deleteIsSuccess,
    setShowDeleteConfirm,
    data,
    router,
    showNotification,
  ])

  const handleConfirmDelete: React.MouseEventHandler<HTMLButtonElement> = (e) =>
    setShowDeleteConfirm(true)

  return (
    <ContentStack data={data} isLoading={isLoading} error={error}>
      {(data) => (
        <>
          <Helmet>
            <title>{data.name}</title>
          </Helmet>
          <Row>
            <Col>
              <PageHeading className="mt-0" level={2}>
                Custom field: "{data.name}"
              </PageHeading>
              <span className="text-muted">
                ID: <code>{data.id}</code>
              </span>
            </Col>
          </Row>
          <pre>{JSON.stringify(data, null, 2)}</pre>
          <Button variant="danger" onClick={handleConfirmDelete}>
            Delete
          </Button>
          {deleteError && (
            <Alert variant="danger">
              <Alert.Heading>Error when deleting custom field</Alert.Heading>
              <pre>{JSON.stringify(deleteError, null, 2)}</pre>
            </Alert>
          )}
          <TypeToConfirmDialog
            onHide={() => setShowDeleteConfirm(false)}
            show={showDeleteConfirm}
            title={`Really delete "${data.name}"?`}
            confirmHintText={
              <span>
                To confirm deletion, please type <strong>{data.name}</strong>{' '}
                into the box below.
              </span>
            }
            isLoading={isDeleting}
            typeToConfirmText={data.name}
            actionText={`Yes, I want to delete "${data.name}"`}
            onConfirm={() => {
              deleteCustomField(customFieldId as IdCustomField)
            }}
          >
            <p>You are about to delete the "{data.name}" custom field!</p>
            <p>
              There are XXXXX contacts with values assigned to this custom
              field.
            </p>
            <p>
              <strong>
                If you proceed with this deletion, recovery may be impossible!
              </strong>
            </p>
          </TypeToConfirmDialog>
        </>
      )}
    </ContentStack>
  )
}

export default CustomFieldFromId
