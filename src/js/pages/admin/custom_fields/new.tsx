import { FieldArray, Formik, FormikErrors, FormikHelpers } from 'formik'
import { snakeCase } from 'lodash/fp'
import React, { useState } from 'react'
import { Alert, Button, Col, Form, Row, Spinner } from 'react-bootstrap'
import { Typeahead } from 'react-bootstrap-typeahead'
import Helmet from 'react-helmet'
import { RouteComponentProps } from 'react-router'
import { useCreateCustomFieldMutation } from 'src/js/api'
import {
  CustomFieldChoiceOptions,
  CustomFieldChoiceOptionsDefaults,
  CustomFieldCreateRequest,
  CustomFieldCreateRequestDefaults,
  CustomFieldRangeOptions,
  CustomFieldRangeOptionsDefaults,
  CustomFieldTypeNames,
  CustomFieldTypes,
  ZdCustomFieldCreateRequest,
} from 'src/js/api/schemas'
import PageHeading from 'src/js/components/common/PageHeading'
import { toFormikValidationSchema } from 'src/js/util/zod-formik-adapter'

interface CustomFieldChoiceTypeaheadProps {
  name: string
  options: CustomFieldChoiceOptions
  setFieldValue: FormikHelpers<CustomFieldCreateRequest>['setFieldValue']
  handleBlur: React.EventHandler<any>
}

const CustomFieldChoiceTypeahead: React.FC<CustomFieldChoiceTypeaheadProps> = ({
  setFieldValue,
  handleBlur,
  options,
  name,
}) => {
  const handleTypeaheadChange = (
    selection: {
      name?: string | null | undefined
      value: string
    }[]
  ) => {
    setFieldValue(
      name,
      selection.map((s) => ({
        name: s.name,
        value: snakeCase(s.name || s.value),
      }))
    )
  }

  return (
    <Typeahead
      id="custom-field-choices"
      allowNew
      selectHintOnEnter
      multiple
      clearButton
      newSelectionPrefix="Add choice: "
      placeholder="Enter some choices..."
      emptyLabel="Enter some choices..."
      labelKey={(c) => c.name || c.value}
      onBlur={handleBlur}
      onChange={handleTypeaheadChange}
      selected={options.choices}
      options={[]}
    />
  )
}

interface CustomFieldOptionsCache {
  choice: CustomFieldChoiceOptions | null
  range: CustomFieldRangeOptions | null
}

const NewCustomField: React.FC<
  RouteComponentProps<Record<string, never>, Record<string, never>>
> = ({ router }) => {
  const [submitForm, { isLoading: isUpdating, isError, error }] =
    useCreateCustomFieldMutation()
  const [optionsCache, setOptionsCache] = useState<CustomFieldOptionsCache>({
    choice: null,
    range: null,
  })

  return (
    <>
      <Helmet>
        <title>New custom field</title>
      </Helmet>
      <Row>
        <Col>
          <PageHeading className="mt-0" level={2}>
            Add a new custom field
          </PageHeading>
        </Col>
      </Row>
      <Row>
        <Col sm={12} xl={8}>
          <Formik
            initialValues={CustomFieldCreateRequestDefaults}
            validationSchema={toFormikValidationSchema(
              ZdCustomFieldCreateRequest
            )}
            onSubmit={async (values, { setSubmitting }) => {
              try {
                const results = await submitForm(values).unwrap()
                setSubmitting(false)
                router.push(`/admin/custom-fields/${results.id}`)
              } catch (err) {
                setSubmitting(false)
              }
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              setFieldValue,
              isSubmitting,
            }) => (
              <Form onSubmit={handleSubmit}>
                <section className="pb-2 mb-2 border-bottom">
                  <Form.Group as={Row} controlId="name">
                    <Col sm={3} className="pt-2">
                      <Form.Label className="font-weight-medium">
                        Name
                      </Form.Label>
                      <Form.Text className="text-muted pb-2">
                        Briefly identify your custom field. This will be shown
                        publicly.
                      </Form.Text>
                    </Col>
                    <Col sm={9}>
                      <Form.Control
                        placeholder="for example: Shirt size"
                        value={values.name}
                        onChange={handleChange}
                        name="name"
                        isValid={touched.name && !errors.name}
                      />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="description">
                    <Col sm={3} className="pt-2">
                      <Form.Label className="font-weight-medium">
                        Description
                      </Form.Label>
                      <Form.Text className="text-muted pb-2">
                        Describe what your custom field is, and what it may be
                        used for (like the text you're reading). This will be
                        shown publicly.
                      </Form.Text>
                    </Col>
                    <Col sm={9}>
                      <Form.Control
                        as="textarea"
                        placeholder="for example: Used to keep track of swag ordering for campaigns or general chapter merchandise."
                        name="description"
                        value={values.description}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        isValid={touched.description && !errors.description}
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} controlId="field_type">
                    <Col sm={3} className="pt-2">
                      <Form.Label className="font-weight-medium">
                        Type of value
                      </Form.Label>
                      <Form.Text className="text-muted pb-2">
                        The kinds of values your custom field will hold (e.g.
                        some text, a number, a checkbox)
                      </Form.Text>
                    </Col>
                    <Col sm={9}>
                      <Form.Control
                        as="select"
                        name="field_type"
                        value={values.field_type}
                        onChange={(e) => {
                          // Save existing options if transitioning from a optionable value
                          if (values.field_type === 'range') {
                            setOptionsCache({
                              ...optionsCache,
                              range: values.options as CustomFieldRangeOptions,
                            })
                          } else if (values.field_type === 'choice') {
                            setOptionsCache({
                              ...optionsCache,
                              choice:
                                values.options as CustomFieldChoiceOptions,
                            })
                          }

                          // Load existing options or set defaults if transitioning to an optionable value
                          if (e.currentTarget.value === 'range') {
                            setFieldValue(
                              'options',
                              optionsCache.range ||
                                CustomFieldRangeOptionsDefaults
                            )
                          } else if (e.currentTarget.value === 'choice') {
                            setFieldValue(
                              'options',
                              optionsCache.choice ||
                                CustomFieldChoiceOptionsDefaults
                            )
                          }

                          handleChange(e)
                        }}
                        onBlur={handleBlur}
                        isValid={touched.field_type && !errors.field_type}
                      >
                        {CustomFieldTypes.map((v) => (
                          <option key={v} value={v}>
                            {CustomFieldTypeNames[v]}
                          </option>
                        ))}
                      </Form.Control>
                    </Col>
                  </Form.Group>
                  {values.field_type === 'choice' && (
                    <Form.Group as={Row} controlId="options">
                      <Col sm={3} className="pt-2">
                        <Form.Label className="font-weight-medium">
                          Choices
                        </Form.Label>
                        <Form.Text className="text-muted pb-2">
                          The choices offered by the custom field.
                        </Form.Text>
                      </Col>
                      <Col sm={9}>
                        <FieldArray name="options.choices">
                          {(arrayHelpers) => (
                            <div>
                              {touched.options &&
                              errors.options != null &&
                              typeof errors.options === 'string' ? (
                                <div>{errors.options}</div>
                              ) : null}
                              {values.options != null && (
                                <CustomFieldChoiceTypeahead
                                  handleBlur={handleBlur}
                                  setFieldValue={setFieldValue}
                                  name="options.choices"
                                  options={
                                    values.options as CustomFieldChoiceOptions
                                  }
                                />
                              )}
                            </div>
                          )}
                        </FieldArray>
                      </Col>
                    </Form.Group>
                  )}
                  {values.field_type === 'range' && (
                    <>
                      <Form.Group as={Row} controlId="range-options-min">
                        <Col sm={3} className="pt-2">
                          <Form.Label className="font-weight-medium">
                            Minimum value
                          </Form.Label>
                          <Form.Text className="text-muted pb-2">
                            The lowest value acceptable by the range.
                          </Form.Text>
                        </Col>
                        <Col sm={9}>
                          <Form.Control
                            type="number"
                            placeholder="0"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="options.start"
                            value={
                              values.options != null
                                ? (values.options as CustomFieldRangeOptions)
                                    .start
                                : 0
                            }
                            isValid={
                              touched.options &&
                              (
                                errors.options as FormikErrors<CustomFieldRangeOptions>
                              )?.start != null
                            }
                          />
                        </Col>
                      </Form.Group>
                      <Form.Group as={Row} controlId="range-options-max">
                        <Col sm={3} className="pt-2">
                          <Form.Label className="font-weight-medium">
                            Maximum value
                          </Form.Label>
                          <Form.Text className="text-muted pb-2">
                            The highest value acceptable by the range.
                          </Form.Text>
                        </Col>
                        <Col sm={9}>
                          <Form.Control
                            type="number"
                            placeholder="0"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="options.end"
                            value={
                              values.options != null
                                ? (values.options as CustomFieldRangeOptions)
                                    .end
                                : 0
                            }
                            isValid={
                              touched.options &&
                              (
                                errors.options as FormikErrors<CustomFieldRangeOptions>
                              )?.end != null
                            }
                          />
                        </Col>
                      </Form.Group>
                      <Form.Group as={Row} controlId="range-options-step">
                        <Col sm={3} className="pt-2">
                          <Form.Label className="font-weight-medium">
                            Step size (optional)
                          </Form.Label>
                          <Form.Text className="text-muted pb-2">
                            The distance between valid values (e.g. a step size
                            of 2 would only allow every other number between min
                            and max)
                          </Form.Text>
                        </Col>
                        <Col sm={9}>
                          <Form.Control
                            type="number"
                            placeholder="0"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="options.step"
                            value={
                              values.options != null
                                ? (values.options as CustomFieldRangeOptions)
                                    .step || 1
                                : 1
                            }
                            isValid={
                              touched.options &&
                              (
                                errors.options as FormikErrors<CustomFieldRangeOptions>
                              )?.step != null
                            }
                          />
                        </Col>
                      </Form.Group>
                    </>
                  )}
                </section>
                <section className="py-2 mb-2">
                  <Row>
                    <Col sm={3}></Col>
                    <Col sm={9}>
                      <pre>{JSON.stringify(values, null, 2)}</pre>
                      <Button type="submit" variant="success">
                        {isSubmitting ? (
                          <Spinner animation="border" size="sm" />
                        ) : (
                          'Create'
                        )}
                      </Button>
                    </Col>
                  </Row>
                </section>
              </Form>
            )}
          </Formik>
          {isError && (
            <Alert variant="danger">
              <Alert.Heading>Error when creating custom field</Alert.Heading>
              <pre>{JSON.stringify(error, null, 2)}</pre>
            </Alert>
          )}
        </Col>
      </Row>
    </>
  )
}

interface CustomFieldOptionsControlProps {
  value: CustomFieldChoiceOptions['choices']
  onChange(e: React.ChangeEvent<any>): void
  onBlur(e: React.FocusEvent<any>): void
  name: string
  isValid: boolean
}

export default NewCustomField
