import * as React from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import Helmet from 'react-helmet'
import { Link } from 'react-router'
import { useGetCustomFieldsListQuery } from 'src/js/api'
import ContentStack from 'src/js/components/common/ContentStack'
import PageHeading from 'src/js/components/common/PageHeading'

const CustomFields: React.FC = () => {
  const { data, isLoading, isError, error } = useGetCustomFieldsListQuery()

  return (
    <>
      <Helmet>
        <title>Custom fields</title>
      </Helmet>
      <Row>
        <Col>
          <PageHeading className="mt-0" level={2}>
            Custom fields
          </PageHeading>
        </Col>
      </Row>
      <Link to="/admin/custom-fields/new">
        <Button variant="success">New custom field</Button>
      </Link>
      <ContentStack data={data} isLoading={isLoading} error={error}>
        {(data) => <pre>{JSON.stringify(data, null, 2)}</pre>}
      </ContentStack>
    </>
  )
}

export default CustomFields
