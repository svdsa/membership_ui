import React, { FC } from 'react'
import { Col, ListGroup, Row } from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { api } from 'src/js/api'
import ImportEventEntry from 'src/js/components/admin/import_review/ImportEventEntry'
import ContentStack from 'src/js/components/common/ContentStack'
import PageHeading from 'src/js/components/common/PageHeading'
import { PaginationBar } from 'src/js/components/common/PaginationBar'
import { usePagedData } from 'src/js/hooks/usePagedData'
import { usePaginatedQuery } from 'src/js/hooks/usePaginatedQuery'

const ImportEvents: FC = () => {
  const paginatedResults = usePaginatedQuery(api.endpoints.getImportsList)
  const {
    pages,
    isLoading: loadingFirstPage,
    isFetching,
    error: importsError,
    hasMore,
  } = paginatedResults
  const { page, pageIndex, setPage, nextPage, previousPage } = usePagedData({
    ...paginatedResults,
  })

  return (
    <>
      <Helmet>
        <title>Imports</title>
      </Helmet>
      <Row>
        <Col>
          <PageHeading className="mt-0" level={2}>
            Imports
          </PageHeading>
        </Col>
      </Row>
      <ContentStack
        data={page}
        isLoading={loadingFirstPage}
        error={importsError}
      >
        {(page) => (
          <>
            {page != null && (
              <ListGroup className="mb-2">
                {page.map((event) => (
                  <ImportEventEntry key={event.id} event={event} />
                ))}
              </ListGroup>
            )}
          </>
        )}
      </ContentStack>
      {pages != null && (
        <div className="d-flex flex-column align-items-center">
          <PaginationBar
            numPages={pages.length}
            pageIndex={pageIndex}
            hasMore={hasMore}
            isFetchingPages={isFetching}
            onSetPageIndex={setPage}
            onNextPage={nextPage}
            onPreviousPage={previousPage}
          />
        </div>
      )}
    </>
  )
}

export default ImportEvents
