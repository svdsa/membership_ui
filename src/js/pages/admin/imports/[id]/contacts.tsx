import { useToggle } from '@react-hookz/web'
import { compact, head, keyBy } from 'lodash'
import pluralize from 'pluralize'
import React, { useCallback, useMemo, useState } from 'react'
import { Button, Col, Modal, Row } from 'react-bootstrap'
import { TreeItemIndex } from 'react-complex-tree'
import { GiJuggler } from 'react-icons/gi'
import { RiGhostFill } from 'react-icons/ri'
import { RouteComponentProps } from 'react-router'
import { useGetImportEventFromIdQuery } from 'src/js/api'
import {
  IdImportEvent,
  ImportEventResponse,
  ImportEventReviewAction,
} from 'src/js/api/schemas'
import { ReviewActionDetails } from 'src/js/components/admin/import_review/ReviewActionDetails'
import {
  ReviewEventEmpty,
  ReviewEventError,
} from 'src/js/components/admin/import_review/ReviewSessionContentStack'
import { ReviewSidebarTree } from 'src/js/components/admin/import_review/ReviewSidebarTree'
import ContentStack from 'src/js/components/common/ContentStack'
import { ErrorBlock } from 'src/js/components/common/ErrorBlock'

interface ImportEventContactsProps {
  event: ImportEventResponse
}

const ImportEventContacts: React.FC<ImportEventContactsProps> = ({ event }) => {
  const [chosenActions, setChosenActions] = useState<ImportEventReviewAction[]>(
    compact([head(event.review_actions.data) ?? null])
  )
  const [actionDirty, toggleActionDirty] = useToggle(false)
  const [pendingDirtyConfirmActions, setPendingDirtyConfirmActions] = useState<
    ImportEventReviewAction[] | null
  >(null)
  const [showDirtyConfirmModal, setShowDirtyConfirmModal] = useToggle(false)

  const actionsById = useMemo(
    () => keyBy(event.review_actions.data, (action) => action.id),
    [event]
  )

  const handleChooseTreeItems = useCallback(
    (items: TreeItemIndex[]) => {
      const newActions = compact(items.map((item) => actionsById[item] ?? null))

      if (newActions.length === 0) {
        // Don't change view if we choose a category item
        return
      }

      if (
        chosenActions != null &&
        newActions.length === 1 &&
        chosenActions.length === 1 &&
        newActions[0].id === chosenActions[0].id
      ) {
        // Don't trigger confirm modal if we choose the same action
        return
      }

      if (actionDirty) {
        setShowDirtyConfirmModal(true)
        setPendingDirtyConfirmActions(newActions)
      } else {
        setChosenActions(newActions)
      }
    },
    [actionDirty, chosenActions, actionsById, setShowDirtyConfirmModal]
  )

  const handleConfirmDirtyDiscard = useCallback(() => {
    if (pendingDirtyConfirmActions != null) {
      setChosenActions(pendingDirtyConfirmActions)
    }

    setPendingDirtyConfirmActions(null)
    toggleActionDirty(false)
    setShowDirtyConfirmModal(false)
  }, [pendingDirtyConfirmActions, setShowDirtyConfirmModal, toggleActionDirty])

  const handleCloseDirtyConfirmModal = useCallback(() => {
    setShowDirtyConfirmModal(false)
    setPendingDirtyConfirmActions(null)
  }, [setShowDirtyConfirmModal])

  const handleUpdateAction = useCallback(() => {
    // HACK work around stale data
    // TODO implement queue of review items and move to next one
    setChosenActions([])
  }, [])

  return (
    <>
      <Row>
        <Col lg={3}>
          <ReviewSidebarTree
            actions={event.review_actions.data}
            onChooseTreeItems={handleChooseTreeItems}
            selectedActions={chosenActions}
          />
        </Col>
        <Col lg={9}>
          {chosenActions == null ||
            (chosenActions.length === 0 && (
              <ErrorBlock
                title="No contact selected"
                message={
                  <p>
                    Choose a contact from the sidebar to view and change their
                    import actions.
                  </p>
                }
                icon={<RiGhostFill />}
              />
            ))}
          {chosenActions != null && chosenActions.length > 1 && (
            <ErrorBlock
              title="Multiple contacts selected"
              message={
                <>
                  <p>
                    Choose a single contact from the sidebar to view and change
                    their import actions.
                  </p>
                </>
              }
              icon={<GiJuggler />}
            />
          )}
          {chosenActions != null && chosenActions.length === 1 && (
            <>
              <ReviewActionDetails
                action={chosenActions[0]}
                onDirty={toggleActionDirty}
                onUpdate={handleUpdateAction}
                readOnly={event.date_reviewed != null}
              />
            </>
          )}
        </Col>
      </Row>
      <Modal show={showDirtyConfirmModal} onHide={handleCloseDirtyConfirmModal}>
        <Modal.Header closeButton>
          <Modal.Title>You have unsaved changes</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Choosing a different contact will discard any changes you've made to{' '}
          <strong>
            {chosenActions.length === 1
              ? chosenActions[0].participant.full_name
              : `${chosenActions.length} ${pluralize(
                  'contact',
                  chosenActions.length
                )}}`}
          </strong>
          .
        </Modal.Body>
        <Modal.Footer>
          <div>
            <Button
              variant="danger"
              onClick={handleConfirmDirtyDiscard}
              className="mr-2"
            >
              Yes, discard changes
            </Button>
            <Button variant="secondary" onClick={handleCloseDirtyConfirmModal}>
              Cancel
            </Button>
          </div>
        </Modal.Footer>
      </Modal>
    </>
  )
}

interface CreateReviewSessionParamProps {
  reviewSessionId: string
}

type CreateReviewSessionProps = RouteComponentProps<
  CreateReviewSessionParamProps,
  Record<string, never>
>

const ImportEventContactsPage: React.FC<CreateReviewSessionProps> = ({
  params: { reviewSessionId: importEventId },
}) => {
  const { data, isLoading, error } = useGetImportEventFromIdQuery(
    importEventId as IdImportEvent
  )

  return (
    <ContentStack
      data={data}
      isLoading={isLoading}
      error={error}
      errorComponent={ReviewEventError}
      emptyComponent={ReviewEventEmpty}
    >
      {(event) => <ImportEventContacts event={event} />}
    </ContentStack>
  )
}

export default ImportEventContactsPage
