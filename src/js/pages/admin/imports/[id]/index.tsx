import { last, sum } from 'lodash'
import pluralize from 'pluralize'
import React from 'react'
import { Badge, Nav } from 'react-bootstrap'
import Helmet from 'react-helmet'
import { RouteComponentProps } from 'react-router'
import { IndexLinkContainer, LinkContainer } from 'react-router-bootstrap'
import {
  useGetImportEventFromIdQuery,
  useUpdateReviewActionsMutation,
} from 'src/js/api'
import { IdImportEvent } from 'src/js/api/schemas'
import ImportEventStateBadge from 'src/js/components/admin/import_review/ImportEventStateBadge'
import { ImportEventTypeSpan } from 'src/js/components/admin/import_review/ImportEventTypeSpan'
import ContentStack from 'src/js/components/common/ContentStack'
import PageHeading from 'src/js/components/common/PageHeading'
import { SecurityPrincipalSpanFromId } from 'src/js/components/common/SecurityPrincipalSpan'
import { TimeAgoSpan } from 'src/js/components/common/TimeAgoSpan'

interface ReviewSessionParamProps {
  reviewSessionId: IdImportEvent
}

type ReviewSessionProps = RouteComponentProps<
  ReviewSessionParamProps,
  Record<string, never>
>

const ImportEventPage: React.FC<ReviewSessionProps> = ({
  params: { reviewSessionId },
  location,
  children,
}) => {
  const {
    data: importEvent,
    isLoading,
    error: reviewError,
  } = useGetImportEventFromIdQuery(reviewSessionId)
  const [
    DEBUG_uploadReview,
    {
      isSuccess: DEBUG_uploadSuccess,
      isLoading: DEBUG_uploadingReview,
      isError: DEBUG_uploadReviewError,
    },
  ] = useUpdateReviewActionsMutation()

  return (
    <ContentStack data={importEvent} error={reviewError} isLoading={isLoading}>
      {(event) => {
        const { title, creator, date_reviewed } = event
        const totalActions = sum(Object.values(event.review_action_counts))

        return (
          <>
            <Helmet>{title} &sdot; Import events</Helmet>
            <header>
              <PageHeading level={2} className="m-0 mb-2">
                {title}
              </PageHeading>
              <div className="display-flex flex-row mb-4">
                <span className="mr-2">
                  <ImportEventStateBadge importEvent={event} />
                </span>
                <span>
                  <SecurityPrincipalSpanFromId id={event.creator} /> created an
                  import of {totalActions} {pluralize('contact', totalActions)}{' '}
                  from <ImportEventTypeSpan event={event} />{' '}
                  <TimeAgoSpan date={event.date_created} />
                </span>
              </div>
            </header>
            <Nav
              variant="tabs"
              activeKey={last(location.pathname.split('/'))}
              id="import-event-pages"
              className="mb-4"
            >
              <Nav.Item>
                <IndexLinkContainer to={`/admin/imports/${reviewSessionId}`}>
                  <Nav.Link eventKey={reviewSessionId}>Overview</Nav.Link>
                </IndexLinkContainer>
              </Nav.Item>
              <Nav.Item>
                <LinkContainer
                  to={`/admin/imports/${reviewSessionId}/contacts`}
                >
                  <Nav.Link eventKey="contacts">
                    Contacts <Badge variant="pill">{totalActions}</Badge>
                  </Nav.Link>
                </LinkContainer>
              </Nav.Item>
            </Nav>
            {children}
          </>
        )
      }}
    </ContentStack>
  )
}

export default ImportEventPage
