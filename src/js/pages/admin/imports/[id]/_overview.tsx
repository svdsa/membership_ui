import { sum } from 'lodash/fp'
import pluralize from 'pluralize'
import React, { useMemo, useState } from 'react'
import { Alert, Card, ListGroup } from 'react-bootstrap'
import { Variant } from 'react-bootstrap/esm/types'
import { FaCheckCircle } from 'react-icons/fa'
import {
  FiCheckCircle,
  FiCpu,
  FiEye,
  FiEyeOff,
  FiGitMerge,
} from 'react-icons/fi'
import { RouteComponentProps } from 'react-router-dom'
import {
  useConfirmImportEventMutation,
  useGetImportEventFromIdQuery,
} from 'src/js/api'
import {
  IdImportEvent,
  ImportEventResponse,
  ImportEventReviewActionType,
} from 'src/js/api/schemas'
import { ImportEventReviewedTimeAgo } from 'src/js/components/admin/import_review/ImportEventReviewedTimeAgo'
import { ImportEventTypeSpan } from 'src/js/components/admin/import_review/ImportEventTypeSpan'
import { ReviewActionIcon } from 'src/js/components/admin/import_review/ReviewActionIcon'
import { ReviewCountActionsSpan } from 'src/js/components/admin/import_review/ReviewCountActionSpan'
import { partitionByManualActions } from 'src/js/components/admin/import_review/utils'
import ContentStack from 'src/js/components/common/ContentStack'
import { LoadingButton } from 'src/js/components/common/LoadingButton'
import TypeToConfirmDialog from 'src/js/components/common/TypeToConfirmDialog'
import { c } from 'src/js/config'

interface ListGroupStackEntryProps {
  icon: React.ReactElement
  title: string
  variant?: Variant
  helpText?: string
}

const ListGroupStackEntry: React.FC<ListGroupStackEntryProps> = ({
  icon,
  title,
  helpText,
  children,
  variant,
}) => (
  <ListGroup.Item variant={variant} className="d-flex flex-row py-3">
    <div className="mr-3">
      <span className="h5">{icon}</span>
    </div>
    <div>
      <div className="font-weight-semibold">{title}</div>
      {helpText != null && helpText.length > 0 && (
        <span className="text-muted small">{helpText}</span>
      )}
      {children != null && <div className="mt-2">{children}</div>}
    </div>
  </ListGroup.Item>
)

interface ReviewOverviewProps {
  event: ImportEventResponse
}

const OpenReviewOverview: React.FC<ReviewOverviewProps> = ({ event }) => {
  const [showConfirmModal, setShowConfirmModal] = useState(false)
  const [
    confirmImport,
    { data: confirmedImport, isLoading: isConfirming, error: confirmError },
  ] = useConfirmImportEventMutation()

  const totalActions = sum(Object.values(event.review_action_counts))
  const [manualActions, automaticActions] = useMemo(
    () => partitionByManualActions(Object.keys(event.review_action_counts)),
    [event]
  )

  const handleConfirmImport: React.MouseEventHandler<HTMLButtonElement> = (
    e
  ) => {
    if (manualActions.length === 0) {
      setShowConfirmModal(true)
    } else {
      confirmImport(event.id)
    }
  }

  const handleConfirmImportModal = () => {
    confirmImport(event.id)
    setShowConfirmModal(false)
  }

  return (
    <section>
      <TypeToConfirmDialog
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        title={`Really confirm "${event.title}"?`}
        typeToConfirmText={totalActions.toLocaleString()}
        confirmHintText={
          <span>Type the number of affected contacts to proceed.</span>
        }
        onConfirm={handleConfirmImportModal}
        actionVariant="success"
      >
        <p className="font-weight-semibold">
          You're about to confirm an import with no manual changes.
        </p>
        <p>
          If you proceed, the system will perform the import and apply all
          automatic actions. These actions include:
        </p>
        {automaticActions.map((key) => (
          <div key={key} className="d-flex flex-row">
            <div className="h5 mr-2">
              <ReviewActionIcon action={key as ImportEventReviewActionType} />
            </div>
            <div className="flex-fill">
              <ReviewCountActionsSpan
                action={key}
                count={event.review_action_counts[key]}
                presentParticiple={true}
              />
            </div>
          </div>
        ))}
        <hr />
        <p>
          These actions will affect a total of <strong>{totalActions}</strong>{' '}
          {pluralize('contact', totalActions)}.
        </p>
      </TypeToConfirmDialog>
      <Card>
        <Card.Header>
          <div className="m-0 d-flex flex-row">
            <div className="mr-3">
              <span className="h5">
                <FiGitMerge />
              </span>
            </div>
            <span className="font-weight-semibold">
              Import {totalActions} {pluralize('contact', totalActions)} from{' '}
              <ImportEventTypeSpan event={event} />
            </span>
          </div>
        </Card.Header>
        <ListGroup variant="flush">
          {manualActions.length === 0 && (
            <ListGroupStackEntry
              icon={<FiEyeOff />}
              title="No comments or review actions yet"
              variant="warning"
              helpText="We recommend manually reviewing the import to
            double-check the automatic suggestions."
            />
          )}
          {manualActions.length > 0 && (
            <ListGroupStackEntry
              icon={
                <span className="text-success">
                  <FiEye />
                </span>
              }
              title="Review actions"
              helpText="These actions will apply when the review is confirmed."
            >
              {manualActions.map((key) => (
                <div key={key} className="d-flex flex-row">
                  <div className="h5 mr-2">
                    <ReviewActionIcon
                      action={key as ImportEventReviewActionType}
                    />
                  </div>
                  <div className="flex-fill">
                    <ReviewCountActionsSpan
                      action={key}
                      count={event.review_action_counts[key]}
                    />
                  </div>
                </div>
              ))}
            </ListGroupStackEntry>
          )}
          {automaticActions.length > 0 && (
            <ListGroupStackEntry
              icon={
                <span className="text-primary">
                  <FiCpu />
                </span>
              }
              title="Auto actions"
              helpText="These actions will apply when the review is confirmed.
            You can override these actions by reviewing the contacts
            manually."
            >
              {automaticActions.map((key) => (
                <div key={key} className="d-flex flex-row">
                  <div className="h5 mr-2">
                    <ReviewActionIcon
                      action={key as ImportEventReviewActionType}
                    />
                  </div>
                  <div className="flex-fill">
                    <ReviewCountActionsSpan
                      action={key}
                      count={event.review_action_counts[key]}
                    />
                  </div>
                </div>
              ))}
            </ListGroupStackEntry>
          )}
          <ListGroup.Item className="d-flex flex-row py-3">
            <div className="mr-3">
              <span className="text-primary h5">
                <FaCheckCircle />
              </span>{' '}
            </div>
            <div>
              <LoadingButton
                variant="success"
                disabled={isConfirming || confirmError != null}
                disabledVariant="outline-success"
                onClick={handleConfirmImport}
                isLoading={isConfirming}
                loadingText="Confirming..."
              >
                Confirm import
              </LoadingButton>
            </div>
          </ListGroup.Item>
        </ListGroup>
        {confirmError != null && (
          <Alert variant="danger">
            <Alert.Heading>Error confirming import</Alert.Heading>
            <p>
              No contacts were changed. This might be a problem with your
              network connection or an issue on our end. Try again later.
            </p>
            <details>
              <summary>Error details</summary>
              <pre>{JSON.stringify(confirmError, null, 2)}</pre>
            </details>
          </Alert>
        )}
      </Card>
    </section>
  )
}

const CompletedReviewOverview: React.FC<ReviewOverviewProps> = ({ event }) => {
  const totalActions = sum(Object.values(event.review_action_counts))
  const [manualActions, automaticActions] = useMemo(
    () => partitionByManualActions(Object.keys(event.review_action_counts)),
    [event]
  )

  if (event.date_reviewed == null || event.reviewer == null) {
    return (
      <section>
        <Card bg="danger" body>
          <Card.Title>Invalid event state</Card.Title>
          <Card.Text>Please contact {c('ADMIN_EMAIL')}.</Card.Text>
        </Card>
      </section>
    )
  }

  return (
    <section>
      <Card className="mb-4">
        <Card.Header>
          <div className="m-0 d-flex flex-row">
            <div className="mr-3">
              <span className="h5">
                <FiGitMerge />
              </span>
            </div>
            <span className="font-weight-semibold">
              Import {totalActions} {pluralize('contact', totalActions)} from{' '}
              <ImportEventTypeSpan event={event} />
            </span>
          </div>
        </Card.Header>
        <ListGroup variant="flush">
          {manualActions.length > 0 && (
            <ListGroupStackEntry
              icon={
                <span className="text-success">
                  <FiEye />
                </span>
              }
              title="Manual review actions"
            >
              {manualActions.map((key) => (
                <div key={key} className="d-flex flex-row">
                  <div className="h5 mr-2">
                    <ReviewActionIcon
                      action={key as ImportEventReviewActionType}
                    />
                  </div>
                  <div className="flex-fill">
                    <ReviewCountActionsSpan
                      action={key}
                      count={event.review_action_counts[key]}
                    />
                  </div>
                </div>
              ))}
            </ListGroupStackEntry>
          )}
          {automaticActions.length > 0 && (
            <ListGroupStackEntry
              icon={
                <span className="text-primary">
                  <FiCpu />
                </span>
              }
              title="Auto review actions"
            >
              {automaticActions.map((key) => (
                <div key={key} className="d-flex flex-row">
                  <div className="h5 mr-2">
                    <ReviewActionIcon
                      action={key as ImportEventReviewActionType}
                    />
                  </div>
                  <div className="flex-fill">
                    <ReviewCountActionsSpan
                      action={key}
                      count={event.review_action_counts[key]}
                    />
                  </div>
                </div>
              ))}
            </ListGroupStackEntry>
          )}
        </ListGroup>
      </Card>
      <Card body>
        <div className="m-0 d-flex flex-row">
          <div className="mr-3">
            <span className="h5 text-success">
              <FiCheckCircle />
            </span>
          </div>
          <span className="font-weight-semibold">
            <ImportEventReviewedTimeAgo event={event} />
          </span>
        </div>
      </Card>
    </section>
  )
}

interface ReviewSessionParamProps {
  reviewSessionId: IdImportEvent
}

type ImportEventOverviewProps = RouteComponentProps<
  ReviewSessionParamProps,
  Record<string, never>
>

const ImportEventOverview: React.FC<ImportEventOverviewProps> = ({
  params: { reviewSessionId },
  router,
}) => {
  const {
    data: importEvent,
    isLoading,
    error: reviewError,
  } = useGetImportEventFromIdQuery(reviewSessionId)

  return (
    <ContentStack data={importEvent} error={reviewError} isLoading={isLoading}>
      {(event) => {
        if (event.date_reviewed == null || event.reviewer == null) {
          return <OpenReviewOverview event={event} />
        } else {
          return <CompletedReviewOverview event={event} />
        }
      }}
    </ContentStack>
  )
}

export default ImportEventOverview
