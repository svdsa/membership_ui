import { MutationActionCreatorResult } from '@reduxjs/toolkit/dist/query/core/buildInitiate'
import {
  dropRight,
  groupBy,
  keyBy,
  mapKeys,
  mapValues,
  partition,
  range,
} from 'lodash'
import { compact, flatten, fromPairs, get, startCase } from 'lodash/fp'
import Papa from 'papaparse'
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import {
  Alert,
  Badge,
  Button,
  Col,
  Form,
  Modal,
  Row,
  Spinner,
} from 'react-bootstrap'
import { Typeahead } from 'react-bootstrap-typeahead'
import DataGrid, { Column } from 'react-data-grid'
import { DropEvent, useDropzone } from 'react-dropzone'
import { BsGearFill } from 'react-icons/bs'
import {
  FiChevronLeft,
  FiChevronRight,
  FiFileText,
  FiUploadCloud,
} from 'react-icons/fi'
import { RouteComponentProps } from 'react-router-dom'
import { api, useCreateImportEventMutation } from 'src/js/api'
import {
  ContactCreateListRelationshipKeys,
  ContactCreateRequest,
  CustomFieldTypeUnion,
  IdCustomField,
  IdTag,
  ImportContactFields,
  ImportEventCreateRequest,
  PostalAddressCreateBase,
  ZdContactCreateRequest,
  ZdImportContactFields,
  ZdImportContactFieldsBase,
} from 'src/js/api/schemas'
import PageHeading from 'src/js/components/common/PageHeading'
import { c } from 'src/js/config'
import { usePaginatedQuery } from 'src/js/hooks/usePaginatedQuery'
import { unquote } from 'src/js/util/strings'
import { findBestMatch } from 'string-similarity'
import type {
  ZodSafeParseError,
  ZodSafeParseResult,
  ZodSafeParseSuccess,
} from 'typings/zod'
import { Brand } from 'utility-types'
import { z, ZodError } from 'zod'

type ImportCsvStep =
  | 'initial'
  | 'columns'
  | 'disambiguate'
  | 'confirm'
  | 'results'

interface ContactSafeParseResult {
  row: Record<string, string>
  result: ZodSafeParseResult<ContactCreateRequest>
}

interface ContactSafeParseError {
  error: ZodError
  row: Record<string, string>
}

const parseRowWithMapping = (mapping: CsvColumnToTargetMapping) => {
  // separate target mappings into kinds
  const targetsByKind = groupBy(
    Object.entries(mapping).filter(([_, target]) => target != null) as [
      string,
      CsvColumnTarget
    ][],
    ([_, target]) => target.kind
  )

  // direct attribute columns (i.e. values that can be directly assigned to
  // the Contact without creating new DB relationships)
  // TODO this might be more complicated than it actually needs to be
  const attributeTargets = fromPairs(
    targetsByKind['attribute'].map(([column, rawTarget]) => {
      const target = rawTarget as CsvColumnAttributeTarget
      return [target.attribute, { column, target }]
    })
  ) as Record<
    keyof ImportContactFields,
    { column: string; target: CsvColumnAttributeTarget }
  >

  // list targets
  const listTargets = groupBy(
    targetsByKind['list'],
    ([_, target]) => (target as CsvColumnListTarget).attribute
  ) as Record<
    ContactCreateListRelationshipKeys,
    [column: string, target: CsvColumnListTarget][]
  >
  const listTargetsSortedByIndex = mapValues(listTargets, (attrs) =>
    attrs.sort(
      ([columnA, firstTarget], [columnB, secondTarget]) =>
        firstTarget.index - secondTarget.index
    )
  )

  const emailAddressMappings = listTargetsSortedByIndex.email_addresses ?? []
  const phoneNumberMappings = listTargetsSortedByIndex.phone_numbers ?? []
  const mailingAddressMappings =
    listTargetsSortedByIndex.mailing_addresses ?? []

  // tags
  const tagMappings = targetsByKind['tag'] ?? []

  // custom fields
  const customFieldPairs = targetsByKind['custom_field'] ?? []
  const customFieldMappings = fromPairs(customFieldPairs)
  const customFieldColumnToIdMapping = fromPairs(
    customFieldPairs.map(([field, mapping]) => [
      field,
      (mapping as CsvColumnCustomFieldTarget).id,
    ])
  )
  const customFieldSchema = z.object(
    mapValues(customFieldMappings, (_mapping) => {
      const mapping = _mapping as CsvColumnCustomFieldTarget

      switch (mapping.fieldType) {
        case 'date':
          return z.date().optional()
        case 'number':
          return z.number().optional()
        case 'text':
        case 'textarea':
          return z.string().optional()
        case 'url':
          return z.string().url().optional()
        default:
          return z.never({
            invalid_type_error: 'Custom field type not supported',
          })
      }
    })
  )

  return (row: Record<string, string>): ContactSafeParseResult => {
    // direct attributes
    const importContactFields: ImportContactFields = mapValues(
      attributeTargets,
      (targetForAttribute) => {
        const column = targetForAttribute.column
        const csvValue = row[column]
        if (csvValue == null) {
          return undefined
        }

        // Don't include empty strings in the final output
        const trimmedCsvValue = csvValue.trim()
        if (trimmedCsvValue.length === 0) {
          return undefined
        }

        return trimmedCsvValue
      }
    )

    const attributeParseResults =
      ZdImportContactFields.safeParse(importContactFields)
    if (!attributeParseResults.success) {
      return { row, result: attributeParseResults }
    }

    // email addresses
    const rowRawEmailValues = compact(
      emailAddressMappings.map(([column, target]) => row[column].trim())
    )

    // phone numbers
    const rowRawPhoneValues = compact(
      phoneNumberMappings.map(([column, target]) => row[column].trim())
    )

    // mailing addresses
    // group address properties by their index
    const addressTargetsGroupedByIndex: Record<
      number,
      [column: string, target: CsvColumnListTarget][]
    > = groupBy(mailingAddressMappings, ([, target]) => target.index)

    const rowRawMailingAddresses = Object.values(addressTargetsGroupedByIndex)
      .map((group) => {
        // for each address
        // key each target by the property
        const keyedByProperty = keyBy(
          group,
          ([, target]) => (target as CsvColumnPostalTarget).property
        ) as Record<
          keyof PostalAddressCreateBase,
          [column: string, target: CsvColumnPostalTarget]
        >

        // retrieve the appropriate column value from the row
        const applyMappingToRow = mapValues(keyedByProperty, ([column]) => {
          const cell = row[column]
          return cell.length > 0 ? cell : undefined
        })

        // the resulting object is { [property]: row value },
        // for each property in the address that we have
        return applyMappingToRow as PostalAddressCreateBase
      })
      .filter(
        (address) => address.zipcode != null && address.zipcode.length > 0
      )

    // custom fields
    const customFieldCellValues = mapValues(
      customFieldMappings,
      (mapping, field) =>
        row[field].trim().length > 0 ? row[field].trim() : undefined
    )
    const columnCustomFieldTypeParseResults = customFieldSchema.safeParse(
      customFieldCellValues
    )
    if (!columnCustomFieldTypeParseResults.success) {
      return { row, result: columnCustomFieldTypeParseResults }
    }
    const customFields = mapKeys(
      columnCustomFieldTypeParseResults.data,
      (_, field) => customFieldColumnToIdMapping[field]
    )

    // tags
    const tags = compact(
      tagMappings.map(([field, mapping]) => {
        const tagMapping = mapping as CsvColumnTagTarget
        const bindingValues = new Set(tagMapping.bindingValues)
        const cellValue = row[field].trim()
        // for each mapping
        // check if the row value is in the accepted binding value
        if (
          bindingValues.has(cellValue) ||
          (cellValue.length === 0 && tagMapping.bindToBlankValues)
        ) {
          // append tag id if accepted
          return tagMapping.id
        } else {
          return null
        }
      })
    )

    const built: ContactCreateRequest = {
      // full name is provided by the transformer
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      full_name: attributeParseResults.data.full_name!,
      display_name: attributeParseResults.data.display_name,
      email_addresses: rowRawEmailValues,
      phone_numbers: rowRawPhoneValues,
      mailing_addresses: rowRawMailingAddresses,
      tags,
      custom_fields: customFields,
    }

    return { row, result: ZdContactCreateRequest.safeParse(built) }
  }
}

function parseRowsAsContacts(
  parsedCsv: Papa.ParseResult<Record<string, string>>,
  mapping: CsvColumnToTargetMapping
): ContactSafeParseResult[] {
  const rows = parsedCsv.data
  const safeParseRowAsContact = parseRowWithMapping(mapping)
  return rows.map((row) => safeParseRowAsContact(row))
}

function partitionValidContacts(parseResults: ContactSafeParseResult[]): {
  valid: ContactCreateRequest[]
  invalid: ContactSafeParseError[]
} {
  // TODO lodash partition doesn't properly output types discriminated by the boolean outcome
  const [passes, errors] = partition(parseResults, (pr) => pr.result.success)

  return {
    valid: passes.map(
      ({ result }) => (result as ZodSafeParseSuccess<ContactCreateRequest>).data
    ),
    invalid: errors.map(({ row, result }) => ({
      row,
      error: (result as ZodSafeParseError).error,
    })),
  }
}

function buildImportEvent(
  title: string,
  participants: ContactCreateRequest[]
): ImportEventCreateRequest {
  return {
    title,
    date_created: new Date(),
    event_type: 'csv_import',
    event_info: {},
    participants,
  }
}

type InvalidGridRow = InvalidGridRowError | InvalidGridRowData

interface InvalidGridRowError {
  rowType: 'error'
  error: string
}

interface InvalidGridRowData {
  rowType: 'data'
  [key: string]: string
}

interface ErrorFormatterProps {
  row: InvalidGridRowError
}

const ErrorFormatter: React.FC<ErrorFormatterProps> = ({ row }) => {
  return <div>{row.error}</div>
}

function buildInvalidRowDataGridColumns(
  parsedCsv: Papa.ParseResult<Record<string, string>>
  // eslint-disable-next-line @typescript-eslint/ban-types
): Column<InvalidGridRow, unknown>[] {
  const fields = parsedCsv.meta.fields

  if (fields == null) {
    return []
  }

  const columnCount = fields.length

  return [
    {
      key: 'expando',
      name: '',
      colSpan(args) {
        return args.type === 'ROW' && args.row.rowType === 'error'
          ? columnCount + 1
          : undefined
      },
      formatter({ row }) {
        if (row.rowType == 'error') {
          return <ErrorFormatter row={row} />
        }

        return null
      },
      width: 0,
    },
    ...fields.map((field) => ({
      key: field,
      name: field,
      resizable: true,
    })),
  ]
}

const EmptyCsvPreview: React.FC = () => (
  <div className="d-flex justify-content-center align-items-center">
    No data in this CSV 🙀
  </div>
)

interface UploadCsvProps {
  onUploadCsv(csvFile: File): void
}

const UploadCsv: React.FC<UploadCsvProps> = ({ onUploadCsv }) => {
  const handleDropAccepted = <T extends File>(files: T[], event: DropEvent) => {
    if (files.length > 0) {
      onUploadCsv(files[0])
    }
  }

  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    accept: ['text/csv', '.csv'],
    multiple: false,
    onDropAccepted: handleDropAccepted,
  })

  return (
    <>
      <div
        {...getRootProps({
          className:
            'dropzone d-flex flex-column justify-content-center align-items-center rounded p-5',
        })}
      >
        <input {...getInputProps()} />
        <div className="my-2">
          <FiFileText size="5em" />
        </div>

        <p>Drag and drop some files here, or click to choose a file.</p>
        <p>
          Any files you choose will stay on your computer until you{' '}
          <strong>confirm the upload</strong> at the end of the import process.
        </p>
      </div>
    </>
  )
}

interface PreviewColumnMapping {
  parsedCsv: Papa.ParseResult<Record<string, string>>
  mapping: CsvColumnToTargetMapping
}

const PreviewColumnMapping: React.FC<PreviewColumnMapping> = ({
  parsedCsv,
  mapping,
}) => {
  if (parsedCsv.data.length === 0) {
    return <div className="preview-csv-empty">No results</div>
  }

  const columns = compact(Object.keys(mapping)).map((csvColumn) => {
    const schemaColumn = mapping[csvColumn]
    return {
      key: `${csvColumn}->${schemaColumn}`,
      name: `"${csvColumn}" to "${schemaColumn}"`,
      minWidth: 100,
      headerRenderer: () => (
        <div>
          <span>{csvColumn}</span>
          <span>
            <FiChevronRight />
          </span>
          <span>{schemaColumn}</span>
        </div>
      ),
    }
  })

  if (columns == null || columns.length == 0) {
    return <div className="preview-csv-empty">No results</div>
  }

  const rows = parsedCsv.data.map((row) =>
    mapKeys(row, (_, csvColumn) => `${csvColumn}->${mapping[csvColumn]}`)
  )

  return (
    <div className="preview-csv-results">
      <DataGrid
        columns={columns}
        emptyRowsRenderer={EmptyCsvPreview}
        rows={rows}
      />
      <p>
        {rows.length} {rows.length !== 1 ? 'rows' : 'row'}
      </p>
    </div>
  )
}

interface CsvColumnAttributeTarget {
  kind: 'attribute'
  attribute: keyof ImportContactFields
}

interface CsvColumnListBase {
  kind: 'list'
  attribute: ContactCreateListRelationshipKeys
  index: number
}

interface CsvColumnEmailTarget extends CsvColumnListBase {
  kind: 'list'
  attribute: 'email_addresses'
}

interface CsvColumnPhoneTarget extends CsvColumnListBase {
  kind: 'list'
  attribute: 'phone_numbers'
}

interface CsvColumnPostalTarget extends CsvColumnListBase {
  kind: 'list'
  attribute: 'mailing_addresses'
  property: keyof PostalAddressCreateBase
}

type CsvColumnListTarget =
  | CsvColumnEmailTarget
  | CsvColumnPhoneTarget
  | CsvColumnPostalTarget

interface CsvColumnCustomFieldTarget {
  kind: 'custom_field'
  id: IdCustomField
  name: string
  fieldType: CustomFieldTypeUnion
}

const FREEFORM_CUSTOM_FIELD_TYPES: Set<CustomFieldTypeUnion> = new Set([
  'date',
  'number',
  'text',
  'textarea',
  'url',
])

interface CsvColumnTagTarget {
  kind: 'tag'
  id: IdTag
  name: string
  bindingValues: string[]
  bindToBlankValues: boolean
}

type CsvColumnTarget =
  | CsvColumnAttributeTarget
  | CsvColumnListTarget
  | CsvColumnTagTarget
  | CsvColumnCustomFieldTarget

type TargetId = Brand<string, 'csv-target-id'>

type CsvColumnToTargetMapping = {
  [csvColumn: string]: CsvColumnTarget | null
}

interface CsvColumnToOptionValueMapping {
  [csvColumn: string]: TargetId | null
}

interface CsvMapperTargetEntry {
  label: string
  target: CsvColumnTarget
}

interface CsvMapperTargetGroup {
  label: string
  entries: CsvMapperTargetEntry[]
}

interface CsvMapperOption {
  label: string
  targetId: TargetId
}

interface CsvMapperOptionGroup {
  optgroupLabel: string
  options: CsvMapperOption[]
}

function inferColumnToOptionMapping(
  columnNames: string[],
  options: CsvMapperOption[]
): CsvColumnToOptionValueMapping {
  return fromPairs(
    compact(
      options.map(({ label, targetId }) => {
        const matches = findBestMatch(label, columnNames)
        const maxMatch = matches.bestMatch.rating
        const matchedField = maxMatch > 0.3 ? matches.bestMatch.target : null
        if (matchedField == null) {
          return null
        }

        return [matchedField, targetId]
      })
    )
  )
}

const generateDeterministicIdFromTarget = (
  target: CsvColumnTarget
): TargetId => {
  switch (target.kind) {
    case 'attribute':
      return target.attribute as TargetId
    case 'list': {
      switch (target.attribute) {
        case 'mailing_addresses':
          return `${target.attribute}_${target.property}_#${target.index}` as TargetId
        default:
          return `${target.attribute}_#${target.index}` as TargetId
      }
    }
    case 'tag':
      return target.id as unknown as TargetId
    case 'custom_field':
      return target.id as unknown as TargetId
    default:
      // Don't really anticipate arriving here
      throw new Error('Unhandled target kind')
  }
}

interface TagOptionsProps {
  columnValues: Set<string>
  targetField: string
  tagMapping: CsvColumnTagTarget
  onChangeTagMapping(mapping: CsvColumnTagTarget): void
}

const TagOptionsModal: React.FC<TagOptionsProps> = ({
  tagMapping,
  onChangeTagMapping,
  columnValues,
}) => {
  const sortedColumnValues = useMemo(
    () => [...columnValues].sort(),
    [columnValues]
  )
  const handleChangeBindingValues = useCallback(
    (selected: string[]) =>
      onChangeTagMapping({ ...tagMapping, bindingValues: selected }),
    [onChangeTagMapping, tagMapping]
  )
  const handleChangeBindToBlank = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) =>
      onChangeTagMapping({
        ...tagMapping,
        bindToBlankValues: e.currentTarget.checked,
      }),
    [onChangeTagMapping, tagMapping]
  )

  return (
    <>
      <Modal.Header closeButton>
        <Modal.Title>
          Choose when to assign the tag "{tagMapping.name}"
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>
              Apply the tag when the column contains the following values:
            </Form.Label>
            <Typeahead
              id="tag-assign-values-input"
              defaultOpen
              autoFocus
              highlightOnlyResult
              multiple
              emptyLabel={
                sortedColumnValues.length === 0
                  ? 'This column has no values'
                  : "You've chosen every value!"
              }
              options={sortedColumnValues}
              selected={tagMapping.bindingValues}
              onChange={handleChangeBindingValues}
            />
          </Form.Group>
          <Form.Group controlId="tag-assign-blanks-check">
            <Form.Check
              checked={tagMapping.bindToBlankValues}
              onChange={handleChangeBindToBlank}
              label="Apply the tag when the column is blank"
            />
          </Form.Group>
        </Form>
      </Modal.Body>
    </>
  )
}

interface CsvColumnMapperProps {
  parsedCsv: Papa.ParseResult<Record<string, string>>
  optgroups: CsvMapperOptionGroup[]
  targetIdToTargets: Record<TargetId, CsvColumnTarget>
  mapping: CsvColumnToTargetMapping
  onUpdateMapping(mapping: CsvColumnToTargetMapping): void
}

const CsvColumnMapper: React.FC<CsvColumnMapperProps> = ({
  parsedCsv,
  optgroups,
  targetIdToTargets,
  mapping,
  onUpdateMapping,
}) => {
  const [optionsModalProps, setOptionsModalProps] = useState<{
    targetField: string
  } | null>(null)

  const columnValues = useMemo(() => {
    if (parsedCsv.meta.fields == null) {
      return null
    }

    return fromPairs(
      parsedCsv.meta.fields.map((field) => {
        const getFieldValueFromRow = get(field)
        const columnValuesForField = parsedCsv.data.map((row) =>
          (getFieldValueFromRow(row) as string).trim()
        )
        const dedupedColumnValuesForField = new Set(
          compact(columnValuesForField)
        )

        return [field, dedupedColumnValuesForField]
      })
    )
  }, [parsedCsv])

  const handleChangeTagMapping = useCallback(
    (tagMapping: CsvColumnTagTarget) => {
      if (optionsModalProps == null) {
        return
      }

      const field = optionsModalProps.targetField
      onUpdateMapping({
        ...mapping,
        [field]: tagMapping,
      })
    },
    [mapping, onUpdateMapping, optionsModalProps]
  )

  const handleCloseOptionsModal = useCallback(() => {
    setOptionsModalProps(null)
  }, [setOptionsModalProps])

  if (parsedCsv.meta.fields == null || parsedCsv.meta.fields.length == 0) {
    return null
  }

  const optionsModalField =
    optionsModalProps != null ? mapping[optionsModalProps.targetField] : null

  return (
    <div className="csv-column-mapper">
      <Row className="mb-2">
        <Col md={4} xs={6}>
          <strong>CSV column</strong>
        </Col>
        <Col md={7} xs={6}>
          <strong>Match to...</strong>
        </Col>
      </Row>

      {[
        ...parsedCsv.meta.fields
          .map((field) => {
            // HACK for typescript null checking (otherwise checking
            // mapping[field] == null won't narrow mapping[field] to a
            // non-null value)
            // likely https://github.com/microsoft/TypeScript/issues/10530
            const mappingForField = mapping[field]

            const handleClickEditOptions: React.MouseEventHandler<HTMLButtonElement> =
              (e) => {
                if (
                  mappingForField != null &&
                  (mappingForField.kind === 'tag' ||
                    mappingForField.kind === 'custom_field')
                ) {
                  setOptionsModalProps({
                    targetField: field,
                  })
                }
              }

            const onUpdateMappingForField: React.ChangeEventHandler<HTMLSelectElement> =
              (e) => {
                const targetMappingForField =
                  targetIdToTargets[e.currentTarget.value as TargetId]

                if (
                  targetMappingForField != null &&
                  (targetMappingForField.kind === 'tag' ||
                    targetMappingForField.kind === 'custom_field')
                ) {
                  setOptionsModalProps({
                    targetField: field,
                  })
                }

                onUpdateMapping({
                  ...mapping,
                  [field]:
                    e.currentTarget.value != 'null'
                      ? targetMappingForField
                      : null,
                })
              }

            return (
              <Row
                className="column-map-entry border-bottom display-flex align-items-center"
                key={field}
              >
                <Col
                  md={4}
                  xs={6}
                  className="d-flex flex-row justify-content-between"
                >
                  <span className="existing-column-name">{field}</span>
                  {mappingForField == null && (
                    <>
                      {' '}
                      <div>
                        <Badge pill variant="warning" className="ml-1">
                          Unmatched
                        </Badge>
                      </div>
                    </>
                  )}
                  {mappingForField != null &&
                    mappingForField.kind === 'tag' &&
                    mappingForField.bindingValues.length === 0 && (
                      <>
                        {' '}
                        <div>
                          <Badge pill variant="warning" className="ml-1">
                            No values
                          </Badge>
                        </div>
                      </>
                    )}
                </Col>
                <Col md={7} xs={6}>
                  <span className="csv-column-name">
                    <Form.Control
                      as="select"
                      value={
                        mappingForField != null
                          ? generateDeterministicIdFromTarget(mappingForField)
                          : 'null'
                      }
                      onChange={onUpdateMappingForField}
                    >
                      <option value="null">
                        Nothing (do not import this column)
                      </option>
                      {optgroups.map((group) => (
                        <optgroup
                          key={group.optgroupLabel}
                          label={group.optgroupLabel}
                        >
                          {group.options.map((opt) => (
                            <option value={opt.targetId} key={opt.targetId}>
                              {opt.label}
                            </option>
                          ))}
                        </optgroup>
                      ))}
                    </Form.Control>
                  </span>
                </Col>
                <Col md={1} xs="auto">
                  {mappingForField != null && mappingForField.kind === 'tag' && (
                    <Button variant="light" onClick={handleClickEditOptions}>
                      <BsGearFill />
                    </Button>
                  )}
                  {mappingForField != null &&
                    mappingForField.kind === 'custom_field' && (
                      <Button variant="light" onClick={handleClickEditOptions}>
                        <BsGearFill />
                      </Button>
                    )}
                </Col>
              </Row>
            )
          })
          .values(),
      ]}

      <Modal show={optionsModalProps != null} onHide={handleCloseOptionsModal}>
        {optionsModalProps != null &&
          optionsModalField != null &&
          optionsModalField.kind === 'tag' && (
            <TagOptionsModal
              {...optionsModalProps}
              tagMapping={optionsModalField as CsvColumnTagTarget}
              onChangeTagMapping={handleChangeTagMapping}
              columnValues={
                columnValues?.[optionsModalProps.targetField] ?? new Set()
              }
            />
          )}
        <Modal.Footer>
          <Button onClick={handleCloseOptionsModal}>Close</Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

function cleanParsedCsv(
  parsedCsv: Papa.ParseResult<Record<string, string>>
): Papa.ParseResult<Record<string, string>> {
  const rowsWithData = parsedCsv.data.filter((row) =>
    Object.values(row).some((cell) => cell.trim().length > 0)
  )

  return {
    ...parsedCsv,
    data: rowsWithData,
  }
}

type ImportCsvProps = RouteComponentProps<
  Record<string, never>,
  Record<string, never>
>

const ImportCsv: React.FC<ImportCsvProps> = ({ router }) => {
  const [csvFile, setCsvFile] = useState<File | null>(null)
  const [parsedCsv, setParsedCsv] = useState<Papa.ParseResult<
    Record<string, string>
  > | null>(null)
  const [parseError, setParseError] = useState<Papa.ParseError | null>(null)
  const [mapping, setMapping] = useState<CsvColumnToTargetMapping | null>(null)
  const [mappingConfirmed, setMappingConfirmed] = useState(false)
  const [participants, setParticipants] = useState<
    ContactCreateRequest[] | null
  >(null)
  // eslint-disable-next-line @typescript-eslint/ban-types
  const [invalidRows, setInvalidRows] = useState<ContactSafeParseError[]>([])
  const [issuesConfirmed, setIssuesConfirmed] = useState(false)
  const [eventTitle, setEventTitle] = useState(
    `${new Date().toISOString().split('T')[0]} CSV Import`
  )
  const [generationError, setGenerationError] = useState<Error | null>(null)
  const {
    items: customFields,
    error: cfError,
    isLoading: cfLoading,
  } = usePaginatedQuery(api.endpoints.getCustomFieldsList, undefined, {
    fetchAllPages: true,
  })
  const {
    items: tags,
    error: tagError,
    isLoading: tagLoading,
  } = usePaginatedQuery(api.endpoints.getTagsList, undefined, {
    fetchAllPages: true,
  })

  // TODO remove this when RTK query 1.7 with mutation resets
  // lands (https://github.com/reduxjs/redux-toolkit/pull/1476)
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const mutationResetRef = useRef<MutationActionCreatorResult<any> | null>(null)
  const [
    createEvent,
    { data: postEventResponse, error: postEventError, isLoading: postingEvent },
  ] = useCreateImportEventMutation()

  const contactAttributeNames = Object.keys(ZdImportContactFieldsBase.shape)
  const isInfoValid = eventTitle != null

  const existingEmailAddressMappings: number = useMemo(() => {
    if (mapping == null) {
      return 0
    }

    return compact(Object.values(mapping)).filter(
      (value) => value.kind === 'list' && value.attribute === 'email_addresses'
    ).length
  }, [mapping])

  const emailAddressEntries: CsvMapperTargetEntry[] = useMemo(() => {
    return range(existingEmailAddressMappings + 1).map((_, i) => ({
      label: `Email address (${i + 1})`,
      target: {
        kind: 'list',
        attribute: 'email_addresses',
        index: i,
      },
    }))
  }, [existingEmailAddressMappings])

  const existingPhoneNumberMappings: number = useMemo(() => {
    if (mapping == null) {
      return 0
    }

    return compact(Object.values(mapping)).filter(
      (value) => value.kind === 'list' && value.attribute === 'phone_numbers'
    ).length
  }, [mapping])

  const phoneNumberEntries: CsvMapperTargetEntry[] = useMemo(() => {
    return range(existingPhoneNumberMappings + 1).map((_, i) => ({
      label: `Phone number (${i + 1})`,
      target: {
        kind: 'list',
        attribute: 'phone_numbers',
        index: i,
      },
    }))
  }, [existingPhoneNumberMappings])

  const existingMailingAddressMappings: number = useMemo(() => {
    if (mapping == null) {
      return 0
    }

    return compact(Object.values(mapping)).filter(
      (value) =>
        value.kind === 'list' &&
        value.attribute === 'mailing_addresses' &&
        value.property === 'zipcode'
    ).length
  }, [mapping])

  const mailingAddressGroups: CsvMapperTargetEntry[][] = useMemo(() => {
    return range(existingMailingAddressMappings + 1).map((_, i) => [
      {
        label: `Street Address (${i + 1}), Line 1`,
        target: {
          kind: 'list',
          attribute: 'mailing_addresses',
          index: i,
          property: 'line1',
        } as CsvColumnPostalTarget,
      },
      {
        label: `Street Address (${i + 1}), Line 2`,
        target: {
          kind: 'list',
          attribute: 'mailing_addresses',
          index: i,
          property: 'line2',
        } as CsvColumnPostalTarget,
      },
      {
        label: `City (${i + 1})`,
        target: {
          kind: 'list',
          attribute: 'mailing_addresses',
          index: i,
          property: 'city',
        } as CsvColumnPostalTarget,
      },
      {
        label: `State (${i + 1})`,
        target: {
          kind: 'list',
          attribute: 'mailing_addresses',
          index: i,
          property: 'state',
        } as CsvColumnPostalTarget,
      },
      {
        label: `ZIP code (${i + 1})`,
        target: {
          kind: 'list',
          attribute: 'mailing_addresses',
          index: i,
          property: 'zipcode',
        } as CsvColumnPostalTarget,
      },
    ])
  }, [existingMailingAddressMappings])

  const mapperGroups: CsvMapperTargetGroup[] = useMemo(() => {
    return compact([
      {
        label: 'Personal info',
        entries: contactAttributeNames.map((name) => ({
          label: startCase(name),
          target: {
            kind: 'attribute',
            attribute: name as keyof ImportContactFields,
          },
        })),
      },
      {
        label: 'Email addresses',
        entries: emailAddressEntries,
      },
      {
        label: 'Phone numbers',
        entries: phoneNumberEntries,
      },
      ...mailingAddressGroups.map((entries, i) => ({
        label: `Postal address #${i + 1}`,
        entries,
      })),
      tags != null &&
        tags.length > 0 && {
          label: 'Tags',
          entries: tags.map((tagObj) => ({
            label: tagObj.name,
            target: {
              kind: 'tag',
              id: tagObj.id,
              name: tagObj.name,
              bindingValues: [],
              bindToBlankValues: false,
            },
          })),
        },
      // TODO custom fields
      customFields != null && customFields.length > 0
        ? {
            label: 'Custom fields (supported fields only)',
            entries: customFields
              // TODO support additional field types
              .filter((cfd) => FREEFORM_CUSTOM_FIELD_TYPES.has(cfd.field_type))
              .map((cfd) => ({
                label: cfd.name,
                target: {
                  kind: 'custom_field',
                  id: cfd.id,
                  name: cfd.name,
                  fieldType: cfd.field_type,
                },
              })),
          }
        : null,
    ])
  }, [
    contactAttributeNames,
    emailAddressEntries,
    phoneNumberEntries,
    mailingAddressGroups,
    tags,
    customFields,
  ])

  // HACK replace with Zod validation (object of flags?)
  const [isMappingValid, mappingErrors]: [boolean, string[]] = useMemo(() => {
    if (mapping == null) {
      return [false, []]
    }

    const allTargetIds = compact(
      Object.values(mapping).map((v) =>
        v != null ? generateDeterministicIdFromTarget(v) : null
      )
    )
    const dedupedTargetIds = new Set(allTargetIds)

    if (allTargetIds.length != dedupedTargetIds.size) {
      return [
        false,
        ['More than one column is matched to the same target field'],
      ]
    }

    const hasAnyNameParts =
      dedupedTargetIds.has('first_name' as TargetId) ||
      dedupedTargetIds.has('middle_name' as TargetId) ||
      dedupedTargetIds.has('last_name' as TargetId)

    if (
      dedupedTargetIds.has('full_name' as TargetId) ==
      (hasAnyNameParts || dedupedTargetIds.has('suffix' as TargetId))
    ) {
      return [false, ['Must provide either full name or name parts, not both']]
    }

    if (!dedupedTargetIds.has('full_name' as TargetId) && !hasAnyNameParts) {
      return [
        false,
        ['Must provide some form of name (either full name or name parts)'],
      ]
    }

    if (
      dedupedTargetIds.has('last_name' as TargetId) &&
      !dedupedTargetIds.has('first_name' as TargetId)
    ) {
      return [
        false,
        [
          "Must provide a first name to use a last name. If only one name exists, use 'Full name' instead",
        ],
      ]
    }

    return [true, []]
  }, [mapping])

  const { optgroups, targetIdToTargets } = useMemo(() => {
    const optgroups: CsvMapperOptionGroup[] = mapperGroups.map((group) => ({
      optgroupLabel: group.label,
      options: group.entries.map((entry) => ({
        label: entry.label,
        targetId: generateDeterministicIdFromTarget(entry.target),
      })),
    }))

    // Map those IDs to the targets so we can retrieve them later
    const targetIdToTargets = fromPairs(
      flatten(mapperGroups.map((group) => group.entries)).map((entry) => [
        generateDeterministicIdFromTarget(entry.target),
        entry.target,
      ])
    )

    return { optgroups, targetIdToTargets }
  }, [mapperGroups])

  const handleBackStartOver = () => {
    setMapping(null)
    setCsvFile(null)
    setParsedCsv(null)
    setMappingConfirmed(false)
    setParticipants(null)
    setInvalidRows([])
  }

  const handleConfirmMapping: React.MouseEventHandler<HTMLButtonElement> =
    () => {
      if (parsedCsv != null && mapping != null) {
        const parseResults = parseRowsAsContacts(parsedCsv, mapping)
        const { valid, invalid } = partitionValidContacts(parseResults)
        setParticipants(valid)
        setInvalidRows(invalid)
        setMappingConfirmed(true)
      }
    }

  const handleConfirmIssues: React.MouseEventHandler<HTMLButtonElement> = () =>
    setIssuesConfirmed(true)

  const handleChangeEventTitle: React.ChangeEventHandler<HTMLInputElement> = (
    e
  ) => {
    setEventTitle(e.currentTarget.value)
  }

  const handleBackEditMapping: React.MouseEventHandler<HTMLButtonElement> = (
    e
  ) => {
    setMappingConfirmed(false)
    setGenerationError(null)
    mutationResetRef.current?.unsubscribe()
  }

  const handleCreateEvent = () => {
    if (participants == null) {
      return
    }

    const event = buildImportEvent(eventTitle, participants)
    mutationResetRef.current = createEvent(event)
  }

  const handleDetailsFormSubmit: React.FormEventHandler<HTMLFormElement> = (
    e
  ) => {
    e.preventDefault()
    e.stopPropagation()

    try {
      if (parsedCsv == null && participants == null) {
        setGenerationError(new Error('Missing or invalid CSV file'))
      }

      if (!e.currentTarget.checkValidity()) {
        setGenerationError(new Error('Form is invalid'))
      }

      handleCreateEvent()
    } catch (e) {
      setGenerationError(e as Error)
    }
  }

  // Parse the csv if the file changes
  useEffect(() => {
    if (csvFile != null) {
      Papa.parse(csvFile, {
        header: true,
        complete: (
          parseResult: Papa.ParseResult<Record<string, string>> | null
        ) => {
          if (parseResult == null) {
            setParsedCsv(null)
          } else {
            const cleaned = cleanParsedCsv(parseResult)
            setParsedCsv(cleaned)
          }
        },
        error: (error, file) => {
          setParseError(error)
          setParsedCsv(null)
        },
        skipEmptyLines: true,
        transform: (v) => unquote(v.trim()),
      })
    } else {
      setParsedCsv(null)
    }
  }, [csvFile])

  // Update the mapping if the parsed CSV or schema change
  useEffect(() => {
    if (
      customFields == null ||
      parsedCsv == null ||
      parsedCsv.meta.fields == null
    ) {
      return
    }

    if (mapping != null && Object.keys(mapping).length > 0) {
      return
    }

    try {
      const options = flatten(optgroups.map((optgroup) => optgroup.options))
      const csvFields = parsedCsv.meta.fields
      const inferredOptions = inferColumnToOptionMapping(csvFields, options)
      const inferredMapping = mapValues(inferredOptions, (targetId) =>
        targetId != null ? targetIdToTargets[targetId] : null
      )
      setMapping(inferredMapping)
    } catch (e) {
      setParseError({
        message: "Couldn't find fields in CSV despite no parse error",
        type: 'missing_fields_unexpected',
        row: 1,
        code: 'missing_fields_unexpected',
      })
    }
  }, [
    customFields,
    parsedCsv,
    contactAttributeNames,
    mapping,
    targetIdToTargets,
    optgroups,
  ])

  useEffect(() => {
    if (
      postEventResponse != null &&
      generationError == null &&
      postEventError == null
    ) {
      router.push(`/admin/imports/${postEventResponse.id}`)
    }
  })

  return (
    <>
      <PageHeading level={2} className="mt-0">
        Import a CSV
      </PageHeading>
      <Row>
        <Col md={12}>
          {/* Step 1: Intro and upload file */}
          {csvFile == null && (
            <>
              <UploadCsv onUploadCsv={setCsvFile} />
            </>
          )}
          {csvFile != null && parsedCsv == null && (
            <div className="preview-csv-loading">Reading CSV...</div>
          )}
          {(parseError != null ||
            (parsedCsv != null && parsedCsv.errors.length > 0)) && (
            <>
              <Alert variant="danger">
                <Alert.Heading>
                  There was a problem reading your CSV.
                </Alert.Heading>
                <p>
                  Please make sure you chose a valid CSV file and try uploading
                  it again. If you continue running into this issue, please
                  contact {c('ADMIN_EMAIL')} and send us the file you're having
                  issues with.
                </p>
                <hr />
                {parseError != null && (
                  <>
                    <pre>
                      <strong>{parseError.type}</strong>
                    </pre>
                    <pre>{parseError.message}</pre>
                    <pre>{parseError.row}</pre>
                    <div className="small">{parseError.code}</div>
                  </>
                )}
                {parsedCsv != null && parsedCsv.errors.length > 0 && (
                  <>
                    <details className="preview-csv-errors">
                      <summary>See error details</summary>
                      <div>
                        {parsedCsv.errors.map((error, i) => (
                          <div className="preview-csv-error" key={i}>
                            {error.row} - {error.type}
                            {error.code}: {error.message}
                          </div>
                        ))}
                      </div>
                    </details>
                  </>
                )}
              </Alert>
              <UploadCsv onUploadCsv={setCsvFile} />
            </>
          )}
          {parsedCsv != null && parsedCsv.data.length === 0 && (
            <>
              <Alert variant="danger">
                <Alert.Heading>
                  Your CSV file appears to be empty.
                </Alert.Heading>
                <p>
                  Please make sure you chose a valid CSV file and try uploading
                  it again. If you continue running into this issue, please
                  contact {c('ADMIN_EMAIL')} and send us the file you're having
                  issues with.
                </p>
              </Alert>
              <UploadCsv onUploadCsv={setCsvFile} />
            </>
          )}

          {/* Step 2: Column map */}
          {parsedCsv != null &&
            !mappingConfirmed &&
            parsedCsv.errors.length === 0 && (
              <>
                <header className="d-flex flex-row justify-content-between">
                  <Button
                    variant="outline-primary"
                    onClick={handleBackStartOver}
                  >
                    <FiChevronLeft /> Start over with a different CSV file
                  </Button>
                </header>
                <div className="d-flex flex-column">
                  <section>
                    <header className="d-flex flex-row justify-content-between align-items-baseline">
                      <PageHeading level={3}>Match columns</PageHeading>
                    </header>
                    {mapping != null ? (
                      <CsvColumnMapper
                        parsedCsv={parsedCsv}
                        optgroups={optgroups}
                        targetIdToTargets={targetIdToTargets}
                        mapping={mapping}
                        onUpdateMapping={setMapping}
                      />
                    ) : (
                      <div>Inferring mappings for columns...</div>
                    )}
                  </section>
                  {mappingErrors != null && mappingErrors.length > 0 && (
                    <Alert variant="danger">
                      <Alert.Heading>Issues you need to fix</Alert.Heading>
                      {dropRight(
                        flatten(
                          mappingErrors.map((errorStr) => [
                            <p>{errorStr}</p>,
                            <hr />,
                          ])
                        )
                      )}
                    </Alert>
                  )}
                  <Button
                    variant={isMappingValid ? 'primary' : 'outline-primary'}
                    disabled={!isMappingValid}
                    className="mt-4 align-self-end"
                    onClick={handleConfirmMapping}
                  >
                    Confirm <FiChevronRight />
                  </Button>
                </div>
              </>
            )}

          {/* Step 3: Validate and correct errors */}
          {parsedCsv != null &&
            mapping != null &&
            mappingConfirmed &&
            parsedCsv.meta.fields != null &&
            invalidRows.length > 0 &&
            !issuesConfirmed && (
              <>
                <header className="d-flex flex-row justify-content-between">
                  <Button
                    variant="outline-primary"
                    onClick={handleBackEditMapping}
                  >
                    <FiChevronLeft /> Go back and edit mapping
                  </Button>
                </header>
                <section className="d-flex flex-column">
                  <PageHeading level={3}>
                    Found some issues with this file
                  </PageHeading>
                  <PageHeading level={4}>
                    {invalidRows.length}{' '}
                    {invalidRows.length !== 1 ? 'rows' : 'row'} with issues
                  </PageHeading>
                  <DataGrid
                    columns={buildInvalidRowDataGridColumns(parsedCsv)}
                    emptyRowsRenderer={EmptyCsvPreview}
                    rows={flatten(
                      invalidRows.map((ir) => [
                        { rowType: 'data', ...ir.row } as InvalidGridRowData,
                        {
                          rowType: 'error',
                          error: JSON.stringify(ir.error.flatten()),
                        } as InvalidGridRowError,
                      ])
                    )}
                  />
                  <p>
                    <strong>
                      We'll skip these rows when importing your CSV.
                    </strong>{' '}
                    You can also edit your CSV file and{' '}
                    <a href="#" onClick={handleBackStartOver}>
                      re-upload your file
                    </a>
                    .
                  </p>
                  <Button
                    variant={isMappingValid ? 'primary' : 'outline-primary'}
                    disabled={!isMappingValid}
                    className="mt-4 align-self-end"
                    onClick={handleConfirmIssues}
                  >
                    Skip rows and continue <FiChevronRight />
                  </Button>
                </section>
              </>
            )}

          {/* Step 4: Paperwork and admin stuff */}
          {parsedCsv != null &&
            mapping != null &&
            mappingConfirmed &&
            (invalidRows.length === 0 || issuesConfirmed) && (
              <>
                <header className="d-flex flex-row justify-content-between">
                  <Button
                    variant="outline-primary"
                    onClick={() => {
                      setMappingConfirmed(false)
                      setIssuesConfirmed(false)

                      // reset mutation state to remove any error messages
                      mutationResetRef.current?.unsubscribe()
                    }}
                  >
                    <FiChevronLeft /> Go back and edit mapping
                  </Button>
                </header>
                <section className="d-flex flex-column">
                  <PageHeading level={3}>Let's wrap things up</PageHeading>
                  <p>
                    Help reviewers identify the members in your CSV file by
                    naming your CSV.
                  </p>
                  <Form onSubmit={handleDetailsFormSubmit}>
                    <Form.Group controlId="title">
                      <Form.Label>Title</Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="YYYY-MM-DD Event Name"
                        value={eventTitle}
                        onChange={handleChangeEventTitle}
                        required
                      />
                      <Form.Text className="text-muted">
                        Give your file a descriptive name. For events, we
                        recommend <strong>YYYY-MM-DD Event Name</strong>, where
                        the date is when the event took place.
                      </Form.Text>
                    </Form.Group>
                    <Button
                      type="submit"
                      variant={isInfoValid ? 'primary' : 'outline-primary'}
                      disabled={!isInfoValid || postingEvent}
                    >
                      {postingEvent ? (
                        <>
                          <span className="ml-2">Uploading... </span>
                          <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                          />
                        </>
                      ) : (
                        <span>
                          Upload CSV <FiUploadCloud />
                        </span>
                      )}
                    </Button>
                  </Form>
                  {generationError != null && (
                    <Alert variant="danger">
                      <Alert.Heading>
                        There was a problem preparing your CSV for upload
                      </Alert.Heading>
                      <pre>{generationError.message}</pre>
                      {generationError.stack && (
                        <pre>{generationError.stack}</pre>
                      )}
                    </Alert>
                  )}
                  {postEventError != null && (
                    <Alert variant="danger">
                      <p>
                        There was a problem uploading your CSV to the server
                      </p>
                      <pre>{JSON.stringify(postEventError, null, 2)}</pre>
                    </Alert>
                  )}
                </section>
              </>
            )}
        </Col>
      </Row>
    </>
  )
}

export default ImportCsv
