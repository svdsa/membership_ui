import debounce from 'debounce-promise'
import { Formik } from 'formik'
import React, { useState } from 'react'
import { Button, Card, Col, Form, Row, Spinner } from 'react-bootstrap'
import Helmet from 'react-helmet'
import {
  useCreateTagMutation,
  useGetTagsListQuery,
  useSearchTagsMutation,
} from 'src/js/api'
import {
  TagCreateRequestDefaults,
  ZdTagCreateRequest,
} from 'src/js/api/schemas'
import ContentStack from 'src/js/components/common/ContentStack'
import PageHeading from 'src/js/components/common/PageHeading'
import { toFormikValidationSchema } from 'src/js/util/zod-formik-adapter'

const Tags: React.FC = () => {
  const { data, isLoading, error } = useGetTagsListQuery()
  const [showCreateTagInput, setShowCreateTagInput] = useState(false)

  return (
    <>
      <Helmet>
        <title>Tags</title>
      </Helmet>
      <Row>
        <Col>
          <PageHeading className="mt-0" level={2}>
            Tags
          </PageHeading>
        </Col>
      </Row>

      <ContentStack data={data} isLoading={isLoading} error={error}>
        {(data) => (
          <>
            <Row className="mb-2">
              <Col>
                <Form.Control placeholder="Search for a tag" />
              </Col>
              <Col md={4} />
              <Col sm="auto">
                <Button
                  variant="success"
                  onClick={() => setShowCreateTagInput(true)}
                >
                  New tag
                </Button>
              </Col>
            </Row>
            {showCreateTagInput && (
              <Row className="mb-2">
                <Col>
                  <Card body>
                    <CreateTagInput
                      onClose={() => setShowCreateTagInput(false)}
                    />
                  </Card>
                </Col>
              </Row>
            )}
            <Row>
              <Col>
                <pre>{JSON.stringify(data, null, 2)}</pre>
              </Col>
            </Row>
          </>
        )}
      </ContentStack>
    </>
  )
}

interface CreateTagInputProps {
  onClose(): void
}

const CreateTagInput: React.FC<CreateTagInputProps> = ({ onClose }) => {
  const [submitForm, { isLoading, error }] = useCreateTagMutation()
  const [trigger] = useSearchTagsMutation()
  const debouncedTrigger = debounce(trigger, 500)

  return (
    <Formik
      initialValues={TagCreateRequestDefaults}
      validationSchema={toFormikValidationSchema(ZdTagCreateRequest)}
      onSubmit={async (values, { setSubmitting }) => {
        try {
          await submitForm({ ...values, name: values.name.trim() })
          setSubmitting(false)
          onClose()
        } catch (err) {}
      }}
      validate={async (values) => {
        if (values.name.length == 0) {
          return
        }

        const conflictResults = await debouncedTrigger({ q: values.name })
        if (conflictResults == null || !('data' in conflictResults)) {
          return
        }

        const { data } = conflictResults.data

        if (data.length > 0) {
          for (const result of data) {
            if (result.name === values.name.trim()) {
              return {
                name: 'A tag with this name already exists',
              }
            }
          }
        }
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => {
        return (
          <Form onSubmit={handleSubmit}>
            <Form.Row className="d-flex">
              <Form.Group as={Col} controlId="name">
                <Form.Label>Tag name</Form.Label>
                <Form.Control
                  placeholder="Tag name"
                  name="name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isValid={touched.name && !errors.name}
                  value={values.name}
                />
              </Form.Group>
              <Form.Group
                as={Col}
                md={4}
                className="d-flex flex-row align-items-end"
              >
                {errors.name && touched.name ? (
                  <div className="text-danger">{errors.name}</div>
                ) : null}
              </Form.Group>
              <Form.Group
                as={Col}
                className="d-flex flex-row align-items-end"
                sm="auto"
                controlId="controls"
              >
                <Button
                  className="mr-2"
                  variant="secondary"
                  onClick={() => onClose()}
                >
                  Cancel
                </Button>
                <Button type="submit" variant="success">
                  {isSubmitting ? (
                    <Spinner animation="border" size="sm" />
                  ) : (
                    'Create tag'
                  )}
                </Button>
              </Form.Group>
            </Form.Row>
          </Form>
        )
      }}
    </Formik>
  )
}

export default Tags
