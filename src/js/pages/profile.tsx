import { capitalize } from 'lodash/fp'
import React, { useState } from 'react'
import {
  Button,
  Col,
  Container,
  ListGroup,
  Row,
  Tab,
  Tabs,
} from 'react-bootstrap'
import Helmet from 'react-helmet'
import { BiGhost } from 'react-icons/bi'
import { FiPhoneOff } from 'react-icons/fi'
import { useGetContactMeQuery } from 'src/js/api'
import {
  ContactResponse,
  EmailAddressResponse,
  PaginatedList,
  PhoneNumberResponse,
  PostalAddressResponse,
} from 'src/js/api/schemas'
import { MemberPronounBundle } from 'src/js/client/MemberClient'
import ContentStack from 'src/js/components/common/ContentStack'
import { UnderConstruction } from 'src/js/components/common/ErrorBlock'
import PageHeading from 'src/js/components/common/PageHeading'
import { ProfilePicture } from 'src/js/components/contacts/ProfilePicture'
import { PronounBadgeListFromBundle } from 'src/js/components/contacts/PronounBadgeList'
import { c } from 'src/js/config'

type ProfileTab = 'membership' | 'groups' | 'meetings'

const Profile: React.FC = () => {
  const { data, isError, isLoading, error } = useGetContactMeQuery()
  const [tab, setTab] = useState<ProfileTab>('membership')

  return (
    <ContentStack data={data} isLoading={isLoading} error={error}>
      {(data) => (
        <>
          <Helmet>
            <title>
              {data != null
                ? `${data.full_name} (${data.display_name})`
                : 'My profile'}
            </title>
          </Helmet>
          <Row className="pt-4">
            <Col xl={2} lg={3} md={4} className="pb-4">
              <ContactSidebar contact={data} />
            </Col>
            <Col xl={10} lg={9} md={8}>
              <Tabs activeKey={tab} onSelect={(t) => setTab(t! as ProfileTab)}>
                <Tab eventKey="membership" title="Membership and dues">
                  <UnderConstruction />
                </Tab>
                <Tab
                  eventKey="groups"
                  title={capitalize(c('GROUP_NAME_PLURAL'))}
                >
                  <UnderConstruction />
                </Tab>
                <Tab eventKey="meetings" title="Meetings">
                  <UnderConstruction />
                </Tab>
              </Tabs>
            </Col>
          </Row>
        </>
      )}
    </ContentStack>
  )
}

interface ContactSidebarProps {
  contact: ContactResponse
}

const ContactSidebar: React.FC<ContactSidebarProps> = ({ contact }) => {
  return (
    <>
      <ProfilePicture contact={contact} size="xl" />
      <section>
        <PageHeading level={2} className="mb-1">
          {contact.full_name}
        </PageHeading>
        <div className="mb-1 h4">"{contact.display_name}"</div>
        <div>
          {contact.pronouns != null ? (
            <PronounBadgeListFromBundle
              bundle={contact.pronouns as MemberPronounBundle}
            />
          ) : (
            'No pronouns specified'
          )}
        </div>
      </section>
      <section>
        <PageHeading level={3} className="h5">
          Contact info
        </PageHeading>
        <ListGroup variant="flush">
          <ListGroup.Item className="px-0">
            <header className="pb-2">
              <strong>Email addresses</strong>
            </header>
            <PagableList list={contact.email_addresses}>
              {(addr: EmailAddressResponse) => (
                <div className="d-flex flex-direction-row justify-content-between">
                  {addr.value}{' '}
                </div>
              )}
            </PagableList>
          </ListGroup.Item>
          <ListGroup.Item className="px-0">
            <header className="pb-2">
              <strong>Phone numbers</strong>
            </header>
            <PagableList
              empty={
                <EmptyPagableList
                  icon={<FiPhoneOff />}
                  text="No phone numbers"
                />
              }
              list={contact.phone_numbers}
            >
              {(addr: PhoneNumberResponse) => (
                <div className="d-flex flex-row justify-content-between">
                  {addr.value}{' '}
                </div>
              )}
            </PagableList>
          </ListGroup.Item>
          <ListGroup.Item className="px-0">
            <header className="pb-2">
              <strong>Mailing addresses</strong>
            </header>
            <PagableList
              empty={
                <EmptyPagableList
                  icon={<BiGhost />}
                  text="No mailing addresses"
                />
              }
              list={contact.mailing_addresses}
            >
              {(addr: PostalAddressResponse) => (
                <div className="d-flex flex-row justify-content-between">
                  {addr.line1}
                  {addr.line2}
                  {addr.city}
                  {addr.state}
                  {addr.zipcode}
                </div>
              )}
            </PagableList>
          </ListGroup.Item>
        </ListGroup>
        <Button variant="outline-secondary" disabled block>
          Edit your profile
        </Button>
      </section>
    </>
  )
}

interface EmptyPagableListProps {
  icon: React.ReactElement
  text: string
}

const EmptyPagableList: React.FC<EmptyPagableListProps> = ({ icon, text }) => (
  <div className="d-flex flex-column align-items-center text-muted">
    {icon}
    <div>{text}</div>
  </div>
)

interface PagableListProps<T> {
  list: PaginatedList
  empty?: React.ReactElement
  children(item: T): React.ReactElement | null
}

const PagableList = <P extends object>(
  props: PagableListProps<P>
): React.ReactElement | null => {
  if (props.list.data.length == 0) {
    return (
      <ListGroup>
        <ListGroup.Item>{props.empty || 'No items'}</ListGroup.Item>
      </ListGroup>
    )
  }

  return (
    <ListGroup>
      {props.list.data.map((item, i) => (
        <ListGroup.Item key={i}>{props.children(item as P)}</ListGroup.Item>
      ))}
    </ListGroup>
  )
}

const ProfileWrapper: React.FC = () => (
  <Container fluid>
    <Profile />
  </Container>
)

export default ProfileWrapper
