const audioRegex =
  /^.*\.(aac)|(au)|(oga)|(m4[abpr])|(mka)|(mp[3c])|(wav)|(wma)$/
const imageRegex = /^.*\.(bmp)|(gif)|(jp[e]?g)|(png)|(tiff)|(webp)$/
const videoRegex = /^.*\.(avi)|(mkv)|(mov)|(mp4)|(og[gvx])|(webm)$/

export function contentTypeForFile(file: File) {
  const filetype = file.type
  const filename = file.name
  if (filetype.startsWith('image') || imageRegex.test(filename)) {
    return 'image'
  } else if (filetype.startsWith('audio') || audioRegex.test(filename)) {
    return 'audio'
  } else if (filetype.startsWith('video') || videoRegex.test(filename)) {
    return 'video'
  } else {
    return null
  }
}

export function contentTypeForUrl(url: string) {
  try {
    const normalizedUrl = new URL(url).href
    if (imageRegex.test(normalizedUrl)) {
      return 'image'
    } else if (audioRegex.test(normalizedUrl)) {
      return 'audio'
    } else if (videoRegex.test(normalizedUrl)) {
      return 'video'
    } else {
      return null
    }
  } catch (e) {
    return null
  }
}

export function sanitizeFileName(filename: string) {
  return filename.replace(/\s/g, '_')
}
