/* eslint-disable @typescript-eslint/no-explicit-any */

// Here be dragons...
//
// This is an attempt to implement a recursive Zod "deepNullable" type and function,
// similar to the built-in "deepPartial" function already in Zod.
//
// References
// - https://github.com/colinhacks/zod/blob/7fe864e34294b465362ba55aa59626208edb1a49/src/helpers/partialUtil.ts
// - https://github.com/colinhacks/zod/blob/fdd708493e4422dba6453908af55ef0d58767c03/src/types.ts#L1397
// - https://github.com/colinhacks/zod/blob/fdd708493e4422dba6453908af55ef0d58767c03/src/types.ts#L1660

import {
  z,
  ZodArray,
  ZodNullable,
  ZodObject,
  ZodOptional,
  ZodTuple,
  ZodTupleItems,
  ZodTypeAny,
} from 'zod'

export type DeepNullable<T extends ZodTypeAny> = T extends ZodObject<
  infer Shape,
  infer Params,
  infer Catchall
>
  ? ZodObject<
      { [k in keyof Shape]: ZodNullable<DeepNullable<Shape[k]>> },
      Params,
      Catchall
    >
  : T extends ZodArray<infer Type, infer Card>
  ? ZodArray<DeepNullable<Type>, Card>
  : T extends ZodOptional<infer Type>
  ? ZodOptional<DeepNullable<Type>>
  : T extends ZodNullable<infer Type>
  ? ZodNullable<DeepNullable<Type>>
  : T extends ZodTuple<infer Items>
  ? {
      [k in keyof Items]: Items[k] extends ZodTypeAny
        ? DeepNullable<Items[k]>
        : never
    } extends infer PI
    ? PI extends ZodTupleItems
      ? ZodTuple<PI>
      : never
    : never
  : T

function deepNullify(schema: ZodTypeAny): any {
  if (schema instanceof ZodObject) {
    const newShape: any = {}

    for (const key in schema.shape) {
      const fieldSchema = schema.shape[key]
      newShape[key] = ZodNullable.create(deepNullify(fieldSchema))
    }
    return new ZodObject({
      ...schema._def,
      shape: () => newShape,
    }) as any
  } else if (schema instanceof ZodArray) {
    return ZodArray.create(deepNullify(schema.element))
  } else if (schema instanceof ZodOptional) {
    return ZodOptional.create(deepNullify(schema.unwrap()))
  } else if (schema instanceof ZodNullable) {
    return ZodNullable.create(deepNullify(schema.unwrap()))
  } else if (schema instanceof ZodTuple) {
    return ZodTuple.create(schema.items.map((item: any) => deepNullify(item)))
  } else {
    return schema
  }
}

export function deepNullable<T extends z.ZodTypeAny>(zod: T): DeepNullable<T> {
  return deepNullify(zod) as any
}
