import React from 'react'

export type ChildrenMaybeFunction<T> =
  | ((props: T) => React.ReactNode)
  | React.ReactNode

export function resolveChildrenFn<T>(
  children: ChildrenMaybeFunction<T>,
  props: T
): React.ReactNode {
  if (typeof children === 'function') {
    return children(props)
  } else {
    return children
  }
}
