import { first, flatten, sortBy } from 'lodash/fp'

export function interleave<T>(...arrays: T[][]): T[] {
  if (arrays.length === 0) {
    return []
  }

  if (arrays.length === 1) {
    return [...arrays[0]]
  }

  const sortByShortestArray = sortBy<T[]>((elem) => elem.length)
  const shortest = first(sortByShortestArray(arrays))

  if (shortest == null || shortest.length === 0) {
    return []
  }

  return flatten(shortest.map((_, idx) => arrays.map((array) => array[idx])))
}
