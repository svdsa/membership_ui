import { fromPairs, isObject } from 'lodash'
import { ApiError, ParameterInvalidError } from 'src/js/api/schemas'

export interface FormikStatus {
  /**
   * A global error message for the entire form received from submission
   * (e.g. a server error).
   */
  formError?: {
    message: string
    innerError?: ApiError
  }
}

/**
 * Process incoming errors and set parameter-specific errors and generic errors
 * as appropriate.
 *
 * @param err incoming errors from the API
 * @param setErrors function to set Formik errors (map of field names to error messages)
 * @param setStatus function to set Formik status
 */
export const transformApiErrorsToFormik = (err: {
  status: number
  data: ApiError | ParameterInvalidError
}): { errors?: { [fieldName: string]: string }; status: FormikStatus } => {
  if (err.data == null || err.data.error_code === 'server_error') {
    return {
      status: {
        formError: {
          message: 'An unexpected error occurred.',
          innerError: err.data,
        },
      },
    }
  }

  const { data } = err

  if (data.error_code === 'parameter_invalid') {
    if (data.params == null) {
      // No parameters provided
      return {
        status: {
          formError: { message: 'Validation error', innerError: data },
        },
      }
    }

    if (Array.isArray(data.params)) {
      const status = {
        formError: {
          message: 'Please fix the errors above.',
          innerError: data,
        },
      }
      if (
        data.params.length > 0 &&
        isObject(data.params[0]) &&
        'loc' in data.params[0]
      ) {
        // Pydantic error params
        return {
          errors: fromPairs(
            data.params.map((param) => [param.loc[0], param.msg])
          ),
          status,
        }
      } else {
        // Generic array of error params
        return {
          errors: fromPairs(
            data.params.map((param) => [
              param,
              "We couldn't validate this. Please try submitting again.",
            ])
          ),
          status,
        }
      }
    }

    // Map of error params to error messages
    return {
      errors: data.params,
      status: {
        formError: {
          message: 'Please fix the errors above.',
          innerError: data,
        },
      },
    }
  }

  return {
    status: {
      formError: { message: err.data.friendly_message, innerError: data },
    },
  }
}
