/* eslint-disable @typescript-eslint/ban-types */

import type { NonUndefined } from 'utility-types'

/**
 * Modify an existing type and override certain members with incompatible types.
 * Useful for forms that largely conform to API shape, but that need to start with null fields
 *
 * See https://stackoverflow.com/questions/41285211/overriding-interface-property-type-defined-in-typescript-d-ts-file
 */
export type Modify<T, R> = Omit<T, keyof R> & R

/**
 * Brand a primitive type to approximate nominal typing in TypeScript.
 *
 * This prevents mixing primitive types in use-cases where mixing could be dangerous.
 *
 * **Branded primitives only allow construction via an explicit cast.**
 *
 * @template T the primitive to brand
 * @template TBrand an identifier for the brand to prevent conflicts. Defaults to a unique symbol type
 *
 *
 * @example
 * type UserId = Brand<string, "user-id">
 * type UserToken = Brand<string, "user-token">
 * const a: UserToken = getUserToken()
 * const b: UserId = a  // error!
 * const c: UserToken = "some-user-token"  // error!
 * const d: UserToken = "some-user-token" as UserToken  // ok
 */
export type Brand<T, TBrand extends string> = T & { __brand: TBrand }

/**
 * "Flavor" a primitive type to allow for slightly more flexible nominal typing in TypeScript.
 *
 * This has the same benefits as branded types, but Flavors allow implicit
 * casting from the underlying primitive type.
 *
 * @template T the primitive to flavor
 * @template TFlavor an identifier for the flavor to prevent conflicts. Defaults to a unique symbol type
 *
 * @example
 * type UserId = Flavor<string, "user-id">
 * type UserToken = Flavor<string, "user-token">
 * const a: UserToken = getUserToken()
 * const b: UserId = a;  // error!
 * const c: UserToken = "some-user-token"  // ok
 * const d: UserToken = "some-user-token" as UserToken  // ok
 */
export type Flavor<T, TFlavor extends string> = T & { __brand?: TFlavor }

/**
 * Nullishly
 * @desc Apply null | undefined to the first level of a structure
 * @example
 *   // Expect: {
 *   //   first?: null | undefined | {
 *   //     second: {
 *   //       name: string;
 *   //     };
 *   //   };
 *   // }
 *   type NestedProps = {
 *     first: {
 *       second: {
 *         name: string;
 *       };
 *     };
 *   };
 *   type NullishlyNestedProps = DeepNullishly<NestedProps>;
 */
export type Nullishly<T> = T extends Function
  ? T
  : T extends Array<infer U>
  ? _NullishlyArray<U>
  : T extends object
  ? _NullishlyObject<T>
  : T | null | undefined
/** @private */
// tslint:disable-next-line:class-name
export type _NullishlyArray<T> = Array<T | null | undefined>
/** @private */
export type _NullishlyObject<T> = { [P in keyof T]?: T[P] | null | undefined }

/**
 * Non-Nullishly
 * @desc Remove null | undefined to the first level of a structure
 * @example
 *   // Expect: {
 *   //   first?: null | undefined | {
 *   //     second: {
 *   //       name: string;
 *   //     };
 *   //   };
 *   // }
 *   type NestedProps = {
 *     first: {
 *       second: {
 *         name: string;
 *       };
 *     };
 *   };
 *   type NullishlyNestedProps = DeepNullishly<NestedProps>;
 */
export type NonNullishly<T> = T extends Function
  ? T
  : T extends Array<infer U>
  ? _NonNullishlyArray<U>
  : T extends object
  ? _NonNullishlyObject<T>
  : T | null | undefined
/** @private */
// tslint:disable-next-line:class-name
export type _NonNullishlyArray<T> = Array<NonNullable<NonUndefined<T>>>
/** @private */
export type _NonNullishlyObject<T> = {
  [P in keyof T]-?: NonNullable<NonUndefined<T[P]>>
}

export type NonNullishKeys<
  T extends object,
  K extends keyof T = keyof T
> = Omit<T, K> & NonNullishly<Pick<T, K>>
