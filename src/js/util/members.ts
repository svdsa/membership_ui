/**
 * Encode the member id in a format expected by the attendance client
 * Temporary implementation until all member routes return encoded IDs
 *
 * @deprecated
 * @param memberId non-encoded member id
 */
export const TEMP_encodeMemberId = (memberId: number): string => {
  return `mbr_${memberId}`
}

/**
 * Decode the member id into a format expected by other clients
 * Temporary implementation until all member routes return encoded IDs
 *
 * @deprecated
 * @param memberId encoded member id
 */
export const TEMP_decodeMemberId = (memberId: string): number => {
  if (!memberId.startsWith(`mbr_`)) {
    throw `Issue decoding member id ${memberId}`
  }

  return parseInt(memberId.replace('mbr_', ''))
}
