import _get from 'lodash/get'

import { serviceLocator } from './serviceLocator'

export const HTTP_GET = 'GET'
export const HTTP_POST = 'POST'
export const HTTP_PUT = 'PUT'
export const HTTP_PATCH = 'PATCH'
export const HTTP_DELETE = 'DELETE'
export type HttpMethod =
  | typeof HTTP_GET
  | typeof HTTP_POST
  | typeof HTTP_PUT
  | typeof HTTP_PATCH
  | typeof HTTP_DELETE

export const SHOW_LOCK = 'SHOW_LOCK'
export const LOCK_SUCCESS = 'LOCK_SUCCESS'
export const LOCK_ERROR = 'LOCK_ERROR'
export const LOGOUT = 'LOGOUT'
export const LOCK_INIT = 'LOCK_INIT'
export const HIDE_LOCK = 'HIDE_LOCK'

export function logError(title: string, err = new Error(title)) {
  const errorMessage = _get(err, ['response', 'body', 'err'], err.toString())
  // also log to console for a longer lasting error message
  console.error(`${title}: ${errorMessage}`, err)
  serviceLocator.notificationSystem?.current?.addNotification({
    title,
    message: errorMessage,
    level: 'error',
  })
}

export function showNotification(title: string, body: string) {
  serviceLocator.notificationSystem?.current?.addNotification({
    title,
    message: body,
    level: 'success',
  })
}
