import { Permission } from 'src/js/api/schemas'

export function hasAdminPermission(permissions: Permission[]): boolean {
  const adminPermissions = permissions.filter(
    (p) => p.slug.indexOf('admin') >= 0
  )
  return adminPermissions.length > 0
}
