import { mapValues } from 'lodash/fp'

export const mapValuesOrNull = mapValues((v) => v || null)
