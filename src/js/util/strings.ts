const quoteStrReg = /[\'\"]/

/**
 * @see https://github.com/lakenen/node-unquote
 * @param str string to unquote
 * @returns
 */
export const unquote = (str: string): string => {
  if (!str) {
    return ''
  }
  if (quoteStrReg.test(str.charAt(0))) {
    str = str.substr(1)
  }
  if (quoteStrReg.test(str.charAt(str.length - 1))) {
    str = str.substr(0, str.length - 1)
  }
  return str
}
