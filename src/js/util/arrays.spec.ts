import { interleave } from 'src/js/util/arrays'

describe('Array#interleave', () => {
  it('should interleave from two arrays', () => {
    const a = [1, 2, 3]
    const b = [4, 5, 6]
    const expected = [1, 4, 2, 5, 3, 6]
    const result = interleave(a, b)
    expect(result).toStrictEqual(expected)
  })

  it('should interleave from three arrays', () => {
    const a = [1, 2, 3]
    const b = [4, 5, 6]
    const c = [7, 8, 9]
    const expected = [1, 4, 7, 2, 5, 8, 3, 6, 9]
    const result = interleave(a, b, c)
    expect(result).toStrictEqual(expected)
  })

  it('should return a copy of a single array unchanged', () => {
    const a = [1, 2, 3]
    const result = interleave(a)
    expect(result).toStrictEqual(a)
    expect(result).not.toBe(a)
  })

  it('should interleave up to the length of the shortest matcher', () => {
    const a = [1, 2, 3]
    const b = [4, 5, 6, 7, 8]
    const c = [7, 8, 9, 10, 11, 12]
    const expected = [1, 4, 7, 2, 5, 8, 3, 6, 9]
    const result = interleave(a, b, c)
    expect(result).toStrictEqual(expected)
  })

  it('should interleave up to the length of the shortest matcher, test 2', () => {
    const a = [1]
    const b = [4, 5, 6, 7, 8]
    const c = [7, 8, 9, 10, 11, 12]
    const expected = [1, 4, 7]
    const result = interleave(a, b, c)
    expect(result).toStrictEqual(expected)
  })

  it('should return an empty array if one of the inputs is empty', () => {
    const a = []
    const b = [4, 5, 6, 7, 8]
    const c = [7, 8, 9, 10, 11, 12]
    const expected = []
    const result = interleave(a, b, c)
    expect(result).toStrictEqual(expected)
  })
})
