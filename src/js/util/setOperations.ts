// these aren't in the js stdlib for whatever reason
// https://exploringjs.com/impatient-js/ch_sets.html#missing-set-operations

export const difference = <T>(a: Set<T>, b: Set<T>): Set<T> =>
  new Set([...a].filter((elem) => !b.has(elem)))

export const intersection = <T>(a: Set<T>, b: Set<T>): Set<T> =>
  new Set([...a].filter((elem) => b.has(elem)))

export const union = <T>(a: Set<T>, b: Set<T>): Set<T> => new Set([...a, ...b])
