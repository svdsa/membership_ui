import { keyBy } from 'lodash'
import request from 'superagent'
import { Api, ApiClient, ApiStatus } from '../client/ApiClient'

export type ResolvedAssetContentType = 'image' | 'audio' | 'video'

export interface ResolvedAsset extends RawAsset {
  view_url: string
  upload_link?: string
}

interface AssetsListResponse extends ApiStatus {
  data: ResolvedAsset[]
}

export interface AssetsById {
  [id: number]: ResolvedAsset
}

export type AssetType = 'EXTERNAL_URL' | 'PRESIGNED_URL'

export interface RawAsset {
  id: number
  title: string
  asset_type: AssetType | null
  last_updated: string
  content_type: string
  external_url?: string
  relative_path?: string
  labels: string[]
}

interface GetAssetResponse extends ApiStatus {
  data: ResolvedAsset
}

interface GetUrlDataResponse {
  url: string
}

interface GetUrlResponse extends ApiStatus {
  data: GetUrlDataResponse
}

export class AssetClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async search({ labels }): Promise<AssetsById | null> {
    const result = await this.api
      .url(`/assets`)
      .query({ labels })
      .get()
      .execute<AssetsListResponse>()

    if (result != null) {
      return keyBy(result.data, (asset) => asset.id)
    } else {
      return null
    }
  }

  async uploadAndCreateAsset(
    asset: RawAsset,
    file: File
  ): Promise<ResolvedAsset> {
    const relativePath = asset.relative_path

    if (relativePath != null) {
      const uploadUrl = await this.getUploadUrl(relativePath)

      if (uploadUrl != null) {
        await this.uploadContent(file, uploadUrl)
        return await this.create(asset)
      } else {
        throw new Error('Invalid upload url')
      }
    } else {
      throw new Error('Invalid relative path')
    }
  }

  async getUploadUrl(relativePath: string): Promise<string | null> {
    const request = this.api
      .url(`/assets/upload_url`)
      .query({ relative_path: relativePath })
      .get()
    const res = await request.execute<GetUrlResponse>()

    if (res != null) {
      return res.data.url
    } else {
      return null
    }
  }

  async getViewUrl(relativePath: string): Promise<string | null> {
    const request = this.api
      .url(`/assets/view_url`)
      .query({ relative_path: relativePath })
      .get()
    const res = await request.execute<GetUrlResponse>()

    if (res != null) {
      return res.data.url
    } else {
      throw new Error('Server responded with bad view URL')
    }
  }

  async uploadContent(content: File, uploadUrl: string): Promise<ApiStatus> {
    // TODO: Use this.api instead of the underlying library
    return await request
      .put(uploadUrl)
      .set('Content-Type', 'application/octet-stream')
      .send(content)
      .then((response) => response.body as ApiStatus)
  }

  async create(asset: RawAsset): Promise<ResolvedAsset> {
    const request = this.api.url(`/assets`).put(asset)
    const updatedAsset = await request.execute<GetAssetResponse>()

    if (updatedAsset != null) {
      return updatedAsset.data
    } else {
      throw new Error('Server responded with bad updated asset')
    }
  }

  async deleteById(assetId: number): Promise<ApiStatus> {
    const response = await this.api
      .url(`/asset/${assetId}`)
      .remove()
      .execute<ApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got an empty response from the server while deleting')
    }
  }
}

export const Assets = new AssetClient(Api)
export default Assets
