import { Api, ApiClient } from '../client/ApiClient'

export interface Candidate {
  id: number
  name: string
  image_url: string
}

export class CandidateClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async create(
    electionId: number,
    candidate: Partial<Candidate>
  ): Promise<Candidate> {
    const response = await this.api
      .url(`/election/${electionId}/candidates`)
      .post(candidate)
      .execute<Candidate>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when creating a candidate'
      )
    }
  }

  async update(
    electionId: number,
    candidateId: number,
    candidate: Partial<Candidate>
  ): Promise<Candidate> {
    const response = await this.api
      .url(`/election/${electionId}/candidates/${candidateId}`)
      .patch(candidate)
      .execute<Candidate>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when updating a candidate'
      )
    }
  }

  async delete(electionId: number, candidateId: number): Promise<void> {
    const response = await this.api
      .url(`/election/${electionId}/candidates/${candidateId}`)
      .remove()
      .execute()

    if (response == null) {
      throw new Error(
        'Got an empty response from the server when deleting a candidate'
      )
    }
  }
}

export const Candidates = new CandidateClient(Api)

export default Candidates
