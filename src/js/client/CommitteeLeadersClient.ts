import { Api, ApiClient } from '../client/ApiClient'

export interface CommitteeLeader {
  id: number
  committee_id: number
  member_id: number
  member_name: string
  role_title: string
  term_start_date: string
  term_end_date: string
}

export interface AddLeadershipRoleArgs {
  committeeId: number
  roleTitle: string
  termLimitMonths: number
}

export interface ReplaceLeaderArgs {
  committeeId: number
  leaderId: number
  memberId: number | null
  termStartDate?: string
  termEndDate?: string
}

export class CommitteeLeadershipClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async list(committeeId: number): Promise<CommitteeLeader[]> {
    const result = await this.api
      .url(`/committee/${committeeId}/leaders`)
      .get()
      .execute<CommitteeLeader[]>()

    if (result != null) {
      return result
    } else {
      throw new Error('Got empty response when fetching committee leaders')
    }
  }

  async addLeadershipRole(
    args: AddLeadershipRoleArgs
  ): Promise<CommitteeLeader> {
    const { committeeId, roleTitle, termLimitMonths } = args
    const payload = {
      role_title: roleTitle,
      term_limit_months: termLimitMonths != null ? termLimitMonths : undefined,
    }
    const result = await this.api
      .url(`/committee/${committeeId}/leaders`)
      .post(payload)
      .execute<CommitteeLeader>()

    if (result != null) {
      return result
    } else {
      throw new Error('Got empty response when adding leadership role')
    }
  }

  async replaceLeader(args: ReplaceLeaderArgs): Promise<CommitteeLeader> {
    const { committeeId, leaderId, memberId, termStartDate, termEndDate } = args
    const payload = {
      member_id: memberId,
      term_start_date: termStartDate,
      term_end_date: termEndDate,
    }
    const result = await this.api
      .url(`/committee/${committeeId}/leaders/${leaderId}`)
      .patch(payload)
      .execute<CommitteeLeader>()

    if (result != null) {
      return result
    } else {
      throw new Error('Got empty response when replacing a leader')
    }
  }

  async listRoleTitles(): Promise<string[]> {
    const result = await this.api
      .url('/committee-leadership-roles')
      .get()
      .execute<string[]>()

    if (result != null) {
      return result
    } else {
      throw new Error(
        'Got empty response when fetching committee leadership role titles'
      )
    }
  }
}

export const CommitteeLeadership = new CommitteeLeadershipClient(Api)

export default CommitteeLeadership
