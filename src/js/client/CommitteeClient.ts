import { keyBy } from 'lodash'
import { Api, ApiClient, ApiStatus } from '../client/ApiClient'

export interface Committee {
  id: number
  name: string
  provisional: boolean
  viewable: boolean
  email: string | null
}

export interface CommitteeMember {
  id: number
  name: string
}

export interface CommitteeDetailed extends Committee {
  admins: CommitteeMember[]
  members: CommitteeMember[]
  active_members: CommitteeMember[]
  inactive: boolean
}

export interface CommitteesById {
  [id: number]: CommitteeDetailed
}

export interface CreateCommitteeProps {
  name: string
  admin_list: string[]
  provisional: boolean
}

interface CreateCommitteeResponse extends ApiStatus {
  created: CommitteeDetailed
}

export class CommitteeClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async all(): Promise<CommitteesById> {
    const result = await this.api
      .url(`/committee/list`)
      .get()
      .execute<CommitteeDetailed[]>()

    if (result != null) {
      return keyBy(result, (committee) => committee.id)
    } else {
      throw new Error('Got empty response when fetching committees')
    }
  }

  async get(committeeId: number): Promise<CommitteeDetailed | null> {
    const response = await this.api
      .url(`/committee/${committeeId}`)
      .get()
      .execute<CommitteeDetailed>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async create({
    name,
    admin_list,
    provisional,
  }: CreateCommitteeProps): Promise<CreateCommitteeResponse> {
    const response = await this.api
      .url(`/committee`)
      .post({
        admin_list: admin_list,
        name: name,
        provisional: provisional,
      })
      .execute<CreateCommitteeResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got an empty response when creating a committee')
    }
  }

  async requestMembership(committeeId: number): Promise<void> {
    const response = await this.api
      .url(`/committee/${committeeId}/member_request`)
      .post()
      .execute()

    if (response == null) {
      throw new Error(
        'Got an empty response when requesting membership in a committee'
      )
    }
  }

  async editInactiveStatus(
    committeeId: number,
    inactive: boolean
  ): Promise<void> {
    const response = await this.api
      .url(`/committee/${committeeId}`)
      .patch({ inactive })
      .execute()

    if (response == null) {
      throw new Error(
        'Got an empty response when attempting to edit committee inactive status'
      )
    }
  }
}

export const Committees = new CommitteeClient(Api)

export default Committees
