import { Candidate } from 'src/js/client/CandidateClient'
import { logError } from '../util/util'
import { Api, ApiClient, ApiStatus } from './ApiClient'

export interface UpdateElectionAttributes extends ElectionDetailsResponse {
  /**
   * Duplicate of `election.id` property
   * TODO: doesn't seem to be used in backend code.
   * @deprecated
   */
  election_id?: number
}

interface ElectionSponsor {
  id: string
  name: string
}

export type ElectionStatus = 'draft' | 'published' | 'final' | 'canceled'
type ElectionTransition = ElectionStatus[]

interface SubmitElectionTransitionResponse extends ApiStatus {
  election_status: ElectionStatus
  transitions: ElectionTransition
}

export interface ElectionResponse {
  id: number
  name: string
  status: ElectionStatus
  start_time: string
  end_time: string
  voting_begins_epoch_millis: number
  voting_ends_epoch_millis: number
}

export interface ElectionDetailsResponse extends ElectionResponse {
  number_winners: number
  candidates: Candidate[]
  sponsors: ElectionSponsor[]
  transitions: ElectionTransition
  description: string
  description_img: string
  author: string
}

type VoteTransferType = 'excess' | 'elimination'
export interface VoteCount {
  starting_votes: number
  votes_received: number
}

export interface VoteCountByCandidate {
  [cid: string]: VoteCount
}

interface ElectionRoundInformation {
  transfer_type: VoteTransferType
  transfer_source: string
  votes: VoteCountByCandidate
}

interface ElectionWinner {
  candidate: number
  round: number
}

interface CandidatesById {
  [id: string]: Candidate
}

export interface ElectionCount {
  ballot_count: number
  quota: number
  candidates: CandidatesById
  winners: ElectionWinner[]
  round_information: ElectionRoundInformation[]
}

export interface GetVoteResponse {
  election_id: number
  ballot_key: string
  rankings: number[]
}

export interface EligibleVoter {
  name: string
  email_address: string
}

export interface EligibleVotersById {
  [memberId: string]: EligibleVoter
}

export interface PaperBallotRequest {
  election_id: number
  ballot_key: string
  override?: boolean
  rankings: number[]
}

interface UpdateElectionResponse extends ApiStatus {
  election: ElectionResponse
}

interface VoteBallot {
  election_id: number
  rankings: number[]
}

export interface SubmitVoteResponse {
  ballot_id: string
}

export class ElectionClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async submitVote(params: VoteBallot): Promise<SubmitVoteResponse | null> {
    const response = await this.api
      .url(`/vote`)
      .post(params)
      .execute<SubmitVoteResponse>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getVote(
    electionId: number,
    ballotKey: string
  ): Promise<GetVoteResponse | null> {
    const response = await this.api
      .url(`/election/${electionId}/vote/${ballotKey}`)
      .get()
      .composeErrorHandler((handler, res) => {
        return res.error.status === 404 || handler(res)
      })
      .executeOr<GetVoteResponse, null>(null)

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getEligibleVoters(
    electionId: number
  ): Promise<EligibleVotersById | null> {
    const response = await this.api
      .url(`/election/eligible/list`, { params: { election_id: electionId } })
      .get()
      .execute<EligibleVotersById>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getElections(): Promise<ElectionResponse[] | null> {
    const response = await this.api
      .url('/election/list')
      .get()
      .execute<ElectionResponse[]>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getElection(
    electionId: number
  ): Promise<ElectionDetailsResponse | null> {
    const result = await this.api
      .url(`/election`, { params: { id: electionId } })
      .get()
      .executeOr<ElectionDetailsResponse, null>(null)

    if (result != null) {
      // add the id since it isn't returned from the db
      return {
        ...result,
        id: electionId,
      }
    } else {
      return null
    }
  }

  async countResults(electionId: number): Promise<ElectionCount | null> {
    const response = await this.api
      .url(`/election/count`, {
        params: {
          id: electionId,
          visualization: true,
        },
      })
      .get()
      .execute<ElectionCount>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  submitPaperBallot(ballot: PaperBallotRequest, override = false) {
    const ballotPost = {
      ...ballot,
      override,
    }
    return this.api
      .url(`/vote/paper`)
      .composeErrorHandler((handler, { error }) => {
        if (error.status === 404) {
          logError(`Cannot submit unclaimed ballot #${ballot.ballot_key}`)
        } else {
          logError(
            `Unexpected error when submitting ballot ${ballotPost}`,
            error
          )
        }
      })
      .post(ballotPost)
      .execute()
  }

  async submitTransition(
    electionId: number,
    transition: ElectionTransition
  ): Promise<SubmitElectionTransitionResponse> {
    const response = await this.api
      .url(`/election/${electionId}/state/${transition}`)
      .put()
      .DEPRECATED_execute<SubmitElectionTransitionResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when submitting an election transition'
      )
    }
  }

  async updateElection(
    electionId: number,
    attributes: UpdateElectionAttributes
  ): Promise<UpdateElectionResponse> {
    if (!electionId) {
      throw new Error(`Invalid election_id: ${electionId}`)
    }
    const attributesPost: UpdateElectionAttributes = {
      ...attributes,
      election_id: electionId,
    }
    const response = await this.api
      .url(`/election/${electionId}`)
      .patch(attributesPost)
      .execute<UpdateElectionResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got an empty response when updating an election')
    }
  }
}

export const Elections = new ElectionClient(Api)
