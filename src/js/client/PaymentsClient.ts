import { Api, ApiClient } from 'src/js/client/ApiClient'

export interface NewMemberForm {
  first_name: string
  last_name: string
  email_address: string
  phone_number: string
  pronouns: string
  address: string
  city: string
  zip: string
}

export type DuesRecurrence = 'one-time' | 'monthly' | 'annually'

export interface DuesSubscriptionPaymentInfoRequest {
  recurrence: DuesRecurrence
  total_amount_cents: number
  national_amount_cents: number
  local_amount_cents: number
  stripe_payment_method_id: string
}

export interface CreateDuesSubscriptionRequest {
  email_address: string
  phone_number?: string
  first_name: string
  last_name: string
  pronouns?: string
  address?: string
  city: string
  zip_code: string
  payment?: DuesSubscriptionPaymentInfoRequest
}

type DuesSubscriptionStatus =
  | 'incomplete'
  | 'incomplete_expired'
  | 'trialing'
  | 'active'
  | 'past_due'
  | 'canceled'
  | 'unpaid'

interface DuesSubscriptionItem {
  amount: number
  interval: 'month' | 'year'
}

export interface CreateDuesSubscriptionResponse {
  email_address: string
  subscription?: {
    id: string
    amount: number
    status: DuesSubscriptionStatus
    items: {
      local: DuesSubscriptionItem
      national: DuesSubscriptionItem
    }
    cancel_at_period_end: boolean
  }
}

export class PaymentsClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async createDuesSubscription(
    request: CreateDuesSubscriptionRequest
  ): Promise<CreateDuesSubscriptionResponse | null> {
    const response = await this.api
      .url('/payment/dues')
      .post(request)
      .execute<CreateDuesSubscriptionResponse>()
    return response
  }
}

export const Payments = new PaymentsClient(Api)
