import { Modify } from 'src/js/util/typeUtils'
import { Api, ApiClient, ApiStatus } from '../client/ApiClient'
import { EligibleMemberListEntry, MemberInfo } from './MemberClient'

interface MeetingBase {
  id: number
  name: string
  committee_id: number | null
  code: number | null
  landing_url: string | null
  owner: string | null
  published: boolean
}

interface MeetingResponse extends MeetingBase {
  start_time: string | null
  end_time: string | null
}

export interface Meeting extends MeetingBase {
  start_time: Date | null
  end_time: Date | null
}

export interface MeetingsById {
  [id: number]: Meeting
}

export type UpdateMeetingRequest = Partial<
  Modify<Meeting, { start_time: string; end_time: string }>
>

interface UpdateMeetingResponse extends ApiStatus {
  meeting: MeetingResponse
}

export interface UpdateAttendeeAttributes {
  update_eligibility_to_vote: boolean
}

export type ProxyTokenState = 'UNCLAIMED' | 'ACCEPTED' | 'REJECTED'
export type ProxyTokenAction = 'accept' | 'reject'
interface CreateProxyTokenResponse {
  proxy_token_id: number
  state: ProxyTokenState
}

interface ProxyTokenResponseShared {
  proxy_token_id: number
  nominator_id: number
  nominator_name: string
  state: ProxyTokenState
}

interface ProxyTokenResponse extends ProxyTokenResponseShared {
  recently_acted_member_id: number
}

interface ProxyTokenActionResponse extends ProxyTokenResponseShared {
  email_sent: boolean
}

export interface VerifyTokenResponse {
  status: string
  member: MemberInfo
}

export type MeetingInvitationStatus = 'NO_RESPONSE' | 'ACCEPTED' | 'DECLINED'

export interface MeetingInvitation {
  id: number
  meeting_id: number
  status: MeetingInvitationStatus
  created_at: string
  name: string
  email: string
}

interface MeetingInvitationResponse {
  status: string
  invitation: MeetingInvitation
}

interface MeetingInvitationBulkInviteCommitteeMembersResponse {
  status: string
  invitations: Array<MeetingInvitation>
}

export interface MeetingAgenda {
  id: number
  meeting_id: number
  text: string
  updated_at: string
}

interface MeetingAgendaEditResponse {
  status: string
  agenda: MeetingAgenda
}

export class MeetingClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  isValidMeetingCode(code: string): boolean {
    const codeInt = parseInt(code, 10)
    if (Number.isNaN(codeInt)) {
      return false
    }

    return codeInt >= 1000 && codeInt <= 9999
  }

  sanitizeMeetingCode(code: string): number | null {
    if (code.length == 0) {
      return null
    } else {
      return parseInt(code)
    }
  }

  async create(meeting: Meeting): Promise<UpdateMeetingResponse> {
    const startTime = meeting.start_time
    const endTime = meeting.end_time
    const startTimeIsoString =
      startTime != null ? startTime.toISOString() : null
    const endTimeIsoString = endTime != null ? endTime.toISOString() : null
    const committeeId = meeting.committee_id || null
    const landingUrl = meeting.landing_url || null

    const response = await this.api
      .url(`/meeting`)
      .post({
        name: meeting.name,
        committee_id: committeeId,
        landing_url: landingUrl,
        start_time: startTimeIsoString,
        end_time: endTimeIsoString,
        published: meeting.published,
      })
      .execute<UpdateMeetingResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response when creating meeting')
    }
  }

  async all(): Promise<Meeting[]> {
    const response = await this.api
      .url(`/meeting/list`)
      .get()
      .executeOr<MeetingResponse[]>([])

    return response.map((meeting) => this.parseMeeting(meeting))
  }

  async get(meetingId: number): Promise<Meeting | null> {
    const response = await this.api
      .url(`/meetings/${meetingId}`)
      .get()
      .execute<MeetingResponse>()

    if (!response) return null

    return this.parseMeeting(response)
  }

  async getAttendees(meetingId: number): Promise<EligibleMemberListEntry[]> {
    const response = await this.api
      .url(`/meetings/${meetingId}/attendees`)
      .get()
      .executeOr<EligibleMemberListEntry[]>([])

    return response
  }

  async addAttendee(meetingId: number, memberId: number): Promise<ApiStatus> {
    const response = await this.api
      .url(`/member/attendee`)
      .post({ meeting_id: meetingId, member_id: memberId })
      .execute<ApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when adding attendee')
    }
  }

  async updateAttendee(
    meetingId: number,
    memberId: number,
    attributes: UpdateAttendeeAttributes
  ): Promise<ApiStatus> {
    const response = await this.api
      .url(`/meetings/${meetingId}/attendees/${memberId}`)
      .patch(attributes)
      .execute<ApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when updating attendee voting status'
      )
    }
  }

  async removeAttendee(
    meetingId: number,
    memberId: number
  ): Promise<ApiStatus> {
    const response = await this.api
      .url(`/meetings/${meetingId}/attendees/${memberId}`)
      .remove()
      .execute<ApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when removing attendee')
    }
  }

  async autogenerateMeetingCode(
    meetingId: number
  ): Promise<UpdateMeetingResponse> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meeting`)
      .patch({
        meeting_id: meetingId,
        code: 'autogenerate',
      })
      .execute<UpdateMeetingResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when autogenerating meeting code'
      )
    }
  }

  async setMeetingCode(
    meetingId: number,
    code: string
  ): Promise<UpdateMeetingResponse> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }

    if (code.length > 0 && !this.isValidMeetingCode(code)) {
      throw new Error(`Invalid meeting code: ${code}`)
    }

    const validatedCode = this.sanitizeMeetingCode(code)

    const response = await this.api
      .url(`/meeting`)
      .patch({
        meeting_id: meetingId,
        code: validatedCode,
      })
      .execute<UpdateMeetingResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when setting meeting code'
      )
    }
  }

  parseMeeting(response: MeetingResponse): Meeting {
    // Conversion to accept new date type conversion below
    const meeting: Meeting = response as unknown as Meeting

    const startTimeString = meeting.start_time
    const endTimeString = meeting.end_time

    return {
      ...meeting,
      // parse received ISO 8601 strings
      start_time:
        startTimeString != null ? new Date(startTimeString) : startTimeString,
      end_time: endTimeString != null ? new Date(endTimeString) : endTimeString,
    }
  }

  async updateMeeting(
    meetingId: number,
    attributes: UpdateMeetingRequest
  ): Promise<Meeting> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    const payload = {
      ...attributes,
      id: meetingId,
      meeting_id: meetingId,
    }

    const response = await this.api
      .url(`/meeting`)
      .patch(payload)
      .execute<UpdateMeetingResponse>()

    if (response != null) {
      return this.parseMeeting(response.meeting)
    } else {
      throw new Error('Got empty response from server when updating meeting')
    }
  }

  async createProxyToken(meetingId: number): Promise<CreateProxyTokenResponse> {
    if (!meetingId) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/proxy-token`)
      .post()
      .execute<CreateProxyTokenResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when creating proxy token'
      )
    }
  }

  async getProxyTokenState(
    meetingId: number,
    proxyTokenId: number
  ): Promise<ProxyTokenResponse | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }
    if (proxyTokenId <= 0) {
      throw new Error(`Invalid proxyTokenId: ${proxyTokenId}`)
    }

    const response = await this.api
      .url(
        `/meetings/${meetingId}/proxy-token/${encodeURIComponent(proxyTokenId)}`
      )
      .get()
      .execute<ProxyTokenResponse>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async acceptOrRejectProxyToken(
    meetingId: number,
    proxyTokenId: number,
    action: ProxyTokenAction
  ): Promise<ProxyTokenActionResponse> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meetingId: ${meetingId}`)
    }

    if (proxyTokenId <= 0) {
      throw new Error(`Invalid proxyTokenId: ${proxyTokenId}`)
    }

    const response = await this.api
      .url(
        `/meetings/${meetingId}/proxy-token/${encodeURIComponent(
          proxyTokenId
        )}/${action}`
      )
      .post()
      .execute<ProxyTokenActionResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got empty response from server when taking action on proxy token'
      )
    }
  }

  async verifyToken(token: string): Promise<VerifyTokenResponse | null> {
    if (token === '') {
      throw new Error('Token must not be blank')
    }

    const response = await this.api
      .url(`/verify-email`)
      .post({ token })
      .execute<VerifyTokenResponse>()

    return response
  }

  async getInvitatitons(
    meetingId: number
  ): Promise<MeetingInvitation[] | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/invitations`)
      .get()
      .execute<MeetingInvitation[]>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async sendInvitation(
    meetingId: number,
    memberId: number
  ): Promise<MeetingInvitation | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting id: ${meetingId}`)
    }
    if (memberId <= 0) {
      throw new Error(`Invalid member id: ${memberId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/invitation`)
      .post({ member_id: memberId })
      .execute<MeetingInvitationResponse>()

    if (response != null && response.status === 'success') {
      return response.invitation
    } else {
      return null
    }
  }

  async bulkInviteCommitteeMembers(
    meetingId: number
  ): Promise<MeetingInvitation[] | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/invite-all-committee-members`)
      .post()
      .execute<MeetingInvitationBulkInviteCommitteeMembersResponse>()

    if (response != null && response.status === 'success') {
      return response.invitations
    } else {
      return null
    }
  }

  async getInvitation(meetingId: number): Promise<MeetingInvitation | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting_id: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/invitation`)
      .get()
      .execute<MeetingInvitation>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async sendRsvp(
    meetingId: number,
    canAttend: boolean
  ): Promise<MeetingInvitation | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meeting id: ${meetingId}`)
    }

    const rsvpString: MeetingInvitationStatus = canAttend
      ? 'ACCEPTED'
      : 'DECLINED'

    const response = await this.api
      .url(`/meetings/${meetingId}/invitation/${rsvpString}`)
      .post()
      .execute<MeetingInvitationResponse>()

    if (response != null && response.status === 'success') {
      return response.invitation
    } else {
      return null
    }
  }

  async getMyInvitations(): Promise<MeetingInvitation[] | null> {
    const response = await this.api
      .url('/invitations')
      .get()
      .execute<MeetingInvitation[]>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getAgenda(meetingId: number): Promise<MeetingAgenda | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meetingId: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/agenda`)
      .get()
      .execute<MeetingAgenda>()

    if (response != null) {
      return response
    } else {
      throw new Error(`Error loading meeting agenda ${meetingId}`)
    }
  }

  async editAgenda(
    meetingId: number,
    text: string,
    checkoutTimestamp: string
  ): Promise<MeetingAgenda | null> {
    if (meetingId <= 0) {
      throw new Error(`Invalid meetingId: ${meetingId}`)
    }

    const response = await this.api
      .url(`/meetings/${meetingId}/agenda`)
      .post({ text: text, checkout_timestamp: checkoutTimestamp })
      .execute<MeetingAgendaEditResponse>()

    if (response != null && response.status === 'success') {
      return response.agenda
    } else {
      throw new Error(`Error editing meeting agenda ${meetingId}`)
    }
  }
}

export const Meetings = new MeetingClient(Api)

export default Meetings
