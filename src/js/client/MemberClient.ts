import { compact } from 'lodash/fp'
import { Brand } from 'src/js/util/typeUtils'
import { Api, ApiClient, ApiStatus } from '../client/ApiClient'
import { ElectionStatus } from './ElectionClient'

export interface MemberInfo {
  first_name: string
  last_name: string
  address?: string
  city?: string
  zipcode?: string
  biography: string
  pronouns?: MemberPronounBundle
  email_address?: string
  additional_email_addresses?: MemberEmailAddress[]
  phone_numbers?: MemberPhoneNumber[]
}

export const allEmails = (member: MemberDetailed): string[] => [
  ...new Set<string>(
    compact([
      ...(member.info.additional_email_addresses?.map((e) => e.email_address) ||
        []),
      member.info.email_address,
    ])
  ),
]

export type MemberPronounBundle = Brand<string, 'member-pronoun-bundle'>
export type MemberPronouns = Brand<string, 'member-pronouns'>

export interface MemberEmailAddress {
  email_address: string
  name: string
  preferred: boolean
  verified: boolean
}

export interface MemberPhoneNumber {
  phone_number: string
  name: string
}

export interface MemberRole {
  role: string
  committee_id: number
  committee_name: string

  /**
   * @deprecated
   */
  committee: string
  date_created: Date
}

interface MemberBasics {
  id: number
  info: MemberInfo
  roles: MemberRole[]
}

interface MemberAttendance {
  meeting_id: number
  name: string
}

export interface MemberVote {
  election_id: number
  election_name: string
  election_status: ElectionStatus
  voted: boolean
}

interface FullMembership extends Membership {
  active: boolean
  ak_id: string
  do_not_call: boolean
  first_name: string
  middle_name: string
  last_name: string
  city: string
  zipcode: string
  join_date: Date
  _date_imported: Date
}

export interface Membership {
  status: string
  address: string[]
  phone_numbers: string[]
  dues_paid_until: Date
  dues_paid_overridden_on: Date | null
}

export interface MemberDetailed extends MemberBasics {
  do_not_call: boolean
  do_not_email: boolean
  is_eligible: boolean
  dues_are_paid: boolean
  meets_attendance_criteria: boolean
  active_in_committee: boolean
  is_member: boolean
  meetings: MemberAttendance[]
  votes: MemberVote[]
  notes: string
  membership?: Membership
}

export interface MemberListEntry {
  id: number
  name: string
  city?: string
  zipcode?: string
  membership: FullMembership
}

export interface EligibleMemberListEntry extends MemberListEntry {
  eligibility: EligibilityAttributes
  email: string
}

interface EligibilityAttributes {
  is_eligible: boolean
  message: string
  meets_attendance_criteria: boolean
  active_in_committee: boolean
  num_votes: number
}

export interface RosterResults {
  members_created: number
  members_updated: number
  memberships_created: number
  memberships_updated: number
  member_roles_added: number
  identities_created: number
  phone_numbers_created: number
  errors: number
  processed: number
  total: number
}

interface ImportResponse extends ApiStatus {
  data: RosterResults
}

export interface CreateMemberForm {
  first_name: string
  last_name: string
  email_address: string
  give_chapter_member_role?: boolean
}

interface CreateMemberResponse extends ApiStatus {
  data: {
    member: MemberBasics
    verify_url: string
    email_sent: boolean
    role_created: boolean
  }
}

export interface UpgradeMemberResponse extends ApiStatus {
  data: UpgradeMemberResponseData
}
export interface UpgradeMemberResponseData {
  auth0_account_created: boolean
  email_sent: boolean
  role_created: boolean
}

interface UpdateMemberResponse extends ApiStatus {
  data: MemberDetailed
}

export interface MemberSearchResponse {
  members: EligibleMemberListEntry[]
  cursor: number
  has_more: boolean
}

export interface MemberNotes {
  notes: string
}

/**
 * TODO: This is a very limited subset based on what the backend allows us to update at the moment
 */
export type UpdateMemberAttributeKeys = 'do_not_call' | 'do_not_email'
export type UpdateMemberAttributes = Pick<
  MemberDetailed,
  UpdateMemberAttributeKeys
>

export interface AdminUpdateMemberAttributes {
  first_name: string
  last_name: string
  pronouns: MemberPronounBundle
  biography: string
  do_not_call: boolean
  do_not_email: boolean
  dues_paid_overridden_on: Date
}

export class MemberClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async getCurrentUser(): Promise<MemberDetailed | null> {
    const response = await this.api
      .url(`/member/details`)
      .get()
      .execute<MemberDetailed>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  private parseMemberDetails(response: MemberDetailed): MemberDetailed | null {
    const membership = response.membership
    if (membership == null) {
      return response
    }

    const membershipWithDates: Membership = {
      ...membership,
      dues_paid_until: new Date(membership.dues_paid_until),
      dues_paid_overridden_on:
        membership.dues_paid_overridden_on != null
          ? new Date(membership.dues_paid_overridden_on)
          : null,
    }

    return {
      ...response,
      membership: membershipWithDates,
    }
  }

  async details(memberId: string): Promise<MemberDetailed | null> {
    const response = await this.api
      .url(`/admin/member/details`)
      .query({ member_id: memberId })
      .get()
      .execute<MemberDetailed>()

    if (response != null) {
      return this.parseMemberDetails(response)
    } else {
      return null
    }
  }

  async updateNotes(
    memberId: number,
    notes: string
  ): Promise<MemberNotes | null> {
    const updateBody: MemberNotes = { notes }

    const response = await this.api
      .url(`/admin/member/notes`)
      .query({ member_id: memberId })
      .put(updateBody)
      .execute<MemberNotes>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async addPhoneNumber(
    memberId: number,
    phoneNumber: Partial<MemberPhoneNumber>
  ): Promise<MemberPhoneNumber[] | null> {
    const response = await this.api
      .url(`/admin/member/phone_numbers`)
      .query({ member_id: memberId })
      .post(phoneNumber)
      .execute<MemberPhoneNumber[]>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async deletePhoneNumber(
    memberId: number,
    phoneNumber: string
  ): Promise<MemberPhoneNumber[] | null> {
    const response = await this.api
      .url(`/admin/member/phone_numbers`)
      .query({ member_id: memberId })
      .delete({ phone_number: phoneNumber })
      .execute<MemberPhoneNumber[]>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async addEmailAddress(
    memberId: number,
    emailAddress: Partial<MemberEmailAddress>
  ): Promise<MemberEmailAddress[] | null> {
    const response = await this.api
      .url(`/admin/member/email_addresses`)
      .query({ member_id: memberId })
      .post(emailAddress)
      .execute<MemberEmailAddress[]>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async deleteEmailAddress(
    memberId: number,
    emailAddress: string
  ): Promise<MemberEmailAddress[] | null> {
    const response = await this.api
      .url(`/admin/member/email_addresses`)
      .query({ member_id: memberId })
      .delete({ email_address: emailAddress })
      .execute<MemberEmailAddress[]>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async upgradeToMember(
    memberId: number
  ): Promise<UpgradeMemberResponse | null> {
    return await this.api.url(`/member/upgrade/${memberId}`).put().execute()
  }

  async suspendMember(memberId: number): Promise<void> {
    await this.api.url(`/member/suspend/${memberId}`).post().execute()
  }

  async exitMember(memberId: number): Promise<void> {
    await this.api.url(`/member/exit/${memberId}`).post().execute()
  }

  async cancelDuesOverride(memberId: number): Promise<void> {
    await this.api.url(`/member/downgrade/${memberId}`).post().execute()
  }

  async all(): Promise<EligibleMemberListEntry[]> {
    const response = await this.api
      .url(`/member/list`)
      .get()
      .executeOr<EligibleMemberListEntry[]>([])

    return response
  }

  async importRoster(file: File): Promise<ImportResponse> {
    const response = await this.api
      .url(`/import`)
      .put()
      .attach('file', file)
      .execute<ImportResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response when importing roster')
    }
  }

  async create(member: CreateMemberForm): Promise<CreateMemberResponse> {
    const response = await this.api
      .url(`/member`)
      .post(member)
      .execute<CreateMemberResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when creating member')
    }
  }

  async updateAdmin(
    memberId: number,
    attributes: Partial<AdminUpdateMemberAttributes>
  ): Promise<UpdateMemberResponse> {
    const response = await this.api
      .url(`/admin/member`)
      .query({ member_id: memberId })
      .patch(attributes)
      .execute<UpdateMemberResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when updating member')
    }
  }

  async update(
    // Only handle contact preference updates right now. Use MemberDetailed
    // or update the Pick property list when API supports more things
    attributes: Partial<UpdateMemberAttributes>
  ): Promise<UpdateMemberResponse> {
    const response = await this.api
      .url(`/member`)
      .patch(attributes)
      .execute<UpdateMemberResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got empty response from server when updating member')
    }
  }

  private parseMemberSearchResponse(
    response: MemberSearchResponse
  ): MemberSearchResponse {
    return {
      ...response,
      members: response.members.map((entry) => {
        const joinDate = entry.membership.join_date
        const duesPaidUntil = entry.membership.dues_paid_until
        const dateImported = entry.membership._date_imported
        return {
          ...entry,
          membership: {
            ...entry.membership,
            joinDate: joinDate != null ? new Date(joinDate) : null,

            duesPaidUntil:
              duesPaidUntil != null ? new Date(duesPaidUntil) : null,
            dateImported: dateImported != null ? new Date(duesPaidUntil) : null,
          },
        }
      }),
    }
  }

  async search(
    query: string,
    pageSize = 10,
    cursor = 0
  ): Promise<MemberSearchResponse> {
    const response = await this.api
      .url(`/member/search`)
      .query({
        query: query,
        page_size: pageSize,
        cursor,
      })
      .get()
      .execute<MemberSearchResponse>()

    if (response != null) {
      const responseWithFixedDates = this.parseMemberSearchResponse(response)
      return responseWithFixedDates
    } else {
      throw new Error(
        'Got empty response from the server when performing a member search'
      )
    }
  }

  async addRole(
    memberId: number,
    role: string,
    committeeId: number | null
  ): Promise<ApiStatus> {
    const response = await this.api
      .url(`/member/role`)
      .post({
        member_id: memberId,
        role: role,
        committee_id: committeeId,
      })
      .execute<ApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when adding a role to a member'
      )
    }
  }

  async removeRole(
    memberId: number,
    role: string,
    committeeId: number | null
  ): Promise<ApiStatus> {
    const response = await this.api
      .url(`/member/role`)
      .copy({
        body: {
          member_id: memberId,
          role: role,
          committee_id: committeeId,
        },
      })
      .remove()
      .execute<ApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when removing a role from a member'
      )
    }
  }

  async merge(memberId: number, memberToRemoveId: number): Promise<ApiStatus> {
    const response = await this.api
      .url(`/member/${memberId}/merge`)
      .query({
        member_to_remove_id: memberToRemoveId,
        force: true,
      })
      .post()
      .execute<ApiStatus>()
    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when merging members'
      )
    }
  }
}

export const Members = new MemberClient(Api)
