import {
  MemberDetailed,
  MemberRole,
  Membership,
} from 'src/js/client/MemberClient'
import { MemberState } from '../redux/reducers/memberReducers'

export function isMemberLoaded(member: MemberState): boolean {
  return member && member.user.data != null
}

export function isMemberFromDetails(member: MemberDetailed): boolean {
  return member.roles?.some(
    (role) => role.role === 'member' && role.committee === 'general'
  )
}

export function isAdmin(member: MemberState): boolean {
  if (!member || member.user.data == null) {
    return false
  }
  return member.user.data.roles.some(
    (role) => role.role === 'admin' && role.committee === 'general'
  )
}

export function isCommitteeAdmin(
  member: MemberState,
  committees: string | string[] = 'any'
): boolean {
  if (!member || !committees || member.user.data == null) {
    return false
  }
  const isAdmin = (role) => role.role === 'admin'
  const committeeSet =
    typeof committees === 'string' ? new Set([committees]) : new Set(committees)
  const filter =
    committees === 'any'
      ? isAdmin
      : (role) => isAdmin(role) && committeeSet.has(role.committee)

  const memberRoles = member.user.data.roles
  return memberRoles.some(filter)
}

export function getRelevantRoles(member: MemberState): MemberRole[] {
  if (!member || member.user.data == null) {
    return []
  }
  return member.user.data.roles.filter(
    (role) => role.role === 'admin' || role.role === 'active'
  )
}

export function isMember(member: MemberState): boolean {
  if (!member || member.user.data == null) {
    return false
  }
  return member.user.data.roles.some(
    (role) => role.role === 'member' && role.committee === 'general'
  )
}

export function isMembershipExpired(
  membership: Membership,
  now: Date = new Date()
): boolean {
  return membership.dues_paid_until < now
}

export enum MembershipStatus {
  NotAMember = 'Not a Member',
  GoodStanding = 'Member - In Good Standing',
  LeavingStanding = 'Member - Leaving Standing',
  OutOfStanding = 'Member - Out of Standing',
  UnknownStanding = 'Member - Unknown Standing',
  Suspended = 'Member - Suspended',
  LeftChapter = 'Not a Member - Left Chapter',
}

export function mapMembershipStatus(member: MemberDetailed): MembershipStatus {
  const status = member.membership?.status

  if (status == null) {
    return MembershipStatus.NotAMember
  }

  return mapMembershipStatusString(status)
}

export function mapMembershipStatusString(status: string): MembershipStatus {
  switch (status) {
    case 'SUSPENDED':
      return MembershipStatus.Suspended
    case 'LEFT_CHAPTER':
      return MembershipStatus.LeftChapter
    case 'UNKNOWN':
      return MembershipStatus.UnknownStanding
    case 'OUT_OF_STANDING':
      return MembershipStatus.OutOfStanding
    case 'GOOD_STANDING':
      return MembershipStatus.GoodStanding
    case 'LEAVING_STANDING':
      return MembershipStatus.LeavingStanding
    default:
      return MembershipStatus.NotAMember
  }
}

const members = {
  isMemberLoaded,
  isAdmin,
  isCommitteeAdmin,
  getRelevantCommittees: getRelevantRoles,
}

export default members
