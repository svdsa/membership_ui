import { Meeting } from '../client/MeetingClient'
import { EligibleMemberListEntry } from '../client/MemberClient'

export const isGeneralMeeting = (meeting: Meeting | null): boolean => {
  if (meeting != null) {
    const labelMatches =
      Boolean(meeting.name.match(/General/)) ||
      Boolean(meeting.name.match(/Regular/)) ||
      Boolean(meeting.name.match(/Special/))
    return labelMatches && meeting.committee_id == null
  } else {
    return false
  }
}

export const countEligibleAttendees = (
  attendees: EligibleMemberListEntry[]
): number => {
  return attendees.filter((attendee) => attendee.eligibility.is_eligible).length
}

export const countProxyVotes = (
  attendees: EligibleMemberListEntry[]
): number => {
  return attendees.reduce((result, attendee) => {
    const isPersonallyEligible = attendee.eligibility.is_eligible
    const numVotes =
      attendee.eligibility.num_votes || isPersonallyEligible ? 1 : 0
    const expectedNumVotes = isPersonallyEligible ? 1 : 0
    const numProxyVotes = numVotes - expectedNumVotes
    return result + numProxyVotes
  }, 0)
}
