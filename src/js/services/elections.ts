import { ElectionResponse } from '../client/ElectionClient'
import { MemberVote } from '../client/MemberClient'

export type EligibleVoteStatus =
  | 'ineligible'
  | 'already voted'
  | 'eligible'
  | 'not found'

export function getVoteStatusForElection(
  election: ElectionResponse,
  vote: MemberVote
): EligibleVoteStatus {
  if (!vote) {
    return 'ineligible'
  }
  const electionId = election.id
  if (electionId && vote.election_id.toString() === electionId.toString()) {
    if (Object.is(vote.voted, true)) {
      return 'already voted'
    } else {
      return 'eligible'
    }
  } else {
    return 'ineligible'
  }
}

export function electionStatus(
  election: ElectionResponse,
  now = new Date()
): string {
  const electionState = election.status
  if (electionState !== 'published') {
    return electionState
  }
  const beginning = new Date(election.voting_begins_epoch_millis)
  const ending = new Date(election.voting_ends_epoch_millis)

  if (beginning && beginning > now) {
    return 'polls not open'
  } else if (ending && ending < now) {
    return 'polls closed'
  }
  return 'polls open'
}
