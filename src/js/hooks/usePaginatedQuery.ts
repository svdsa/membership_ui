/* eslint-disable @typescript-eslint/no-explicit-any */

// originally inspired by useInfiniteQuery from
// https://github.com/reduxjs/redux-toolkit/discussions/1163#discussioncomment-876186

import { useDeepCompareEffect } from '@react-hookz/web'
import {
  QueryDefinition,
  skipToken,
  SkipToken,
} from '@reduxjs/toolkit/dist/query'
import { QueryHooks } from '@reduxjs/toolkit/dist/query/react/buildHooks'
import { flatten } from 'lodash'
import { useCallback, useMemo, useRef, useState } from 'react'
import { PaginationQueryArgs, ZdPaginatedList } from 'src/js/api/schemas'

// infer result type from endpoint - there is probably a better way of doing this
type GetResultTypeFromEndpoint<Endpoint> = Endpoint extends QueryHooks<
  QueryDefinition<any, any, string, infer ResultType, string>
>
  ? ResultType
  : never

interface UsePaginationOptions {
  fetchAllPages?: boolean
  limit?: number

  /** Return a list of items in `items` even if there are more items to load */
  showPartialItems?: boolean
}

interface UsePaginationState {
  hasMore: boolean
  limit?: number
  nextEndingBefore?: string
  nextStartingAfter?: string
}

export interface UsePaginatedQueryResult<T extends any[]> {
  pages: T[]
  items: T[number][] | null
  isLoading: boolean
  isFetching: boolean
  isComplete: boolean
  isError: boolean
  error?: any
  hasMore: boolean
  fetchNextPage(): void
  refetch(): void
}

export function usePaginatedQuery<
  Args extends PaginationQueryArgs = any,
  Endpoint extends QueryHooks<QueryDefinition<Args, any, any, any, any>> = any,
  ResultType extends any[] = GetResultTypeFromEndpoint<Endpoint>['data']
>(
  endpoint: Endpoint,
  _args?: Args | SkipToken,
  _options?: UsePaginationOptions
): UsePaginatedQueryResult<ResultType> {
  const paginationArgs = useRef<UsePaginationState | undefined>(undefined)
  const [pages, setPages] = useState<Map<string, ResultType>>(new Map())
  const [trigger, result] = endpoint.useLazyQuery()
  const [args, setArgs] = useState<Args | SkipToken | undefined>(_args)
  const [options, setOptions] = useState<UsePaginationOptions | undefined>(
    _options
  )

  const fetchNextPage = useCallback(() => {
    if (paginationArgs.current !== undefined && args !== skipToken) {
      trigger({
        ...args,
        limit: options?.limit,
        starting_after: paginationArgs.current.nextStartingAfter,
        ending_before: paginationArgs.current.nextEndingBefore,
      } as Args)
    }
  }, [args, options, trigger])

  const fetchFromScratch = useCallback(() => {
    setPages(new Map())

    if (args != null && args !== skipToken) {
      console.warn('First trigger with args')
      trigger({ ...args, limit: options?.limit }, true)
    } else if (args == null) {
      console.warn('First trigger without args')
      trigger({ limit: options?.limit } as Args, true)
    }

    paginationArgs.current = undefined
  }, [args, options?.limit, trigger])

  useDeepCompareEffect(() => {
    if (_options != null) {
      setOptions(_options)
    }
  }, [_options])

  useDeepCompareEffect(() => {
    if (_args != null) {
      setArgs(_args)
    }
  }, [_args])

  useDeepCompareEffect(() => {
    // Clear existing pagination state and start over
    /* Criteria for reset:
     * - this is the first time the query is run or the endpoint has changed
     * - major resource arguments have changed (e.g. id)
     * - the should reset flag is flipped
     */
    if (result.isUninitialized) {
      fetchFromScratch()
    }
  }, [endpoint, result.isUninitialized, trigger, options, fetchFromScratch])

  useDeepCompareEffect(() => {
    const asyncEffect = async () => {
      if (!result.isSuccess) {
        return
      }

      const maybePaginatedList = await ZdPaginatedList.safeParseAsync(
        result.data
      )

      if (!maybePaginatedList.success) {
        throw maybePaginatedList.error
      }

      const {
        has_more,
        links,
        data: newPage,
        url: pageUrl,
      } = maybePaginatedList.data

      paginationArgs.current = {
        hasMore: has_more,
        nextStartingAfter: links?.next?.starting_after,
        nextEndingBefore: links?.next?.ending_before,
      }
      setPages(new Map(pages.set(pageUrl, newPage as ResultType)))

      if (has_more && options?.fetchAllPages) {
        fetchNextPage()
      }
    }

    asyncEffect()
  }, [result.data, result.isSuccess, pages, options, fetchNextPage])

  return useMemo(() => {
    const hasMore =
      paginationArgs.current !== undefined && paginationArgs.current.hasMore
    const pagesAsArray = Array.from(pages.values())
    const items = flatten<ResultType[number]>(pagesAsArray)
    const isComplete = !hasMore

    return {
      pages: pagesAsArray,
      items:
        options?.showPartialItems && !options?.fetchAllPages
          ? items
          : isComplete
          ? items
          : null,
      isLoading: result.isLoading || hasMore,
      isFetching: result.isFetching,
      isComplete,
      hasMore,
      fetchNextPage,
      refetch: fetchFromScratch,
      error: result.error,
      isError: result.isError,
    }

    // update the return results if the endpoint changes
  }, [
    pages,
    result,
    options?.showPartialItems,
    options?.fetchAllPages,
    fetchNextPage,
    fetchFromScratch,
  ])
}
