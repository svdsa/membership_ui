import { useCallback, useLayoutEffect, useState } from 'react'

export interface UsePagedDataProps<TPages extends unknown[][]> {
  pages: TPages | undefined
  isFetching: boolean
  hasMore: boolean
  fetchNextPage(): void
}

export interface UsePagedDataResult<TPage extends unknown[]> {
  page: TPage | undefined
  pageIndex: number
  loadedPageIndex: number
  isLastPage: boolean
  setPage(pageIndex: number): void
  nextPage(): void
  previousPage(): void
}

export function usePagedData<TPages extends unknown[][]>({
  pages,
  isFetching,
  hasMore,
  fetchNextPage,
}: UsePagedDataProps<TPages>): UsePagedDataResult<TPages[number]> {
  const [waitingForPage, setWaitingForPage] = useState<number | null>(null)
  const [pageIndex, setPageIndex] = useState(0)
  const [availablePageAtIndex, setAvailablePageAtIndex] = useState<
    TPages[number] | undefined
  >(pages?.[pageIndex] ?? undefined)

  const handlePreviousPage = useCallback(() => {
    if (pageIndex - 1 < 0) {
      return
    }

    setPageIndex(pageIndex - 1)
  }, [pageIndex, setPageIndex])

  const handleJumpPage = useCallback(
    (pageIndex: number) => {
      if (pages != null && pageIndex >= 0 && pageIndex < pages.length) {
        setPageIndex(pageIndex)
      }
    },
    [pages]
  )

  const handleNextPage = useCallback(() => {
    if (pages == null) {
      return
    }

    if (hasMore && pageIndex + 1 >= pages.length) {
      fetchNextPage()
      setWaitingForPage(pageIndex + 1)
    } else {
      setPageIndex(pageIndex + 1)
    }
  }, [pageIndex, setPageIndex, fetchNextPage, hasMore, pages])

  useLayoutEffect(() => {
    if (pages == null) {
      return
    }

    if (isFetching) {
      return
    }

    if (waitingForPage != null && waitingForPage < pages.length) {
      setPageIndex(waitingForPage)
      setAvailablePageAtIndex(pages[waitingForPage])
      setWaitingForPage(null)
    } else if (pageIndex < pages.length) {
      setAvailablePageAtIndex(pages[pageIndex])
    }
  }, [waitingForPage, pages, isFetching, pageIndex])

  return {
    page: availablePageAtIndex,
    pageIndex: waitingForPage ?? pageIndex,
    isLastPage: pages != null && !hasMore && pageIndex == pages.length - 1,
    loadedPageIndex: pageIndex,
    setPage: handleJumpPage,
    nextPage: handleNextPage,
    previousPage: handlePreviousPage,
  }
}
