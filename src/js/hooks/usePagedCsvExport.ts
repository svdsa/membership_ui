import { useMemo } from 'react'
import { UsePaginatedQueryResult } from 'src/js/hooks/usePaginatedQuery'
import { useDeepCompareMemo } from 'use-deep-compare'
import flat from 'flat'
import { flatten } from 'lodash'
import Papa from 'papaparse'
import saveAs from 'file-saver'

interface UsePaginatedCsvExportOptions<T extends Record<string, unknown>> {
  parseDataToRows(entry: T): Record<string, unknown>
  filename?: string
  dataType?: string
}

interface UsePaginatedCsvExportActions {
  downloadCsvFromPaginatedQuery(
    pagedData: UsePaginatedQueryResult<unknown[]>
  ): void
}

const generateFilename = (
  dataType: string | undefined,
  items: Record<string, unknown>[] | null
): string => {
  // Current ISO time in second precision, with filesystem safe characters
  const isoTime = new Date().toISOString().split('.')[0].replace(/[:]/g, '')

  if (dataType != null && dataType.trim().length > 0) {
    // use datatype as filename
    return `${isoTime}-${dataType}.csv`
  }

  if (
    items != null &&
    items.length > 0 &&
    items[0].object != null &&
    (items[0].object as string).trim().length > 0
  ) {
    // use object type from first array element of page as filename
    return `${isoTime}-${items[0].object}.csv`
  }
  return `${isoTime}-export.csv`
}

const defaultParser = <T extends Record<string, unknown>>(
  entry: T
): Record<string, unknown> => {
  // smash the object flat into a single level of keys (nested object separated by '.')
  return flat(entry)
}

const enumerateAllUsedKeys = (entries: Record<string, unknown>[]): string[] => {
  const keysPerEntry = entries.map(Object.keys)
  const allKeys = new Set<string>(flatten(keysPerEntry))
  return Array.from(allKeys).sort()
}

export const usePaginatedCsvExport = <T extends Record<string, unknown>>(
  _options?: UsePaginatedCsvExportOptions<T>
): UsePaginatedCsvExportActions => {
  const options: UsePaginatedCsvExportOptions<T> = useDeepCompareMemo(
    () => _options ?? { parseDataToRows: defaultParser },
    [_options]
  )

  return useMemo(
    () => ({
      downloadCsvFromPaginatedQuery: (
        pagedData: UsePaginatedQueryResult<T[]>
      ) => {
        if (pagedData.items == null || pagedData.items.length === 0) {
          throw new Error('Data is empty')
        }

        const filename =
          options.filename ??
          generateFilename(options.dataType, pagedData.items)

        const rows = pagedData.items.map(options.parseDataToRows)

        const columnNames = enumerateAllUsedKeys(rows)

        const csv = Papa.unparse(rows, { quotes: true, columns: columnNames })
        const blob = new Blob([csv], {
          type: 'text/csv;charset=utf-8',
        })
        return saveAs(blob, filename ?? options.filename)
      },
    }),
    [options.dataType, options.filename, options.parseDataToRows]
  )
}
